# Changelog

## Upcoming

## 1.4.0

### Upgrade requirements

- Run db migrations: `diesel migration run`
- Run the whitespace removal utility: `cargo run --bin intertextual-cli utilities remove-leading-whitespaces`

### Release notes

- Feature: Format limited subsets of markdown in story descriptions, comments, and recommendations
- Feature: Comments and recommendations can contain @mentions of users, which are converted to
profile page links and notify the users when used
- Feature: Multiple authors for a single story
- Feature: Editors and beta-readers for a story
- UX improvement: Indicate new vs. updated stories on the front page
- UX improvement: Allow authors to show or hide their unpublished stories from the front page
(default is to hide them)
- UX improvement: Added an image for link embeds (e.g. when pasting the link in Twitter or Discord)
- UX Improvement: Show the last entry for statistics graphs as dashed
- Fixed: Don't escape user profile description HTML
- Fixed: Skip pandoc processing for HTML files
- Fixed: Catch more cases when sanitizing files
- Fixed: Always remove extra whitespace at the start of normal paragraphs
- Fixed: Reconfigure TinyMCE editor to generate strikethroughs and underlines that can pass through
sanitization

## 1.3.0

### Upgrade requirements

- Run db migrations: `diesel migration run`

### Release notes

- Feature: Admin panel now includes overall site stats
- Feature: Story submission page now includes a list of suggested tags
- Feature: Stories and search results can be sorted by last update
- Feature: Button to display a link for sharing a story
- UX improvement: Reworked story recommendation UI
- UX improvement: User descriptions are now editable in the rich text editor
- UX improvement: CW tags are always shown at the start of the tag
  list, and a warning is displayed on story cards when CW tags are
  present in spoilers
- UX improvement: CSS works better with Chrome and Edge
- UX improvement: Settings popup closes when you press escape, click
  outside it, or scroll away
- Fixed: `intertextual-cli import` now performs the same character set
  conversion and BBCode conversion as when chapter files are uploaded
  via the web interface
- Fixed: Sitemap no longer 500s

## 1.2.0

### Upgrade requirements

- Run db migrations: `diesel migration run`

### Release notes

- Feature: Added default author notes that can be changed in user settings
- Feature: Added a new account-based story exclusion feature based on denylisted tags and authors
- Feature: Added a long-term, token based login option that can be revoked from the user settings
- UX Improvement: Automatically open story links in a new tab for all newly created stories
- UX Improvement: Pre-validate the form client-side when registering or creating new stories
- UX Improvement: Confirmation callout after creating or editing most user-facing things
- Fixed: incorrect rendering on copy-pasting images in the TinyMCE editor
- Fixed: Login is now kept when being redirected the website from other sites
- Security: Changed most state-modifying endpoints to POST requests, preventing CSRF
- Security: Limit Pandoc's max heap size

## 1.1.0

### Upgrade requirements:

- Run db migrations: `diesel migration run`

### Release notes:

Added separate input fields for per-chapter Forewords and Afterwords
authors can optionally add, replacing the "Author's Notes" tool in the
chapter content editor.

## 0.1.0

The initial version number we've been using for ages because we forgot
to bump it ever.
