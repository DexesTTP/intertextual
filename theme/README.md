# Theme folder

In this folder, you can add :
- An icon for your website called "icon-dark-mode.png" and "icon-light-mode.png"
- An image to appear on preview cards for your website on twitter/etc, called `card-image.png`
- A favicon for your website called "favicon.ico"
- An override for your website css called "base.css". You should base this file on the "base.css" file in the "static/default_theme" folder.

Note : If you need to add other files inside of the folder than the ones listed above (for example, if you use `background-image` in your theme's CSS), you need to add a `ALLOWED_THEME_FILES` environment variable to your project with a colon-separated list of the extra filenames you want to allow. For example :

```
ALLOWED_THEME_FILES=background-1.png:background-2.png
```

Will allow you to use the files `theme/background-1.png` and `theme/background-2.png` by using the `/theme/background-1.png` and `/theme/background-2.png` URLs respectively
