#!/bin/bash
set -euf -o pipefail

dest=rsync://user@example.com/some_dir
export PASSPHRASE=CHANGE_ME
full_interval=20D
remove_threshold=60D

backupdir=/opt/intertextual/backup
mkdir -p -m 700 $backupdir

pg_dump --file=$backupdir/intertextual.sql --format=plain intertextual
rm -rf $backupdir/theme
cp -a /opt/intertextual/live/theme $backupdir/theme
cp /opt/intertextual/live/.env $backupdir/intertextual.env

duplicity --full-if-older-than $full_interval $backupdir $dest
duplicity remove-older-than $remove_threshold --force $dest
