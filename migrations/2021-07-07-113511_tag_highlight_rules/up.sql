-- Your SQL goes here

/*
 * This information contains the current highlighted tag rules for an user.
 */
ALTER TABLE users ADD COLUMN tag_highlight_rules JSONB DEFAULT NULL;

/*
 * This table contains some pre-configurations for the highlighted tag rules.
 */
CREATE TABLE tag_highlight_rules_templates (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  name VARCHAR(128) NOT NULL UNIQUE,
  description TEXT NOT NULL,
  tag_highlight_rules JSONB DEFAULT NULL
);
