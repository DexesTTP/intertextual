/*
 * A given user can mark a story for later.
 */
CREATE TABLE user_story_for_later (
  user_id uuid NOT NULL REFERENCES users ON DELETE CASCADE,
  story_id uuid NOT NULL REFERENCES stories ON DELETE CASCADE,
  approval_date timestamp without time zone NOT NULL DEFAULT NOW(),
  PRIMARY KEY (user_id, story_id)
);
