/*
 * Stores the user IDs and tokens required to validate and revoke log-in cookie information
 */
CREATE TABLE auth_cookies_tokens (
  user_id uuid NOT NULL REFERENCES users ON DELETE CASCADE,
  token_id uuid NOT NULL DEFAULT uuid_generate_v4(),
  token_name TEXT NOT NULL,
  last_visit timestamp without time zone NOT NULL DEFAULT NOW(),
  valid_until timestamp without time zone NOT NULL,
  PRIMARY KEY (user_id, token_id)
);
