DROP TABLE last_story_cache_check;
ALTER TABLE stories DROP COLUMN cached_first_publication_date;
ALTER TABLE stories DROP COLUMN cached_last_update_date;
