ALTER TABLE stories ADD COLUMN cached_first_publication_date timestamp without time zone NOT NULL DEFAULT NOW();
ALTER TABLE stories ADD COLUMN cached_last_update_date timestamp without time zone NOT NULL DEFAULT NOW();

/* This table is used for cached story field computation, and contains a single entry */
CREATE TABLE last_story_cache_check (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  time timestamp without time zone NOT NULL
);
