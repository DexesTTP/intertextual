CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

/* === === === === === === === */
/* ===     Basic data      === */
/* === === === === === === === */

/*
 * User accounts are the base building block of the website.
 * Each account is uniquely identified by its "username", the id is just for easier handling
 * of inter-database interaction.
 */
CREATE TABLE users (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  username VARCHAR(32) NOT NULL UNIQUE,
  display_name VARCHAR(64) NOT NULL,
  account_created_at timestamp without time zone NOT NULL DEFAULT NOW(),
  pw_hash TEXT NOT NULL,
  /* If this is set to true, then the user is required to enter a new password after logging in */
  password_reset_required BOOLEAN NOT NULL DEFAULT FALSE,
  /* If this is set to true, then the user is an administrator. */
  administrator BOOLEAN NOT NULL DEFAULT FALSE,
  /*
   * Optional e-mail address of the user. This is not required, and will only be used to be
   * displayed below the username
   */
  email TEXT,
  /* Optional markdow-formatted text to show on the user's own page. */
  details TEXT DEFAULT NULL,
  /*
   * The current theme used by this user. This maps to the url theme, and while the user is logged
   * in, this will replace the URL theme altogether. It will be changed when a logged-in user
   * changes their own theme via the controls.
   * Once the user re-logs in, the theme used in the database will be prioritized.
   */
  theme TEXT DEFAULT NULL,
  /*
   * Custom user settings. This is a serialized string, used to handle per-user setting data in a
   * generic fashion.
   */
  settings JSONB DEFAULT NULL,
  /* If set to true, the user has dactivated this account */
  deactivated_by_user BOOLEAN NOT NULL DEFAULT FALSE,
  /* If set to true, a moderator has banned this account until the given date */
  banned_until timestamp without time zone DEFAULT NULL,
  /* If set to true, a moderator has muted this account until the given date */
  muted_until timestamp without time zone DEFAULT NULL,
  /* If set to true, a moderator has warned this account until the given date */
  warned_until timestamp without time zone DEFAULT NULL
);

/*
 * Stories are a list of related chapters created by a single author or a group of authors.
 * A story always contains at least one chapter. If a story only contains one chapter, then it is
 * considered standalone and will contain special characteristics.
 */
CREATE TABLE stories (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  /* Only used internally */
  story_internally_created_at timestamp without time zone NOT NULL DEFAULT NOW(),
  title VARCHAR(128) NOT NULL,
  /* This is a simplified version of the title, used to create an unique URL for this story */
  url_fragment VARCHAR(32) NOT NULL,
  /*
   * Whether this story is a "collaboration" between authors.
   * This information should be kept aligned with the table "author_to_story_associations".
   * If this story has several authors, it MUST be marked as a collaboration. If this condition is
   * not met, the data retrieval methods used on this database might return incorrect results.
   */
  is_collaboration BOOLEAN NOT NULL,
  description TEXT NOT NULL,
  /* Recommendations are a way for users to recommend this story to other users. This value
   * describes whether notifications are allowed or not for this story.
   * 0 = recommendations are not allowed (existing recommendations will be hidden)
   * 1 = recommendations are allowed, but only displayed on the recommender's own page
   * 2 = recommendations will be shown on the recommender's page and have a link on the story page
   */
  recommendations_enabled INTEGER NOT NULL DEFAULT 0,
  /* Handles whether the comments are enabled or not for this story */
  comments_enabled BOOLEAN NOT NULL DEFAULT FALSE
);

/*
 * The "author to story association" table associates one or several authors per story.
 * If there is several authors for a single story, then the column "is_collaboration" of this story
 * MUST be set to true. If this condition is not met, the data retrieval methods used on this
 * database might return incorrect results.
 */
CREATE TABLE author_to_story_associations (
  author_id uuid NOT NULL REFERENCES users ON DELETE CASCADE,
  story_id uuid NOT NULL REFERENCES stories ON DELETE CASCADE,
  PRIMARY KEY (author_id, story_id)
);

/*
 * Chapters are a chunk of text that can be shown by one or several authors.
 * Chapters have several fields (publication date, update date, creation date, etc...)
 */
CREATE TABLE chapters (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  /* Only used internally */
  chapter_internally_created_at timestamp without time zone NOT NULL DEFAULT NOW(),
  story_id uuid NOT NULL REFERENCES stories ON DELETE CASCADE,
  /*
   * The chapter number. There should always be a chapter number 1, 2, 3, etc... in the database.
   * Chapter numbers will automatically be updated as needed
   */
  chapter_number INTEGER NOT NULL,
  /* The chapter title. Optional, can be set to null to use a default "chapter N" title. */
  title VARCHAR(128),
  /* The actual text inside the chapter. */
  content TEXT NOT NULL,
  /* The number of words inside the chapter. Pre-computed result based on the contents */
  word_count INTEGER NOT NULL,
  /*
   * This field is used to order the chapter (and story) by creation date.
   * Note : as opposed to the "created at" field, this is the "official" creation date (the first
   * time it was shown as published.)
   */
  official_creation_date timestamp without time zone DEFAULT NULL,
  /* This field stores the last update to this chapter. If null, it is considered not edited. */
  update_date timestamp without time zone DEFAULT NULL,
  /* This field is used to determine when this will be made available to the public. */
  show_publicly_after_date timestamp without time zone DEFAULT NULL,
  /* If this chapter has been moderator-locked, then it cannot be published again */
  moderator_locked BOOLEAN NOT NULL DEFAULT FALSE,
  /* The reason for the moderation lock. */
  moderator_locked_reason TEXT DEFAULT NULL,
  UNIQUE(story_id, chapter_number)
);

/*
 * The tag_categories table stores categories that must be associated with each tag.
 * Every tag must have a category, so by default this migration creates two categories :
 * The "Content Warnings" category, and the "Others" category.
 *
 * Note : some categories can have special behaviours
 * This is for example used for content warnings that MUST be applied to the story when relevant.
 */
CREATE TABLE tag_categories (
  /*
   * The ID of the category.
   * This ID will not be shown directly to the user, but might be used in form fields or URLs
   */
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  /* The display name of the category. */
  display_name VARCHAR(128) NOT NULL UNIQUE,
  /* The description of the category. */
  description TEXT NOT NULL,
  /*
   * The order of this category. Used when displaying categories in order, such as on the tag
   * selection page.
   */
  order_index SERIAL,
  /*
   * Whether tags from this category are always displayed as "major".
   * 
   * Note : If a tag from an "is_always_displayed" category is marked as spoiler, then it will be
   * hidden regardless of this value.
   */
  is_always_displayed BOOLEAN NOT NULL,
  /* Whether new user-defined tags can be added to this category */
  allow_user_defined_tags BOOLEAN NOT NULL,
  /* These are used on the search screen */
  quick_select_for_included_tag_search BOOLEAN NOT NULL DEFAULT FALSE,
  quick_select_for_excluded_tag_search BOOLEAN NOT NULL DEFAULT FALSE
);

INSERT INTO tag_categories (
  display_name,
  description,
  is_always_displayed,
  allow_user_defined_tags,
  quick_select_for_excluded_tag_search,
  order_index
) VALUES
  (
    'Content Warning',
    'These tags are mandatory and must be checked when they apply to the story',
    TRUE,
    FALSE,
    TRUE,
    0
  ),
  (
    'Other',
    'Add tags to this category when they do not fit in any other category',
    FALSE,
    TRUE,
    FALSE,
    100
  );

/*
 * Canonical_tags contains the tag info to show to the user.
 * Every canonical tag has a self-alias entry in internal_tags. 
*/
CREATE TABLE canonical_tags (
  /*
   * The ID of the tag.
   * This ID will not be shown directly to the user, but might be used in form fields or URLs.
   */
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  /*
   * The canonical name of the tag. Will be the name displayed to the user in all cases.
   */
  display_name VARCHAR(128) NOT NULL UNIQUE,
  /* The category asscoiated to this tag and all associated tags. */
  category_id uuid NOT NULL REFERENCES tag_categories ON DELETE CASCADE,
  /*
   * Checkable tags are tags that are explicitely shown to the author as a checkable list on story
   * creation.
   */
  is_checkable BOOLEAN NOT NULL DEFAULT FALSE,
  /*
   * Tag descriptions are optional additional information about a tag. This information is shown
   * for example when a user searches for a single tag, to give additional definition for the tag.
   */
  description TEXT DEFAULT NULL
);

/*
 * Internal tags are pointers to the canonical tags.
 * Note that the tag name in internal_tags can bee different from the canonical tag name, this is
 * exactly the point : the tag names of this table are non-authoritative, and should instead be
 * mapped to the canonicalized information available from the referenced table.
 */
CREATE TABLE internal_to_canonical_tag_associations (
  /*
   * The ID of the tag.
   * This ID will not be shown directly to the user, but might be used in form fields or URLs.
   */
  internal_id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  /*
   * All tags without any canonical associations are discarded.
   */
  canonical_id uuid NOT NULL REFERENCES canonical_tags ON DELETE CASCADE,
  /*
   * The non-canonical name of the tag. This is the original association, and shouldn't be shown
   * to the user.
   */
  internal_name VARCHAR(128) NOT NULL UNIQUE,
  /*
   * The number of stories with this tag associated to it.
   * This is pre-computed in order to quickly order tags by frequency, or to display it in the UI
   */
  tagged_stories INTEGER NOT NULL DEFAULT 0
);

/*
 * This table contains associations between a tag and a story. This association is unique for each
 * tag, though deduplicating might still need to be done on the actual canonical tags behind the
 * targeted internal tag.
 */
CREATE TABLE story_to_internal_tag_associations (
  story_id uuid NOT NULL REFERENCES stories ON DELETE CASCADE,
  internal_tag_id uuid NOT NULL REFERENCES internal_to_canonical_tag_associations ON DELETE CASCADE,
  /*
   * Describes what this tag is. Usual tag types are : "Major", "Standard", "Spoiler".
   * This internally matches to the TagAssociationType enum. 
   */
  tag_type INTEGER NOT NULL,
  PRIMARY KEY (story_id, internal_tag_id)
);

/*
 * This is the general table to hold every "numeric notification" and every "event".
 * - In the default layout, Numeric Notifications are highly visible red squares with a number.
 * - In the default layout, events are a discrete grey dot next to the name.
 *  
 * Each user can decide which kind of notification is shown in which way (or not shown at all).
 * In addition, users cna revisit previously read notifications at a later date.
 */
CREATE TABLE notifications (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  /* This is the ID of the user who is going to see this notification */
  target_user_id uuid NOT NULL REFERENCES users ON DELETE CASCADE,
  /* The event time can point to the future, in which case it should not be shown to the user */
  notification_time timestamp without time zone NOT NULL,
  /* The internal description is an unique string used to handle notification deduplication. */
  internal_description TEXT NOT NULL,
  /*
   * These categories map to the origin of the notification, and are used to classify whether to
   * display the notification as a "numeric notification" or as an "event" or not.
   * See the NotificationCategory enum for more details about the possible values.
   */
  category INTEGER NOT NULL,
  /* The message shown to the user */
  message TEXT NOT NULL,
  /* Where the user should be taken if they click on the notification */
  link TEXT NOT NULL,
  /*
   * Notifications are set to "read" if the user click on them. There's also a way to un-set this
   * and revisit old notifications, or to set a notification as read without clicking on it.
   */
  read BOOLEAN NOT NULL DEFAULT FALSE
);

/* === === === === === === === */
/* === Feedback and stats  === */
/* === === === === === === === */

/*
 * This stores anonymous hits on a story.
 * One hit loosely corresponds to one view on a webpage.
 * Hits are debounced via the algorithm defined in utils, by default by IP every one hour.
 */
CREATE TABLE story_hits (
  id SERIAL PRIMARY KEY,
  chapter_id uuid NOT NULL REFERENCES chapters ON DELETE CASCADE,
  hit_date timestamp without time zone NOT NULL DEFAULT NOW()
);

/*
 * A given user can give a single approval to a story.
 * Note : This can be localized as "thumbs-up", "kudos" or any specific term used for your
 * implementation via the "themes" folder.
 */
CREATE TABLE user_approvals (
  user_id uuid NOT NULL REFERENCES users ON DELETE CASCADE,
  story_id uuid NOT NULL REFERENCES stories ON DELETE CASCADE,
  approval_date timestamp without time zone NOT NULL DEFAULT NOW(),
  PRIMARY KEY (user_id, story_id)
);

/* A given user can provide one or several comments on a story. */
CREATE TABLE comments (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  commenter_id uuid NOT NULL REFERENCES users ON DELETE CASCADE,
  chapter_id uuid NOT NULL REFERENCES chapters ON DELETE CASCADE,
  created timestamp without time zone NOT NULL DEFAULT NOW(),
  updated timestamp without time zone DEFAULT NULL,
  message TEXT NOT NULL
);

/*
 * A given user can give a single recommendation for a story.
 * Note : This can be localized as "review", "story suggestion" or any specific term used for your
 * implementation via the "themes" folder.
 */
CREATE TABLE recommendations (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  story_id uuid NOT NULL REFERENCES stories ON DELETE CASCADE,
  /* The user who wrote the recommendation */
  user_id uuid NOT NULL REFERENCES users ON DELETE CASCADE,
  recommendation_date timestamp without time zone NOT NULL DEFAULT NOW(),
  /*
   * Whether this review has been "featured". Featured reviews are shown directly on the user's
   * page below their stories if they have any, instead of being shown behind a link on their page
   */
  featured_by_user BOOLEAN NOT NULL DEFAULT FALSE,
  description TEXT NOT NULL
);

/* === === === === === === === */
/* ===     Follow stuff    === */
/* === === === === === === === */

/*
 * Used to notify an user that the given author posted a new story, a new chapter, or updated
 * a chapter
 */
CREATE TABLE user_author_follows (
  user_id uuid NOT NULL REFERENCES users ON DELETE CASCADE,
  author_id uuid NOT NULL REFERENCES users ON DELETE CASCADE,
  follow_date timestamp without time zone NOT NULL DEFAULT NOW(),
  PRIMARY KEY (user_id, author_id)
);

/*
 * Used to notify an user that the given author posted a new chapter or updated a chapter to a
 * single story specifically. It can be redundant with "user_author_follows".
 */
CREATE TABLE user_story_follows (
  user_id uuid NOT NULL REFERENCES users ON DELETE CASCADE,
  story_id uuid NOT NULL REFERENCES stories ON DELETE CASCADE,
  follow_date timestamp without time zone NOT NULL DEFAULT NOW(),
  PRIMARY KEY (user_id, story_id)
);

/* Used to notify an user that the given author posted a recommendation */
CREATE TABLE user_author_recommendation_follows (
  user_id uuid NOT NULL REFERENCES users ON DELETE CASCADE,
  author_id uuid NOT NULL REFERENCES users ON DELETE CASCADE,
  follow_date timestamp without time zone NOT NULL DEFAULT NOW(),
  PRIMARY KEY (user_id, author_id)
);

/* === === === === === === === */
/* ===  Moderation tools   === */
/* === === === === === === === */

/* This table contains a summary of all actions taken by a moderator. */
CREATE TABLE moderation_actions (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  username VARCHAR(32) NOT NULL,
  action_date timestamp without time zone NOT NULL DEFAULT NOW(),
  action_type TEXT NOT NULL,
  message TEXT NOT NULL
);

/* This table contains communications between the moderation team and a given user */
CREATE TABLE modmail (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  user_id uuid NOT NULL REFERENCES users ON DELETE CASCADE,
  message_date timestamp without time zone NOT NULL DEFAULT NOW(),
  message TEXT NOT NULL,
  is_user_to_mods BOOLEAN NOT NULL,
  is_read_by_user BOOLEAN NOT NULL DEFAULT FALSE,
  handled_by_moderator_id uuid DEFAULT NULL
);

/* This table is used when a recommendation gets reported by an user */
CREATE TABLE chapter_reports (
  report_date timestamp without time zone NOT NULL DEFAULT NOW(),
  chapter_id uuid NOT NULL REFERENCES chapters ON DELETE CASCADE,
  reporter_id uuid NOT NULL REFERENCES users ON DELETE CASCADE,
  handled_by_moderator_id uuid DEFAULT NULL,
  reason TEXT NOT NULL,
  PRIMARY KEY (chapter_id, reporter_id)
);

/* This table is used when a recommendation gets reported by an user */
CREATE TABLE recommendation_reports (
  report_date timestamp without time zone NOT NULL DEFAULT NOW(),
  /* The story with the recommendation */
  story_id uuid NOT NULL REFERENCES stories ON DELETE CASCADE,
  /* The user who wrote the recommendation */
  user_id uuid NOT NULL REFERENCES users ON DELETE CASCADE,
  reporter_id uuid NOT NULL REFERENCES users ON DELETE CASCADE,
  handled_by_moderator_id uuid DEFAULT NULL,
  reason TEXT NOT NULL,
  PRIMARY KEY (story_id, user_id, reporter_id)
);

/* This table is used when a comment gets reported by an user */
CREATE TABLE comment_reports (
  report_date timestamp without time zone NOT NULL DEFAULT NOW(),
  comment_id uuid NOT NULL REFERENCES comments ON DELETE CASCADE,
  reporter_id uuid NOT NULL REFERENCES users ON DELETE CASCADE,
  handled_by_moderator_id uuid DEFAULT NULL,
  reason TEXT NOT NULL,
  PRIMARY KEY (comment_id, reporter_id)
);

/* This table is used when an user gets warned by a moderator */
CREATE TABLE user_warnings (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  warning_date timestamp without time zone NOT NULL DEFAULT NOW(),
  moderator_id uuid NOT NULL REFERENCES users ON DELETE CASCADE,
  user_id uuid NOT NULL REFERENCES users ON DELETE CASCADE,
  reason TEXT NOT NULL
);

/* This table is used when an user requests a deletion of their account */
CREATE TABLE account_deletion_log (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  account_id uuid NOT NULL,
  is_active_deletion_request BOOLEAN NOT NULL,
  can_be_cancelled_by_user BOOLEAN NOT NULL,
  request_date timestamp without time zone NOT NULL DEFAULT NOW(),
  request_information TEXT NOT NULL
);

/*
 * This table is used to store pending notification generation requests
 * E.g. when a story is generated, a single entry is generated in this table. Then,
 * a worker picks up that entry, generate each of the entries needed in the notifications
 * table, and deletes the entry in this table
 */
CREATE TABLE pending_notification_generation_requests (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  request_time timestamp without time zone NOT NULL DEFAULT NOW(),
  request_type TEXT NOT NULL,
  target_id uuid NOT NULL,
  currently_handled_by_task_worker_index INTEGER DEFAULT NULL
);

/* Create the indexes needed to speed up most queries */
CREATE INDEX ON users (id);
CREATE INDEX ON stories (id);
CREATE INDEX ON stories (title);
CREATE INDEX ON author_to_story_associations (story_id);
CREATE INDEX ON author_to_story_associations (author_id);
CREATE INDEX ON chapters (story_id);
CREATE INDEX ON story_to_internal_tag_associations (story_id);
CREATE INDEX ON story_to_internal_tag_associations (internal_tag_id);
CREATE INDEX ON internal_to_canonical_tag_associations (internal_id);
CREATE INDEX ON internal_to_canonical_tag_associations (canonical_id);
CREATE INDEX ON canonical_tags (id);
CREATE INDEX ON canonical_tags (category_id);
CREATE INDEX ON tag_categories (id);
CREATE INDEX ON notifications (target_user_id);

/* This table is used for static pages */
CREATE TABLE static_pages (
  page_name VARCHAR(32) PRIMARY KEY,
  content TEXT NOT NULL,
  last_edition_time timestamp without time zone NOT NULL DEFAULT NOW()
);

/* Insert the default static pages in the table */
INSERT INTO static_pages
       ( page_name , content )
VALUES (  'rules'  ,    ''   ), (   'tos'   ,    ''   );

/* This table is used for registration parameters, and contains a single entry */
CREATE TABLE registration_parameters (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  registrations_enabled BOOLEAN NOT NULL,
  limit_enabled BOOLEAN NOT NULL,
  limit_max_registrations INTEGER NOT NULL,
  limit_hours INTEGER NOT NULL
);

/* Insert the default registration parameters in the table */
INSERT INTO registration_parameters
       ( registrations_enabled , limit_enabled , limit_max_registrations , limit_hours )
VALUES (         TRUE          ,      TRUE     ,          5000           ,      24     );
