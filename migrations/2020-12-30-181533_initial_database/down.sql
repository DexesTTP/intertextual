DROP TABLE static_pages;
DROP TABLE registration_parameters;
DROP TABLE pending_notification_generation_requests;
DROP TABLE account_deletion_log;
DROP TABLE user_warnings;
DROP TABLE comment_reports;
DROP TABLE recommendation_reports;
DROP TABLE chapter_reports;
DROP TABLE modmail;
DROP TABLE moderation_actions;

DROP TABLE user_author_recommendation_follows;
DROP TABLE user_story_follows;
DROP TABLE user_author_follows;

DROP TABLE recommendations;
DROP TABLE comments;
DROP TABLE user_approvals;
DROP TABLE story_hits;

DROP TABLE notifications;
DROP TABLE story_to_internal_tag_associations;
DROP TABLE internal_to_canonical_tag_associations;
DROP TABLE canonical_tags;
DROP TABLE tag_categories;
DROP TABLE chapters;
DROP TABLE author_to_story_associations;
DROP TABLE stories;
DROP TABLE users;
