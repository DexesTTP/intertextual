/*
 * This table contains canonical tags excluded by an user
 */
CREATE TABLE excluded_authors_for_user (
  user_id uuid NOT NULL REFERENCES users ON DELETE CASCADE,
  author_id uuid NOT NULL REFERENCES users ON DELETE CASCADE,
  PRIMARY KEY (user_id, author_id)
);
