-- This file should undo anything in `up.sql`
ALTER TABLE user_to_story_associations DROP COLUMN creation_time;
ALTER TABLE user_to_story_associations DROP COLUMN creative_role;
ALTER TABLE user_to_story_associations RENAME COLUMN user_id TO author_id;
ALTER TABLE user_to_story_associations RENAME TO author_to_story_associations;
