-- Your SQL goes here
ALTER TABLE author_to_story_associations RENAME TO user_to_story_associations;
ALTER TABLE user_to_story_associations RENAME COLUMN author_id TO user_id;

-- Values can be :
-- 1 = beta-reader (read access to text, even if unpublished)
-- 2 = editor (read/write access to text, even if unpublished)
-- 3 = nonconfirmed author (read/write access and can add/remove access to people, but doesn't show up in the public authors list)
-- 3 = confirmed author (same as nonconfirmed, shows up in the public authors list)
ALTER TABLE user_to_story_associations ADD COLUMN creative_role INTEGER NOT NULL DEFAULT 4;
ALTER TABLE user_to_story_associations ADD COLUMN creation_time timestamp without time zone NOT NULL DEFAULT NOW();
