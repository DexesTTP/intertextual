'use strict';

function checkUsernameAPI(username, callback) {
  var httpRequest = new XMLHttpRequest();

  httpRequest.onreadystatechange = function () {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
      if (httpRequest.status === 200) {
        callback(JSON.parse(httpRequest.responseText), null);
      } else {
        callback(null, httpRequest.status);
      }
    }
  };

  httpRequest.open('GET', '/api/username_available/?username=' + username);
  httpRequest.send();
}

function checkStoryUrlAPI(is_collaboration, username, story_url, callback) {
  var httpRequest = new XMLHttpRequest();

  httpRequest.onreadystatechange = function () {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
      if (httpRequest.status === 200) {
        callback(JSON.parse(httpRequest.responseText), null);
      } else {
        callback(null, httpRequest.status);
      }
    }
  };

  var url = '';
  if (is_collaboration) {
    url =
      '/api/story_url_available?is_collaboration=true&url_fragment=' +
      story_url;
  } else {
    url =
      '/api/story_url_available?username=' +
      username +
      '&url_fragment=' +
      story_url;
  }
  httpRequest.open('GET', url);
  httpRequest.send();
}

function setupRequiredFieldsGroup(groupName, extra_checks, check_on_init) {
  var inputTextFormFields = document.querySelectorAll(
    "input[type='text'][data-is-required-field-for='" + groupName + "']"
  );
  var inputSubmitFields = document.querySelectorAll(
    "input[type='submit'][data-is-submit-field-for='" + groupName + "']"
  );
  var inputErrorField = document.querySelector(
    "div[data-is-error-field-for='" + groupName + "']"
  );

  if (!inputTextFormFields.length) {
    console.warn(
      'Could not find any mandatory fields for group : ' + groupName
    );
    return;
  }

  if (!inputSubmitFields.length) {
    console.warn('Could not find any submit buttons for group : ' + groupName);
    return;
  }

  if (!inputErrorField) {
    console.warn('Could not find the error area for group : ' + groupName);
    return;
  }

  function checkValidToSubmit() {
    function finalize(all_results) {
      var invalidFieldNames = '';
      var invalidFieldMessages = '';
      for (var i = 0; i < all_results.length; ++i) {
        var result = all_results[i];
        if (result.ok) {
          continue;
        }
        if (!!invalidFieldNames) {
          invalidFieldNames += ' ';
        }
        invalidFieldNames += '"' + result.form_field_name + '"';
        if (!!invalidFieldMessages) {
          invalidFieldMessages += '<br />\n';
        }
        invalidFieldMessages +=
          '"' + result.form_field_name + '" : ' + result.message;
      }

      if (invalidFieldNames) {
        for (var i = 0; i < inputSubmitFields.length; ++i) {
          var formField = inputSubmitFields.item(i);
          formField.setAttribute('disabled', 'true');
        }
        inputErrorField.style.display = 'block';
        inputErrorField.innerHTML =
          'Fields ' +
          invalidFieldNames +
          ' are not valid : <br />\n' +
          invalidFieldMessages;
      } else {
        for (var i = 0; i < inputSubmitFields.length; ++i) {
          var formField = inputSubmitFields.item(i);
          formField.removeAttribute('disabled');
        }
        inputErrorField.style.display = 'none';
        inputErrorField.textContent = '';
      }
    }

    var all_results = [];
    var total_checks = inputTextFormFields.length;
    function collect(results) {
      all_results.push(results);
      if (all_results.length === total_checks) {
        finalize(all_results);
      }
    }

    for (var i = 0; i < total_checks; ++i) {
      var formField = inputTextFormFields.item(i);
      if (!formField) {
        collect({
          ok: false,
          message: 'Website Error (no value)',
          form_field_name: formFieldName,
        });
        continue;
      }
      var formFieldName = formField.getAttribute('data-field-name');
      var value = formField.value;
      if (!value) {
        collect({
          ok: false,
          message: 'The field cannot be empty',
          form_field_name: formFieldName,
        });
        continue;
      }
      var formFieldExtraChecks = formField.getAttribute('data-extra-checks');
      if (!formFieldExtraChecks) {
        collect({ ok: true, form_field_name: formFieldName });
        continue;
      }

      var check = extra_checks[formFieldExtraChecks];
      check(formField.value, function (is_ok, message) {
        collect({
          ok: is_ok,
          message: message,
          form_field_name: formFieldName,
        });
      });
    }
  }

  for (var i = 0; i < inputTextFormFields.length; ++i) {
    var formField = inputTextFormFields.item(i);
    formField.addEventListener('input', checkValidToSubmit);
    formField.addEventListener('keyup', checkValidToSubmit);
  }

  if (check_on_init) {
    checkValidToSubmit();
  }
}
