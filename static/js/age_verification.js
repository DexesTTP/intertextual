function age_verified() {
    var cookie = "age_verification=ok; path=/; samesite=strict";
    if (document.getElementById("age-verification-permanent").checked) {
        var expdate = new Date();
        expdate.setUTCFullYear(expdate.getUTCFullYear()+20);
        cookie += "; expires=" + expdate.toUTCString();
    }
    if (document.getElementById("age-verification-secure-cookie").value === "true") {
        cookie += "; secure";
    }
    document.cookie = cookie;
    document.getElementById("age-verification").style.display = "none";
}

document.addEventListener("DOMContentLoaded", function(event) {
    if (document.cookie.split("; ").indexOf("age_verification=ok") === -1) {
        document.getElementById("age-verification").style.display = "";
    }
});
