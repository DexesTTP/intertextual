/// Queries the API for tag information, based on the tag name
///
/// Callback is a function(result, error)
/// Result type : [{tag_name: string, tag_category: string, story_count: number}]
function tag_api_query(clean_tag_name, callback) {
    var httpRequest = new XMLHttpRequest();

    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            if (httpRequest.status === 200) {
                callback(JSON.parse(httpRequest.responseText), null);
            }
            else {
                callback(null, httpRequest.status);
            }
        }
    };

    httpRequest.open('GET', '/api/tags/?q=' + clean_tag_name);
    httpRequest.send();
}

/// Queries the API for an user information, based on the username or display name
///
/// Callback is a function(result, error)
/// Result type : [{username: string, display_name: string}]
function user_api_query(clean_user_name, callback) {
    var httpRequest = new XMLHttpRequest();

    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            if (httpRequest.status === 200) {
                callback(JSON.parse(httpRequest.responseText), null);
            }
            else {
                callback(null, httpRequest.status);
            }
        }
    };
    httpRequest.open('GET', '/api/users/?q=' + clean_user_name);
    httpRequest.send();
}

/// Queries the API for an author information, based on the author username or display name
///
/// Callback is a function(result, error)
/// Result type : [{username: string, display_name: string, story_count: number}]
function author_api_query(clean_author_name, callback) {
    var httpRequest = new XMLHttpRequest();

    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            if (httpRequest.status === 200) {
                callback(JSON.parse(httpRequest.responseText), null);
            }
            else {
                callback(null, httpRequest.status);
            }
        }
    };
    httpRequest.open('GET', '/api/authors/?q=' + clean_author_name);
    httpRequest.send();
}

function setup_general_autocompletion(id) {
    function execute_autocomplete(item, choice_name) {
        var text_shards = item.value.split(" ");
        text_shards.pop();

        var next_tag = "#" + choice_name;
        if (choice_name.startsWith("author:")) {
            var author_name = choice_name.substring("author:".length);
            if (/\s/.test(author_name)) {
                next_tag = "author:\"" + author_name + "\"";
            } else {
                next_tag = choice_name;
            }
        }

        item.value = (text_shards.join(" ") + " " + next_tag).trim() + " ";
    }

    function allow_tab_autocomplete(e, item) {
        var text_shards = item.value.split(" ");
        var last = text_shards.pop();
        if (!last.trim()) {
            return false;
        }

        return true;
    }

    function find_generic_field_autocomplete(tag_search, callback) {
        if (tag_search.startsWith("author:")) {
            var author_name = tag_search.substring("author:".length);
            author_api_query(author_name, function(result, error) {
                if (error) {
                    callback(result, error);
                    return;
                }
                // Hack : Just format the author like a tag
                callback(result.map(authorData => ({
                    tag_name: "author:" + authorData.display_name,
                    story_count: authorData.story_count,
                    tag_category: "@" + authorData.username,
                })), null);
            });
            return;
        }

        if (tag_search.startsWith("#")) {
            tag_search = tag_search.substring(1);
        }

        tag_api_query(tag_search, callback);
    }

    function retrieve_choice_list(item, callback) {
        var text_shards = item.value.split(" ");
        var last = text_shards[text_shards.length - 1];
        if (!last.trim()) {
            callback(null, null);
            return;
        }

        find_generic_field_autocomplete(last, function(result, error) {
            if (!!error) {
                callback(null, error);
                return;
            }

            if (result.length === 0) {
                callback(null, null);
                return;
            }

            var entries = [];
            for (var i = 0; i < result.length; ++i) {
                let entry = result[i];
                let html = "<span class=\"autocomplete-tag-entry\">" + entry.tag_name + "</span>"
                         + " <span class=\"autocomplete-tag-story-count\">(" + entry.story_count + " stories)</span>"
                         + " <span class=\"autocomplete-tag-category\">[" + entry.tag_category + "]</span>";
                entries.push({
                    autocomplete_metadata: entry.tag_name,
                    html: html,
                });
            }

            console.log(entries);
            callback({
                entries: entries,
            }, null);
        });
    }

    internal_setup_autocompletion_for(id, retrieve_choice_list, allow_tab_autocomplete, execute_autocomplete);
}

function setup_tag_autocompletion(id) {
    function execute_autocomplete(item, choice_name) {
        var text_shards = item.value.split(" ");
        text_shards.pop();

        var next_tag = "#" + choice_name;
        item.value = (text_shards.join(" ") + " " + next_tag).trim() + " ";
    }

    function allow_tab_autocomplete(e, item) {
        var text_shards = item.value.split(" ");
        var last = text_shards.pop();
        if (!last.trim()) {
            return false;
        }

        return true;
    }

    function retrieve_choice_list(item, callback) {
        var text_shards = item.value.split(" ");
        var search_item = text_shards[text_shards.length - 1];
        if (!search_item.trim()) {
            callback(null, null);
            return;
        }

        if (search_item.startsWith("#")) {
            search_item = search_item.substring(1);
        }

        tag_api_query(search_item, function(result, error) {
            if (!!error) {
                callback(null, error);
                return;
            }

            if (result.length === 0) {
                callback(null, null);
                return;
            }

            var entries = [];
            for (var i = 0; i < result.length; ++i) {
                let entry = result[i];
                let html = "<span class=\"autocomplete-tag-entry\">" + entry.tag_name + "</span>"
                         + " <span class=\"autocomplete-tag-story-count\">(" + entry.story_count + " stories)</span>"
                         + " <span class=\"autocomplete-tag-category\">[" + entry.tag_category + "]</span>";
                entries.push({
                    autocomplete_metadata: entry.tag_name,
                    html: html,
                });
            }

            callback({
                entries: entries,
            }, null);
        });
    }

    internal_setup_autocompletion_for(id, retrieve_choice_list, allow_tab_autocomplete, execute_autocomplete);
}

function setup_user_autocompletion(id) {
    function execute_autocomplete(item, choice_name) {
        var text_shards = item.value.split(" ");
        text_shards.pop();
        item.value = (text_shards.join(" ") + " " + choice_name).trim();
    }

    function allow_tab_autocomplete(e, item) {
        var text_shards = item.value.split(" ");
        var last = text_shards.pop();
        if (!last.trim()) {
            return false;
        }

        return true;
    }

    function retrieve_choice_list(item, callback) {
        var text_shards = item.value.split(" ");
        var search_item = text_shards[text_shards.length - 1];
        if (!search_item.trim()) {
            callback(null, null);
            return;
        }

        user_api_query(search_item, function(result, error) {
            if (!!error) {
                callback(null, error);
                return;
            }

            if (result.length === 0) {
                callback(null, null);
                return;
            }

            var entries = [];
            for (var i = 0; i < result.length; ++i) {
                let entry = result[i];
                let html = "<span class=\"autocomplete-tag-entry\">" + entry.display_name + "</span>"
                         + " <span class=\"autocomplete-tag-category\">[@" + entry.username + "]</span>";
                entries.push({
                    autocomplete_metadata: entry.username,
                    html: html,
                });
            }

            callback({
                entries: entries,
            }, null);
        });
    }

    internal_setup_autocompletion_for(id, retrieve_choice_list, allow_tab_autocomplete, execute_autocomplete);
}

function setup_author_autocompletion(id) {
    function execute_autocomplete(item, choice_name) {
        var text_shards = item.value.split(" ");
        text_shards.pop();
        item.value = (text_shards.join(" ") + " " + choice_name).trim();
    }

    function allow_tab_autocomplete(e, item) {
        var text_shards = item.value.split(" ");
        var last = text_shards.pop();
        if (!last.trim()) {
            return false;
        }

        return true;
    }

    function retrieve_choice_list(item, callback) {
        var text_shards = item.value.split(" ");
        var search_item = text_shards[text_shards.length - 1];
        if (!search_item.trim()) {
            callback(null, null);
            return;
        }

        author_api_query(search_item, function(result, error) {
            if (!!error) {
                callback(null, error);
                return;
            }

            if (result.length === 0) {
                callback(null, null);
                return;
            }

            var entries = [];
            for (var i = 0; i < result.length; ++i) {
                let entry = result[i];
                let html = "<span class=\"autocomplete-tag-entry\">" + entry.display_name + "</span>"
                         + " <span class=\"autocomplete-tag-story-count\">(" + entry.story_count + " stories)</span>"
                         + " <span class=\"autocomplete-tag-category\">[@" + entry.username + "]</span>";
                entries.push({
                    autocomplete_metadata: entry.username,
                    html: html,
                });
            }

            callback({
                entries: entries,
            }, null);
        });
    }

    internal_setup_autocompletion_for(id, retrieve_choice_list, allow_tab_autocomplete, execute_autocomplete);
}

function setup_comment_mention_autocompletion(id) {
    function execute_autocomplete(item, choice_name) {
        var text_shards = item.value.split(" ");
        text_shards.pop();

        item.value = (text_shards.join(" ") + " @" + choice_name).trim() + " ";
    }

    function allow_tab_autocomplete(e, item) {
        var text_shards = item.value.split(" ");
        var last = text_shards.pop();
        if (!last.trim()) {
            return false;
        }

        if (!last.startsWith("@")) {
            return false;
        }

        return true;
    }

    function retrieve_choice_list(item, callback) {
        var text_shards = item.value.split(" ");
        var search_item = text_shards[text_shards.length - 1];
        if (!search_item.trim()) {
            callback(null, null);
            return;
        }

        if (!search_item.startsWith("@")) {
            return;
        }

        search_item = search_item.substring(1);

        user_api_query(search_item, function(result, error) {
            if (!!error) {
                callback(null, error);
                return;
            }

            if (result.length === 0) {
                callback(null, null);
                return;
            }

            var entries = [];
            for (var i = 0; i < result.length; ++i) {
                let entry = result[i];
                let html = "<span class=\"autocomplete-tag-entry\">" + entry.display_name + "</span>"
                         + " <span class=\"autocomplete-tag-story-count\">&nbsp;</span>"
                         + " <span class=\"autocomplete-tag-category\">[@" + entry.username + "]</span>";
                entries.push({
                    autocomplete_metadata: entry.username,
                    html: html,
                });
            }

            callback({
                entries: entries,
            }, null);
        });
    }

    internal_setup_autocompletion_for(id, retrieve_choice_list, allow_tab_autocomplete, execute_autocomplete);
}


/// Setups the autocompletion for a kind of data
///
/// Parameters :
/// - id : the element id of the input element to autocomplete
/// - retrieve_choice_list - type function(element, callback): method to get the autocomplete data.
///   Takes a callback function(result, error), where
///   result is of type { entries: [{ html: string, autocomplete_metadata: any }] }
/// - allow_tab_autocomplete - type function(element): returns true if the tab key should autocomplete at this point. False will prevent the tab
/// - execute_autocoplete - type function(element, autocomplete_metadata): This should modify the content of the element based on the autocomplete_metadata
///   used for this item
function internal_setup_autocompletion_for(id, retrieve_choice_list, allow_tab_autocomplete, execute_autocomplete) {
    var item = document.getElementById(id);
    var first_entry_in_list = null;
    if (!item) {
        console.warn("Could not bind autocomplete field. id not found : ", id);
        return;
    }

    function select_choice(choice_name) {
        execute_autocomplete(item, choice_name);
        hide_choices_if_needed();
    }

    function show_choices_if_needed() {
        retrieve_choice_list(item, function(result, error) {
            if (!result || !!error) {
                if (!!error) {
                    console.warn("Could not autocomplete : ", error);
                }
                hide_choices_if_needed();
                return;
            }

            // Get (or create) the autocomplete choice box
            // TODO : position the box at the wanted position (below the cursor) if needed (right now
            // it's enough to just put it below, but it's not ideal for e.g. mentions)
            var element = document.getElementById(id + "-autocomplete");
            if (!element) {
                element = document.createElement("div");
                element.id = id + "-autocomplete";
                element.className = "autocomplete-list"
                var bb = item.getBoundingClientRect();
                element.style.top = ((window.scrollY || window.pageYOffset) + bb.top + bb.height) + "px",
                element.style.left = ((window.scrollX || window.pageXOffset) + bb.left) + "px",
                document.body.appendChild(element);
            }

            // Clean up the contents of element, in case there is any
            var node;
            while (node = element.firstChild) {
                element.removeChild(node);
            }

            // Add new contents
            for (var i = 0; i < result.entries.length; ++i) {
                var entry = result.entries[i];
                var node = document.createElement("div");
                node.className = "autocomplete-entry";
                node.innerHTML = entry.html;
                let choice = select_choice.bind(this, entry.autocomplete_metadata);
                node.addEventListener("mousedown", choice);
                element.appendChild(node);

                if (i === 0) {
                    first_entry_in_list = entry.autocomplete_metadata;
                }
            }
        });
    }

    function hide_choices_if_needed() {
        var element = document.getElementById(id + "-autocomplete");
        if (element) {
            document.body.removeChild(element);
        }
        first_entry_in_list = null;
    }

    item.addEventListener("focusin", show_choices_if_needed);

    item.addEventListener("input", show_choices_if_needed);

    item.addEventListener("keydown", function swallow_enter_if_needed(e) {
        if (e.key != "Tab") {
            return;
        }

        if (!first_entry_in_list) {
            return;
        }

        if (!allow_tab_autocomplete(e, item)) {
            return;
        }

        e.preventDefault();
        select_choice(first_entry_in_list);
    });

    item.addEventListener("focusout", function() {
        requestAnimationFrame(hide_choices_if_needed);
    });
}
