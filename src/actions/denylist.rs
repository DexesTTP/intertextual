//! # Denylist
//!
//! This module contains methods dealing with per-user denylists
use diesel::prelude::*;

use crate::models::error::IntertextualError;
use crate::schema::*;

pub fn find_excluded_canonical_tag_ids_for_user(
    user_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Vec<uuid::Uuid>, IntertextualError> {
    let result = excluded_canonical_tags_for_user::table
        .filter(excluded_canonical_tags_for_user::user_id.eq(user_id))
        .select(excluded_canonical_tags_for_user::canonical_tag_id)
        .load::<uuid::Uuid>(conn)?;
    Ok(result)
}

pub fn find_excluded_author_ids_for_user(
    user_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Vec<uuid::Uuid>, IntertextualError> {
    let result = excluded_authors_for_user::table
        .filter(excluded_authors_for_user::user_id.eq(user_id))
        .select(excluded_authors_for_user::author_id)
        .load::<uuid::Uuid>(conn)?;
    Ok(result)
}

pub fn check_if_user_ignored_other_user(
    denlyist_user_id: uuid::Uuid,
    target_user_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<bool, IntertextualError> {
    let result = diesel::dsl::select(diesel::dsl::exists(
        excluded_authors_for_user::table
            .filter(excluded_authors_for_user::user_id.eq(denlyist_user_id))
            .filter(excluded_authors_for_user::author_id.eq(target_user_id)),
    ))
    .get_result(conn)?;
    Ok(result)
}

/// Methods to modify user denylists
pub mod modifications {
    use diesel::prelude::*;

    use crate::models::error::IntertextualError;
    use crate::schema::*;

    pub fn exclude_canonical_tag_for_user(
        user_id: uuid::Uuid,
        canonical_tag_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        diesel::insert_into(excluded_canonical_tags_for_user::table)
            .values((
                excluded_canonical_tags_for_user::user_id.eq(user_id),
                excluded_canonical_tags_for_user::canonical_tag_id.eq(canonical_tag_id),
            ))
            .execute(conn)?;
        Ok(())
    }

    pub fn remove_excluded_canonical_tag_for_user(
        user_id: uuid::Uuid,
        canonical_tag_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        diesel::delete(excluded_canonical_tags_for_user::table.find((user_id, canonical_tag_id)))
            .execute(conn)?;
        Ok(())
    }

    pub fn exclude_author_for_user(
        user_id: uuid::Uuid,
        author_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        diesel::insert_into(excluded_authors_for_user::table)
            .values((
                excluded_authors_for_user::user_id.eq(user_id),
                excluded_authors_for_user::author_id.eq(author_id),
            ))
            .execute(conn)?;
        Ok(())
    }

    pub fn remove_excluded_author_for_user(
        user_id: uuid::Uuid,
        author_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        diesel::delete(excluded_authors_for_user::table.find((user_id, author_id)))
            .execute(conn)?;
        Ok(())
    }
}
