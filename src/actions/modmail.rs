//! # Modmail
//!
//! This module contains methods related to sending or receiving messages from and to the moderation team
use diesel::prelude::*;

use crate::models;
use crate::models::error::IntertextualError;
use crate::schema::*;

/// Get the `limit` modmails sent to user `user_id`, skipping the `start` first modmails
pub fn get_modmail_for_user(
    user_id: uuid::Uuid,
    start: i64,
    limit: i64,
    conn: &PgConnection,
) -> Result<Vec<models::modmail::Modmail>, IntertextualError> {
    let result = modmail::table
        .filter(modmail::user_id.eq(user_id))
        .order_by(modmail::message_date.desc())
        .offset(start)
        .limit(limit)
        .load(conn)?;
    Ok(result)
}

/// Get the total number of modmails sent to user `user_id`
pub fn get_modmail_count_for_user(
    user_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<i64, IntertextualError> {
    let result = modmail::table
        .filter(modmail::user_id.eq(user_id))
        .count()
        .get_result::<i64>(conn)?;
    Ok(result)
}

/// Gets the modmail information as raw data, to use for data processing purposes
pub mod gdpr {
    use diesel::prelude::*;

    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::schema::*;

    /// Get all modmails sent by the user
    pub fn all_modmail_for_user(
        user_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<Vec<models::modmail::Modmail>, IntertextualError> {
        let result = modmail::table
            .filter(modmail::user_id.eq(user_id))
            .filter(modmail::is_user_to_mods.eq(true))
            .order_by(modmail::message_date.desc())
            .load(conn)?;
        Ok(result)
    }
}

/// This module contains methods used for modification purposes : sending modmails to the moderation team
pub mod modifications {
    use diesel::prelude::*;

    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::models::formats::RichBlock;
    use crate::schema::*;

    /// Represents a new modmail sent by the user
    #[derive(Insertable)]
    #[table_name = "modmail"]
    pub struct NewUserModmail {
        /// The ID of the user sending the modmail
        pub user_id: uuid::Uuid,
        /// The content of the modmail
        pub message: RichBlock,
        /// Whether this modmail has been read by the user. Will be set to true, any other value will be ignored.
        pub is_read_by_user: bool,
        /// Whether this modmail has been sent by the user to the moderators. Will be set to true, any other value will be ignored.
        pub is_user_to_mods: bool,
    }

    /// Send a new modmail written by the user to the moderation team
    ///
    /// Note : if you want a moderator to send a modmail, use the [`super::admins::send_new_modmail`] method
    pub fn send_new_modmail(
        modmail: models::modmail::NewModmail,
        conn: &PgConnection,
    ) -> Result<models::modmail::Modmail, IntertextualError> {
        let new_modmail = NewUserModmail {
            user_id: modmail.user_id,
            message: modmail.message,
            is_read_by_user: true,
            is_user_to_mods: true,
        };

        let result = diesel::insert_into(modmail::table)
            .values(new_modmail)
            .get_result(conn)?;
        Ok(result)
    }
}

/// Contains the methods that should only be accessed by the administration team
pub mod admins {
    use diesel::prelude::*;

    use crate::actions::utils::log_mod_action;
    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::models::formats::RichBlock;
    use crate::models::users::ModeratorUserWrapper;
    use crate::schema::*;

    /// Represents a new modmail sent by a moderator
    #[derive(Insertable)]
    #[table_name = "modmail"]
    pub struct NewModeratorModmail {
        /// The ID of the user receiving the modmail
        pub user_id: uuid::Uuid,
        /// The contents of the modmail
        pub message: RichBlock,
        /// Whether this modmail has been sent by the user to the moderators. Will be set to false, any other value will be ignored.
        pub is_user_to_mods: bool,
        /// Must contain the ID of the moderator who initiated the message
        pub handled_by_moderator_id: Option<uuid::Uuid>,
    }

    /// Get the `limit` modmails not handled by a moderator, skipping the `start` first modmails
    pub fn get_unhandled_modmails(
        start: i64,
        limit: i64,
        conn: &PgConnection,
    ) -> Result<Vec<models::modmail::Modmail>, IntertextualError> {
        let result = modmail::table
            .filter(modmail::handled_by_moderator_id.is_null())
            .order_by(modmail::message_date.desc())
            .offset(start)
            .limit(limit)
            .load(conn)?;
        Ok(result)
    }

    /// Get the total count of modmails not handled by a moderator
    pub fn get_unhandled_modmail_count(conn: &PgConnection) -> Result<i64, IntertextualError> {
        let result = modmail::table
            .filter(modmail::handled_by_moderator_id.is_null())
            .count()
            .get_result::<i64>(conn)?;
        Ok(result)
    }

    /// Get the `limit` next modmails, skipping the `start` first modmails
    pub fn get_all_modmails(
        start: i64,
        limit: i64,
        conn: &PgConnection,
    ) -> Result<Vec<models::modmail::Modmail>, IntertextualError> {
        let result = modmail::table
            .order_by(modmail::message_date.desc())
            .offset(start)
            .limit(limit)
            .load(conn)?;
        Ok(result)
    }

    /// Get the total count of modmails
    pub fn get_all_modmail_count(conn: &PgConnection) -> Result<i64, IntertextualError> {
        let result = modmail::table.count().get_result::<i64>(conn)?;
        Ok(result)
    }

    /// Get the modmail with id `id`
    pub fn get_modmail_by_id(
        id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<Option<models::modmail::Modmail>, IntertextualError> {
        let result = modmail::table.find(id).first(conn).optional()?;
        Ok(result)
    }

    /// Sends a new system modmail to the user
    ///
    /// Note 1 : Ensure that the message is valid markdown, and does not contain raw data that
    /// could be parsed otherwise.
    ///
    /// Note 2 : This should only be used for code-generated modmails, and will not generate a
    /// notification on its own
    pub fn send_new_system_modmail(
        moderator: ModeratorUserWrapper,
        user_id: uuid::Uuid,
        message: String,
        conn: &PgConnection,
    ) -> Result<models::modmail::Modmail, IntertextualError> {
        let new_modmail = NewModeratorModmail {
            user_id,
            message: RichBlock::from_raw_text(&message),
            is_user_to_mods: false,
            handled_by_moderator_id: Some(moderator.id()),
        };
        let result = diesel::insert_into(modmail::table)
            .values(new_modmail)
            .get_result(conn)?;
        Ok(result)
    }

    /// Sends a new modmail to an user.
    ///
    /// The modmail will be automatically marked as 'handled' by the given moderator.
    pub fn send_new_modmail(
        moderator: ModeratorUserWrapper,
        modmail: models::modmail::NewModmail,
        conn: &PgConnection,
    ) -> Result<models::modmail::Modmail, IntertextualError> {
        conn.transaction::<models::modmail::Modmail, IntertextualError, _>(|| {
            let new_modmail = NewModeratorModmail {
                user_id: modmail.user_id,
                message: modmail.message,
                is_user_to_mods: false,
                handled_by_moderator_id: Some(moderator.id()),
            };

            // Notify the user : you got mail
            let notification = models::notifications::NewNotification {
                target_user_id: modmail.user_id,
                notification_time: chrono::Utc::now().naive_utc(),
                internal_description: format!("modmail_u{}", modmail.user_id),
                message: "You received a message from the moderation team".to_string(),
                link: "/moderation_contact/".to_string(),
                category: models::notifications::NotificationCategory::ModmailReceived.into(),
            };
            crate::actions::notifications::modifications::add_notification(notification, conn)?;

            let result = diesel::insert_into(modmail::table)
                .values(new_modmail)
                .get_result(conn)?;
            Ok(result)
        })
    }

    /// Mark the given `modmail` as 'handled' by the given `moderator`, and log the `log_entry` to the database.
    pub fn set_modmail_as_handled(
        moderator: ModeratorUserWrapper,
        modmail: models::modmail::Modmail,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            let _result = diesel::update(modmail::table.find(modmail.id))
                .set(modmail::handled_by_moderator_id.eq(Some(moderator.id())))
                .get_result::<models::modmail::Modmail>(conn)?;
            let mod_action = log_mod_action(log_entry, conn)?;
            Ok(mod_action)
        })
    }

    /// Mark the given `modmail` as not handled by any moderator, and log the `log_entry` to the database.
    pub fn set_modmail_as_unhandled(
        modmail: models::modmail::Modmail,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            let _result = diesel::update(modmail::table.find(modmail.id))
                .set(modmail::handled_by_moderator_id.eq(Option::<uuid::Uuid>::None))
                .get_result::<models::modmail::Modmail>(conn)?;
            let mod_action = log_mod_action(log_entry, conn)?;
            Ok(mod_action)
        })
    }
}
