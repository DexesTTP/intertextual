//! # Action Utilities
//!
//! This module contains methods used to log various moderation actions and retrieve this log
use diesel::prelude::*;

use crate::models;
use crate::models::error::IntertextualError;
use crate::schema::*;

/// Writes a moderator action to the database
///
/// Note : When using this method within another action, ensure that they are wrapped together into a
/// transaction using [`diesel::connection::Connection::transaction`]
pub fn log_mod_action(
    action: models::admin::NewModerationAction,
    conn: &PgConnection,
) -> Result<models::admin::ModerationAction, IntertextualError> {
    let result = diesel::insert_into(moderation_actions::table)
        .values(action)
        .get_result::<models::admin::ModerationAction>(conn)?;
    Ok(result)
}

/// Gets the number of moderation log entries in the database
pub fn get_moderation_log_entry_count(conn: &PgConnection) -> Result<i64, IntertextualError> {
    let result = moderation_actions::table.count().get_result::<i64>(conn)?;
    Ok(result)
}

/// Gets the next `limit` moderation log entries, skipping the first `start`
pub fn get_moderation_log(
    start: i64,
    limit: i64,
    conn: &PgConnection,
) -> Result<Vec<models::admin::ModerationAction>, IntertextualError> {
    let result = moderation_actions::table
        .order(moderation_actions::action_date.desc())
        .offset(start)
        .limit(limit)
        .load::<models::admin::ModerationAction>(conn)?;
    Ok(result)
}
