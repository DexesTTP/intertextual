//! # Reports
//!
//! This module contains methods related to reporting a chapter, recommendation or comment to the moderation team
use diesel::prelude::*;

use crate::models;
use crate::models::error::IntertextualError;
use crate::schema::*;

/// Check whether the goven user has reported the given chapter
pub fn user_has_reported_chapter(
    user_id: uuid::Uuid,
    chapter_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<bool, IntertextualError> {
    let result = chapter_reports::table
        .filter(chapter_reports::chapter_id.eq(chapter_id))
        .filter(chapter_reports::reporter_id.eq(user_id))
        .get_result::<models::stories::ChapterReport>(conn)
        .optional()?;
    Ok(result.is_some())
}

/// Check whether the given user has reported the given recommendation
pub fn user_has_reported_recommendation(
    user_id: uuid::Uuid,
    recommender_id: uuid::Uuid,
    story_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<bool, IntertextualError> {
    let result = recommendation_reports::table
        .filter(recommendation_reports::story_id.eq(story_id))
        .filter(recommendation_reports::user_id.eq(recommender_id))
        .filter(recommendation_reports::reporter_id.eq(user_id))
        .get_result::<models::recommendations::RecommendationReport>(conn)
        .optional()?;
    Ok(result.is_some())
}

/// Check whether the given user has reported the given comment
pub fn user_has_reported_comment(
    user_id: uuid::Uuid,
    comment_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<bool, IntertextualError> {
    let result = comment_reports::table
        .filter(comment_reports::comment_id.eq(comment_id))
        .filter(comment_reports::reporter_id.eq(user_id))
        .get_result::<models::comments::CommentReport>(conn)
        .optional()?;
    Ok(result.is_some())
}

/// Gets the raw data related to the reporting feature, to be used in generic dumping
pub mod gdpr {
    use diesel::prelude::*;

    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::schema::*;

    /// Get the list of all reported chapters by this user
    pub fn all_chapter_reports_for_user(
        user_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<
        Vec<(
            models::stories::Story,
            models::stories::Chapter,
            models::stories::ChapterReport,
        )>,
        IntertextualError,
    > {
        let result = chapter_reports::table
            .filter(chapter_reports::reporter_id.eq(user_id))
            .inner_join(chapters::table)
            .inner_join(stories::table.on(stories::id.eq(chapters::story_id)))
            .select((
                stories::all_columns,
                chapters::all_columns,
                chapter_reports::all_columns,
            ))
            .load::<(
                models::stories::Story,
                models::stories::Chapter,
                models::stories::ChapterReport,
            )>(conn)?;
        Ok(result)
    }

    /// Get the list of all reported recommendations by this user
    pub fn all_recommendation_reports_for_user(
        user_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<
        Vec<(
            models::users::User,
            models::stories::Story,
            models::recommendations::RecommendationReport,
        )>,
        IntertextualError,
    > {
        let result = recommendation_reports::table
            .filter(recommendation_reports::reporter_id.eq(user_id))
            .inner_join(users::table.on(users::id.eq(recommendation_reports::user_id)))
            .inner_join(stories::table.on(stories::id.eq(recommendation_reports::story_id)))
            .select((
                users::all_columns,
                stories::all_columns,
                recommendation_reports::all_columns,
            ))
            .load::<(
                models::users::User,
                models::stories::Story,
                models::recommendations::RecommendationReport,
            )>(conn)?;
        Ok(result)
    }

    /// Get the list of all reported comments by this user
    pub fn all_comment_reports_for_user(
        user_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<
        Vec<(
            models::stories::Story,
            models::stories::Chapter,
            models::comments::Comment,
            models::comments::CommentReport,
        )>,
        IntertextualError,
    > {
        let result = comment_reports::table
            .filter(comment_reports::reporter_id.eq(user_id))
            .inner_join(comments::table.on(comments::id.eq(comment_reports::comment_id)))
            .inner_join(chapters::table.on(chapters::id.eq(comments::chapter_id)))
            .inner_join(stories::table.on(stories::id.eq(chapters::story_id)))
            .select((
                stories::all_columns,
                chapters::all_columns,
                comments::all_columns,
                comment_reports::all_columns,
            ))
            .load::<(
                models::stories::Story,
                models::stories::Chapter,
                models::comments::Comment,
                models::comments::CommentReport,
            )>(conn)?;
        Ok(result)
    }
}

/// Methods related to the modification of reports by the users
pub mod modifications {
    use diesel::prelude::*;

    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::schema::*;

    /// Report a chapter (as the given user)
    pub fn report_chapter(
        reporter_id: uuid::Uuid,
        chapter_id: uuid::Uuid,
        reason: String,
        conn: &PgConnection,
    ) -> Result<models::stories::ChapterReport, IntertextualError> {
        let report = diesel::insert_into(chapter_reports::table)
            .values((
                chapter_reports::reporter_id.eq(reporter_id),
                chapter_reports::chapter_id.eq(chapter_id),
                chapter_reports::reason.eq(reason),
            ))
            .get_result(conn)?;
        Ok(report)
    }

    /// Cancel your report of a chapter (as the given user)
    pub fn unreport_chapter(
        reporter_id: uuid::Uuid,
        chapter_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<Option<models::stories::ChapterReport>, IntertextualError> {
        let report = diesel::delete(
            chapter_reports::table
                .filter(chapter_reports::reporter_id.eq(reporter_id))
                .filter(chapter_reports::chapter_id.eq(chapter_id)),
        )
        .get_result(conn)
        .optional()?;
        Ok(report)
    }

    /// Report a recommendation (as the given user)
    pub fn report_recommendation(
        reporter_id: uuid::Uuid,
        recommender_id: uuid::Uuid,
        story_id: uuid::Uuid,
        reason: String,
        conn: &PgConnection,
    ) -> Result<models::recommendations::RecommendationReport, IntertextualError> {
        let report = diesel::insert_into(recommendation_reports::table)
            .values((
                recommendation_reports::reporter_id.eq(reporter_id),
                recommendation_reports::user_id.eq(recommender_id),
                recommendation_reports::story_id.eq(story_id),
                recommendation_reports::reason.eq(reason),
            ))
            .get_result(conn)?;
        Ok(report)
    }

    /// Cancel your report of a recommendation (as the given user)
    pub fn unreport_recommendation(
        reporter_id: uuid::Uuid,
        recommender_id: uuid::Uuid,
        story_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<Option<models::recommendations::RecommendationReport>, IntertextualError> {
        let report = diesel::delete(
            recommendation_reports::table
                .filter(recommendation_reports::reporter_id.eq(reporter_id))
                .filter(recommendation_reports::user_id.eq(recommender_id))
                .filter(recommendation_reports::story_id.eq(story_id)),
        )
        .get_result(conn)
        .optional()?;
        Ok(report)
    }

    /// Report a comment (as the given user)
    pub fn report_comment(
        reporter_id: uuid::Uuid,
        comment_id: uuid::Uuid,
        reason: String,
        conn: &PgConnection,
    ) -> Result<models::comments::CommentReport, IntertextualError> {
        let report = diesel::insert_into(comment_reports::table)
            .values((
                comment_reports::reporter_id.eq(reporter_id),
                comment_reports::comment_id.eq(comment_id),
                comment_reports::reason.eq(reason),
            ))
            .get_result(conn)?;
        Ok(report)
    }

    /// Cancel your report of a comment (as the given user)
    pub fn unreport_comment(
        reporter_id: uuid::Uuid,
        comment_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<Option<models::comments::CommentReport>, IntertextualError> {
        let unreported_report = diesel::delete(
            comment_reports::table
                .filter(comment_reports::reporter_id.eq(reporter_id))
                .filter(comment_reports::comment_id.eq(comment_id)),
        )
        .get_result(conn)
        .optional()?;
        Ok(unreported_report)
    }
}

/// Methods related to handling reports as an administration feature
pub mod admin {
    use diesel::prelude::*;

    use crate::actions::utils::log_mod_action;
    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::schema::*;

    #[derive(Clone)]
    /// Mode to get the reports as
    pub enum ListMode {
        /// Use this mode to get the reports that were not handled by a moderator
        New,
        /// Use this mode to get all reports, regardless of handling status
        All,
    }

    /// Get the overall number of unhandled reports, combining every type of report
    pub fn get_unhandled_report_count(conn: &PgConnection) -> Result<i64, IntertextualError> {
        conn.transaction::<i64, IntertextualError, _>(|| {
            let chapters = chapter_reports::table
                .filter(chapter_reports::handled_by_moderator_id.is_null())
                .count()
                .get_result::<i64>(conn)?;
            let recommendations = recommendation_reports::table
                .filter(recommendation_reports::handled_by_moderator_id.is_null())
                .count()
                .get_result::<i64>(conn)?;
            let comments = comment_reports::table
                .filter(comment_reports::handled_by_moderator_id.is_null())
                .count()
                .get_result::<i64>(conn)?;
            Ok(chapters + recommendations + comments)
        })
    }

    /// Get the number of chapter reports for the given `mode`
    pub fn get_chapter_reports_count(
        mode: ListMode,
        conn: &PgConnection,
    ) -> Result<i64, IntertextualError> {
        match mode {
            ListMode::All => {
                let result = chapter_reports::table.count().get_result::<i64>(conn)?;
                Ok(result)
            }
            ListMode::New => {
                let result = chapter_reports::table
                    .filter(chapter_reports::handled_by_moderator_id.is_null())
                    .count()
                    .get_result::<i64>(conn)?;
                Ok(result)
            }
        }
    }

    /// Get the next `limit` chapter reports, skipping the `start` first, for the given `mode`
    pub fn get_chapter_reports(
        mode: ListMode,
        start: i64,
        count: i64,
        conn: &PgConnection,
    ) -> Result<
        Vec<(
            models::users::User,
            models::stories::Chapter,
            models::stories::ChapterReport,
        )>,
        IntertextualError,
    > {
        conn.transaction::<Vec<(
            models::users::User,
            models::stories::Chapter,
            models::stories::ChapterReport,
        )>, IntertextualError, _>(|| {
            let mut results = chapter_reports::table
                .inner_join(users::table)
                .inner_join(chapters::table)
                .into_boxed();

            match mode {
                ListMode::New => {
                    results = results
                        .filter(chapter_reports::handled_by_moderator_id.is_null())
                        .offset(start)
                        .limit(count);
                }
                ListMode::All => {
                    results = results.offset(start).limit(count);
                }
            }

            let results = results
                .select((
                    users::all_columns,
                    chapters::all_columns,
                    chapter_reports::all_columns,
                ))
                .load::<(
                    models::users::User,
                    models::stories::Chapter,
                    models::stories::ChapterReport,
                )>(conn)?;
            Ok(results)
        })
    }

    /// Gets the given chapter report for the reporter
    pub fn get_chapter_report(
        reporter_id: uuid::Uuid,
        chapter_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<
        Option<(
            models::users::User,
            models::stories::Chapter,
            models::stories::ChapterReport,
        )>,
        IntertextualError,
    > {
        let results = chapter_reports::table
            .filter(chapter_reports::reporter_id.eq(reporter_id))
            .filter(chapter_reports::chapter_id.eq(chapter_id))
            .inner_join(users::table)
            .inner_join(chapters::table)
            .select((
                users::all_columns,
                chapters::all_columns,
                chapter_reports::all_columns,
            ))
            .first::<(
                models::users::User,
                models::stories::Chapter,
                models::stories::ChapterReport,
            )>(conn)
            .optional()?;
        Ok(results)
    }

    /// Set the given chapter report as handled by the given moderator
    pub fn set_chapter_report_handled(
        moderator_id: uuid::Uuid,
        report: models::stories::ChapterReport,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            let _updated_report = diesel::update(
                chapter_reports::table
                    .filter(chapter_reports::reporter_id.eq(report.reporter_id))
                    .filter(chapter_reports::chapter_id.eq(report.chapter_id)),
            )
            .set(chapter_reports::handled_by_moderator_id.eq(moderator_id))
            .get_result::<models::stories::ChapterReport>(conn)?;
            let mod_action = log_mod_action(log_entry, conn)?;
            Ok(mod_action)
        })
    }

    /// Set the given chapter report as unhandled
    pub fn set_chapter_report_unhandled(
        report: models::stories::ChapterReport,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            let _updated_report = diesel::update(
                chapter_reports::table
                    .filter(chapter_reports::reporter_id.eq(report.reporter_id))
                    .filter(chapter_reports::chapter_id.eq(report.chapter_id)),
            )
            .set(chapter_reports::handled_by_moderator_id.eq(Option::<uuid::Uuid>::None))
            .get_result::<models::stories::ChapterReport>(conn)?;
            let mod_action = log_mod_action(log_entry, conn)?;
            Ok(mod_action)
        })
    }

    /// Get the number of recommendation reports for the given `mode`
    pub fn get_recommendation_reports_count(
        mode: ListMode,
        conn: &PgConnection,
    ) -> Result<i64, IntertextualError> {
        match mode {
            ListMode::All => {
                let result = recommendation_reports::table
                    .count()
                    .get_result::<i64>(conn)?;
                Ok(result)
            }
            ListMode::New => {
                let result = recommendation_reports::table
                    .filter(recommendation_reports::handled_by_moderator_id.is_null())
                    .count()
                    .get_result::<i64>(conn)?;
                Ok(result)
            }
        }
    }

    /// Get the next `limit` recommendation reports, skipping the `start` first, for the given `mode`
    pub fn get_recommendation_reports(
        mode: ListMode,
        start: i64,
        count: i64,
        conn: &PgConnection,
    ) -> Result<
        Vec<(
            models::users::User,
            models::recommendations::Recommendation,
            models::recommendations::RecommendationReport,
        )>,
        IntertextualError,
    > {
        conn.transaction::<Vec<(
            models::users::User,
            models::recommendations::Recommendation,
            models::recommendations::RecommendationReport,
        )>, IntertextualError, _>(|| {
            let mut results = recommendation_reports::table
                .inner_join(users::table.on(users::id.eq(recommendation_reports::reporter_id)))
                .inner_join(
                    recommendations::table.on(recommendations::story_id
                        .eq(recommendation_reports::story_id)
                        .and(recommendations::user_id.eq(recommendation_reports::user_id))),
                )
                .into_boxed();

            match mode {
                ListMode::New => {
                    results = results
                        .filter(recommendation_reports::handled_by_moderator_id.is_null())
                        .offset(start)
                        .limit(count);
                }
                ListMode::All => {
                    results = results.offset(start).limit(count);
                }
            }

            let results = results
                .select((
                    users::all_columns,
                    recommendations::all_columns,
                    recommendation_reports::all_columns,
                ))
                .load::<(
                    models::users::User,
                    models::recommendations::Recommendation,
                    models::recommendations::RecommendationReport,
                )>(conn)?;
            Ok(results)
        })
    }

    /// Get the specific recommendation report for the given reporter, recommender and story
    pub fn get_recommendation_report(
        reporter_id: uuid::Uuid,
        user_id: uuid::Uuid,
        story_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<
        Option<(
            models::users::User,
            models::recommendations::Recommendation,
            models::recommendations::RecommendationReport,
        )>,
        IntertextualError,
    > {
        let results = recommendation_reports::table
            .filter(recommendation_reports::reporter_id.eq(reporter_id))
            .filter(recommendation_reports::user_id.eq(user_id))
            .filter(recommendation_reports::story_id.eq(story_id))
            .inner_join(users::table.on(users::id.eq(recommendation_reports::reporter_id)))
            .inner_join(
                recommendations::table.on(recommendations::story_id
                    .eq(recommendation_reports::story_id)
                    .and(recommendations::user_id.eq(recommendation_reports::user_id))),
            )
            .select((
                users::all_columns,
                recommendations::all_columns,
                recommendation_reports::all_columns,
            ))
            .first::<(
                models::users::User,
                models::recommendations::Recommendation,
                models::recommendations::RecommendationReport,
            )>(conn)
            .optional()?;
        Ok(results)
    }

    /// Set the given recommendation report as handled
    pub fn set_recommendation_report_handled(
        moderator_id: uuid::Uuid,
        report: models::recommendations::RecommendationReport,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            let _updated_report = diesel::update(
                recommendation_reports::table
                    .filter(recommendation_reports::reporter_id.eq(report.reporter_id))
                    .filter(recommendation_reports::user_id.eq(report.user_id))
                    .filter(recommendation_reports::story_id.eq(report.story_id)),
            )
            .set(recommendation_reports::handled_by_moderator_id.eq(moderator_id))
            .get_result::<models::recommendations::RecommendationReport>(conn)?;
            let mod_action = log_mod_action(log_entry, conn)?;
            Ok(mod_action)
        })
    }

    /// Set the given recommendation report as unhandled
    pub fn set_recommendation_report_unhandled(
        report: models::recommendations::RecommendationReport,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            let _updated_report = diesel::update(
                recommendation_reports::table
                    .filter(recommendation_reports::reporter_id.eq(report.reporter_id))
                    .filter(recommendation_reports::user_id.eq(report.user_id))
                    .filter(recommendation_reports::story_id.eq(report.story_id)),
            )
            .set(recommendation_reports::handled_by_moderator_id.eq(Option::<uuid::Uuid>::None))
            .get_result::<models::recommendations::RecommendationReport>(conn)?;
            let mod_action = log_mod_action(log_entry, conn)?;
            Ok(mod_action)
        })
    }

    /// Get the total comment reports count for the given `mode`
    pub fn get_comment_reports_count(
        mode: ListMode,
        conn: &PgConnection,
    ) -> Result<i64, IntertextualError> {
        match mode {
            ListMode::All => {
                let result = comment_reports::table.count().get_result::<i64>(conn)?;
                Ok(result)
            }
            ListMode::New => {
                let result = comment_reports::table
                    .filter(comment_reports::handled_by_moderator_id.is_null())
                    .count()
                    .get_result::<i64>(conn)?;
                Ok(result)
            }
        }
    }

    /// Get the next `limit` comment reports, skipping the `start` first, for the given `mode`
    pub fn get_comment_reports(
        mode: ListMode,
        start: i64,
        count: i64,
        conn: &PgConnection,
    ) -> Result<
        Vec<(
            models::users::User,
            models::comments::Comment,
            models::comments::CommentReport,
        )>,
        IntertextualError,
    > {
        conn.transaction::<Vec<(
            models::users::User,
            models::comments::Comment,
            models::comments::CommentReport,
        )>, IntertextualError, _>(|| {
            let mut results = comment_reports::table
                .inner_join(users::table)
                .inner_join(comments::table)
                .into_boxed();

            match mode {
                ListMode::New => {
                    results = results
                        .filter(comment_reports::handled_by_moderator_id.is_null())
                        .offset(start)
                        .limit(count);
                }
                ListMode::All => {
                    results = results.offset(start).limit(count);
                }
            }

            let results = results
                .select((
                    users::all_columns,
                    comments::all_columns,
                    comment_reports::all_columns,
                ))
                .load::<(
                    models::users::User,
                    models::comments::Comment,
                    models::comments::CommentReport,
                )>(conn)?;
            Ok(results)
        })
    }

    /// Get the report by `reporter_id` for the comment `comment_id`
    pub fn get_comment_report(
        reporter_id: uuid::Uuid,
        comment_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<
        Option<(
            models::users::User,
            models::comments::Comment,
            models::comments::CommentReport,
        )>,
        IntertextualError,
    > {
        let results = comment_reports::table
            .filter(comment_reports::reporter_id.eq(reporter_id))
            .filter(comment_reports::comment_id.eq(comment_id))
            .inner_join(users::table)
            .inner_join(comments::table)
            .select((
                users::all_columns,
                comments::all_columns,
                comment_reports::all_columns,
            ))
            .first::<(
                models::users::User,
                models::comments::Comment,
                models::comments::CommentReport,
            )>(conn)
            .optional()?;
        Ok(results)
    }

    /// Set the report by `reporter_id` for the comment `comment_id` as handled by the moderator `moderator_id`
    pub fn set_comment_report_handled(
        moderator_id: uuid::Uuid,
        report: models::comments::CommentReport,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            let _updated_report = diesel::update(
                comment_reports::table
                    .filter(comment_reports::reporter_id.eq(report.reporter_id))
                    .filter(comment_reports::comment_id.eq(report.comment_id)),
            )
            .set(comment_reports::handled_by_moderator_id.eq(moderator_id))
            .get_result::<models::comments::CommentReport>(conn)?;
            let mod_action = log_mod_action(log_entry, conn)?;
            Ok(mod_action)
        })
    }

    /// Set the report by `reporter_id` for the comment `comment_id` as unhandled
    pub fn set_comment_report_unhandled(
        report: models::comments::CommentReport,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            let _updated_report = diesel::update(
                comment_reports::table
                    .filter(comment_reports::reporter_id.eq(report.reporter_id))
                    .filter(comment_reports::comment_id.eq(report.comment_id)),
            )
            .set(comment_reports::handled_by_moderator_id.eq(Option::<uuid::Uuid>::None))
            .get_result::<models::comments::CommentReport>(conn)?;
            let mod_action = log_mod_action(log_entry, conn)?;
            Ok(mod_action)
        })
    }
}
