//! # User Approvals
//!
//! This module contains methods used to create and remove user approvals
use diesel::prelude::*;

use crate::models;
use crate::models::error::IntertextualError;
use crate::schema::*;

type UserApprovalType = (uuid::Uuid, uuid::Uuid, chrono::NaiveDateTime);

/// Gets the total approval count given by the user
pub fn total_user_approval_count_given_by_user(
    user_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<i64, IntertextualError> {
    let approval_count = user_approvals::table
        .filter(user_approvals::user_id.eq(user_id))
        .count()
        .get_result::<i64>(conn)?;
    Ok(approval_count)
}

/// Gets all the stories where the `user_id` gave an approval, and when that approval was given
pub fn user_approvals_by_user(
    user_id: uuid::Uuid,
    start: i64,
    limit: i64,
    conn: &PgConnection,
) -> Result<Vec<(models::stories::Story, chrono::NaiveDateTime)>, IntertextualError> {
    let result = user_approvals::table
        .filter(user_approvals::user_id.eq(user_id))
        .inner_join(stories::table)
        .select((stories::all_columns, user_approvals::approval_date))
        .order_by(user_approvals::approval_date.desc())
        .offset(start)
        .limit(limit)
        .load::<(models::stories::Story, chrono::NaiveDateTime)>(conn)?;
    Ok(result)
}

/// Gets the times where the `story_id` received an approval, and when that approval was given
pub fn find_user_approvals_through_time(
    story_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Vec<chrono::NaiveDateTime>, IntertextualError> {
    let result = user_approvals::table
        .filter(user_approvals::story_id.eq(story_id))
        .select(user_approvals::approval_date)
        .load::<chrono::NaiveDateTime>(conn)?;
    Ok(result)
}

/// Gets the total approval count for the given `story_id`
pub fn total_user_approval_count_for_story(
    story_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<i64, IntertextualError> {
    let approval_count = user_approvals::table
        .filter(user_approvals::story_id.eq(story_id))
        .count()
        .get_result::<i64>(conn)?;
    Ok(approval_count)
}

/// Checks whether the given `user_id` has given an approval for the given `story_id`
pub fn has_given_user_approval(
    user_id: uuid::Uuid,
    story_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<bool, IntertextualError> {
    let kudo = user_approvals::table
        .filter(user_approvals::user_id.eq(user_id))
        .filter(user_approvals::story_id.eq(story_id))
        .first::<UserApprovalType>(conn)
        .optional()?;
    Ok(kudo.is_some())
}

/// Gets the raw data related to user approval
pub mod gdpr {
    use diesel::prelude::*;

    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::schema::*;

    /// Gets all the stories where the `user_id` gave an approval, and when
    pub fn all_user_approvals_from_user(
        user_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<Vec<(models::stories::Story, chrono::NaiveDateTime)>, IntertextualError> {
        let all_approvals = user_approvals::table
            .filter(user_approvals::user_id.eq(user_id))
            .inner_join(stories::table)
            .select((stories::all_columns, user_approvals::approval_date))
            .load::<(models::stories::Story, chrono::NaiveDateTime)>(conn)?;
        Ok(all_approvals)
    }
}

/// Methods to add or remove approvals
pub mod modifications {
    use diesel::prelude::*;

    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::prelude::*;
    use crate::schema::*;

    type UserApprovalType = (uuid::Uuid, uuid::Uuid, chrono::NaiveDateTime);

    /// Set the user `user_id` as having approved the story `story_id`
    pub fn add_user_approval(
        user_id: uuid::Uuid,
        story_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        conn.transaction::<(), IntertextualError, _>(|| {
            // Store the approval
            diesel::insert_into(user_approvals::table)
                .values((
                    user_approvals::user_id.eq(user_id),
                    user_approvals::story_id.eq(story_id),
                ))
                .get_result::<UserApprovalType>(conn)?;

            // Notify the authors
            let story: models::stories::Story = stories::table.find(story_id).first(conn)?;
            let author_list = crate::actions::stories::find_authors_by_story(
                story_id,
                &FilterMode::BypassFilters,
                conn,
            )?;
            for author in author_list {
                let notification = models::notifications::NewNotification {
                    target_user_id: author.id,
                    notification_time: chrono::Utc::now().naive_utc(),
                    internal_description: if story.is_collaboration {
                        format!("collabs{}userapproval{}", story.id, user_id)
                    } else {
                        format!("a{}s{}userapproval{}", author.id, story.id, user_id)
                    },
                    message: format!("Your story {} got a snap !", story.title),
                    link: if story.is_collaboration {
                        format!("/collaboration/{}/", story.url_fragment)
                    } else {
                        format!("/@{}/{}/", author.username, story.url_fragment)
                    },
                    category: models::notifications::NotificationCategory::UserApproval.into(),
                };
                crate::actions::notifications::modifications::add_notification(notification, conn)?;
            }
            Ok(())
        })
    }

    /// Remove any approval from user `user_id` to the story `story_id`
    pub fn remove_user_approval(
        user_id: uuid::Uuid,
        story_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        conn.transaction::<(), IntertextualError, _>(|| {
            diesel::delete(
                user_approvals::table
                    .filter(user_approvals::user_id.eq(user_id))
                    .filter(user_approvals::story_id.eq(story_id)),
            )
            .get_result::<UserApprovalType>(conn)?;

            // Remove the notifications to the authors
            let story: models::stories::Story = stories::table.find(story_id).first(conn)?;
            let author_list = crate::actions::stories::find_authors_by_story(story_id, &FilterMode::BypassFilters, conn)?;
            for author in author_list {
                let internal_description = if story.is_collaboration {
                    format!("collabs{}userapproval{}", story.id, user_id)
                } else {
                    format!("a{}s{}userapproval{}", author.id, story.id, user_id)
                };
                crate::actions::notifications::modifications::clean_all_unread_notifications_with_internal_description(&internal_description, conn)?;
            }

            Ok(())
        })
    }
}

/// These methods are used by administrators to check user approvals
pub mod admin {
    use diesel::prelude::*;

    use crate::models::error::IntertextualError;
    use crate::schema::*;

    /// Gets the total number of user approvals
    pub fn find_total_user_approval_count(conn: &PgConnection) -> Result<i64, IntertextualError> {
        let result = user_approvals::table.count().get_result::<i64>(conn)?;
        Ok(result)
    }

    /// Gets the times where any story received an approval
    pub fn find_user_approval_times_between(
        start: chrono::NaiveDateTime,
        end: chrono::NaiveDateTime,
        conn: &PgConnection,
    ) -> Result<Vec<chrono::NaiveDateTime>, IntertextualError> {
        let result = user_approvals::table
            .filter(user_approvals::approval_date.gt(start))
            .filter(user_approvals::approval_date.le(end))
            .select(user_approvals::approval_date)
            .load::<chrono::NaiveDateTime>(conn)?;
        Ok(result)
    }
}
