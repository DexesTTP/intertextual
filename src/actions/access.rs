//! # Access
//!
//! This module contains methods related to accessing or editing a story
use std::convert::TryFrom;

use diesel::prelude::*;

use crate::models;
use crate::models::error::IntertextualError;
use crate::models::stories::CreativeRole;
use crate::schema::*;

/// Get the creative role of an user for a given story
pub fn get_creative_role(
    story_id: uuid::Uuid,
    user_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<CreativeRole, IntertextualError> {
    let result = user_to_story_associations::table
        .find((user_id, story_id))
        .select(user_to_story_associations::creative_role)
        .first::<i32>(conn)
        .optional()?;

    match result {
        Some(value) => Ok(CreativeRole::try_from(value).unwrap_or(CreativeRole::None)),
        _ => Ok(CreativeRole::None),
    }
}

/// Get the list of users with a defined creative role for a given story
pub fn get_all_users_with_creative_roles(
    story_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Vec<(models::users::User, CreativeRole)>, IntertextualError> {
    Ok(user_to_story_associations::table
        .filter(user_to_story_associations::story_id.eq(story_id))
        .inner_join(users::table)
        .select((
            users::all_columns,
            user_to_story_associations::creative_role,
        ))
        .load::<(models::users::User, i32)>(conn)?
        .into_iter()
        .map(|(user, role)| {
            (
                user,
                CreativeRole::try_from(role).unwrap_or(CreativeRole::None),
            )
        })
        .filter(|(_, role)| !matches!(role, CreativeRole::None))
        .collect())
}

/// Require the user to have edition rights for this story.
pub fn require_edition_rights_for(
    story_id: uuid::Uuid,
    user_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<(), IntertextualError> {
    let creative_role = get_creative_role(story_id, user_id, conn)?;

    if creative_role.can_edit() {
        Ok(())
    } else {
        Err(IntertextualError::AccessRightsRequired)
    }
}

/// Require the user to have managements rights for this story or better.
pub fn require_management_rights_for(
    story_id: uuid::Uuid,
    user_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<(), IntertextualError> {
    let creative_role = get_creative_role(story_id, user_id, conn)?;

    if creative_role.can_manage() {
        Ok(())
    } else {
        Err(IntertextualError::AccessRightsRequired)
    }
}

/// These methods are used to handle modifications of the story access
pub mod modifications {
    use diesel::prelude::*;

    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::models::stories::CreativeRole;
    use crate::schema::*;

    /// Set the creative role of an user
    pub fn set_creative_role_for_user(
        story: models::stories::Story,
        user: models::users::User,
        creative_role: CreativeRole,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        conn.transaction::<(), IntertextualError, _>(|| {
            let result = user_to_story_associations::table
                .find((user.id, story.id))
                .select(user_to_story_associations::creative_role)
                .first::<i32>(conn)
                .optional()?;
            match result {
                Some(_) => {
                    if creative_role == CreativeRole::None {
                        // Standard access modes shouldn't be stored in the database at all,
                        // for ease of use.
                        let _ = diesel::delete(
                            user_to_story_associations::table.find((user.id, story.id)),
                        )
                        .execute(conn)?;
                    } else {
                        let _ = diesel::update(
                            user_to_story_associations::table.find((user.id, story.id)),
                        )
                        .set(user_to_story_associations::creative_role.eq(i32::from(creative_role)))
                        .execute(conn)?;
                    }
                }
                None => {
                    // Standard access modes shouldn't be stored in the database at all,
                    // for ease of use.
                    if creative_role != CreativeRole::None {
                        let _ = diesel::insert_into(user_to_story_associations::table)
                            .values((
                                user_to_story_associations::user_id.eq(user.id),
                                user_to_story_associations::story_id.eq(story.id),
                                user_to_story_associations::creative_role
                                    .eq(i32::from(creative_role)),
                            ))
                            .execute(conn)?;
                    }
                }
            };
            Ok(())
        })
    }
}
