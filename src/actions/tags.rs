//! # Tags
//!
//! This module contains methods used to create, edit, tag or untag stories
use diesel::prelude::*;

use crate::models;
use crate::models::error::IntertextualError;
use crate::models::tags::TagHighlightRule;
use crate::schema::*;

/// Find the internal tags related to a single story
///
/// Note : This can return several internal tags related to the same canonical tag.
/// As such, this should only be used when editing the list of tags.
pub fn find_internal_tags_by_story_id(
    story_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<
    Vec<(
        models::tags::TagCategory,
        models::tags::CanonicalTag,
        models::tags::InternalTagAssociation,
        models::tags::StoryToInternalTagAssociation,
    )>,
    IntertextualError,
> {
    let result = story_to_internal_tag_associations::table
        .filter(story_to_internal_tag_associations::story_id.eq(story_id))
        .inner_join(internal_to_canonical_tag_associations::table)
        .inner_join(
            canonical_tags::table
                .on(canonical_tags::id.eq(internal_to_canonical_tag_associations::canonical_id)),
        )
        .inner_join(tag_categories::table.on(tag_categories::id.eq(canonical_tags::category_id)))
        .select((
            tag_categories::all_columns,
            canonical_tags::all_columns,
            internal_to_canonical_tag_associations::all_columns,
            story_to_internal_tag_associations::all_columns,
        ))
        .load::<(
            models::tags::TagCategory,
            models::tags::CanonicalTag,
            models::tags::InternalTagAssociation,
            models::tags::StoryToInternalTagAssociation,
        )>(conn)?;
    Ok(result)
}

/// Find all the canonical tags related to a single story, without duplicates. Used to show information about the story
///
/// Note : Most of the time, the methods [`find_major_canonical_tags_by_story_id`], [`find_minor_canonical_tags_by_story_id`]
/// or [`find_spoiler_canonical_tags_by_story_id`] might return the relevant results directly
pub fn find_all_canonical_tags_by_story_id(
    story_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Vec<(models::tags::TagCategory, models::tags::CanonicalTag)>, IntertextualError> {
    let mut result = story_to_internal_tag_associations::table
        .filter(story_to_internal_tag_associations::story_id.eq(story_id))
        .inner_join(internal_to_canonical_tag_associations::table)
        .inner_join(
            canonical_tags::table
                .on(canonical_tags::id.eq(internal_to_canonical_tag_associations::canonical_id)),
        )
        .inner_join(tag_categories::table.on(tag_categories::id.eq(canonical_tags::category_id)))
        .select((tag_categories::all_columns, canonical_tags::all_columns))
        .distinct_on(canonical_tags::all_columns)
        .load::<(models::tags::TagCategory, models::tags::CanonicalTag)>(conn)?;
    sort_tag_list(&mut result);
    Ok(result)
}

/// Find the major canonical tags related to a single story, without duplicates. Used to show information about the story
pub fn find_major_canonical_tags_by_story_id(
    story_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Vec<(models::tags::TagCategory, models::tags::CanonicalTag)>, IntertextualError> {
    let mut result = story_to_internal_tag_associations::table
        .filter(story_to_internal_tag_associations::story_id.eq(story_id))
        .inner_join(internal_to_canonical_tag_associations::table)
        .inner_join(
            canonical_tags::table
                .on(canonical_tags::id.eq(internal_to_canonical_tag_associations::canonical_id)),
        )
        .inner_join(tag_categories::table.on(tag_categories::id.eq(canonical_tags::category_id)))
        // Never display spoiler tags
        .filter(
            story_to_internal_tag_associations::tag_type
                .ne(i32::from(models::tags::TagAssociationType::Spoiler)),
        )
        // Display either major tags or tags that aren't 'always displayed' tags
        .filter(
            story_to_internal_tag_associations::tag_type
                .eq(i32::from(models::tags::TagAssociationType::Major))
                .or(tag_categories::is_always_displayed.eq(true)),
        )
        .select((tag_categories::all_columns, canonical_tags::all_columns))
        .distinct_on(canonical_tags::all_columns)
        .load::<(models::tags::TagCategory, models::tags::CanonicalTag)>(conn)?;
    sort_tag_list(&mut result);
    Ok(result)
}

/// Find the minor canonical tags related to a single story, without duplicates. Used to show information about the story
pub fn find_minor_canonical_tags_by_story_id(
    story_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Vec<(models::tags::TagCategory, models::tags::CanonicalTag)>, IntertextualError> {
    let mut result = story_to_internal_tag_associations::table
        .filter(story_to_internal_tag_associations::story_id.eq(story_id))
        .inner_join(internal_to_canonical_tag_associations::table)
        .inner_join(
            canonical_tags::table
                .on(canonical_tags::id.eq(internal_to_canonical_tag_associations::canonical_id)),
        )
        .inner_join(tag_categories::table.on(tag_categories::id.eq(canonical_tags::category_id)))
        // Never display spoiler tags
        .filter(
            story_to_internal_tag_associations::tag_type
                .ne(i32::from(models::tags::TagAssociationType::Spoiler)),
        )
        // Display non-major tags that aren't 'always displayed' tags
        .filter(
            story_to_internal_tag_associations::tag_type
                .ne(i32::from(models::tags::TagAssociationType::Major))
                .and(tag_categories::is_always_displayed.eq(false)),
        )
        .select((tag_categories::all_columns, canonical_tags::all_columns))
        .distinct_on(canonical_tags::all_columns)
        .load::<(models::tags::TagCategory, models::tags::CanonicalTag)>(conn)?;
    sort_tag_list(&mut result);
    Ok(result)
}

/// Find the spoiler canonical tags related to a single story, without duplicates. Used to show information about the story
pub fn find_spoiler_canonical_tags_by_story_id(
    story_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Vec<(models::tags::TagCategory, models::tags::CanonicalTag)>, IntertextualError> {
    let mut result = story_to_internal_tag_associations::table
        .filter(story_to_internal_tag_associations::story_id.eq(story_id))
        .inner_join(internal_to_canonical_tag_associations::table)
        .inner_join(
            canonical_tags::table
                .on(canonical_tags::id.eq(internal_to_canonical_tag_associations::canonical_id)),
        )
        .inner_join(tag_categories::table.on(tag_categories::id.eq(canonical_tags::category_id)))
        // Display only spoiler tags
        .filter(
            story_to_internal_tag_associations::tag_type
                .eq(i32::from(models::tags::TagAssociationType::Spoiler)),
        )
        .select((tag_categories::all_columns, canonical_tags::all_columns))
        .distinct_on(canonical_tags::all_columns)
        .load::<(models::tags::TagCategory, models::tags::CanonicalTag)>(conn)?;
    sort_tag_list(&mut result);
    Ok(result)
}

pub fn sort_tag_list(list: &mut Vec<(models::tags::TagCategory, models::tags::CanonicalTag)>) {
    list.sort_by(|(c1, t1), (c2, t2)| {
        if c1.is_always_displayed && !c2.is_always_displayed {
            std::cmp::Ordering::Less
        } else if c2.is_always_displayed && !c1.is_always_displayed {
            std::cmp::Ordering::Greater
        } else {
            t1.display_name
                .to_lowercase()
                .cmp(&t2.display_name.to_lowercase())
        }
    });
}

/// Find the given canonical tag using its tag ID
pub fn find_canonical_tag_by_id(
    id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Option<models::tags::CanonicalTag>, IntertextualError> {
    let result = canonical_tags::table
        .find(id)
        .first::<models::tags::CanonicalTag>(conn)
        .optional()?;
    Ok(result)
}

/// Find the given internal tag using its tag ID
pub fn find_internal_tag_by_id(
    id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Option<models::tags::InternalTagAssociation>, IntertextualError> {
    let result = internal_to_canonical_tag_associations::table
        .find(id)
        .first::<models::tags::InternalTagAssociation>(conn)
        .optional()?;
    Ok(result)
}

/// Find the given internal tag using its tag ID
pub fn find_internal_tag_hierarchy_by_id(
    id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<
    Option<(
        models::tags::TagCategory,
        models::tags::CanonicalTag,
        models::tags::InternalTagAssociation,
    )>,
    IntertextualError,
> {
    let result = internal_to_canonical_tag_associations::table
        .find(id)
        .inner_join(
            canonical_tags::table
                .on(internal_to_canonical_tag_associations::canonical_id.eq(canonical_tags::id)),
        )
        .inner_join(tag_categories::table.on(canonical_tags::category_id.eq(tag_categories::id)))
        .select((
            tag_categories::all_columns,
            canonical_tags::all_columns,
            internal_to_canonical_tag_associations::all_columns,
        ))
        .first::<(
            models::tags::TagCategory,
            models::tags::CanonicalTag,
            models::tags::InternalTagAssociation,
        )>(conn)
        .optional()?;
    Ok(result)
}

/// Find the given canonical tag using its tag name.
///
/// Note : If the tag name of an internal tag is given, the associated canonical tag is returned
pub fn find_tag_by_name(
    name: &str,
    conn: &PgConnection,
) -> Result<Option<(models::tags::TagCategory, models::tags::CanonicalTag)>, IntertextualError> {
    let result = internal_to_canonical_tag_associations::table
        .filter(internal_to_canonical_tag_associations::internal_name.eq(name))
        .inner_join(canonical_tags::table.inner_join(tag_categories::table))
        .select((tag_categories::all_columns, canonical_tags::all_columns))
        .first::<(models::tags::TagCategory, models::tags::CanonicalTag)>(conn)
        .optional()?;
    Ok(result)
}

/// Find the internal tags linked to a canonical tag.
pub fn find_internal_tags_equivalent_to(
    tag: &models::tags::CanonicalTag,
    conn: &PgConnection,
) -> Result<Vec<models::tags::InternalTagAssociation>, IntertextualError> {
    let result = internal_to_canonical_tag_associations::table
        .filter(internal_to_canonical_tag_associations::canonical_id.eq(tag.id))
        .load::<models::tags::InternalTagAssociation>(conn)?;
    Ok(result)
}

/// Get the list of tag categories
pub fn get_all_categories(
    conn: &PgConnection,
) -> Result<Vec<models::tags::TagCategory>, IntertextualError> {
    let result = tag_categories::table
        .order_by(tag_categories::order_index)
        .load::<models::tags::TagCategory>(conn)?;
    Ok(result)
}

pub fn get_all_tags(
    conn: &PgConnection,
) -> Result<Vec<models::tags::CanonicalTag>, IntertextualError> {
    let result = canonical_tags::table.load::<models::tags::CanonicalTag>(conn)?;
    Ok(result)
}

pub fn get_popular_tags(
    limit: i64,
    conn: &PgConnection,
) -> Result<Vec<models::tags::CanonicalTag>, IntertextualError> {
    let result = internal_to_canonical_tag_associations::table
        .inner_join(canonical_tags::table)
        .order_by(internal_to_canonical_tag_associations::tagged_stories.desc())
        .limit(limit)
        .load::<(
            models::tags::InternalTagAssociation,
            models::tags::CanonicalTag,
        )>(conn)?;
    let mut result: Vec<_> = result.into_iter().map(|(_internal, tag)| tag).collect();
    result.sort_by(|t, u| t.display_name.cmp(&u.display_name));
    result.dedup_by_key(|t| t.id);
    Ok(result)
}

/// Get the list of quick-select include search categories (used in advanced search)
pub fn get_quick_select_for_included_tag_search_categories(
    conn: &PgConnection,
) -> Result<Vec<models::tags::TagCategory>, IntertextualError> {
    let result = tag_categories::table
        .filter(tag_categories::quick_select_for_included_tag_search.eq(true))
        .order_by(tag_categories::order_index)
        .load::<models::tags::TagCategory>(conn)?;
    Ok(result)
}

/// Get the list of quick-select exclude search categories (used in advanced search)
pub fn get_quick_select_for_excluded_tag_search_categories(
    conn: &PgConnection,
) -> Result<Vec<models::tags::TagCategory>, IntertextualError> {
    let result = tag_categories::table
        .filter(tag_categories::quick_select_for_excluded_tag_search.eq(true))
        .order_by(tag_categories::order_index)
        .load::<models::tags::TagCategory>(conn)?;
    Ok(result)
}

/// Get the given tag category by its name
pub fn get_category_by_name(
    category_name: &str,
    conn: &PgConnection,
) -> Result<Option<models::tags::TagCategory>, IntertextualError> {
    let result = tag_categories::table
        .filter(tag_categories::display_name.eq(category_name))
        .first::<models::tags::TagCategory>(conn)
        .optional()?;
    Ok(result)
}

/// Get the list of checkable tags in a given category (used in tag selection and advanced search)
pub fn get_checkable_tags_by_category(
    category_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Vec<models::tags::CanonicalTag>, IntertextualError> {
    let result = canonical_tags::table
        .filter(canonical_tags::category_id.eq(category_id))
        .filter(canonical_tags::is_checkable.eq(true))
        .load::<models::tags::CanonicalTag>(conn)?;
    Ok(result)
}

/// Get the list of tags all for a given category
pub fn get_tags_by_category(
    category_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Vec<models::tags::CanonicalTag>, IntertextualError> {
    let result = canonical_tags::table
        .filter(canonical_tags::category_id.eq(category_id))
        .load::<models::tags::CanonicalTag>(conn)?;
    Ok(result)
}

/// Get the number of stories for a given canonical tag
pub fn get_total_story_count(
    canonical_tag_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<i64, IntertextualError> {
    let result: i32 = canonical_tags::table
        .find(canonical_tag_id)
        .inner_join(internal_to_canonical_tag_associations::table)
        .select(internal_to_canonical_tag_associations::tagged_stories)
        .load::<i32>(conn)?
        .into_iter()
        .sum();
    Ok(result as i64)
}

/// Search for a given tag with the given partial tag name
pub fn search_from_query(
    query: String,
    conn: &PgConnection,
) -> Result<
    Vec<(
        models::tags::TagCategory,
        models::tags::CanonicalTag,
        models::tags::InternalTagAssociation,
    )>,
    IntertextualError,
> {
    let trimmed_query = query.trim().trim_start_matches('#');
    if trimmed_query.is_empty() {
        return Ok(vec![]);
    }

    let mut result = internal_to_canonical_tag_associations::table
        .filter(
            internal_to_canonical_tag_associations::internal_name
                .ilike(format!("%{}%", trimmed_query)),
        )
        .inner_join(canonical_tags::table.inner_join(tag_categories::table))
        .select((
            tag_categories::all_columns,
            canonical_tags::all_columns,
            internal_to_canonical_tag_associations::all_columns,
        ))
        .load::<(
            models::tags::TagCategory,
            models::tags::CanonicalTag,
            models::tags::InternalTagAssociation,
        )>(conn)?;
    result.sort_by_cached_key(|(_, _, k)| {
        // Get the 'most exact" first while maintaining the numerical order
        if k.internal_name == query {
            "1".to_string()
        } else if k.internal_name.starts_with(&query) {
            format!("2{}", k.internal_name)
        } else {
            format!("3{}", k.internal_name)
        }
    });
    Ok(result)
}

/// Get the list of all tag highlight rule templates
pub fn get_tag_highlight_rule_templates_list(
    conn: &PgConnection,
) -> Result<Vec<(String, String)>, IntertextualError> {
    let result = tag_highlight_rules_templates::table
        .select((
            tag_highlight_rules_templates::name,
            tag_highlight_rules_templates::description,
        ))
        .order_by(tag_highlight_rules_templates::name.desc())
        .load::<(String, String)>(conn)?;
    Ok(result)
}

/// Get the rules for a single template
pub fn get_tag_highlight_rule_template_by_name(
    name: &str,
    conn: &PgConnection,
) -> Result<Vec<TagHighlightRule>, IntertextualError> {
    let raw_data = tag_highlight_rules_templates::table
        .filter(tag_highlight_rules_templates::name.eq(name))
        .select(tag_highlight_rules_templates::tag_highlight_rules)
        .first::<Option<serde_json::Value>>(conn)?;

    match raw_data {
        None => Ok(vec![]),
        Some(value) => {
            let result = serde_json::from_value::<Vec<TagHighlightRule>>(value)
                .ok()
                .unwrap_or_else(Vec::new);
            Ok(result)
        }
    }
}

/// Methods to modify tag associations with stories
pub mod modifications {
    use diesel::prelude::*;

    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::schema::*;

    /// Representation of a new canonical tag before insertion
    #[derive(Insertable)]
    #[table_name = "canonical_tags"]
    pub struct NewCanonicalTag {
        /// The associated category ID for this tag.
        pub category_id: uuid::Uuid,
        /// The name of this tag.
        pub display_name: String,
    }

    /// Representation of a new internal tag before insertion
    #[derive(Insertable)]
    #[table_name = "internal_to_canonical_tag_associations"]
    pub struct NewInternalTagAssociation {
        /// The associated canonical ID for this tag.
        pub canonical_id: uuid::Uuid,
        /// The internal name of this tag
        pub internal_name: String,
    }

    /// Gets (if the tag exists) or creates (if it doesn't) the given internal tag in the database.
    ///
    /// Note : If the tag needs to be created, it will be created as a canonical tag in the given category
    pub fn get_or_insert_tag(
        category_id: uuid::Uuid,
        internal_name: &str,
        conn: &PgConnection,
    ) -> Result<
        (
            models::tags::InternalTagAssociation,
            models::tags::CanonicalTag,
        ),
        IntertextualError,
    > {
        conn.transaction::<(
            models::tags::InternalTagAssociation,
            models::tags::CanonicalTag,
        ), IntertextualError, _>(|| {
            let existing_data = internal_to_canonical_tag_associations::table
                .filter(internal_to_canonical_tag_associations::internal_name.eq(internal_name))
                .inner_join(canonical_tags::table)
                .first::<(
                    models::tags::InternalTagAssociation,
                    models::tags::CanonicalTag,
                )>(conn)
                .optional()?;
            if let Some(existing_data) = existing_data {
                return Ok(existing_data);
            }
            let canonical_tag = diesel::insert_into(canonical_tags::table)
                .values(NewCanonicalTag {
                    category_id,
                    display_name: internal_name.to_string(),
                })
                .get_result::<models::tags::CanonicalTag>(conn)?;
            let internal_tag = diesel::insert_into(internal_to_canonical_tag_associations::table)
                .values(NewInternalTagAssociation {
                    canonical_id: canonical_tag.id,
                    internal_name: internal_name.to_string(),
                })
                .get_result::<models::tags::InternalTagAssociation>(conn)?;
            Ok((internal_tag, canonical_tag))
        })
    }

    /// Associates the given internal tag to a story wiht the given `tag_type` (major, standard, spoiler)
    pub fn add_tag_to_story(
        story_id: uuid::Uuid,
        internal_tag: models::tags::InternalTagAssociation,
        tag_type: models::tags::TagAssociationType,
        conn: &PgConnection,
    ) -> Result<models::tags::StoryToInternalTagAssociation, IntertextualError> {
        let internal_tag_id = internal_tag.internal_id;
        let association = models::tags::StoryToInternalTagAssociation {
            story_id,
            internal_tag_id,
            tag_type: tag_type.into(),
        };
        let association_result = diesel::insert_into(story_to_internal_tag_associations::table)
            .values(association)
            .get_result::<models::tags::StoryToInternalTagAssociation>(conn)?;
        Ok(association_result)
    }

    /// Updates the given internal tag association to a story to the new given `tag_type` (major, standard, spoiler)
    pub fn update_story_association_type(
        story_id: uuid::Uuid,
        internal_id: uuid::Uuid,
        tag_type: models::tags::TagAssociationType,
        conn: &PgConnection,
    ) -> Result<models::tags::StoryToInternalTagAssociation, IntertextualError> {
        let association_result =
            diesel::update(story_to_internal_tag_associations::table.find((story_id, internal_id)))
                .set(story_to_internal_tag_associations::tag_type.eq(i32::from(tag_type)))
                .get_result::<models::tags::StoryToInternalTagAssociation>(conn)?;
        Ok(association_result)
    }

    /// Removes the tag association with the given story
    pub fn remove_tag_from_story(
        story_id: uuid::Uuid,
        internal_tag: models::tags::InternalTagAssociation,
        conn: &PgConnection,
    ) -> Result<models::tags::StoryToInternalTagAssociation, IntertextualError> {
        let association_result = diesel::delete(
            story_to_internal_tag_associations::table
                .filter(story_to_internal_tag_associations::story_id.eq(story_id))
                .filter(
                    story_to_internal_tag_associations::internal_tag_id
                        .eq(internal_tag.internal_id),
                ),
        )
        .get_result::<models::tags::StoryToInternalTagAssociation>(conn)?;
        Ok(association_result)
    }
}

pub mod automated_actions {
    use diesel::prelude::*;
    use models::tags::InternalTagAssociation;

    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::models::stories::CreativeRole;
    use crate::schema::*;

    pub fn recompute_tag_counts(conn: &PgConnection) -> Result<(), IntertextualError> {
        // Note : No need to wrap this in a transaction since we don't really care whether the results are exact.
        let tag_list =
            internal_to_canonical_tag_associations::table.load::<InternalTagAssociation>(conn)?;
        for tag in tag_list {
            let associated_visible_stories = stories::table
                .inner_join(story_to_internal_tag_associations::table)
                .filter(story_to_internal_tag_associations::internal_tag_id.eq(tag.internal_id))
                .inner_join(chapters::table)
                .filter(chapters::show_publicly_after_date.lt(chrono::Utc::now().naive_utc()))
                .filter(chapters::moderator_locked.eq(false))
                .inner_join(user_to_story_associations::table)
                .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
                .inner_join(users::table.on(user_to_story_associations::user_id.eq(users::id)))
                .filter(users::deactivated_by_user.eq(false))
                .filter(
                    users::banned_until
                        .lt(chrono::Utc::now().naive_utc())
                        .or(users::banned_until.is_null()),
                )
                .select(stories::id)
                .distinct()
                .load::<uuid::Uuid>(conn)?;
            let story_count = associated_visible_stories.len() as i32;
            diesel::update(internal_to_canonical_tag_associations::table.find(tag.internal_id))
                .set(internal_to_canonical_tag_associations::tagged_stories.eq(story_count))
                .execute(conn)?;
        }

        Ok(())
    }
}

/// Methods to edit the tag categories or behaviours, or to link canonical and internal tags together
pub mod admin {
    use diesel::prelude::*;

    use crate::actions::utils::log_mod_action;
    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::models::tags::TagHighlightRule;
    use crate::schema::*;

    /// Represents a tag category for insertion in the database
    #[derive(Insertable)]
    #[table_name = "tag_categories"]
    pub struct NewTagCategory {
        /// Display name of the category
        pub display_name: String,
        /// Description of the category
        pub description: String,
        /// Whether tags in this category are always major tags (major or spoiler)
        pub is_always_displayed: bool,
        /// Whether users can add non-existing tags to this category
        pub allow_user_defined_tags: bool,
    }

    /// Finds a category using its ID
    pub fn find_category_by_id(
        id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<Option<models::tags::TagCategory>, IntertextualError> {
        let result = tag_categories::table
            .find(id)
            .first::<models::tags::TagCategory>(conn)
            .optional()?;
        Ok(result)
    }

    /// Finds an internal tag using its ID
    pub fn find_internal_tag_by_id(
        internal_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<Option<models::tags::InternalTagAssociation>, IntertextualError> {
        let result = internal_to_canonical_tag_associations::table
            .filter(internal_to_canonical_tag_associations::internal_id.eq(internal_id))
            .first::<models::tags::InternalTagAssociation>(conn)
            .optional()?;
        Ok(result)
    }

    /// Find the canonical tag for a given internal tag using its ID
    fn find_matching_canonical_tag_from_internal_tag_id(
        internal_id: uuid::Uuid,
        conn: &PgConnection,
    ) -> Result<models::tags::CanonicalTag, IntertextualError> {
        let result =
            internal_to_canonical_tag_associations::table
                .filter(internal_to_canonical_tag_associations::internal_id.eq(internal_id))
                .inner_join(canonical_tags::table.on(
                    canonical_tags::id.eq(internal_to_canonical_tag_associations::canonical_id),
                ))
                .select(canonical_tags::all_columns)
                .first::<models::tags::CanonicalTag>(conn)?;
        Ok(result)
    }

    /// Create a new category, and log the action to the moderation database
    pub fn add_new_category(
        new_category: NewTagCategory,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<(models::tags::TagCategory, models::admin::ModerationAction), IntertextualError>
    {
        conn.transaction::<(models::tags::TagCategory, models::admin::ModerationAction), IntertextualError, _>(|| {
            let category = diesel::insert_into(tag_categories::table)
                .values(new_category)
                .get_result::<models::tags::TagCategory>(conn)?;
            let mod_action = log_mod_action(log_entry, conn)?;
            Ok((category, mod_action))
        })
    }

    /// Update a category name, description or characteristics, and log the action to the moderation database
    pub fn update_category(
        category: models::tags::TagCategory,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<(models::tags::TagCategory, models::admin::ModerationAction), IntertextualError>
    {
        conn.transaction::<(models::tags::TagCategory, models::admin::ModerationAction), IntertextualError, _>(|| {
        let result = diesel::update(tag_categories::table.find(category.id))
            .set((
                tag_categories::display_name.eq(category.display_name),
                tag_categories::description.eq(category.description),
                tag_categories::order_index.eq(category.order_index),
                tag_categories::is_always_displayed.eq(category.is_always_displayed),
                tag_categories::allow_user_defined_tags.eq(category.allow_user_defined_tags),
                tag_categories::quick_select_for_included_tag_search
                    .eq(category.quick_select_for_included_tag_search),
                tag_categories::quick_select_for_excluded_tag_search
                    .eq(category.quick_select_for_excluded_tag_search),
            ))
            .get_result::<models::tags::TagCategory>(conn)?;
        let mod_action = log_mod_action(log_entry, conn)?;
        Ok((result, mod_action))
        })
    }

    /// Update a canonical tag name, description or characteristics, and log the action to the moderation database
    pub fn update_tag(
        tag: models::tags::CanonicalTag,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<(models::tags::CanonicalTag, models::admin::ModerationAction), IntertextualError>
    {
        conn.transaction::<(models::tags::CanonicalTag, models::admin::ModerationAction), IntertextualError, _>(|| {
            let result = diesel::update(canonical_tags::table.find(tag.id))
                .set((
                    canonical_tags::category_id.eq(tag.category_id),
                    canonical_tags::description.eq(tag.description),
                    canonical_tags::is_checkable.eq(tag.is_checkable),
                    canonical_tags::show_in_popularity_listing.eq(tag.show_in_popularity_listing),
                ))
                .get_result::<models::tags::CanonicalTag>(conn)?;
            let mod_action = log_mod_action(log_entry, conn)?;
            Ok((result, mod_action))
        })
    }

    /// Link an internal tag to a canonical tag, and log the action to the moderation database
    pub fn link_tag(
        internal_tag: models::tags::InternalTagAssociation,
        canonical_tag: models::tags::CanonicalTag,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            let existing_canonical_tag =
                find_matching_canonical_tag_from_internal_tag_id(internal_tag.internal_id, conn)?;

            // Sanity check.
            if existing_canonical_tag.id == canonical_tag.id {
                // We are trying to link to... Ourself ? Break out of here.
                panic!("Cannot link tag to itself. Use Unlink for this");
            }

            // Link the internal tag to the wanted canonical tag.
            diesel::update(internal_to_canonical_tag_associations::table.filter(
                internal_to_canonical_tag_associations::internal_id.eq(internal_tag.internal_id),
            ))
            .set(internal_to_canonical_tag_associations::canonical_id.eq(canonical_tag.id))
            .get_result::<models::tags::InternalTagAssociation>(conn)?;

            if existing_canonical_tag.display_name == internal_tag.internal_name {
                // Delete the canonical tag of the name we just linked
                diesel::delete(
                    canonical_tags::table.filter(canonical_tags::id.eq(existing_canonical_tag.id)),
                )
                .get_result::<models::tags::CanonicalTag>(conn)?;
            }

            let mod_action = log_mod_action(log_entry, conn)?;
            Ok(mod_action)
        })
    }

    /// Unlink an internal tag to a canonical tag, and log the action to the moderation database
    ///
    /// Note : Unlinking a tag will create a new canonical tag with the same name
    pub fn unlink_tag(
        internal_tag: models::tags::InternalTagAssociation,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::tags::CanonicalTag, IntertextualError> {
        conn.transaction::<models::tags::CanonicalTag, IntertextualError, _>(|| {
            let existing_canonical_tag =
                find_matching_canonical_tag_from_internal_tag_id(internal_tag.internal_id, conn)?;

            // Sanity check.
            if existing_canonical_tag.display_name == internal_tag.internal_name {
                // We don't have anything to do, the tag is already unlinked
                return Ok(existing_canonical_tag);
            }

            // Create a new canonical tag
            let new_canonical_tag = diesel::insert_into(canonical_tags::table)
                .values(super::modifications::NewCanonicalTag {
                    category_id: existing_canonical_tag.category_id,
                    display_name: internal_tag.internal_name.clone(),
                })
                .get_result::<models::tags::CanonicalTag>(conn)?;

            // Link the internal tag to the new canonical tag
            diesel::update(internal_to_canonical_tag_associations::table.filter(
                internal_to_canonical_tag_associations::internal_id.eq(internal_tag.internal_id),
            ))
            .set(internal_to_canonical_tag_associations::canonical_id.eq(new_canonical_tag.id))
            .get_result::<models::tags::InternalTagAssociation>(conn)?;

            let _mod_action = log_mod_action(log_entry, conn)?;
            Ok(new_canonical_tag)
        })
    }

    /// Delete a tag category and log the action.
    ///
    /// Note : this will delete all associated internal and canonical tags, as well as remove the tags from associated stories
    pub fn delete_category(
        category: models::tags::TagCategory,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            diesel::delete(tag_categories::table.find(category.id))
                .get_result::<models::tags::TagCategory>(conn)?;
            let mod_action = log_mod_action(log_entry, conn)?;
            Ok(mod_action)
        })
    }

    /// Delete a given internal tag and log the action.
    ///
    /// Note : this will delete all associated internal and canonical tags, as well as remove the tags from associated stories
    pub fn delete_tag(
        internal_tag: models::tags::InternalTagAssociation,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            let existing_canonical_tag =
                find_matching_canonical_tag_from_internal_tag_id(internal_tag.internal_id, conn)?;

            if existing_canonical_tag.display_name == internal_tag.internal_name {
                // Delete the canonical tag
                // The internal tag will be cascaded out by this operation, so we don't need to delete it explicitly.
                diesel::delete(canonical_tags::table.find(existing_canonical_tag.id))
                    .get_result::<models::tags::CanonicalTag>(conn)?;
            } else {
                // Delete only the internal tag
                diesel::delete(
                    internal_to_canonical_tag_associations::table.filter(
                        internal_to_canonical_tag_associations::internal_id
                            .eq(internal_tag.internal_id),
                    ),
                )
                .get_result::<models::tags::InternalTagAssociation>(conn)?;
            }

            let mod_action = log_mod_action(log_entry, conn)?;
            Ok(mod_action)
        })
    }

    /// Create a rule template
    pub fn create_tag_highlight_rule_template(
        name: String,
        description: String,
        tag_highlight_rules: Vec<TagHighlightRule>,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            let rules = serde_json::to_value(tag_highlight_rules)
                .map_err(|_| IntertextualError::InternalServerError)?;
            diesel::insert_into(tag_highlight_rules_templates::table)
                .values((
                    tag_highlight_rules_templates::name.eq(name),
                    tag_highlight_rules_templates::description.eq(description),
                    tag_highlight_rules_templates::tag_highlight_rules.eq(Some(rules)),
                ))
                .execute(conn)?;

            let mod_action = log_mod_action(log_entry, conn)?;
            Ok(mod_action)
        })
    }
    /// Update an existing rule template
    pub fn update_tag_highlight_rule_template_metadata(
        existing_name: String,
        new_name: String,
        new_description: String,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            let existing = tag_highlight_rules_templates::table
                .filter(tag_highlight_rules_templates::name.eq(&existing_name))
                .select(tag_highlight_rules_templates::id)
                .first::<uuid::Uuid>(conn)
                .optional()?;
            match existing {
                Some(existing_id) => {
                    diesel::update(tag_highlight_rules_templates::table.find(existing_id))
                        .set((
                            tag_highlight_rules_templates::name.eq(new_name),
                            tag_highlight_rules_templates::description.eq(new_description),
                        ))
                        .execute(conn)?;

                    let mod_action = log_mod_action(log_entry, conn)?;
                    Ok(mod_action)
                }
                None => Err(IntertextualError::FormFieldFormatError {
                    form_field_name: "Template name",
                    message: format!("The template {} was not found", existing_name),
                }),
            }
        })
    }

    /// Update an existing rule template
    pub fn update_tag_highlight_rule_template_content(
        existing_name: String,
        tag_highlight_rules: Vec<TagHighlightRule>,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            let rules = serde_json::to_value(tag_highlight_rules)
                .map_err(|_| IntertextualError::InternalServerError)?;
            let existing = tag_highlight_rules_templates::table
                .filter(tag_highlight_rules_templates::name.eq(&existing_name))
                .select(tag_highlight_rules_templates::id)
                .first::<uuid::Uuid>(conn)
                .optional()?;
            match existing {
                Some(existing_id) => {
                    diesel::update(tag_highlight_rules_templates::table.find(existing_id))
                        .set((tag_highlight_rules_templates::tag_highlight_rules.eq(Some(rules)),))
                        .execute(conn)?;

                    let mod_action = log_mod_action(log_entry, conn)?;
                    Ok(mod_action)
                }
                None => Err(IntertextualError::FormFieldFormatError {
                    form_field_name: "Template name",
                    message: format!("The template {} was not found", existing_name),
                }),
            }
        })
    }

    /// Delete a rule template
    pub fn delete_tag_highlight_rule_template(
        name: String,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            diesel::delete(tag_highlight_rules_templates::table)
                .filter(tag_highlight_rules_templates::name.eq(name))
                .execute(conn)?;

            let mod_action = log_mod_action(log_entry, conn)?;
            Ok(mod_action)
        })
    }
}
