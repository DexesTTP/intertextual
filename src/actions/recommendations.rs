//! # Recommendations
//!
//! This module contains methods related to the story recommendation system
use diesel::prelude::*;

use crate::models;
use crate::models::error::IntertextualError;
use crate::schema::*;

/// Gets the list of all recommendations for story `story_id`
pub fn find_recommendations_by_story(
    story_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Vec<(models::users::User, models::recommendations::Recommendation)>, IntertextualError>
{
    let result = recommendations::table
        .filter(recommendations::story_id.eq(story_id))
        .inner_join(users::table.on(recommendations::user_id.eq(users::id)))
        .filter(users::deactivated_by_user.eq(false))
        .filter(
            users::banned_until
                .lt(chrono::Utc::now().naive_utc())
                .or(users::banned_until.is_null()),
        )
        .select((users::all_columns, recommendations::all_columns))
        .load::<(models::users::User, models::recommendations::Recommendation)>(conn)?;
    Ok(result)
}

/// Gets the list of all recommendations written by the user `user_id`
pub fn find_recommendations_by_user(
    user_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<
    Vec<(
        models::stories::Story,
        models::recommendations::Recommendation,
    )>,
    IntertextualError,
> {
    let result = recommendations::table
        .filter(recommendations::user_id.eq(user_id))
        .inner_join(stories::table.inner_join(chapters::table))
        .filter(
            stories::recommendations_enabled
                .eq(i32::from(models::stories::RecommendationsState::UserOnly))
                .or(stories::recommendations_enabled
                    .eq(i32::from(models::stories::RecommendationsState::Enabled))),
        )
        .filter(chapters::show_publicly_after_date.lt(chrono::Utc::now().naive_utc()))
        .filter(chapters::moderator_locked.eq(false))
        .distinct_on(recommendations::story_id)
        .select((stories::all_columns, recommendations::all_columns))
        .load::<(
            models::stories::Story,
            models::recommendations::Recommendation,
        )>(conn)?;
    Ok(result)
}

/// Gets the recommendation written by the user `user_id` for the story `story_id` if it exists
pub fn find_recommendation_by_user_and_story(
    user_id: uuid::Uuid,
    story_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<Option<models::recommendations::Recommendation>, IntertextualError> {
    let result = recommendations::table
        .filter(recommendations::story_id.eq(story_id))
        .filter(recommendations::user_id.eq(user_id))
        .inner_join(stories::table.inner_join(chapters::table))
        .filter(
            stories::recommendations_enabled
                .eq(i32::from(models::stories::RecommendationsState::UserOnly))
                .or(stories::recommendations_enabled
                    .eq(i32::from(models::stories::RecommendationsState::Enabled))),
        )
        .filter(chapters::show_publicly_after_date.lt(chrono::Utc::now().naive_utc()))
        .filter(chapters::moderator_locked.eq(false))
        .distinct_on(recommendations::story_id)
        .select(recommendations::all_columns)
        .first::<models::recommendations::Recommendation>(conn)
        .optional()?;
    Ok(result)
}

/// List of methods to add, remove and edit recommendations
pub mod modifications {
    use diesel::prelude::*;

    use crate::constants::RECOMMENDATION_URL;
    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::prelude::*;
    use crate::schema::*;

    /// Add a new recommendation
    pub fn create_recommendation(
        new_recommendation: models::recommendations::NewRecommendation,
        conn: &PgConnection,
    ) -> Result<models::recommendations::Recommendation, IntertextualError> {
        conn.transaction::<models::recommendations::Recommendation, IntertextualError, _>(|| {
            let user_id = new_recommendation.user_id;
            let story_id = new_recommendation.story_id;

            let created_recommendation = diesel::insert_into(recommendations::table)
                .values(new_recommendation)
                .get_result::<models::recommendations::Recommendation>(conn)?;

            crate::actions::follows::notifications::notify_new_recommendation(
                &created_recommendation,
                conn,
            )?;

            // Signal to all the authors of the story that they just got a recommendation
            let user = users::table
                .find(user_id)
                .first::<models::users::User>(conn)?;
            let story: models::stories::Story = stories::table.find(story_id).first(conn)?;
            let author_list = crate::actions::stories::find_authors_by_story(
                story_id,
                &FilterMode::BypassFilters,
                conn,
            )?;
            for author in author_list {
                let notification = models::notifications::NewNotification {
                    target_user_id: author.id,
                    notification_time: chrono::Utc::now().naive_utc(),
                    internal_description: if story.is_collaboration {
                        format!("collabs{}reco{}", story.id, user.id)
                    } else {
                        format!("a{}s{}reco{}", author.id, story.id, user.id)
                    },
                    message: format!(
                        "The user {} suggested your story {} !",
                        user.display_name, story.title,
                    ),
                    link: if story.is_collaboration {
                        format!(
                            "/{}/@{}/collaboration/{}",
                            RECOMMENDATION_URL, user.username, story.url_fragment
                        )
                    } else {
                        format!(
                            "/{}/@{}/@{}/{}",
                            RECOMMENDATION_URL, user.username, author.username, story.url_fragment
                        )
                    },
                    category: models::notifications::NotificationCategory::Recommended.into(),
                };
                crate::actions::notifications::modifications::add_notification(notification, conn)?;
            }

            Ok(created_recommendation)
        })
    }

    /// Set the given recommendation as featured / not featured by the user
    pub fn update_recommendation_featured_by_user(
        recommendation: models::recommendations::Recommendation,
        featured_by_user: bool,
        conn: &PgConnection,
    ) -> Result<models::recommendations::Recommendation, IntertextualError> {
        let updated_recommendation = diesel::update(recommendations::table.find(recommendation.id))
            .set(recommendations::featured_by_user.eq(featured_by_user))
            .get_result::<models::recommendations::Recommendation>(conn)?;
        Ok(updated_recommendation)
    }

    /// Update the message of the given recommendation
    pub fn update_recommendation_description(
        recommendation: models::recommendations::Recommendation,
        new_description: String,
        conn: &PgConnection,
    ) -> Result<models::recommendations::Recommendation, IntertextualError> {
        let updated_recommendation = diesel::update(recommendations::table.find(recommendation.id))
            .set(recommendations::description.eq(new_description))
            .get_result::<models::recommendations::Recommendation>(conn)?;
        Ok(updated_recommendation)
    }

    /// Delete the given recommendation.
    ///
    /// Note : This method should not be used by administrators. Check the [`super::admin::delete_recommendation_administrator`] method of that.
    pub fn delete_recommendation(
        recommendation: models::recommendations::Recommendation,
        conn: &PgConnection,
    ) -> Result<(), IntertextualError> {
        conn.transaction::<(), IntertextualError, _>(|| {
            diesel::delete(recommendations::table.find(recommendation.id))
                .get_result::<models::recommendations::Recommendation>(conn)?;
            diesel::delete(
                recommendation_reports::table
                    .filter(recommendation_reports::story_id.eq(recommendation.story_id))
                    .filter(recommendation_reports::user_id.eq(recommendation.user_id)),
            )
            .load::<models::recommendations::RecommendationReport>(conn)?;
            Ok(())
        })
    }
}

/// These methods are related to administrative actions for the recommendations
pub mod admin {
    use diesel::prelude::*;

    use crate::actions::utils::log_mod_action;
    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::schema::*;

    #[derive(Clone, Copy, PartialEq, Eq)]
    pub enum RecommendationsSortMode {
        CreationAsc,
        CreationDesc,
    }

    /// Gets the total number of recommendations
    pub fn find_total_recommendations_count(conn: &PgConnection) -> Result<i64, IntertextualError> {
        let result = recommendations::table.count().get_result::<i64>(conn)?;
        Ok(result)
    }

    /// Gets the recommendations written between the two given times
    pub fn find_all_recommendation_times_between(
        start: chrono::NaiveDateTime,
        end: chrono::NaiveDateTime,
        conn: &PgConnection,
    ) -> Result<Vec<chrono::NaiveDateTime>, IntertextualError> {
        let result = recommendations::table
            .filter(recommendations::recommendation_date.gt(start))
            .filter(recommendations::recommendation_date.le(end))
            .select(recommendations::recommendation_date)
            .load::<chrono::NaiveDateTime>(conn)?;
        Ok(result)
    }

    /// Gets the recommendations ordered by most recently written
    pub fn find_all_recommendations_by_sort_mode(
        start: i64,
        limit: i64,
        sort_mode: RecommendationsSortMode,
        conn: &PgConnection,
    ) -> Result<Vec<models::recommendations::Recommendation>, IntertextualError> {
        let result = match sort_mode {
            RecommendationsSortMode::CreationAsc => recommendations::table
                .offset(start)
                .limit(limit)
                .order_by(recommendations::recommendation_date.asc())
                .load::<models::recommendations::Recommendation>(conn)?,
            RecommendationsSortMode::CreationDesc => recommendations::table
                .offset(start)
                .limit(limit)
                .order_by(recommendations::recommendation_date.desc())
                .load::<models::recommendations::Recommendation>(conn)?,
        };
        Ok(result)
    }

    /// Delete the given recommendation, and log the action to the database
    pub fn delete_recommendation_administrator(
        moderator: models::users::ModeratorUserWrapper,
        recommendation: models::recommendations::Recommendation,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            let user_id = recommendation.user_id;
            super::modifications::delete_recommendation(recommendation, conn)?;

            // Show the action as modmail
            crate::actions::modmail::admins::send_new_system_modmail(
                moderator,
                user_id,
                format!(
                    "System message. One of your recommendations has been deleted for the following reason : {}",
                    log_entry.message
                ),
                conn,
            )?;

            // Store the action in the moderation log
            let mod_action = log_mod_action(log_entry, conn)?;

            // Notify the user
            let notification = models::notifications::NewNotification {
                target_user_id: user_id,
                notification_time: chrono::Utc::now().naive_utc(),
                internal_description: format!("administrator_recommendation_deleted_u{}", user_id),
                message: "One of your recommendations has been deleted by a moderator".to_string(),
                link: "/moderation_contact/".to_string(),
                category: models::notifications::NotificationCategory::SystemMessage.into(),
            };
            crate::actions::notifications::modifications::add_notification(notification, conn)?;

            Ok(mod_action)
        })
    }
}
