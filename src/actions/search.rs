//! # Search
//!
//! This module contains the core of the search engine
use diesel::dsl::any;
use diesel::prelude::*;
use itertools::Itertools;

use crate::models::error::IntertextualError;
use crate::models::filter::FilterMode;
use crate::models::stories::CreativeRole;
use crate::models::stories::StorySearchResult;
use crate::models::stories::StorySortMode;
use crate::models::tags::CanonicalTag;
use crate::schema::*;

pub struct SearchInformation {
    pub tags: Vec<CanonicalTag>,
    pub excluded_tags: Vec<CanonicalTag>,
    pub title: Option<String>,
    pub author: Option<String>,
    pub description: Option<String>,
}

/// Execute the search given the following parameters
///
/// * `tags` : The tags to include ; the search results will include at least one of these tags
/// * `excluded_tags` : The tags to exclude ; the search results will never contain any of these tags
/// * `title` : The optional title fragment ; the search results will always match against this title fragment if given
/// * `author` : The optional author fragment ; the search results will always match against this author fragment if given
/// * `description` : The optional description fragment ; the search results will always match against this description fragment if given
/// * `filter_mode` : How to filter the results. See [`FilterMode`] for details
pub fn execute_search(
    search_information: SearchInformation,
    sort_mode: StorySortMode,
    filter_mode: &FilterMode,
    conn: &PgConnection,
) -> Result<Vec<StorySearchResult>, IntertextualError> {
    if search_information.tags.is_empty()
        && search_information.title.is_none()
        && search_information.author.is_none()
        && search_information.description.is_none()
    {
        // If we only have an exclude list, then don't even search.
        return Ok(vec![]);
    }
    // FIXME : While this works, this is horribly inefficient.
    // Right now, this :
    // - Queries the DB for all stories with AT LEAST one of the requested (excluded or included tag) (with all the authors, descriptions, etc... rules too),
    // - Returns a Vec<(Story, CanonicalTag)> with the story DUPLICATED for each matched tag,
    // - Manually (read : use an iter to) regroup the vec by story and creates a Vec<(Story, Iter<CanonicalTags>)> with this result
    // - Filters out the stories where the list of tags contains an excluded tags
    // - Aggregates the results with a number of matched tag compared to the total
    // - Creates a third vec with this result.
    //
    // We should find another way to filter it properly directly via a DB query (with a function or a view, maybe ?)
    // Of course, diesel does not expose HAVING or similar things, because that would be too easy...
    let mut query = story_to_internal_tag_associations::table
        .inner_join(internal_to_canonical_tag_associations::table)
        .inner_join(
            canonical_tags::table
                .on(canonical_tags::id.eq(internal_to_canonical_tag_associations::canonical_id)),
        )
        .inner_join(
            chapters::table.on(chapters::story_id.eq(story_to_internal_tag_associations::story_id)),
        )
        .inner_join(stories::table)
        .inner_join(
            user_to_story_associations::table
                .on(user_to_story_associations::story_id.eq(stories::id)),
        )
        .filter(
            user_to_story_associations::creative_role.ge(CreativeRole::min_unpublished_access()),
        )
        .inner_join(users::table.on(users::id.eq(user_to_story_associations::user_id)))
        .into_boxed();
    if !search_information.tags.is_empty() {
        let tag_list: Vec<uuid::Uuid> = search_information
            .tags
            .iter()
            .chain(&search_information.excluded_tags)
            .map(|t| t.id)
            .collect();
        query = query.filter(canonical_tags::id.eq(any(tag_list)));
    }
    if let Some(title) = search_information.title {
        query = query.filter(stories::title.ilike(format!("%{}%", title)));
    }
    if let Some(author) = search_information.author {
        query = query.filter(
            user_to_story_associations::creative_role
                .eq(CreativeRole::author())
                .and(
                    users::username
                        .ilike(format!("%{}%", author))
                        .or(users::display_name.ilike(format!("%{}%", author))),
                ),
        );
    }
    if let Some(description) = search_information.description {
        query = query.filter(stories::description.ilike(format!("%{}%", description)));
    }
    match filter_mode {
        FilterMode::Standard => {
            query = query
                .filter(user_to_story_associations::creative_role.eq(CreativeRole::author()))
                .filter(users::deactivated_by_user.eq(false))
                .filter(
                    users::banned_until
                        .lt(chrono::Utc::now().naive_utc())
                        .or(users::banned_until.is_null()),
                )
                .filter(chapters::show_publicly_after_date.lt(chrono::Utc::now().naive_utc()))
                .filter(chapters::moderator_locked.eq(false));
        }
        FilterMode::IncludeDirectAccess { own_user_id } => {
            // Note: This won't work as expected in case of author filtering : the stories that
            //       are not published will only be shown if the user is the searched author, but
            //       not if they're a co-author, editor or read-only reader.
            query = query
                .filter(
                    user_to_story_associations::creative_role
                        .eq(CreativeRole::author())
                        .and(users::deactivated_by_user.eq(false))
                        .or(users::id.eq(own_user_id)),
                )
                .filter(
                    user_to_story_associations::creative_role
                        .eq(CreativeRole::author())
                        .and(
                            users::banned_until
                                .lt(chrono::Utc::now().naive_utc())
                                .or(users::banned_until.is_null()),
                        )
                        .or(users::id.eq(own_user_id)),
                )
                .filter(
                    user_to_story_associations::creative_role
                        .eq(CreativeRole::author())
                        .and(chapters::show_publicly_after_date.lt(chrono::Utc::now().naive_utc()))
                        .or(user_to_story_associations::user_id.eq(own_user_id)),
                )
                .filter(chapters::moderator_locked.eq(false));
        }
        FilterMode::BypassFilters => {}
    };
    let query = query.select((
        stories::id,
        canonical_tags::id,
        stories::title,
        stories::story_internally_created_at,
        stories::cached_first_publication_date,
        stories::cached_last_update_date,
    ));

    let query = match sort_mode {
        StorySortMode::TitleAsc => query.order(stories::title.asc()),
        StorySortMode::TitleDesc => query.order(stories::title.desc()),
        StorySortMode::InternalCreationAsc => {
            query.order(stories::story_internally_created_at.asc())
        }
        StorySortMode::InternalCreationDesc => {
            query.order(stories::story_internally_created_at.desc())
        }
        StorySortMode::PublicationAsc => query.order(stories::cached_first_publication_date.asc()),
        StorySortMode::PublicationDesc => {
            query.order(stories::cached_first_publication_date.desc())
        }
        StorySortMode::LastUpdateAsc => query.order(stories::cached_last_update_date.asc()),
        StorySortMode::LastUpdateDesc => query.order(stories::cached_last_update_date.desc()),
    };

    let query = query.distinct();
    let query_result = query.load::<(
        uuid::Uuid,
        uuid::Uuid,
        String,
        chrono::NaiveDateTime,
        chrono::NaiveDateTime,
        chrono::NaiveDateTime,
    )>(conn)?;
    let tag_ids: Vec<uuid::Uuid> = search_information.tags.iter().map(|t| t.id).collect();
    let excluded_tag_ids: Vec<uuid::Uuid> = search_information
        .excluded_tags
        .iter()
        .map(|t| t.id)
        .collect();
    let grouped_results = query_result.into_iter().group_by(|(s, _, _, _, _, _)| *s);
    let mut filtered_results = grouped_results
        .into_iter()
        .filter_map(|(story_id, group)| {
            let mut found_tags = 0;
            for (_, tag_id, _, _, _, _) in group {
                if excluded_tag_ids.contains(&tag_id) {
                    return None;
                }
                if tag_ids.contains(&tag_id) {
                    found_tags += 1;
                }
            }
            Some(StorySearchResult {
                story_id,
                found_tags,
                requested_tags: tag_ids.len(),
            })
        })
        .collect::<Vec<StorySearchResult>>();
    filtered_results.sort_by_key(|v| -(v.found_tags as isize));
    Ok(filtered_results)
}
