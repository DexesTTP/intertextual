//! # Static pages
//!
//! This module contains methods used to read or modify static pages, such as the rules or FAQ page.
use diesel::prelude::*;

use crate::models::error::IntertextualError;
use crate::models::formats::SanitizedHtml;
use crate::schema::*;

#[derive(Clone, Copy)]
pub enum StaticPageKind {
    Faq,
    Legal,
    Rules,
    TermsOfService,
    Contact,
}

impl StaticPageKind {
    fn db_string(&self) -> &'static str {
        match self {
            StaticPageKind::Contact => "contact",
            StaticPageKind::Faq => "faq",
            StaticPageKind::Legal => "legal",
            StaticPageKind::Rules => "rules",
            StaticPageKind::TermsOfService => "tos",
        }
    }

    pub fn title(&self) -> &'static str {
        match self {
            StaticPageKind::Contact => "Contact Us",
            StaticPageKind::Faq => "Frequently Asked Questions",
            StaticPageKind::Legal => "Legal Mentions",
            StaticPageKind::Rules => "Rules",
            StaticPageKind::TermsOfService => "Terms of Service",
        }
    }

    pub fn from_page_url(page: &str) -> Option<StaticPageKind> {
        match page {
            "contact" => Some(StaticPageKind::Contact),
            "faq" => Some(StaticPageKind::Faq),
            "rules" => Some(StaticPageKind::Rules),
            "legal" => Some(StaticPageKind::Legal),
            "terms_of_service" => Some(StaticPageKind::TermsOfService),
            _ => None,
        }
    }

    pub fn page_url(&self) -> &'static str {
        match self {
            StaticPageKind::Contact => "contact",
            StaticPageKind::Faq => "faq",
            StaticPageKind::Rules => "rules",
            StaticPageKind::Legal => "legal",
            StaticPageKind::TermsOfService => "terms_of_service",
        }
    }
}

/// Get the static page for the given content kind
pub fn get_static_page(
    kind: StaticPageKind,
    conn: &PgConnection,
) -> Result<SanitizedHtml, IntertextualError> {
    let content = static_pages::table
        .find(kind.db_string())
        .select(static_pages::content)
        .get_result::<SanitizedHtml>(conn)
        .optional()?;
    Ok(content.unwrap_or_else(SanitizedHtml::new))
}

/// These methods are used by administrators to edit the static pages
pub mod admin {
    use diesel::prelude::*;

    use crate::actions::utils::log_mod_action;
    use crate::models;
    use crate::models::error::IntertextualError;
    use crate::models::formats::SanitizedHtml;
    use crate::schema::*;

    pub fn set_static_page(
        kind: super::StaticPageKind,
        new_content: &SanitizedHtml,
        log_entry: models::admin::NewModerationAction,
        conn: &PgConnection,
    ) -> Result<models::admin::ModerationAction, IntertextualError> {
        conn.transaction::<models::admin::ModerationAction, IntertextualError, _>(|| {
            diesel::insert_into(static_pages::table)
                .values((
                    static_pages::page_name.eq(kind.db_string()),
                    static_pages::content.eq(new_content),
                ))
                .on_conflict(static_pages::page_name)
                .do_update()
                .set((
                    static_pages::content.eq(new_content),
                    static_pages::last_edition_time.eq(chrono::Utc::now().naive_utc()),
                ))
                .execute(conn)?;

            // Store the action in the moderation log
            let mod_action = log_mod_action(log_entry, conn)?;

            Ok(mod_action)
        })
    }
}
