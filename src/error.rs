use actix_web::{error::BlockingError, HttpResponse, ResponseError};
use thiserror::Error;

use intertextual::models::error::IntertextualError;

use crate::app::common_pages::*;
use crate::app::persistent::PersistentData;
use crate::app::persistent::PersistentTemplate;

#[derive(Error, Debug)]
pub enum AppError {
    #[error("{error}")]
    IoError {
        error: std::io::Error,
        persistent: PersistentTemplate,
    },

    #[error("{error}")]
    DBPoolError {
        error: diesel::r2d2::PoolError,
        persistent: PersistentTemplate,
    },

    #[error("{error}")]
    IntertextualError {
        error: IntertextualError,
        persistent: PersistentTemplate,
    },

    #[error("{error}")]
    TemplateError {
        error: askama::Error,
        persistent: PersistentTemplate,
    },

    #[error("{error}")]
    MultipartError {
        error: actix_multipart::MultipartError,
        persistent: PersistentTemplate,
    },

    #[error("The multipart form field was too long and could not be parsed")]
    MultipartFormFieldTooLongError { persistent: PersistentTemplate },

    #[error("blocking operation canceled")]
    Canceled,
}

pub trait IntoApp {
    fn into_app(self, data: &PersistentData) -> AppError;
}

pub trait MapApp<T> {
    fn map_err_app(self, data: &PersistentData) -> Result<T, AppError>;
}

impl<T, E: IntoApp + std::fmt::Debug> MapApp<T> for Result<T, E> {
    fn map_err_app(self, data: &PersistentData) -> Result<T, AppError> {
        self.map_err(|e| e.into_app(data))
    }
}

impl IntoApp for std::io::Error {
    fn into_app(self, data: &PersistentData) -> AppError {
        AppError::IoError {
            error: self,
            persistent: data.into(),
        }
    }
}

impl IntoApp for diesel::r2d2::PoolError {
    fn into_app(self, data: &PersistentData) -> AppError {
        AppError::DBPoolError {
            error: self,
            persistent: data.into(),
        }
    }
}

impl IntoApp for IntertextualError {
    fn into_app(self, data: &PersistentData) -> AppError {
        AppError::IntertextualError {
            error: self,
            persistent: data.into(),
        }
    }
}

impl IntoApp for askama::Error {
    fn into_app(self, data: &PersistentData) -> AppError {
        AppError::TemplateError {
            error: self,
            persistent: data.into(),
        }
    }
}

impl IntoApp for actix_multipart::MultipartError {
    fn into_app(self, data: &PersistentData) -> AppError {
        AppError::MultipartError {
            error: self,
            persistent: data.into(),
        }
    }
}

impl From<BlockingError<AppError>> for AppError {
    fn from(error: BlockingError<AppError>) -> AppError {
        match error {
            BlockingError::Error(e) => e,
            BlockingError::Canceled => AppError::Canceled,
        }
    }
}

impl<E: IntoApp + std::fmt::Debug> IntoApp for BlockingError<E> {
    fn into_app(self, data: &PersistentData) -> AppError {
        match self {
            BlockingError::Error(e) => e.into_app(data),
            BlockingError::Canceled => AppError::Canceled,
        }
    }
}

impl ResponseError for AppError {
    fn error_response(&self) -> HttpResponse {
        match self {
            AppError::IntertextualError { error, persistent } => match error {
                IntertextualError::DBError(_) => {
                    eprintln!("{}", error);
                    internal_server_error(persistent.clone())
                }
                IntertextualError::FromUtf8Error(_) => {
                    eprintln!("{}", error);
                    internal_server_error(persistent.clone())
                }
                IntertextualError::IOError(_) => {
                    eprintln!("{}", error);
                    internal_server_error(persistent.clone())
                }
                IntertextualError::PandocError(_) => {
                    eprintln!("{}", error);
                    internal_server_error(persistent.clone())
                }
                IntertextualError::AccessRightsRequired => not_accessible(
                    persistent.clone(),
                    Some("You do not have enough rights to access this page".to_string()),
                ),
                IntertextualError::AccessRightsRequiredObfuscated => {
                    not_found(persistent.clone(), None)
                }
                IntertextualError::AuthorChapterNotFound {
                    author_username,
                    chapter_number,
                    url_fragment,
                } => not_found(
                    persistent.clone(),
                    Some(format!(
                        "The chapter {} of story {} from author @{} does not exist",
                        chapter_number, url_fragment, author_username
                    )),
                ),
                IntertextualError::UserNotFound { username } => not_found(
                    persistent.clone(),
                    Some(format!("The user @{} does not exist", username)),
                ),
                IntertextualError::AuthorStoryNotFound {
                    author_username,
                    url_fragment,
                } => not_found(
                    persistent.clone(),
                    Some(format!(
                        "The story {} from author @{} does not exist",
                        url_fragment, author_username
                    )),
                ),
                IntertextualError::CollaborationStoryNotFound { url_fragment } => not_found(
                    persistent.clone(),
                    Some(format!("The collaboration {} does not exist", url_fragment)),
                ),
                IntertextualError::CollaborationChapterNotFound {
                    url_fragment,
                    chapter_number,
                } => not_found(
                    persistent.clone(),
                    Some(format!(
                        "The chapter {} of collaboration {} does not exist",
                        chapter_number, url_fragment
                    )),
                ),
                IntertextualError::AuthorRecommendationNotFound {
                    recommender_username,
                    author_username,
                    url_fragment,
                } => not_found(
                    persistent.clone(),
                    Some(format!(
                        "The recommendation of story {} by @{} by user @{} does not exist",
                        url_fragment, author_username, recommender_username
                    )),
                ),
                IntertextualError::CollaborationRecommendationNotFound {
                    recommender_username,
                    url_fragment,
                } => not_found(
                    persistent.clone(),
                    Some(format!(
                        "The recommendation of collaboration {} by user @{} does not exist",
                        url_fragment, recommender_username
                    )),
                ),
                IntertextualError::CommentNotFound { comment_id } => not_found(
                    persistent.clone(),
                    Some(format!("The comment {} does not exist", comment_id)),
                ),
                IntertextualError::NotificationNotFound { notification_id } => not_found(
                    persistent.clone(),
                    Some(format!(
                        "The notification {} does not exist",
                        notification_id
                    )),
                ),
                IntertextualError::ModmailNotFound { modmail_id } => not_found(
                    persistent.clone(),
                    Some(format!(
                        "The moderation message {} does not exist",
                        modmail_id
                    )),
                ),
                IntertextualError::FormFieldFormatError {
                    form_field_name,
                    message,
                } => bad_request(
                    persistent.clone(),
                    Some(format!("Error for field {} : {}", form_field_name, message)),
                ),
                IntertextualError::LoginRequired => not_accessible(
                    persistent.clone(),
                    Some("You need to be logged in to access this page".to_string()),
                ),
                IntertextualError::LoginRequiredObfuscated => not_found(persistent.clone(), None),
                IntertextualError::InvalidPassword => not_accessible(
                    persistent.clone(),
                    Some("The provided password is incorrect".to_string()),
                ),
                IntertextualError::PageDoesNotExist => not_found(persistent.clone(), None),
                IntertextualError::InternalServerError => internal_server_error(persistent.clone()),
                IntertextualError::Canceled => internal_server_error(persistent.clone()),
            },

            AppError::MultipartFormFieldTooLongError { persistent } => {
                eprintln!("The multipart form field was longer than expected");
                bad_request(
                    persistent.clone(),
                    Some(
                        "Bad request : the multipart form field was bigger than the accepted size"
                            .to_string(),
                    ),
                )
            }

            AppError::MultipartError { error, persistent } => {
                eprintln!("{}", error);
                bad_request(
                    persistent.clone(),
                    Some(
                        "Bad request : the multipart form entry had an invalid format".to_string(),
                    ),
                )
            }

            AppError::IoError { error, persistent } => {
                eprintln!("{}", error);
                internal_server_error(persistent.clone())
            }

            AppError::TemplateError { error, persistent } => {
                eprintln!("{}", error);
                internal_server_error(persistent.clone())
            }
            AppError::DBPoolError { error, persistent } => {
                eprintln!("{}", error);
                internal_server_error(persistent.clone())
            }

            AppError::Canceled => HttpResponse::InternalServerError().finish(),
        }
    }
}
