table! {
    account_deletion_log (id) {
        id -> Uuid,
        account_id -> Uuid,
        is_active_deletion_request -> Bool,
        can_be_cancelled_by_user -> Bool,
        request_date -> Timestamp,
        request_information -> Text,
    }
}

table! {
    auth_cookies_tokens (user_id, token_id) {
        user_id -> Uuid,
        token_id -> Uuid,
        token_name -> Text,
        last_visit -> Timestamp,
        valid_until -> Timestamp,
    }
}

table! {
    canonical_tags (id) {
        id -> Uuid,
        display_name -> Varchar,
        category_id -> Uuid,
        is_checkable -> Bool,
        description -> Nullable<Text>,
        show_in_popularity_listing -> Bool,
    }
}

table! {
    chapter_reports (chapter_id, reporter_id) {
        report_date -> Timestamp,
        chapter_id -> Uuid,
        reporter_id -> Uuid,
        handled_by_moderator_id -> Nullable<Uuid>,
        reason -> Text,
    }
}

table! {
    chapters (id) {
        id -> Uuid,
        chapter_internally_created_at -> Timestamp,
        story_id -> Uuid,
        chapter_number -> Int4,
        title -> Nullable<Varchar>,
        content -> Text,
        word_count -> Int4,
        official_creation_date -> Nullable<Timestamp>,
        update_date -> Nullable<Timestamp>,
        show_publicly_after_date -> Nullable<Timestamp>,
        moderator_locked -> Bool,
        moderator_locked_reason -> Nullable<Text>,
        foreword -> Text,
        afterword -> Text,
    }
}

table! {
    comment_reports (comment_id, reporter_id) {
        report_date -> Timestamp,
        comment_id -> Uuid,
        reporter_id -> Uuid,
        handled_by_moderator_id -> Nullable<Uuid>,
        reason -> Text,
    }
}

table! {
    comments (id) {
        id -> Uuid,
        commenter_id -> Uuid,
        chapter_id -> Uuid,
        created -> Timestamp,
        updated -> Nullable<Timestamp>,
        message -> Text,
    }
}

table! {
    excluded_authors_for_user (user_id, author_id) {
        user_id -> Uuid,
        author_id -> Uuid,
    }
}

table! {
    excluded_canonical_tags_for_user (user_id, canonical_tag_id) {
        user_id -> Uuid,
        canonical_tag_id -> Uuid,
    }
}

table! {
    internal_to_canonical_tag_associations (internal_id) {
        internal_id -> Uuid,
        canonical_id -> Uuid,
        internal_name -> Varchar,
        tagged_stories -> Int4,
    }
}

table! {
    last_story_cache_check (id) {
        id -> Uuid,
        time -> Timestamp,
    }
}

table! {
    moderation_actions (id) {
        id -> Uuid,
        username -> Varchar,
        action_date -> Timestamp,
        action_type -> Text,
        message -> Text,
    }
}

table! {
    modmail (id) {
        id -> Uuid,
        user_id -> Uuid,
        message_date -> Timestamp,
        message -> Text,
        is_user_to_mods -> Bool,
        is_read_by_user -> Bool,
        handled_by_moderator_id -> Nullable<Uuid>,
    }
}

table! {
    notifications (id) {
        id -> Uuid,
        target_user_id -> Uuid,
        notification_time -> Timestamp,
        internal_description -> Text,
        category -> Int4,
        message -> Text,
        link -> Text,
        read -> Bool,
    }
}

table! {
    pending_notification_generation_requests (id) {
        id -> Uuid,
        request_time -> Timestamp,
        request_type -> Text,
        target_id -> Uuid,
        currently_handled_by_task_worker_index -> Nullable<Int4>,
    }
}

table! {
    recommendation_reports (story_id, user_id, reporter_id) {
        report_date -> Timestamp,
        story_id -> Uuid,
        user_id -> Uuid,
        reporter_id -> Uuid,
        handled_by_moderator_id -> Nullable<Uuid>,
        reason -> Text,
    }
}

table! {
    recommendations (id) {
        id -> Uuid,
        story_id -> Uuid,
        user_id -> Uuid,
        recommendation_date -> Timestamp,
        featured_by_user -> Bool,
        description -> Text,
    }
}

table! {
    registration_parameters (id) {
        id -> Uuid,
        registrations_enabled -> Bool,
        limit_enabled -> Bool,
        limit_max_registrations -> Int4,
        limit_hours -> Int4,
    }
}

table! {
    static_pages (page_name) {
        page_name -> Varchar,
        content -> Text,
        last_edition_time -> Timestamp,
    }
}

table! {
    stories (id) {
        id -> Uuid,
        story_internally_created_at -> Timestamp,
        title -> Varchar,
        url_fragment -> Varchar,
        is_collaboration -> Bool,
        description -> Text,
        recommendations_enabled -> Int4,
        comments_enabled -> Bool,
        is_multi_chapter -> Bool,
        cached_first_publication_date -> Timestamp,
        cached_last_update_date -> Timestamp,
    }
}

table! {
    story_hits (id) {
        id -> Int4,
        chapter_id -> Uuid,
        hit_date -> Timestamp,
    }
}

table! {
    story_to_internal_tag_associations (story_id, internal_tag_id) {
        story_id -> Uuid,
        internal_tag_id -> Uuid,
        tag_type -> Int4,
    }
}

table! {
    tag_categories (id) {
        id -> Uuid,
        display_name -> Varchar,
        description -> Text,
        order_index -> Int4,
        is_always_displayed -> Bool,
        allow_user_defined_tags -> Bool,
        quick_select_for_included_tag_search -> Bool,
        quick_select_for_excluded_tag_search -> Bool,
    }
}

table! {
    tag_highlight_rules_templates (id) {
        id -> Uuid,
        name -> Varchar,
        description -> Text,
        tag_highlight_rules -> Nullable<Jsonb>,
    }
}

table! {
    user_approvals (user_id, story_id) {
        user_id -> Uuid,
        story_id -> Uuid,
        approval_date -> Timestamp,
    }
}

table! {
    user_author_follows (user_id, author_id) {
        user_id -> Uuid,
        author_id -> Uuid,
        follow_date -> Timestamp,
    }
}

table! {
    user_author_recommendation_follows (user_id, author_id) {
        user_id -> Uuid,
        author_id -> Uuid,
        follow_date -> Timestamp,
    }
}

table! {
    user_story_follows (user_id, story_id) {
        user_id -> Uuid,
        story_id -> Uuid,
        follow_date -> Timestamp,
    }
}

table! {
    user_story_for_later (user_id, story_id) {
        user_id -> Uuid,
        story_id -> Uuid,
        approval_date -> Timestamp,
    }
}

table! {
    user_to_story_associations (user_id, story_id) {
        user_id -> Uuid,
        story_id -> Uuid,
        creative_role -> Int4,
        creation_time -> Timestamp,
    }
}

table! {
    user_warnings (id) {
        id -> Uuid,
        warning_date -> Timestamp,
        moderator_id -> Uuid,
        user_id -> Uuid,
        reason -> Text,
    }
}

table! {
    users (id) {
        id -> Uuid,
        username -> Varchar,
        display_name -> Varchar,
        account_created_at -> Timestamp,
        pw_hash -> Text,
        password_reset_required -> Bool,
        administrator -> Bool,
        email -> Nullable<Text>,
        details -> Nullable<Text>,
        theme -> Nullable<Text>,
        settings -> Nullable<Jsonb>,
        deactivated_by_user -> Bool,
        banned_until -> Nullable<Timestamp>,
        muted_until -> Nullable<Timestamp>,
        warned_until -> Nullable<Timestamp>,
        default_foreword -> Text,
        default_afterword -> Text,
        tag_highlight_rules -> Nullable<Jsonb>,
    }
}

joinable!(auth_cookies_tokens -> users (user_id));
joinable!(canonical_tags -> tag_categories (category_id));
joinable!(chapter_reports -> chapters (chapter_id));
joinable!(chapter_reports -> users (reporter_id));
joinable!(chapters -> stories (story_id));
joinable!(comment_reports -> comments (comment_id));
joinable!(comment_reports -> users (reporter_id));
joinable!(comments -> chapters (chapter_id));
joinable!(comments -> users (commenter_id));
joinable!(excluded_canonical_tags_for_user -> canonical_tags (canonical_tag_id));
joinable!(excluded_canonical_tags_for_user -> users (user_id));
joinable!(internal_to_canonical_tag_associations -> canonical_tags (canonical_id));
joinable!(modmail -> users (user_id));
joinable!(notifications -> users (target_user_id));
joinable!(recommendation_reports -> stories (story_id));
joinable!(recommendations -> stories (story_id));
joinable!(recommendations -> users (user_id));
joinable!(story_hits -> chapters (chapter_id));
joinable!(story_to_internal_tag_associations -> internal_to_canonical_tag_associations (internal_tag_id));
joinable!(story_to_internal_tag_associations -> stories (story_id));
joinable!(user_approvals -> stories (story_id));
joinable!(user_approvals -> users (user_id));
joinable!(user_story_follows -> stories (story_id));
joinable!(user_story_follows -> users (user_id));
joinable!(user_story_for_later -> stories (story_id));
joinable!(user_story_for_later -> users (user_id));
joinable!(user_to_story_associations -> stories (story_id));
joinable!(user_to_story_associations -> users (user_id));

allow_tables_to_appear_in_same_query!(
    account_deletion_log,
    auth_cookies_tokens,
    canonical_tags,
    chapter_reports,
    chapters,
    comment_reports,
    comments,
    excluded_authors_for_user,
    excluded_canonical_tags_for_user,
    internal_to_canonical_tag_associations,
    last_story_cache_check,
    moderation_actions,
    modmail,
    notifications,
    pending_notification_generation_requests,
    recommendation_reports,
    recommendations,
    registration_parameters,
    static_pages,
    stories,
    story_hits,
    story_to_internal_tag_associations,
    tag_categories,
    tag_highlight_rules_templates,
    user_approvals,
    user_author_follows,
    user_author_recommendation_follows,
    user_story_follows,
    user_story_for_later,
    user_to_story_associations,
    user_warnings,
    users,
);
