//! # Models : Modmail
//!
//! This module contains models related to messages between users and moderators

use chrono::NaiveDateTime;
use std::cmp::{Eq, PartialEq};

use super::formats::RichBlock;
use super::users::User;
use crate::schema::*;

/// Represents a message between an user and the moderation team
#[derive(Debug, Identifiable, Queryable, Associations, PartialEq, Eq)]
#[belongs_to(User, foreign_key = "user_id")]
#[table_name = "modmail"]
pub struct Modmail {
    /// The unique ID of the message
    pub id: uuid::Uuid,
    /// The ID of the user who is talking with/to the moderation team
    pub user_id: uuid::Uuid,
    /// The date the message was sent
    pub message_date: NaiveDateTime,
    /// The message's contents
    pub message: RichBlock,
    /// Whether the message was sent from user to mods. If false, the moderation team wrote this message
    pub is_user_to_mods: bool,
    /// Whether this message was read by the user
    pub is_read_by_user: bool,
    /// The ID of the moderator who handled the message, or [`None`] if the moderation team did not handle the message
    pub handled_by_moderator_id: Option<uuid::Uuid>,
}

/// Represents a new message ready to be inserted in the database
pub struct NewModmail {
    /// The ID of the user who is sending a message to the moderation team
    pub user_id: uuid::Uuid,
    /// The message's contents
    pub message: RichBlock,
}
