//! # Models : Users
//!
//! This module contains models related to users and user descriptions

use serde::{Deserialize, Serialize};
use std::collections::HashMap;

use crate::models::error::IntertextualError;
use crate::models::formats::SanitizedHtml;
use crate::models::notifications::NotificationCategory;
use crate::models::stories::RecommendationsState;
use crate::models::stories::Story;
use crate::models::tags::TagHighlightRule;
use crate::schema::*;

/// Represents a default publication mode.
///
/// This mode is used to decide of a publication date when creating new stories and chapters.
#[derive(Clone, Copy, Deserialize, Eq, PartialEq, Serialize)]
#[repr(u32)]
pub enum PreferredPublicationMode {
    /// New stories will propose to be published immediately by default
    PublishNow,
    /// New stories will will propose to be published at a set weekday and hour by default.
    ///
    /// Note : This mode is accompanied by the members [UserSettings::preferred_publication_day] and
    /// [UserSettings::preferred_publication_hour] in the user settings.
    PublishLater,

    /// New stories will not propose to be published by default
    DoNotPublish,
}

/// Represents a notification level for a given notification type.
///
/// See [User::default_notification_level] for the default value for each notification.
#[derive(Clone, Copy, Deserialize, Eq, PartialEq, Serialize)]
#[repr(u32)]
pub enum NotificationLevel {
    /// When unread, a red square with a number inside will be displayed in the corner of the username.
    ///
    /// The number corresponds to the number of unread notifications with this notification level.
    RedNumber,

    /// When unread, a grey dot will be displayed in the corner of the username.
    GreyDot,

    /// When unread, no visual indication will be shown.
    None,
}

/// Represents the user settings
///
/// Note : this information is serialized as JSONb inside the database.
pub struct UserSettings {
    /// Settings for how to display unread notifications.
    ///
    /// Notification settings associate a [NotificationCategory], which describes what the notification
    /// is about, with a [NotificationLevel], which describes how to display a given notification when unread.
    pub notification_settings: HashMap<NotificationCategory, NotificationLevel>,

    /// What should be the default comment enabling state on new stories
    pub preferred_comments_enabled: bool,

    /// What should be the default recommendation enabling state on new stories
    pub preferred_recommendations_enabled: RecommendationsState,

    /// What should be the default selected publication settings
    pub preferred_publication_mode: PreferredPublicationMode,

    /// Defines the preferred weekday for new publications.
    ///
    /// Note : If the weekday is [None], then the weekday willbe ignored, and only the [UserSettings::preferred_publication_hour]
    /// setting will be taken into account.
    ///
    /// Note : This setting is only used if the default selected publication setting is [PreferredPublicationMode::PublishLater]
    pub preferred_publication_day: Option<chrono::Weekday>,

    /// Defines the preferred hour for new publications.
    ///
    /// Note : This setting is only used if the default selected publication setting is [PreferredPublicationMode::PublishLater]
    pub preferred_publication_hour: u32,

    /// Gets the UUID of this author's featured stories, if defined
    pub featured_story_ids: Vec<uuid::Uuid>,

    /// Gets or sets the default card display mode used e.g. on the Latest Stories list
    pub card_display_mode: Option<String>,
}

/// This is a serializable version of the [UserSettings] struct, allowing for optional and default values.
///
/// See the [UserSettings] struct for details
#[derive(Deserialize, Serialize)]
struct UserSettingsSerialized {
    /// See [UserSettings::notification_settings] for details
    notification_settings: Option<HashMap<NotificationCategory, NotificationLevel>>,
    /// See [UserSettings::preferred_comments_enabled] for details
    preferred_comments_enabled: Option<bool>,
    /// See [UserSettings::preferred_recommendations_enabled] for details
    preferred_recommendations_enabled: Option<RecommendationsState>,
    /// See [UserSettings::preferred_publication_mode] for details
    preferred_publication_mode: Option<PreferredPublicationMode>,
    /// See [UserSettings::preferred_publication_day] for details
    preferred_publication_day: Option<chrono::Weekday>,
    /// See [UserSettings::preferred_publication_hour] for details
    preferred_publication_hour: Option<u32>,
    /// See [UserSettings::featured_story_ids] for details
    featured_story_ids: Vec<uuid::Uuid>,
    /// See [UserSettings::card_display_mode] for details
    card_display_mode: Option<String>,
}

/// Represents a very short description of an user
pub struct ShortUserEntry {
    /// The username (see [`User::username`])
    pub username: String,
    /// The display name of the user (see [`User::display_name`])
    pub display_name: String,
}

/// Represents an account deletion log entry
#[derive(Serialize, Deserialize, Insertable)]
#[table_name = "account_deletion_log"]
pub struct NewAccountDeletionLog {
    /// The ID of the account to delete
    pub account_id: uuid::Uuid,
    /// Whether this request is pending (false for cancellation log entries)
    pub is_active_deletion_request: bool,
    /// Whether this request can be cancelled by the user (false if this request comes from a moderator)
    pub can_be_cancelled_by_user: bool,
    /// Additional description about the nature of this request
    pub request_information: String,
}

/// Represents an account deletion log entry
#[derive(Debug, Identifiable, Queryable)]
#[table_name = "account_deletion_log"]
pub struct AccountDeletionLog {
    /// The ID of the request itself
    pub id: uuid::Uuid,
    /// The ID of the account to delete
    pub account_id: uuid::Uuid,
    /// Whether this request is pending (false for cancellation log entries)
    pub is_active_deletion_request: bool,
    /// Whether this request can be cancelled by the user (false if this request comes from a moderator)
    pub can_be_cancelled_by_user: bool,
    /// The date teh request was made
    pub request_date: chrono::NaiveDateTime,
    /// Additional description about the nature of this request
    pub request_information: String,
}

/// Represents a user, as described in the database.
#[derive(Debug, Identifiable, Queryable)]
#[table_name = "users"]
pub struct User {
    /// The internal user identifier.
    ///
    /// Note : The User ID is an internal value, and should never be shown in user-facing screens.
    /// If users need to be uniquely identified, use the [User::username] field which is guaranteed
    /// to be unique.
    pub id: uuid::Uuid,

    /// The username of this user. This is the preferred way to give user-specific path inside URLs.
    ///
    /// Note : This username is guaranteed to be globally unique.
    ///
    /// Note (2) : By convention, usernames should be precedeed by an `@` symbol
    ///
    /// # Examples
    ///
    /// ```
    /// let username = "user_name";
    /// let display_name = "Display Name";
    /// let full_display_name = format!("{} (@{})", display_name, username);
    /// assert_eq!(full_display_name, "Display Name (@user_name)");
    /// ```
    pub username: String,

    /// The display name of this user. This is the preferred way to display a user name.
    ///
    /// Note : This value can be duplicated across users
    ///
    /// # Examples
    ///
    /// ```
    /// let username = "user_name";
    /// let display_name = "Display Name";
    /// let full_display_name = format!("{} (@{})", display_name, username);
    /// assert_eq!(full_display_name, "Display Name (@user_name)");
    /// ```
    pub display_name: String,

    /// Timestamp indicating when the user account was created
    ///
    /// Note : All timestamps stored in the database are UTC
    pub account_created_at: chrono::NaiveDateTime,

    /// The password hash for this user, encoded via [argonautica].
    ///
    /// Note: Verifying a password against this app requires a secret key, stored in the
    /// global state. See [crate::models::shared::AppState] for more details.
    ///
    /// # Examples
    ///
    /// ```
    /// use argonautica::Verifier;
    ///
    /// // The password hash, retrieved from this field.
    /// let pw_hash = "hash";
    /// // The raw password, retrieved from the user (for example, on login).
    /// let raw_password = "clear_password";
    /// // The password secret key, available in a shared state. See [intertextual::AppState] for details.
    /// let password_secret_key = "secret_key";
    ///
    /// // Verify whether the user-provided password matches the hash stored in the database.
    /// let password_matches_hash = Verifier::default()
    ///     .with_hash(pw_hash)
    ///     .with_password(raw_password)
    ///     .with_secret_key(password_secret_key)
    ///     .verify();
    /// ```
    pub pw_hash: String,

    /// Whether this user's password must be reset after logging in.
    pub password_reset_required: bool,

    /// Whether this user is an administrator or not.
    pub administrator: bool,

    /// The user-provided email address. This field is optional.
    pub email: Option<String>,

    /// The user-provided details, as sanitized HTML. This field is optional.
    pub details: Option<SanitizedHtml>,

    /// The theme this user uses while browsing the website. Handled independently by the front-end (see the `app/persistent` part).
    pub theme: Option<String>,

    /// The user settings. See the [UserSettings] class for details
    pub settings: Option<serde_json::Value>,

    /// Indicates whether the user's account is deactivated. Handled by the user, stories should not be shown for deactivated accounts.
    pub deactivated_by_user: bool,

    /// Indicates whether the user's account has been banned. Handled by the moderation team, stories should not be shown for banned accounts.
    pub banned_until: Option<chrono::NaiveDateTime>,

    /// Indicates whether the user's account has been muted. Handled by the moderation team, users should not be able to comment/review for muted accounts.
    pub muted_until: Option<chrono::NaiveDateTime>,

    /// Indicates whether the user's account has been warned. Handled by the moderation team, no effect except showing an orange square.
    pub warned_until: Option<chrono::NaiveDateTime>,

    /// Sanitized HTML snippet to use as the initial value of the Foreword field when this user adds a new story/chapter
    pub default_foreword: SanitizedHtml,

    /// Sanitized HTML snippet to use as the initial value of the Afterword field when this user adds a new story/chapter
    pub default_afterword: SanitizedHtml,

    /// This describes the tag highlight rules, as configured by the user.
    ///
    /// This deserializes as a [Vec<TagHighlightRule>].
    /// See either the [User::get_tag_highlight_rules] method or the [TagHighlightRule] class for more details.
    pub tag_highlight_rules: Option<serde_json::Value>,
}

/// Wraps a moderator ID inside a simple struct, used for moderation actions
#[derive(Clone)]
pub struct ModeratorUserWrapper {
    id: uuid::Uuid,
}

impl ModeratorUserWrapper {
    /// Creates a moderator wrapper from user information, or returns an error if the user does not have enough rights
    pub fn from_user(user: &User) -> Result<Self, crate::models::error::IntertextualError> {
        if user.administrator {
            Ok(ModeratorUserWrapper { id: user.id })
        } else {
            Err(IntertextualError::AccessRightsRequired)
        }
    }

    /// Gets the ID of the wrapped user
    pub fn id(&self) -> uuid::Uuid {
        self.id
    }
}

impl From<&User> for ShortUserEntry {
    fn from(author: &User) -> ShortUserEntry {
        ShortUserEntry {
            username: author.username.clone(),
            display_name: author.display_name.clone(),
        }
    }
}

impl UserSettings {
    /// Serializes the user settings
    pub fn to_serializable(&self) -> Result<serde_json::Value, serde_json::Error> {
        serde_json::to_value(UserSettingsSerialized {
            notification_settings: Some(self.notification_settings.clone()),
            preferred_comments_enabled: Some(self.preferred_comments_enabled),
            preferred_recommendations_enabled: Some(self.preferred_recommendations_enabled),
            preferred_publication_mode: Some(self.preferred_publication_mode),
            preferred_publication_day: self.preferred_publication_day,
            preferred_publication_hour: Some(self.preferred_publication_hour),
            featured_story_ids: self.featured_story_ids.clone(),
            card_display_mode: self.card_display_mode.clone(),
        })
    }

    /// Gets the next publication date from the user settings.
    pub fn get_next_publication_date(
        &self,
        current_time: chrono::NaiveDateTime,
    ) -> chrono::NaiveDateTime {
        use chrono::{Datelike, Timelike};
        // Get the next publication date, hour-wise
        let mut publication_date = {
            if current_time.hour() > self.preferred_publication_hour {
                current_time.date().succ()
            } else {
                current_time.date()
            }
        };

        // Go to the next requested weekday, if needed.
        if let Some(weekday) = self.preferred_publication_day {
            while publication_date.weekday() != weekday {
                publication_date = publication_date.succ();
            }
        }

        publication_date.and_hms(self.preferred_publication_hour, 0, 0)
    }
}

const DEFAULT_COMMENTS_ENABLED: bool = false;
const DEFAULT_RECOMMENDATIONS_ENABLED: RecommendationsState = RecommendationsState::Disabled;
const DEFAULT_PUBLICATION_MODE: PreferredPublicationMode = PreferredPublicationMode::DoNotPublish;
const DEFAULT_PUBLICATION_DATE: Option<chrono::Weekday> = Some(chrono::Weekday::Sat);
const DEFAULT_PUBLICATION_HOUR: u32 = 16;

impl User {
    /// Get the parsed user settings from this user's account
    pub fn get_settings(&self) -> UserSettings {
        let settings: UserSettingsSerialized = self
            .settings
            .as_ref()
            .and_then(|s| serde_json::from_value(s.clone()).ok())
            .unwrap_or(UserSettingsSerialized {
                notification_settings: Some(HashMap::new()),
                preferred_comments_enabled: Some(DEFAULT_COMMENTS_ENABLED),
                preferred_recommendations_enabled: Some(DEFAULT_RECOMMENDATIONS_ENABLED),
                preferred_publication_mode: Some(DEFAULT_PUBLICATION_MODE),
                preferred_publication_day: DEFAULT_PUBLICATION_DATE,
                preferred_publication_hour: Some(DEFAULT_PUBLICATION_HOUR),
                featured_story_ids: vec![],
                card_display_mode: None,
            });
        UserSettings {
            notification_settings: settings.notification_settings.unwrap_or_else(HashMap::new),
            preferred_comments_enabled: settings
                .preferred_comments_enabled
                .unwrap_or(DEFAULT_COMMENTS_ENABLED),
            preferred_recommendations_enabled: settings
                .preferred_recommendations_enabled
                .unwrap_or(DEFAULT_RECOMMENDATIONS_ENABLED),
            preferred_publication_mode: settings
                .preferred_publication_mode
                .unwrap_or(DEFAULT_PUBLICATION_MODE),
            preferred_publication_day: settings.preferred_publication_day,
            preferred_publication_hour: settings
                .preferred_publication_hour
                .unwrap_or(DEFAULT_PUBLICATION_HOUR),
            featured_story_ids: settings.featured_story_ids,
            card_display_mode: settings.card_display_mode,
        }
    }

    /// Get the tag highlight rules configured by this user.
    ///
    /// If this value is None, then the user didn't define any highlight rules. In this
    /// case, the website default rules will be applied.
    pub fn get_tag_highlight_rules(&self) -> Option<Vec<TagHighlightRule>> {
        let tag_highlight_rules: Option<Vec<TagHighlightRule>> = self
            .tag_highlight_rules
            .as_ref()
            .and_then(|s| serde_json::from_value(s.clone()).ok());
        tag_highlight_rules
    }

    /// Whether the current notification category should be shown in red
    pub fn should_show_red_notify_for(&self, category: NotificationCategory) -> bool {
        let settings = self.get_settings();
        let level = settings.notification_settings.get(&category);
        let level = level.copied();
        let level = level.unwrap_or_else(|| User::default_notification_level(category));
        level == NotificationLevel::RedNumber
    }

    /// Whether the current notification category should be shown in grey
    pub fn should_show_event_for(&self, category: NotificationCategory) -> bool {
        let settings = self.get_settings();
        let level = settings.notification_settings.get(&category);
        let level = level.copied();
        let level = level.unwrap_or_else(|| User::default_notification_level(category));
        level == NotificationLevel::GreyDot
    }

    /// Check whether this user has administrator access, and return an error
    pub fn require_administrator_level(&self) -> Result<(), IntertextualError> {
        match self.administrator {
            true => Ok(()),
            false => Err(IntertextualError::AccessRightsRequiredObfuscated),
        }
    }

    /// Check whether this user can report the given chapter, returns an error if not
    pub fn require_report_chapter_rights_for(
        &self,
        authors: &[User],
        story: &Story,
        chapter: &crate::models::stories::Chapter,
    ) -> Result<(), IntertextualError> {
        if authors.iter().any(|a| a.id == self.id) {
            return Err(IntertextualError::AccessRightsRequired);
        }

        let _ = story;
        let _ = chapter;
        Ok(())
    }

    /// Check whether this user can see the statistics for the given user, returns an error if not
    pub fn require_see_statistics_rights_for(
        &self,
        users: &[User],
    ) -> Result<(), IntertextualError> {
        if self.administrator {
            return Ok(());
        }

        if users.iter().any(|a| a.id == self.id) {
            return Ok(());
        }

        Err(IntertextualError::AccessRightsRequired)
    }

    /// Check whether this user can edit the target user's settings, returns an error if not
    pub fn require_edit_user_settings_for(&self, user: &Self) -> Result<(), IntertextualError> {
        if self.administrator {
            return Ok(());
        }

        if self.id == user.id {
            return Ok(());
        }

        Err(IntertextualError::AccessRightsRequired)
    }

    /// Check whether this user can edit the given recommendation, returns an error if not
    pub fn require_recommendation_edit_rights_for(
        &self,
        recommender: &Self,
    ) -> Result<(), IntertextualError> {
        if self.administrator {
            return Ok(());
        }

        if self.id == recommender.id {
            return Ok(());
        }

        Err(IntertextualError::AccessRightsRequired)
    }

    /// Check whether this user can see the recommendations for the given story, returns an error if not
    pub fn require_recommendation_see_from_story_rights_for(
        &self,
        authors: &[User],
        story: &Story,
    ) -> Result<(), IntertextualError> {
        if self.administrator {
            // Administrators can always see this
            return Ok(());
        }

        if authors.iter().any(|a| a.id == self.id) {
            // Authors can always see this.
            return Ok(());
        }

        if !RecommendationsState::Enabled.matches(&story.recommendations_enabled) {
            return Ok(());
        }

        // Recommendations cannot be seen directly from the story page
        Err(IntertextualError::AccessRightsRequiredObfuscated)
    }

    /// Check whether this user can write a recommendations for the given story, returns an error if not
    pub fn require_recommendation_write_rights_for(
        &self,
        authors: &[User],
        story: &Story,
    ) -> Result<(), IntertextualError> {
        if self.is_muted() {
            return Err(IntertextualError::AccessRightsRequired);
        }

        if authors.iter().any(|a| a.id == self.id) {
            // You cannot write recommendations for your own stories.
            return Err(IntertextualError::AccessRightsRequired);
        }

        if RecommendationsState::Disabled.matches(&story.recommendations_enabled) {
            // Recommendations are disabled for this story
            return Err(IntertextualError::AccessRightsRequired);
        }

        Ok(())
    }

    /// Check whether this user can give an approval for the given story, returns an error if not
    pub fn require_approval_rights_for(
        &self,
        authors: &[User],
        story: &Story,
    ) -> Result<(), IntertextualError> {
        if authors.iter().any(|a| a.id == self.id) {
            // You cannot give approvals to your own story
            return Err(IntertextualError::AccessRightsRequired);
        }

        let _ = story;
        Ok(())
    }

    /// Check whether this user can report the given recommendation, returns an error if not
    pub fn require_report_recommendation_rights_for(
        &self,
        recommender: &User,
        authors: &[User],
        story: &Story,
    ) -> Result<(), IntertextualError> {
        if recommender.id == self.id {
            return Err(IntertextualError::AccessRightsRequired);
        }

        let _ = authors;
        let _ = story;
        Ok(())
    }

    /// Check whether this user can see comments for the given story, returns an error if not
    pub fn require_see_comments_for(
        &self,
        authors: &[User],
        story: &Story,
    ) -> Result<(), IntertextualError> {
        if self.administrator {
            // Administrators can always see comments
            return Ok(());
        }

        if authors.iter().any(|a| a.id == self.id) {
            // The story authors can always see comments
            return Ok(());
        }

        if story.comments_enabled {
            // If the comments are enabled, then everyone can see comments
            return Ok(());
        }

        Err(IntertextualError::AccessRightsRequiredObfuscated)
    }

    /// Check whether this user can edit the given comment for the given story, returns an error if not
    pub fn require_edit_comment_rights_for(
        &self,
        authors: &[User],
        story: &Story,
        commenter: &User,
    ) -> Result<(), IntertextualError> {
        if self.id == commenter.id {
            // The comment author can edit its own comment
            return Ok(());
        }

        let _ = authors;
        let _ = story;
        Err(IntertextualError::AccessRightsRequired)
    }

    /// Check whether this user can report the given comment for the given story, returns an error if not
    pub fn require_report_comment_rights_for(
        &self,
        authors: &[User],
        story: &Story,
        commenter: &User,
    ) -> Result<(), IntertextualError> {
        if self.id == commenter.id {
            // The comment author cannot report its own comment
            return Err(IntertextualError::AccessRightsRequired);
        }

        let _ = authors;
        let _ = story;
        Ok(())
    }

    /// Check whether this user can delete the given comment for the given story, returns an error if not
    pub fn require_delete_comment_rights_for(
        &self,
        authors: &[User],
        story: &Story,
        commenter: &User,
    ) -> Result<(), IntertextualError> {
        if authors.iter().any(|a| a.id == self.id) {
            // The story author can clean up comments
            return Ok(());
        }

        if self.id == commenter.id {
            // The comment author can delete its own comment
            return Ok(());
        }

        let _ = story;
        Err(IntertextualError::AccessRightsRequired)
    }

    /// Check whether this user can write a recommendation for the given story
    pub fn can_write_recommendation_for(
        &self,
        authors: &[User],
        story: &Story,
        recommendations: &[(User, crate::models::recommendations::Recommendation)],
    ) -> bool {
        if self.is_muted() {
            return false;
        }

        if authors.iter().any(|a| a.id == self.id) {
            // The story authors cannot write a recommendation for their story
            return false;
        }

        if RecommendationsState::Disabled.matches(&story.recommendations_enabled) {
            // Recommendations are disabled
            return false;
        }

        // Disallow re-writing a recommendation for this user.
        for (user, _) in recommendations {
            if self.id == user.id {
                return false;
            }
        }

        true
    }

    /// Check whether this user can write a comment for the given story
    pub fn can_write_comment_for(&self, authors: &[User], story: &Story) -> bool {
        if self.is_muted() {
            return false;
        }

        if story.comments_enabled {
            return true;
        }

        let _ = authors;
        false
    }

    /// Get the default notification level for the given category
    pub fn default_notification_level(category: NotificationCategory) -> NotificationLevel {
        match category {
            NotificationCategory::Unknown => NotificationLevel::None,
            NotificationCategory::UserApproval => NotificationLevel::GreyDot,
            NotificationCategory::Recommended => NotificationLevel::GreyDot,
            NotificationCategory::Comment => NotificationLevel::RedNumber,
            NotificationCategory::UserFollowedYou => NotificationLevel::GreyDot,
            NotificationCategory::UserFollowedYourStory => NotificationLevel::GreyDot,
            NotificationCategory::UserFollowedYourRecommendations => NotificationLevel::GreyDot,
            NotificationCategory::FollowNewStory => NotificationLevel::RedNumber,
            NotificationCategory::FollowNewChapter => NotificationLevel::RedNumber,
            NotificationCategory::FollowUpdated => NotificationLevel::RedNumber,
            NotificationCategory::FollowNewRecommendation => NotificationLevel::RedNumber,
            NotificationCategory::ModmailReceived => NotificationLevel::RedNumber,
            NotificationCategory::SystemMessage => NotificationLevel::RedNumber,
        }
    }

    /// Indicates whether this user has an active warning
    pub fn is_warned(&self) -> bool {
        matches!(self.warned_until, Some(v) if v > chrono::Utc::now().naive_utc())
    }

    /// Indicates whether this user is currently muted
    pub fn is_muted(&self) -> bool {
        matches!(self.muted_until, Some(v) if v > chrono::Utc::now().naive_utc())
    }

    /// Indicates whether this user is currently banned
    pub fn is_banned(&self) -> bool {
        matches!(self.banned_until, Some(v) if v > chrono::Utc::now().naive_utc())
    }
}
