//! # Models : Stories and Chapters
//!
//! This module contains models related to stories, chapters and chapter reports,
//! as well as various utilities to handle settings and facilitate usage

use std::cmp::{Eq, PartialEq};
use std::convert::TryFrom;
use std::sync::Arc;

use chrono::NaiveDateTime;
use num_enum::{IntoPrimitive, TryFromPrimitive};
use serde::{Deserialize, Serialize};

use crate::models::error::IntertextualError;
use crate::models::filter::FilterMode;
use crate::models::formats::RichTagline;
use crate::models::formats::SanitizedHtml;
use crate::models::shared::SharedSiteData;
use crate::models::tags::TagHighlightRule;
use crate::models::users::ShortUserEntry;
use crate::models::users::User;
use crate::schema::*;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
/// Story sorting mode, to use for partial story queries
pub enum StorySortMode {
    /// Get the story list by ascending title (A first)
    TitleAsc,
    /// Get the story list by descending title (Z first)
    TitleDesc,
    /// Get the story list by internal creation date (oldest first)
    ///
    /// Note : Creation date is not a publicly available information. This option should not normally be proposed to users.
    InternalCreationAsc,
    /// Get the story list by internal creation date (newest first)
    ///
    /// Note : Creation date is not a publicly available information. This option should not normally be proposed to users.
    InternalCreationDesc,
    /// Get the story list by first publication date (oldest first)
    PublicationAsc,
    /// Get the story list by first publication date (newest first)
    PublicationDesc,
    /// Get the story list by last update date (oldest first)
    LastUpdateAsc,
    /// Get the story list by last update date (newest first)
    LastUpdateDesc,
}

/// Represents the role of a given user for a given story
///
#[derive(
    Clone, Copy, Debug, Deserialize, Serialize, Eq, PartialEq, IntoPrimitive, TryFromPrimitive,
)]
#[repr(i32)]
pub enum CreativeRole {
    /// Standard story access only when published
    ///
    /// Note: The "none" role should not appear in the database
    None = 0,
    /// Beta readers can access stories even when the story is unpublished
    BetaReader = 1,
    /// Read and write rights are the ability to change the content and metadata of a story.
    Editor = 2,
    /// Unconfirmed authors have full ownership rights, but are not displayed in the list
    /// of authors for a given story.
    /// Ownership rights are the right to change the story publication time, manage who can
    /// access the story, manage co-authors, and see statistics for the story.
    UnconfirmedAuthor = 3,
    /// Authors have full ownership rights, and are displayed in the list of authors for a
    /// given story.
    /// Ownership rights are the right to change the story publication time, manage who can
    /// access the story, manage co-authors, and see statistics for the story.
    Author = 4,
}

impl CreativeRole {
    pub fn min_unpublished_access() -> i32 {
        i32::from(CreativeRole::BetaReader)
    }

    pub fn author() -> i32 {
        i32::from(CreativeRole::Author)
    }

    /// Indicates whether the user can edit the story
    pub fn can_edit(&self) -> bool {
        match self {
            CreativeRole::Author => true,
            CreativeRole::UnconfirmedAuthor => true,
            CreativeRole::Editor => true,
            CreativeRole::BetaReader => false,
            CreativeRole::None => false,
        }
    }

    /// Indicates whether the user can manage the story
    pub fn can_manage(&self) -> bool {
        match self {
            CreativeRole::Author => true,
            CreativeRole::UnconfirmedAuthor => true,
            CreativeRole::Editor => false,
            CreativeRole::BetaReader => false,
            CreativeRole::None => false,
        }
    }
}

/// Represents whether recommendations can be used for this story
#[derive(
    Clone, Copy, Debug, Deserialize, Serialize, Eq, PartialEq, IntoPrimitive, TryFromPrimitive,
)]
#[repr(i32)]
pub enum RecommendationsState {
    /// Recommendations cannot be used
    Disabled = 0,
    /// Recommendations can be written, but the story page won't allow accessing them
    UserOnly = 1,
    /// Recommendations can be written and accessed from the story page
    Enabled = 2,
}

impl std::str::FromStr for RecommendationsState {
    type Err = IntertextualError;

    fn from_str(other: &str) -> Result<RecommendationsState, IntertextualError> {
        Ok(match other {
            "enable" => RecommendationsState::Enabled,
            "users_only" => RecommendationsState::UserOnly,
            _ => RecommendationsState::Disabled,
        })
    }
}

impl RecommendationsState {
    /// Checks whether the given value matches the given recommendation state
    pub fn matches(&self, value: &i32) -> bool {
        let state =
            RecommendationsState::try_from(*value).unwrap_or(RecommendationsState::Disabled);
        self == &state
    }
}

/// Represents a story already inside the database
#[derive(Debug, Identifiable, Queryable, PartialEq, Eq, Clone)]
#[table_name = "stories"]
pub struct Story {
    /// Internal identifier of the story. Must not be shown to the user.
    pub id: uuid::Uuid,

    /// This is only used for internal bookkeeping. Timestamp indicating when the story was internally created by the author.
    ///
    /// Note : All timestamps stored in the database are UTC
    pub story_internally_created_at: chrono::NaiveDateTime,

    /// Title of the story.
    pub title: String,

    /// URL of the story.
    ///
    /// This should be either unique for each user, or unique for all collaborations, depending of the value of [Story::is_collaboration]
    pub url_fragment: String,

    /// Whether this story is from a single author or from a collaboration.
    ///
    /// Note : Stories with this flag set to false MUST have a single associated author, otherwise the behaviour is undefined.
    pub is_collaboration: bool,

    /// Description of this story, stored as formatted in Markdown format.
    pub description: RichTagline,

    /// Whether recommendations for this story are enabled. Must be casted to the [RecommendationsState] type to be used.
    pub recommendations_enabled: i32,

    /// Whether comments for this story are enabled.
    pub comments_enabled: bool,

    /// Whether to show the "chapter index" screen even there is a single chapter for this story or not
    pub is_multi_chapter: bool,

    /// Cached timestamp indicating the first publication date of the story. This might be out of sync with the actual value.
    ///
    /// Note : All timestamps stored in the database are UTC
    pub cached_first_publication_date: NaiveDateTime,

    /// Cached timestamp indicating the last edition date of the story. This might be out of sync with the actual value.
    ///
    /// Note : All timestamps stored in the database are UTC
    pub cached_last_update_date: NaiveDateTime,
}

impl Story {
    /// Get the path of the current story based on the list of authors for this story
    pub fn author_path(&self, authors: &[crate::models::users::User]) -> String {
        match authors.first() {
            Some(author) if !self.is_collaboration => format!("@{}", author.username),
            _ => "collaboration".to_string(),
        }
    }
}

/// Represents a chapter already inside the database
#[derive(Debug, Identifiable, Queryable, Associations, PartialEq, Eq)]
#[belongs_to(Story, foreign_key = "story_id")]
#[table_name = "chapters"]
pub struct Chapter {
    /// Internal identifier of the chapter. Must not be shown to the user.
    pub id: uuid::Uuid,

    /// This is only used for internal bookkeeping. Timestamp indicating when the story was internally created by the author.
    ///
    /// Note : All timestamps stored in the database are UTC
    pub chapter_internally_created_at: chrono::NaiveDateTime,

    /// Identifier of the story containing this chapter.
    pub story_id: uuid::Uuid,

    /// Chapter number, used to find chapters 1, 2, etc...
    ///
    /// Note : the chapters of a story must be maintained at creation, deletion, or reordering time in:
    ///
    /// - ascending order
    /// - starting from 1
    /// - with no gaps inbetween.
    ///
    /// Some algorithms may rely on these rules to function properly.
    pub chapter_number: i32,

    /// Optional custom title of the chapter.
    pub title: Option<String>,

    /// Content of the chapter, formatted as trusted HTML.
    pub content: SanitizedHtml,

    /// Number of words in this chapter. Computed every time the [Chapter::content] field is updated.
    pub word_count: i32,

    /// Timestamp indicating the official creation date of the story. This can differ from the
    /// [Chapter::chapter_internally_created_at] date if the chapter was published at a later date.
    ///
    /// Note : All timestamps stored in the database are UTC
    pub official_creation_date: Option<NaiveDateTime>,

    /// Timestamp indicating the last time that this story was edited.
    /// This should only be set if the story was shown publicly when the edition happened.
    ///
    /// Note : All timestamps stored in the database are UTC
    pub last_edition_date: Option<NaiveDateTime>,

    /// Timestamp indicating whether the story should be shown publicly.
    ///
    /// Note : this is not the offical publication date, see [Chapter::official_creation_date] for this.
    ///
    /// If this timestamp is set to [None], then the sotry MUST NOT be shown to a user other than authors or moderators.
    /// If this timestamp is set to a date in the future, then the story MUST NOT be shown to a user other than authors or moderators.
    ///
    /// Note : All timestamps stored in the database are UTC
    pub show_publicly_after_date: Option<NaiveDateTime>,

    /// Whether this chapter was locked by a moderator. Locked stories should only be shown to the authros or moderators.
    pub moderator_locked: bool,

    /// The reason that this chapter was locked by a moderator.
    pub moderator_locked_reason: Option<String>,

    /// Author's foreword for the chapter, as sanitized HTML
    pub foreword: SanitizedHtml,

    /// Author's afterword for the chapter, as sanitized HTML
    pub afterword: SanitizedHtml,
}

/// Represents a simplified chapter, used for metadata without needing to query the chapter contents as well
#[derive(Debug, Identifiable, Queryable, Associations, PartialEq, Eq)]
#[belongs_to(Story, foreign_key = "story_id")]
#[table_name = "chapters"]
pub struct ChapterMetadata {
    /// See [Chapter::id] for details
    pub id: uuid::Uuid,

    /// See [Chapter::chapter_internally_created_at] for details
    pub chapter_internally_created_at: chrono::NaiveDateTime,

    /// See [Chapter::story_id] for details
    pub story_id: uuid::Uuid,

    /// See [Chapter::chapter_number] for details
    pub chapter_number: i32,

    /// See [Chapter::title] for details
    pub title: Option<String>,

    /// See [Chapter::word_count] for details
    pub word_count: i32,

    /// See [Chapter::official_creation_date] for details
    pub official_creation_date: Option<NaiveDateTime>,

    /// See [Chapter::last_edition_date] for details
    pub last_edition_date: Option<NaiveDateTime>,

    /// See [Chapter::show_publicly_after_date] for details
    pub show_publicly_after_date: Option<NaiveDateTime>,

    /// See [Chapter::moderator_locked] for details
    pub moderator_locked: bool,
}

impl ChapterMetadata {
    /// Get the title of this chapter
    pub fn get_title(&self, story: &Story, is_standalone: &bool) -> String {
        if *is_standalone {
            return String::from(&story.title);
        }
        match &self.title {
            Some(title) => format!("{} - {}", title, story.title),
            None => format!("Chapter {} - {}", self.chapter_number, story.title),
        }
    }
}

impl Chapter {
    /// Get the title of this chapter
    pub fn get_title(&self, story: &Story, is_standalone: &bool) -> String {
        if *is_standalone {
            return String::from(&story.title);
        }
        match &self.title {
            Some(title) => format!("{} - {}", title, story.title),
            None => format!("Chapter {} - {}", self.chapter_number, story.title),
        }
    }

    /// Gets whether this chapter is published
    pub fn is_published(&self) -> bool {
        match self.show_publicly_after_date {
            None => false,
            Some(time) => time <= chrono::Utc::now().naive_utc(),
        }
    }

    /// Gets this chapter's publication date (for author and admininstration UI)
    pub fn formatted_publication_date(&self) -> String {
        self.show_publicly_after_date
            .map(|d| d.format("%Y-%m-%d %H:%M (UTC+00)").to_string())
            .unwrap_or_else(|| "No planned publication date".to_string())
    }

    /// Gets this chapter's first publication date (for user-facing UI)
    pub fn formatted_first_publication(&self) -> String {
        self.official_creation_date
            .map(|d| d.format("%Y-%m-%d").to_string())
            .unwrap_or_else(|| "[no first publication date]".to_string())
    }

    /// Gets this chapter's first publication date (for metadata tags)
    pub fn w3cdtf_formatted_first_publication(&self) -> String {
        self.official_creation_date
            .map(|d| d.format("%Y-%m-%d").to_string())
            .unwrap_or_else(String::new)
    }

    /// If this chapter has been locked, get the reason. Otherwise, returns an empty string
    pub fn moderator_locked_reason(&self) -> String {
        match &self.moderator_locked_reason {
            Some(reason) => String::from(reason),
            None => String::new(),
        }
    }
}

/// Represents a new story from a given author, before insertion in the database
pub struct NewSingleAuthorStory {
    /// The ID of the author who wants to publish this story
    pub author_id: uuid::Uuid,
    /// The URL of this story
    pub url_fragment: String,
    /// The title of this story
    pub title: String,
    /// The description of this story, formatted as Markdown
    pub description: RichTagline,
    /// Author's foreword
    pub foreword: SanitizedHtml,
    /// The contents of this story
    pub content: SanitizedHtml,
    /// The number of words in this story
    pub word_count: i32,
    /// Author's afterword
    pub afterword: SanitizedHtml,
    /// Whether recommendations can be written or shown for this story (see [`RecommendationsState`])
    pub recommendations_enabled: RecommendationsState,
    /// Whether comments can be written for this story
    pub comments_enabled: bool,
    /// Whether to show the "chapter index" screen even there is a single chapter for this story or not
    pub is_multi_chapter: bool,
}

/// Represents a new story from several authors, before insertion in the database
pub struct NewCollaborationStory {
    /// The ID of the authors who wants to publish this story
    pub author_id_list: Vec<uuid::Uuid>,
    /// The URL of this story
    pub url_fragment: String,
    /// The title of this story
    pub title: String,
    /// The description of this story, formatted as Markdown
    pub description: RichTagline,
    /// Authors' foreword
    pub foreword: SanitizedHtml,
    /// The contents of this story
    pub content: SanitizedHtml,
    /// The number of words in this story
    pub word_count: i32,
    /// Authors' afterword
    pub afterword: SanitizedHtml,
    /// Whether recommendations can be written or shown for this story (see [`RecommendationsState`])
    pub recommendations_enabled: RecommendationsState,
    /// Whether comments can be written for this story
    pub comments_enabled: bool,
    /// Whether to show the "chapter index" screen even there is a single chapter for this story or not
    pub is_multi_chapter: bool,
}

/// Represents a new chapter for an existing story, before insertion in the database
pub struct NewChapter {
    /// The ID of the story to add the chapter to
    pub story_id: uuid::Uuid,
    /// The title of the chapter, or [`None`] to use the default title
    pub title: Option<String>,
    /// Author's foreword
    pub foreword: SanitizedHtml,
    /// The contents of the chapter
    pub content: SanitizedHtml,
    /// The number of words in the chapter
    pub word_count: i32,
    /// Author's afterword
    pub afterword: SanitizedHtml,
}

/// Represents the result of a search operation
#[derive(std::cmp::Eq)]
pub struct StorySearchResult {
    /// The UUID of the matched story
    pub story_id: uuid::Uuid,
    /// The number of tags found matching the request
    pub found_tags: usize,
    /// The number of tags in the request
    pub requested_tags: usize,
}

impl std::cmp::Ord for StorySearchResult {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.found_tags.cmp(&other.found_tags)
    }
}

impl std::cmp::PartialOrd for StorySearchResult {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl std::cmp::PartialEq for StorySearchResult {
    fn eq(&self, other: &Self) -> bool {
        self.found_tags == other.found_tags
    }
}

/// Represents a report on a given chapter
#[derive(Debug, Queryable, Associations, PartialEq, Eq)]
#[belongs_to(Chapter, foreign_key = "chapter_id")]
#[belongs_to(User, foreign_key = "reporter_id")]
#[table_name = "chapter_reports"]
pub struct ChapterReport {
    /// The date of the report
    pub report_date: NaiveDateTime,
    /// The ID of the reported chapter
    pub chapter_id: uuid::Uuid,
    /// The user who reported this recommendation
    pub reporter_id: uuid::Uuid,
    /// Whether this report was handled by a moderator, and the ID of the moderator who handled it if relevant
    pub handled_by_moderator_id: Option<uuid::Uuid>,
    /// The reason for the report
    pub reason: String,
}

/// Represents the story information used to summarize an existing story
pub struct StoryEntry {
    /// The title of the story
    pub title: String,
    /// The URL fragment for this story
    pub url_fragment: String,
    /// The authors for this story
    pub authors: Vec<ShortUserEntry>,
    /// These tags have been excluded by the user in their settings.
    ///
    /// Note: Excluded authors still appear in the [`StoryEntry::authors`] field.
    pub excluded_authors: Vec<ShortUserEntry>,
    /// The start of the story url : either an `@author` username if the story is not a collaboration, or the string `collaboration` if it is.
    pub author_path: String,
    /// The short description of the story
    pub description: RichTagline,
    /// The number of chapters in the story
    pub chapter_count: usize,
    /// The first publication date of this story
    pub first_publication: chrono::NaiveDateTime,
    /// The last update date of this story
    pub last_update: chrono::NaiveDateTime,
    /// Whether the author is visible when logged out
    pub is_author_not_visible: bool,
    /// Whether the story is visible when logged out
    pub has_published_chapters: bool,
    /// Whether the story has any locked chapters
    pub has_locked_chapters: bool,
    /// Whether this story has draft chapters (under the current filtering mode)
    pub has_draft_chapters: bool,
    /// Whether this story has planified (but unpublished) chapters (under the current filtering mode)
    pub has_planified_chapters: bool,
    /// Whether the current user can mark this story for later
    pub can_mark_for_later: bool,
    /// Whether the story is marked for later by the current user
    pub is_marked_for_later: bool,
    /// The number of words in this story
    pub word_count: i32,
    /// The major tags for this story
    pub major_tags: Vec<(
        crate::models::tags::TagCategory,
        crate::models::tags::CanonicalTag,
    )>,
    /// The minor tags for this story
    pub minor_tags: Vec<(
        crate::models::tags::TagCategory,
        crate::models::tags::CanonicalTag,
    )>,
    /// The spoiler tags for this story
    pub spoiler_tags: Vec<(
        crate::models::tags::TagCategory,
        crate::models::tags::CanonicalTag,
    )>,
    /// The list of spoilered mandatory tags for this story
    ///
    /// Note: Spoilered mandatory tags still appear in other categories.
    pub spoilered_mandatory_tags: Vec<(
        crate::models::tags::TagCategory,
        crate::models::tags::CanonicalTag,
    )>,
    /// These tags have been excluded by the user in their settings.
    ///
    /// Note: Excluded tags still appear in other categories.
    pub excluded_tags: Vec<(
        crate::models::tags::TagCategory,
        crate::models::tags::CanonicalTag,
    )>,
    /// If set, the story is associated to a color that will be shown
    /// on the story card.
    pub story_color: Option<String>,
    /// If set, the story is associated to a symbol that will be shown
    /// on the story card.
    pub story_symbol: Option<String>,
}

/// A collection of information needed for extra story queries.
#[derive(Clone, Debug)]
pub struct StoryQueryExtraInfos {
    pub maybe_user_id: Option<uuid::Uuid>,
    pub filter_mode: FilterMode,
    pub tag_highlight_rules: Arc<Vec<TagHighlightRule>>,
}

impl StoryQueryExtraInfos {
    /// Gets the information collection when the user is not always logged in.
    ///
    /// Note: Calling this method can require some processing time. As such, it is
    /// often recommended to call it once and clone the result as needed.
    pub fn from_maybe_user(
        data: &SharedSiteData,
        maybe_user: &Option<&User>,
    ) -> StoryQueryExtraInfos {
        StoryQueryExtraInfos {
            maybe_user_id: maybe_user.map(|u| u.id),
            filter_mode: FilterMode::from_login_opt(maybe_user),
            tag_highlight_rules: match maybe_user {
                Some(user) => StoryQueryExtraInfos::get_tag_highlight_rules(data, user),
                None => data.default_tag_highlight_rules.clone(),
            },
        }
    }

    /// Gets the information collection from a logged-in user
    ///
    /// Note: Calling this method can require some processing time. As such, it is
    /// often recommended to call it once and clone the result as needed.
    pub fn from_user(data: &SharedSiteData, user: &User) -> StoryQueryExtraInfos {
        StoryQueryExtraInfos {
            maybe_user_id: Some(user.id),
            filter_mode: FilterMode::from_login(user),
            tag_highlight_rules: StoryQueryExtraInfos::get_tag_highlight_rules(data, user),
        }
    }

    /// Used to get the tag highlight rules for a given user, or the default website
    /// rules if the user didn't configure any.
    fn get_tag_highlight_rules(data: &SharedSiteData, user: &User) -> Arc<Vec<TagHighlightRule>> {
        user.get_tag_highlight_rules()
            .map(Arc::new)
            .unwrap_or_else(|| data.default_tag_highlight_rules.clone())
    }
}

impl StoryEntry {
    /// Creates a story entry from a story, filter mode, and a database connection.
    pub fn from_database(
        story: Story,
        user_info: &StoryQueryExtraInfos,
        conn: &diesel::PgConnection,
    ) -> Result<Self, IntertextualError> {
        let maybe_user_id = user_info.maybe_user_id;
        let filter_mode = user_info.filter_mode.clone();
        let authors = crate::actions::stories::find_authors_by_story(story.id, &filter_mode, conn)?;
        let chapters = crate::actions::stories::find_chapters_metadata_by_story_id(
            story.id,
            &filter_mode,
            conn,
        )?;
        let major_tags =
            crate::actions::tags::find_major_canonical_tags_by_story_id(story.id, conn)?;
        let minor_tags =
            crate::actions::tags::find_minor_canonical_tags_by_story_id(story.id, conn)?;
        let spoiler_tags =
            crate::actions::tags::find_spoiler_canonical_tags_by_story_id(story.id, conn)?;
        let (can_mark_for_later, is_marked_for_later) = match maybe_user_id {
            None => (false, false),
            Some(user_id) if authors.iter().any(|a| a.id == user_id) => (false, false),
            Some(user_id) => (
                true,
                crate::actions::marked_for_later::has_marked_for_later(user_id, story.id, conn)?,
            ),
        };
        let (excluded_tag_ids, excluded_author_ids) = match maybe_user_id {
            Some(user_id) => (
                crate::actions::denylist::find_excluded_canonical_tag_ids_for_user(user_id, conn)?,
                crate::actions::denylist::find_excluded_author_ids_for_user(user_id, conn)?,
            ),
            None => (vec![], vec![]),
        };
        let excluded_tags = if excluded_tag_ids.is_empty() {
            // The excluded tags list is empty, so we can short-circuit this check.
            vec![]
        } else {
            major_tags
                .iter()
                .chain(minor_tags.iter())
                .chain(spoiler_tags.iter())
                .filter(|(_, tag)| excluded_tag_ids.contains(&tag.id))
                .cloned()
                .collect()
        };
        let spoilered_mandatory_tags = spoiler_tags
            .iter()
            .filter(|(c, _)| c.is_always_displayed)
            .cloned()
            .collect();
        let author_path = story.author_path(&authors);
        let now = chrono::Utc::now().naive_utc();
        let at_least_one_author_is_globally_visible = authors
            .iter()
            .any(|a| !a.is_banned() && !a.deactivated_by_user);
        let has_locked_chapters = chapters.iter().any(|c| c.moderator_locked);
        let has_published_chapters = chapters
            .iter()
            .any(|c| c.show_publicly_after_date.filter(|p| p <= &now).is_some());
        let has_draft_chapters = chapters
            .iter()
            .any(|c| c.show_publicly_after_date.is_none());
        let has_planified_chapters = chapters
            .iter()
            .any(|c| c.show_publicly_after_date.filter(|p| p > &now).is_some());
        let first_publication = chapters
            .iter()
            .filter_map(|c| c.official_creation_date)
            .min();
        let last_update = chapters
            .iter()
            .filter_map(|c| c.last_edition_date.or(c.official_creation_date))
            .max();
        let excluded_authors: Vec<ShortUserEntry> = authors
            .iter()
            .filter(|a| excluded_author_ids.contains(&a.id))
            .map(ShortUserEntry::from)
            .collect();
        let authors = authors.iter().map(ShortUserEntry::from).collect();
        let first_publication = first_publication.unwrap_or(now);
        let last_update = last_update.unwrap_or(now);

        let tag_names: Vec<&str> = major_tags
            .iter()
            .chain(minor_tags.iter())
            .chain(spoiler_tags.iter())
            .map(|(_, t)| t.display_name.as_ref())
            .collect();
        let matching_tag_highlight = user_info
            .tag_highlight_rules
            .iter()
            .find(|m| m.matches(&tag_names));

        let story_color = matching_tag_highlight
            .and_then(|t| t.story_color.as_ref())
            .map(|c| c.to_color());
        let story_symbol = matching_tag_highlight
            .and_then(|t| t.story_symbol.as_ref())
            .cloned();

        Ok(Self {
            title: story.title,
            url_fragment: story.url_fragment,
            authors,
            excluded_authors,
            author_path,
            description: story.description,
            chapter_count: chapters.len(),
            word_count: chapters.iter().map(|c| c.word_count).sum(),
            first_publication,
            last_update,
            is_author_not_visible: !at_least_one_author_is_globally_visible,
            has_locked_chapters,
            has_published_chapters,
            has_draft_chapters,
            has_planified_chapters,
            can_mark_for_later,
            is_marked_for_later,
            major_tags,
            minor_tags,
            spoiler_tags,
            spoilered_mandatory_tags,
            excluded_tags,
            story_color,
            story_symbol,
        })
    }

    /// Get the list of mandatery categories with spoilered tag as a comma-separated text
    pub fn get_comma_separated_mandatory_category_names(&self) -> String {
        self.spoilered_mandatory_tags
            .iter()
            .map(|(c, _)| c.display_name.clone())
            .collect::<std::collections::HashSet<String>>()
            .into_iter()
            .collect::<Vec<String>>()
            .join(", ")
    }

    /// Get the extra classes to append to the story card.
    pub fn get_card_extra_classes(&self) -> &'static str {
        if self.is_author_not_visible {
            return " story-card-hidden-story";
        }
        if !self.has_published_chapters && !self.has_planified_chapters {
            return " story-card-hidden-story";
        }
        if self.has_locked_chapters {
            return " story-card-warning-story";
        }
        if self.has_planified_chapters {
            return " story-card-warning-story";
        }
        if self.has_draft_chapters {
            return " story-card-warning-story";
        }

        ""
    }

    /// Get the message to show in the top left corner, or an empty string if none is required
    pub fn get_card_corner_message(&self) -> &'static str {
        if self.is_author_not_visible && self.authors.len() > 1 {
            return "All accounts are deactivated/banned";
        }
        if self.is_author_not_visible {
            return "Account is deactivated/banned";
        }
        if self.has_locked_chapters && self.chapter_count == 1 {
            return "This story is locked";
        }
        if self.has_locked_chapters {
            return "Chapters in this story are locked";
        }
        if !self.has_published_chapters && self.has_planified_chapters {
            return "This story is planified for a future release";
        }
        if !self.has_published_chapters && self.has_draft_chapters {
            return "This story is a draft";
        }
        if self.has_planified_chapters {
            return "Some chapters are planified for a future release";
        }
        if self.has_draft_chapters {
            return "Some chapters are drafts";
        }

        ""
    }

    /// Gets the last update of the story, formatted in standard format
    pub fn last_update_formatted(&self) -> String {
        self.last_update.format("%Y-%m-%d").to_string()
    }

    pub fn is_excluded(&self) -> bool {
        let has_excluded_tags = !self.excluded_tags.is_empty();
        let has_excluded_authors = !self.excluded_authors.is_empty();
        has_excluded_tags || has_excluded_authors
    }
}
