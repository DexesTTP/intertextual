//! # Models : Notifications
//!
//! This module contains models related to the user notification mechanism

use chrono::NaiveDateTime;
use num_enum::{IntoPrimitive, TryFromPrimitive};
use serde::{Deserialize, Serialize};
use std::{convert::TryFrom, fmt::Display};

use crate::models::users::User;
use crate::schema::*;

/// Represents the type of user notification
#[derive(
    Copy, Clone, Debug, Deserialize, Eq, Hash, PartialEq, Serialize, IntoPrimitive, TryFromPrimitive,
)]
#[repr(i32)]
pub enum NotificationCategory {
    /// This notification's type is unknown
    Unknown,
    /// This notification represents an user giving an approval to a story
    UserApproval,
    /// This notification represents an user writing a recommendation for a story
    Recommended,
    /// This notification represents an user writing a comment for a story
    Comment,
    /// This notification represents an user following another user
    UserFollowedYou,
    /// This notification represents an user following a story
    UserFollowedYourStory,
    /// This notification represents an user following another user's recommendations
    UserFollowedYourRecommendations,
    /// This notification represents a new story published by a followed author
    FollowNewStory,
    /// This notification represents a new chapter published by a followed author or in a followed story
    FollowNewChapter,
    /// This notification represents an updated chapter published by a followed author or in a followed story
    FollowUpdated,
    /// This notification represents an new recommendation published by a followed user
    FollowNewRecommendation,
    /// This notification represents a received message by the moderation team
    ModmailReceived,
    /// This notification represents a system message due to a moderation action
    SystemMessage,
}

impl NotificationCategory {
    /// Transforms a notification category into a string readable by the user (used in the settings screen)
    pub fn to_user_facing_string(&self) -> &'static str {
        match self {
            NotificationCategory::Unknown => "Unknown notification",
            NotificationCategory::UserApproval => "Your story got a snap",
            NotificationCategory::Recommended => "A user suggested your story",
            NotificationCategory::Comment => {
                "A user mentioned you in a comment or commented on your story"
            }
            NotificationCategory::UserFollowedYou => {
                "A user followed your new stories and chapters"
            }
            NotificationCategory::UserFollowedYourStory => "A user followed one of your stories",
            NotificationCategory::UserFollowedYourRecommendations => {
                "A user followed your recommendations"
            }
            NotificationCategory::FollowNewStory => "An author you follow posted a new story",
            NotificationCategory::FollowNewChapter => "A story you follow got a new chapter",
            NotificationCategory::FollowUpdated => "A story you follow got updated",
            NotificationCategory::FollowNewRecommendation => {
                "A user you follow suggested a new story"
            }
            NotificationCategory::ModmailReceived => "The moderation team sent you a message",
            NotificationCategory::SystemMessage => {
                "There was a website system operation (ban, unban, warning, etc...)"
            }
        }
    }

    /// Lists the notification categories whose settings can be changed by the user
    pub fn categories_with_settings() -> &'static [NotificationCategory] {
        &[
            NotificationCategory::UserApproval,
            NotificationCategory::Recommended,
            NotificationCategory::Comment,
            NotificationCategory::UserFollowedYou,
            NotificationCategory::UserFollowedYourStory,
            NotificationCategory::UserFollowedYourRecommendations,
            NotificationCategory::FollowNewStory,
            NotificationCategory::FollowNewChapter,
            NotificationCategory::FollowUpdated,
            NotificationCategory::FollowNewRecommendation,
        ]
    }
}

impl Display for NotificationCategory {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", i32::from(*self))
    }
}

impl std::cmp::PartialEq<i32> for NotificationCategory {
    fn eq(&self, other: &i32) -> bool {
        let value = NotificationCategory::try_from(*other).unwrap_or(NotificationCategory::Unknown);
        value == *self
    }
}

impl std::cmp::PartialEq<NotificationCategory> for i32 {
    fn eq(&self, other: &NotificationCategory) -> bool {
        let value = NotificationCategory::try_from(*self).unwrap_or(NotificationCategory::Unknown);
        value == *other
    }
}

/// Represents a notification stored in the database
#[derive(Queryable, Insertable, Associations)]
#[belongs_to(User, foreign_key = "target_user_id")]
#[table_name = "notifications"]
pub struct Notification {
    /// The ID of the notification
    pub id: uuid::Uuid,
    /// The ID of the user who should receive the notification
    pub target_user_id: uuid::Uuid,
    /// The date at which the notification has been received. If this date is in the future, then this notification is planified and should not be published immediately
    pub notification_time: NaiveDateTime,
    /// The internal description of the notification. Used for filtering, should not be shown in the UI
    pub internal_description: String,
    /// The category of the notification (see [`NotificationCategory`])
    pub category: i32,
    /// The notification message, should be shown in the UI
    pub message: String,
    /// The notification link, should be accessed when clicked on
    pub link: String,
    /// Whether the notification has been read by this user
    pub read: bool,
}

/// Represents a notification before being stored in the database
#[derive(Insertable)]
#[table_name = "notifications"]
pub struct NewNotification {
    /// The ID of the user who should receive the notification
    pub target_user_id: uuid::Uuid,
    /// The date at which the notification has been received. If this date is in the future, then this notification is planified and should not be published immediately
    pub notification_time: NaiveDateTime,
    /// The internal description of the notification. Used for filtering, should not be shown in the UI
    pub internal_description: String,
    /// The category of the notification (see [`NotificationCategory`])
    pub category: i32,
    /// The notification message, should be shown in the UI
    pub message: String,
    /// The notification link, should be accessed when clicked on
    pub link: String,
}
