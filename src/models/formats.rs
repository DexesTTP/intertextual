//! # Models : Formats
//!
//! This module contains wrapper types for various formats of rich
//! text.

use comrak::{markdown_to_html, ComrakOptions};
use diesel::{
    backend::Backend, deserialize::FromSql, pg::PgConnection, serialize::ToSql, sql_types::Text,
};
use log::{error, info};
use regex::{Captures, Regex};
use serde::{Deserialize, Serialize};

use crate::actions::users::find_user_by_username;
use crate::models::filter::FilterMode;
use std::collections::hash_set::HashSet;

/// HTML restricted to a subset of tags and attributes that we believe
/// are safe to allow in user-submitted story content
#[derive(Clone, Debug, Eq, PartialEq, AsExpression, FromSqlRow, Serialize)]
#[sql_type = "Text"]
#[serde(transparent)]
pub struct SanitizedHtml {
    inner: String,
}

impl SanitizedHtml {
    /// A new empty SanitizedHtml object
    pub fn new() -> Self {
        SanitizedHtml {
            inner: String::new(),
        }
    }

    /// Perform HTML sanitization on a string. If tag_restriction is
    /// not None, the set of allowed tags will be further restricted
    /// to the intersection of our default set of safe tags and the
    /// tags in tag_restriction.
    pub fn from_restricted(input: &str, tag_restriction: Option<&HashSet<&str>>) -> Self {
        let input = modernize_tags(input);

        let mut tags = hashset![
            "a",
            "abbr",
            "article",
            "aside",
            "b",
            "bdi",
            "bdo",
            "blockquote",
            "br",
            "cite",
            "code",
            "dd",
            "del",
            "details",
            "dfn",
            "div",
            "dl",
            "dt",
            "em",
            "h1",
            "h2",
            "h3",
            "h4",
            "h5",
            "h6",
            "hr",
            "i",
            "ins",
            "kbd",
            "li",
            "mark",
            "ol",
            "p",
            "pre",
            "q",
            "rp",
            "rt",
            "rtc",
            "ruby",
            "s",
            "samp",
            "small",
            "span",
            "strong",
            "sub",
            "summary",
            "sup",
            "u",
            "ul",
            "var",
            "wbr",
        ];

        if let Some(r) = tag_restriction {
            tags = tags.intersection(r).copied().collect();
        }

        let output = ammonia::Builder::default()
            .tags(tags)
            .tag_attributes(hashmap![
                "a" => hashset!["href"],
                "abbr" => hashset!["title"],
                "bdo" => hashset!["dir"],
                "details" => hashset!["open"],
                "ol" => hashset!["start"],
            ])
            .url_schemes(hashset!["gopher", "http", "https", "mailto"])
            .url_relative(ammonia::UrlRelative::PassThrough)
            .set_tag_attribute_value("a", "target", "_blank")
            .clean(&input)
            .to_string();

        SanitizedHtml {
            inner: output.trim().to_string(),
        }
    }

    /// Perform HTML sanitization on a string
    pub fn from(input: &str) -> Self {
        SanitizedHtml::from_restricted(input, None)
    }

    /// Apply a function to the inner HTML string and re-sanitize the result
    pub fn map<F>(self, f: F) -> Self
    where
        F: FnOnce(String) -> String,
    {
        SanitizedHtml::from(&f(self.inner))
    }

    /// The plain text content of the HTML with all tags removed
    pub fn text(&self) -> String {
        ammonia::Builder::default()
            .tags(hashset![])
            .clean(&self.inner)
            .to_string()
    }

    /// Whether the HTML is empty
    pub fn is_empty(&self) -> bool {
        self.inner.is_empty()
    }

    /// Pointer to the inner HTML string
    pub fn as_str(&self) -> &str {
        &self.inner
    }
}

impl Default for SanitizedHtml {
    fn default() -> Self {
        Self::new()
    }
}

impl<ST, DB> FromSql<ST, DB> for SanitizedHtml
where
    DB: Backend,
    String: FromSql<ST, DB>,
{
    fn from_sql(bytes: Option<&DB::RawValue>) -> diesel::deserialize::Result<Self> {
        Ok(SanitizedHtml::from(&String::from_sql(bytes)?))
    }
}

impl<DB> ToSql<Text, DB> for SanitizedHtml
where
    DB: Backend,
    String: ToSql<Text, DB>,
{
    fn to_sql<W: std::io::Write>(
        &self,
        out: &mut diesel::serialize::Output<W, DB>,
    ) -> diesel::serialize::Result {
        self.inner.to_sql(out)
    }
}

impl<'de> Deserialize<'de> for SanitizedHtml {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        Ok(SanitizedHtml::from(&String::deserialize(deserializer)?))
    }
}

fn markdown_to_restricted_html(md: &str, tag_restriction: &HashSet<&str>) -> SanitizedHtml {
    let mut options = ComrakOptions::default();
    options.extension.strikethrough = true;
    options.parse.smart = true;
    SanitizedHtml::from_restricted(&markdown_to_html(md, &options), Some(tag_restriction))
}

// Try to replace some deprecated HTML tags with modern equivalents,
// so we can keep our sanitization simpler and more
// standards-compliant / future-proof.
fn modernize_tags(input: &str) -> String {
    let rewrite = lol_html::rewrite_str(
        input,
        lol_html::RewriteStrSettings {
            element_content_handlers: vec![
                lol_html::element!("strike", |el| { Ok(el.set_tag_name("s")?) }),
                lol_html::element!("tt", |el| { Ok(el.set_tag_name("code")?) }),
            ],
            ..lol_html::RewriteStrSettings::default()
        },
    );
    match rewrite {
        Ok(output) => output,
        Err(e) => {
            error!("error rewriting html tags: {}", e);
            String::from(input)
        }
    }
}

/// Markdown, intended to be a single line with minimal rich text
/// formatting; will be treated accordingly when converting to HTML.
#[derive(Clone, Debug, Eq, PartialEq, AsExpression, FromSqlRow, Deserialize, Serialize)]
#[sql_type = "Text"]
#[serde(transparent)]
pub struct RichTagline {
    pub inner: String,
}

impl RichTagline {
    pub fn html(&self) -> SanitizedHtml {
        markdown_to_restricted_html(
            &self.inner,
            &hashset!["b", "del", "em", "i", "s", "strong", "u",],
        )
    }

    pub fn is_empty(&self) -> bool {
        self.inner.is_empty()
    }
}

impl<ST, DB> FromSql<ST, DB> for RichTagline
where
    DB: Backend,
    String: FromSql<ST, DB>,
{
    fn from_sql(bytes: Option<&DB::RawValue>) -> diesel::deserialize::Result<Self> {
        Ok(RichTagline {
            inner: String::from_sql(bytes)?,
        })
    }
}

impl<DB> ToSql<Text, DB> for RichTagline
where
    DB: Backend,
    String: ToSql<Text, DB>,
{
    fn to_sql<W: std::io::Write>(
        &self,
        out: &mut diesel::serialize::Output<W, DB>,
    ) -> diesel::serialize::Result {
        self.inner.to_sql(out)
    }
}

/// Markdown, intended to have limited hypertext formatting, but can
/// also include @mentions of site users; will be treated accordingly
/// when converting to HTML.
#[derive(Clone, Debug, Eq, PartialEq, AsExpression, FromSqlRow, Deserialize, Serialize)]
#[sql_type = "Text"]
#[serde(transparent)]
pub struct RichBlock {
    pub inner: String,
}

lazy_static! {
    static ref MENTION_RE: Regex =
        Regex::new(r#"(?P<context>^|\s|[(\[{"'*_~])@(?P<username>[a-zA-Z0-9_]{1,32})\b"#).unwrap();
}

fn replace_mention(caps: &Captures, conn: &PgConnection) -> String {
    let context = match caps.name("context") {
        Some(m) => m.as_str(),
        None => {
            error!(
                "Failed to convert @mention: capture group 'context' missing in {:?}",
                caps
            );
            return caps.get(0).unwrap().as_str().to_string();
        }
    };

    let username = match caps.name("username") {
        Some(m) => m.as_str(),
        None => {
            error!(
                "Failed to convert @mention: capture group 'username' missing in {:?}",
                caps
            );
            return caps.get(0).unwrap().as_str().to_string();
        }
    };

    let user = match find_user_by_username(username, &FilterMode::Standard, conn) {
        Ok(Some(user)) => user,
        Ok(None) => {
            info!("Failed to convert @mention of user {}: not found", username);
            return caps.get(0).unwrap().as_str().to_string();
        }
        Err(e) => {
            error!("Failed to convert @mention of user {}: {:?}", username, e);
            return caps.get(0).unwrap().as_str().to_string();
        }
    };

    std::format!("{}[@{}](/@{}/)", context, user.display_name, user.username)
}

impl RichBlock {
    /// Convert a raw string to a RichBlock format
    pub fn from_raw_text(content: &str) -> RichBlock {
        let inner = content.replace("_", "\\_");
        let inner = inner.replace("*", "\\*");
        let inner = inner.replace(">", "\\>");
        RichBlock { inner }
    }

    pub fn html(&self, conn: &PgConnection) -> SanitizedHtml {
        let with_converted_mentions =
            MENTION_RE.replace_all(&self.inner, |caps: &Captures| replace_mention(caps, conn));

        markdown_to_restricted_html(
            with_converted_mentions.as_ref(),
            &hashset![
                "a",
                "b",
                "blockquote",
                "br",
                "del",
                "em",
                "i",
                "p",
                "s",
                "strong",
                "u",
            ],
        )
    }

    /// Returns a set of usernames @mentioned in this block; does not
    /// check whether these usernames are actually present in the
    /// database.
    pub fn mentions(&self) -> HashSet<String> {
        MENTION_RE
            .captures_iter(&self.inner)
            .filter_map(|caps| caps.name("username").map(|m| m.as_str().to_owned()))
            .collect()
    }
}

impl<ST, DB> FromSql<ST, DB> for RichBlock
where
    DB: Backend,
    String: FromSql<ST, DB>,
{
    fn from_sql(bytes: Option<&DB::RawValue>) -> diesel::deserialize::Result<Self> {
        Ok(RichBlock {
            inner: String::from_sql(bytes)?,
        })
    }
}

impl<DB> ToSql<Text, DB> for RichBlock
where
    DB: Backend,
    String: ToSql<Text, DB>,
{
    fn to_sql<W: std::io::Write>(
        &self,
        out: &mut diesel::serialize::Output<W, DB>,
    ) -> diesel::serialize::Result {
        self.inner.to_sql(out)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn richblock_mentions() {
        let block = RichBlock {
            inner: "@test_1 has email address test1@gmail.com (@test2 can confirm, so can @t3st.) \
                    Has anyone seen @4test lately? also why isn't \
                    @illegally_long_username_that_shouldnt_be_a_match \
                    answering my @ mentions? I'll go ask @_TeSt_fIvE_"
                .to_owned(),
        };
        assert_eq!(
            hashset![
                "test_1".to_owned(),
                "test2".to_owned(),
                "t3st".to_owned(),
                "4test".to_owned(),
                "_TeSt_fIvE_".to_owned(),
            ],
            block.mentions(),
        );
    }

    #[test]
    fn sanitization_and_rewriting() {
        assert_eq!(
            SanitizedHtml::from("<div style=\"color: red;\"><p>Check out this <strike>horrifying</strike> totally normal image!</p><img src=\"http://example.com/image.png\"/></div>").inner,
            String::from("<div><p>Check out this <s>horrifying</s> totally normal image!</p></div>"),
        );
    }
}
