//! # Models : Administration
//!
//! This module contains models related to the administration log and user warning list

use chrono::NaiveDateTime;

use crate::schema::*;

/// Represents a moderation action log entry
#[derive(Identifiable, Queryable)]
#[table_name = "moderation_actions"]
pub struct ModerationAction {
    /// The unique ID of the entry
    pub id: uuid::Uuid,
    /// The username of the moderator who initiated the action
    pub username: String,
    /// The date the action was taken
    pub action_date: NaiveDateTime,
    /// The type of action that was taken (internal string)
    pub action_type: String,
    /// The message linked to the action (usually, given by the moderator)
    pub message: String,
}

/// Represents a new moderation action log entry prior to inserting it into the database
#[derive(Insertable)]
#[table_name = "moderation_actions"]
pub struct NewModerationAction {
    /// The username of the moderator who initiated the action
    pub username: String,
    /// The type of action that was taken (internal string)
    pub action_type: String,
    /// The message linked to the action (usually, given by the moderator)
    pub message: String,
}

/// Represents a new user warning entry prior to inserting it into the database
#[derive(Insertable)]
#[table_name = "user_warnings"]
pub struct NewUserWarning {
    /// The moderator who initiated the warning
    pub moderator_id: uuid::Uuid,
    /// The user targeted by the warning
    pub user_id: uuid::Uuid,
    /// The reason for the warning to be added
    pub reason: String,
}

/// Represents an user warning entry
#[derive(Identifiable, Queryable)]
#[table_name = "user_warnings"]
pub struct UserWarning {
    /// The internal ID of the warning
    pub id: uuid::Uuid,
    /// The date at which the warning was added
    pub warning_date: NaiveDateTime,
    /// The moderator who initiated the warning
    pub moderator_id: uuid::Uuid,
    /// The user targeted by the warning
    pub user_id: uuid::Uuid,
    /// The reason for the warning to be added
    pub reason: String,
}
