//! # Models : Recommendations
//!
//! This module contains models related to the user recommendations of other user's stories

use chrono::NaiveDateTime;

use super::formats::RichBlock;
use super::stories::Story;
use super::users::User;
use crate::schema::*;

/// Represents a recommendation currently stored in the database
#[derive(Queryable, Insertable, Associations)]
#[belongs_to(Story, foreign_key = "story_id")]
#[belongs_to(User, foreign_key = "user_id")]
#[table_name = "recommendations"]
pub struct Recommendation {
    /// The Id of the recommendation itself
    pub id: uuid::Uuid,
    /// The story that this recommendation refers to
    pub story_id: uuid::Uuid,
    /// The user who recommended this story
    pub user_id: uuid::Uuid,
    /// The date the recommendation is written at
    pub recommendation_date: NaiveDateTime,
    /// Whether this recommendation is "featured" directly on the user's page (not used currently)
    pub featured_by_user: bool,
    /// The extra information linked to this recommendation, formatted in Markdown format
    pub description: RichBlock,
}

/// Represents a recommendation before being stored in the database
#[derive(Insertable)]
#[table_name = "recommendations"]
pub struct NewRecommendation {
    /// The story that this recommendation refers to
    pub story_id: uuid::Uuid,
    /// The user who recommended this story
    pub user_id: uuid::Uuid,
    /// Whether this recommendation is "featured" directly on the user's page (not used currently)
    pub featured_by_user: bool,
    /// The extra information linked to this recommendation, formatted in Markdown format
    pub description: RichBlock,
}

/// Represents the report of a recommendation
#[derive(Debug, Queryable, Associations, PartialEq, Eq)]
#[belongs_to(Story, foreign_key = "story_id")]
#[belongs_to(User, foreign_key = "reporter_id")]
#[table_name = "recommendation_reports"]
pub struct RecommendationReport {
    /// The date of the report
    pub report_date: NaiveDateTime,
    /// The story that this recommendation refers to
    pub story_id: uuid::Uuid,
    /// The user who recommended this story
    pub user_id: uuid::Uuid,
    /// The user who reported this recommendation
    pub reporter_id: uuid::Uuid,
    /// Whether this report was handled by a moderator, and the ID of the moderator who handled it if relevant
    pub handled_by_moderator_id: Option<uuid::Uuid>,
    /// The reason for the report
    pub reason: String,
}
