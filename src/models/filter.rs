//! # Models : Filter
//!
//! This module contains the filtering mechanism used throughout the library

use crate::models::users::User;

/// Used to filter results based on characteristics
#[derive(Clone, Debug)]
pub enum FilterMode {
    /// Standard filtering : used when there is no special constraints
    Standard,
    /// Include unpublished stories from a given author
    IncludeDirectAccess {
        /// The author ID whose access rights are allowed
        own_user_id: uuid::Uuid,
    },
    /// Include all unpublished stories
    BypassFilters,
}

impl FilterMode {
    /// Gets the filter mode using a logged-in user
    pub fn from_login(login_user: &User) -> Self {
        Self::from_login_opt(&Some(login_user))
    }

    /// Gets the filter mode using raw log-in information (might represent an unidentified user)
    pub fn from_login_opt(login_user: &Option<&User>) -> Self {
        match &login_user {
            Some(login_user) if login_user.administrator => Self::BypassFilters,
            Some(login_user) => Self::IncludeDirectAccess {
                own_user_id: login_user.id,
            },
            _ => Self::Standard,
        }
    }

    /// Gets the filter mode using raw log-in information (might represent an unidentified user)
    /// But don't allow the "BypassFilter" mode.
    pub fn from_login_opt_without_bypass(login_user: &Option<&User>) -> Self {
        match &login_user {
            Some(login_user) => Self::IncludeDirectAccess {
                own_user_id: login_user.id,
            },
            _ => Self::Standard,
        }
    }
}
