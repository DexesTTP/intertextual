//! # Models : Comments
//!
//! This module contains models related to comments and comment reports

use chrono::NaiveDateTime;
use std::cmp::{Eq, PartialEq};

use super::formats::RichBlock;
use super::stories::Chapter;
use super::users::User;
use crate::schema::*;

/// Represents a single comment
#[derive(Debug, Identifiable, Queryable, Associations, PartialEq, Eq)]
#[belongs_to(User, foreign_key = "commenter_id")]
#[belongs_to(Chapter, foreign_key = "chapter_id")]
#[table_name = "comments"]
pub struct Comment {
    /// The internal ID of the comment
    pub id: uuid::Uuid,
    /// The ID of the comment author
    pub commenter_id: uuid::Uuid,
    /// The ID of the chapter this comment is linked to
    pub chapter_id: uuid::Uuid,
    /// When the comment was created
    pub created: NaiveDateTime,
    /// The last time the comment was edited, or [`None`] if the comment was never edited
    pub updated: Option<NaiveDateTime>,
    /// The message of the comment
    pub message: RichBlock,
}

/// Represents a comment before its insertion in the database
#[derive(Insertable)]
#[table_name = "comments"]
pub struct NewComment {
    /// The ID of the comment author
    pub commenter_id: uuid::Uuid,
    /// The ID of the chapter this comment is linked to
    pub chapter_id: uuid::Uuid,
    /// The message of the comment
    pub message: RichBlock,
}

/// Represents a comment report
#[derive(Debug, Queryable, Associations, PartialEq, Eq)]
#[belongs_to(Comment, foreign_key = "comment_id")]
#[belongs_to(User, foreign_key = "reporter_id")]
#[table_name = "comment_reports"]
pub struct CommentReport {
    /// The date of the report
    pub report_date: NaiveDateTime,
    /// The ID of the reported comment
    pub comment_id: uuid::Uuid,
    /// The ID of the user who reported the comment
    pub reporter_id: uuid::Uuid,
    /// Whether this report was handled by a moderator, and the ID of the moderator who handled it if relevant
    pub handled_by_moderator_id: Option<uuid::Uuid>,
    /// The reason for the report
    pub reason: String,
}
