use diesel::PgConnection;

use intertextual::actions;
use intertextual::models::error::IntertextualError;
use intertextual::models::filter::FilterMode;
use intertextual::models::shared::SharedSiteData;
use intertextual::utils::publication::PublicationDate;

#[allow(dead_code)]
pub enum SitemapUpdateMode {
    Always,
    Hourly,
    Daily,
    Weekly,
    Monthly,
    Yearly,
    Never,
}

impl SitemapUpdateMode {
    pub fn to_xml_name(&self) -> &'static str {
        match self {
            SitemapUpdateMode::Always => "always",
            SitemapUpdateMode::Hourly => "hourly",
            SitemapUpdateMode::Daily => "daily",
            SitemapUpdateMode::Weekly => "weekly",
            SitemapUpdateMode::Monthly => "monthly",
            SitemapUpdateMode::Yearly => "yearly",
            SitemapUpdateMode::Never => "never",
        }
    }
}

pub struct SitemapEntry {
    pub url: String,
    pub date: chrono::NaiveDateTime,
    pub update_mode: SitemapUpdateMode,
}

impl SitemapEntry {
    pub fn from_date(url: String, date: chrono::NaiveDateTime) -> SitemapEntry {
        let a_month_ago = (chrono::Utc::now() - chrono::Duration::days(30)).naive_utc();
        if date > a_month_ago {
            SitemapEntry::weekly(url, date)
        } else {
            SitemapEntry::yearly(url, date)
        }
    }

    pub fn weekly(url: String, date: chrono::NaiveDateTime) -> SitemapEntry {
        SitemapEntry {
            url,
            date,
            update_mode: SitemapUpdateMode::Weekly,
        }
    }

    pub fn yearly(url: String, date: chrono::NaiveDateTime) -> SitemapEntry {
        SitemapEntry {
            url,
            date,
            update_mode: SitemapUpdateMode::Yearly,
        }
    }
}

pub fn generate_sitemap_files(
    site_data: &SharedSiteData,
    default_publication_date: &PublicationDate,
    conn: &PgConnection,
) -> Result<Vec<SitemapEntry>, IntertextualError> {
    let last_publication =
        default_publication_date.get_last_publication_date(chrono::Utc::now().naive_utc());
    let base = &site_data.base_url;
    let filter_mode = FilterMode::Standard;

    let mut url_entries = vec![];

    url_entries.push(SitemapEntry::weekly(format!("{}/", base), last_publication));
    url_entries.push(SitemapEntry::weekly(
        format!("{}/authors/", base),
        last_publication,
    ));
    url_entries.push(SitemapEntry::weekly(
        format!("{}/stories/", base),
        last_publication,
    ));
    url_entries.push(SitemapEntry::weekly(
        format!("{}/tags/", base),
        last_publication,
    ));
    url_entries.push(SitemapEntry::weekly(
        format!("{}/whatsnew/", base),
        last_publication,
    ));

    let authors = actions::users::find_all_users(&filter_mode, conn)?;
    for author in authors {
        let mut author_update = None;
        let stories = actions::stories::find_all_stories_by_author(author.id, &filter_mode, conn)?;
        for story in stories {
            let mut story_update = None;
            let chapters =
                actions::stories::find_chapters_metadata_by_story_id(story.id, &filter_mode, conn)?;
            for chapter in chapters {
                let chapter_update = chapter.last_edition_date.or(chapter.official_creation_date);
                if let Some(chapter_update) = chapter_update {
                    match story_update {
                        // The story update is newer, keep it
                        Some(story_update) if story_update > chapter_update => {}
                        // The story update is empty or older, swap it
                        _ => {
                            story_update.replace(chapter_update);
                        }
                    }

                    if story.is_collaboration {
                        // We will generate the actual URLs later, to prevent duplications
                        continue;
                    }

                    url_entries.push(SitemapEntry::from_date(
                        format!(
                            "{}/@{}/{}/{}/",
                            base, author.username, story.url_fragment, chapter.chapter_number
                        ),
                        chapter_update,
                    ));
                }
            }

            if let Some(story_update) = story_update {
                match author_update {
                    // The story update is newer, keep it
                    Some(author_update) if author_update > story_update => {}
                    // The story update is empty or older, swap it
                    _ => {
                        author_update.replace(story_update);
                    }
                }

                if story.is_collaboration {
                    // We will generate the actual URLs later, to prevent duplications
                    continue;
                }

                url_entries.push(SitemapEntry::from_date(
                    format!("{}/@{}/{}/", base, author.username, story.url_fragment),
                    story_update,
                ));
            }
        }

        if let Some(author_update) = author_update {
            url_entries.push(SitemapEntry::from_date(
                format!("{}/@{}/", base, author.username),
                author_update,
            ));
        }
    }

    let collaboration_stories =
        actions::stories::find_all_collaboration_stories(&filter_mode, conn)?;
    for story in collaboration_stories {
        let chapters =
            actions::stories::find_chapters_metadata_by_story_id(story.id, &filter_mode, conn)?;
        let mut story_update = None;
        for chapter in chapters {
            let chapter_update = chapter.last_edition_date.or(chapter.official_creation_date);
            if let Some(chapter_update) = chapter_update {
                url_entries.push(SitemapEntry::from_date(
                    format!(
                        "{}/collaboration/{}/{}/",
                        base, story.url_fragment, chapter.chapter_number
                    ),
                    chapter_update,
                ));
                match story_update {
                    // The story update is newer, keep it
                    Some(story_update) if story_update > chapter_update => {}
                    // The story update is empty or older, swap it
                    _ => {
                        story_update.replace(chapter_update);
                    }
                }
            }
        }

        if let Some(story_update) = story_update {
            url_entries.push(SitemapEntry::from_date(
                format!("{}/collaboration/{}/", base, story.url_fragment),
                story_update,
            ));
        }
    }

    Ok(url_entries)
}
