use std::{convert::TryFrom, sync::Arc};

use actix_web::web;
use serde::{Deserialize, Serialize};

use intertextual::models::error::IntertextualError;
use intertextual::models::notifications::NotificationCategory;
use intertextual::models::shared::AppState;
use intertextual::models::shared::SharedSiteData;
use intertextual::models::users::User;

use crate::app::identity::Identity;
use crate::app::identity::IdentityData;
use crate::error::*;

const DEFAULT_FONT_SIZE: u32 = 18;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum PersistentTheme {
    Light,
    Dark,
}

impl PersistentTheme {
    pub fn as_str(&self) -> &'static str {
        match self {
            PersistentTheme::Light => "l",
            PersistentTheme::Dark => "d",
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum PersistentFontFamily {
    Serif,
    SansSerif,
}

impl PersistentFontFamily {
    pub fn as_str(&self) -> &'static str {
        match self {
            PersistentFontFamily::Serif => "s",
            PersistentFontFamily::SansSerif => "n",
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum PersistentTextAlignment {
    Left,
    Justify,
}

impl PersistentTextAlignment {
    pub fn as_str(&self) -> &'static str {
        match self {
            PersistentTextAlignment::Left => "l",
            PersistentTextAlignment::Justify => "j",
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum PersistentContentSize {
    Wide,
    Narrow,
}

impl PersistentContentSize {
    pub fn as_str(&self) -> &'static str {
        match self {
            PersistentContentSize::Wide => "w",
            PersistentContentSize::Narrow => "n",
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum PersistentParagraphLayout {
    Classic,
    Modern,
}

impl PersistentParagraphLayout {
    pub fn as_str(&self) -> &'static str {
        match self {
            PersistentParagraphLayout::Classic => "c",
            PersistentParagraphLayout::Modern => "m",
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct PersistentQuery {
    pub t: Option<String>,
}

#[derive(Debug)]
pub struct LoginUserData {
    pub user: User,
    pub unread_notifications_count: i64,
    pub has_unread_events: bool,
    pub marked_for_later: i64,
    pub admin_notifications: i64,
}

#[derive(Debug)]
pub struct PersistentData {
    site: Arc<SharedSiteData>,
    login: Option<LoginUserData>,
    theme: PersistentTheme,
    font_family: PersistentFontFamily,
    text_alignment: PersistentTextAlignment,
    content_size: PersistentContentSize,
    paragraph_layout: PersistentParagraphLayout,
    font_size: u32,
}

#[derive(Debug, Clone)]
pub struct LoginUserTemplate {
    pub username: String,
    pub display_name: String,
    pub unread_notifications_count: i64,
    pub has_unread_events: bool,
    pub marked_for_later: i64,
    pub admin_notifications: i64,
}

#[derive(Debug, Clone)]
pub struct PersistentTemplate {
    pub site: Arc<SharedSiteData>,
    pub login: Option<LoginUserTemplate>,
    theme: PersistentTheme,
    font_family: PersistentFontFamily,
    text_alignment: PersistentTextAlignment,
    content_size: PersistentContentSize,
    paragraph_layout: PersistentParagraphLayout,
    font_size: u32,
}

impl PersistentData {
    pub fn from_no_id(data: &AppState) -> PersistentData {
        PersistentData {
            site: data.site.clone(),
            login: None,
            theme: PersistentTheme::Light,
            font_family: PersistentFontFamily::Serif,
            text_alignment: PersistentTextAlignment::Left,
            content_size: PersistentContentSize::Wide,
            paragraph_layout: PersistentParagraphLayout::Classic,
            font_size: DEFAULT_FONT_SIZE,
        }
    }

    pub async fn from_query(
        data: &AppState,
        id: Identity,
        query: &PersistentQuery,
    ) -> Result<PersistentData, AppError> {
        Ok(PersistentData::from_raw_query_data(data, id, &query.t).await?)
    }

    pub async fn from_raw_query_data(
        data: &AppState,
        id: Identity,
        url_theme: &Option<String>,
    ) -> Result<PersistentData, AppError> {
        let login = identity_user(id, data).await?;

        // Get the raw theme description (user theme or URL theme, as needed)
        let mut raw_theme_description = login.as_ref().map_or(url_theme, |l| &l.theme);

        match &login {
            Some(user) => {
                // Check if the theme needs to be updated
                let url_theme_is_not_user_theme = match (url_theme, &user.theme) {
                    /* There is both a defined URL and user theme. Literally check if they're the same or not. */
                    (Some(url_theme), Some(user_theme)) => url_theme != user_theme,
                    /* There was an url theme and no user theme, so assume the user had a default theme and we want to change it to a non-default theme */
                    /* NOTE : this is technically incorrect since the URL theme could be the default theme, but it's overall harmless */
                    (Some(_url_theme), None) => true,
                    /* When a user is logged in, theme is no longer sent via the URL. This is the normal case, and there's nothing to update. */
                    (None, _user_theme) => false,
                };
                if url_theme_is_not_user_theme {
                    // Update the theme description currently stored in the database
                    let user_id = user.id;
                    web::block({
                        let url_theme = url_theme.clone();
                        let conn = data
                            .pool
                            .get()
                            .map_err(|e| e.into_app(&PersistentData::from_no_id(data)))?;
                        move || {
                            intertextual::actions::users::modifications::update_user_theme(
                                user_id, url_theme, &conn,
                            )
                        }
                    })
                    .await
                    .map_err(|e| e.into_app(&PersistentData::from_no_id(data)))?;

                    // At this point, the actual theme description comes from the URL theme, but earlier we selected the old user theme. Fix that.
                    raw_theme_description = url_theme;
                }
            }
            None => {}
        };

        // Get the 'unread notifications count' and the 'has unread events' query, if needed
        let (unread_notifications_count, has_unread_events) = match &login {
            Some(user) => {
                // TODO : retrieve this from the database
                let user_id = user.id;
                let unread_notification_categories = web::block({
                    let conn = data
                        .pool
                        .get()
                        .map_err(|e| e.into_app(&PersistentData::from_no_id(data)))?;
                    move || {
                        intertextual::actions::notifications::find_all_unread_notifications_by_user_id(
                            user_id, &conn,
                        )
                    }
                })
                .await
                .map_err(|e| e.into_app(&PersistentData::from_no_id(data)))?
                .into_iter()
                .map(|e| {
                    NotificationCategory::try_from(e.category)
                        .unwrap_or(NotificationCategory::Unknown)
                })
                .collect::<Vec<NotificationCategory>>();
                let notifications = unread_notification_categories
                    .iter()
                    .filter(|n| user.should_show_red_notify_for(**n))
                    .count();
                let has_events = unread_notification_categories
                    .iter()
                    .any(|n| user.should_show_event_for(*n));
                (notifications as i64, has_events)
            }
            None => (0i64, false),
        };
        // Get the 'marked for later' story count if needed
        let marked_for_later = match &login {
            Some(user) => web::block({
                    let user_id = user.id;
                    let conn = data
                        .pool
                        .get()
                        .map_err(|e| e.into_app(&PersistentData::from_no_id(data)))?;
                    move || {
                        intertextual::actions::marked_for_later::total_marked_for_later_count_given_by_user(
                            user_id, &conn,
                        )
                    }
                })
                .await
                .map_err(|e| e.into_app(&PersistentData::from_no_id(data)))?,
            None => 0i64,
        };
        let admin_notifications = match &login {
            Some(user) if user.administrator => web::block({
                let conn = data
                    .pool
                    .get()
                    .map_err(|e| e.into_app(&PersistentData::from_no_id(data)))?;
                move || -> Result<i64, IntertextualError> {
                    Ok(
                        intertextual::actions::modmail::admins::get_unhandled_modmail_count(&conn)?
                            + intertextual::actions::reports::admin::get_unhandled_report_count(
                                &conn,
                            )?,
                    )
                }
            })
            .await
            .map_err(|e| e.into_app(&PersistentData::from_no_id(data)))?,
            _ => 0i64,
        };

        // Parse the "raw full theme" into understandable components
        let (theme, font_family, text_alignment, content_size, paragraph_layout, font_size) =
            match raw_theme_description {
                Some(theme) => parse_theme(theme),
                None => (
                    PersistentTheme::Light,
                    PersistentFontFamily::Serif,
                    PersistentTextAlignment::Left,
                    PersistentContentSize::Wide,
                    PersistentParagraphLayout::Classic,
                    DEFAULT_FONT_SIZE,
                ),
            };

        // Create the right persistent data
        Ok(PersistentData {
            site: data.site.clone(),
            login: login.map(|user| LoginUserData {
                user,
                unread_notifications_count,
                has_unread_events,
                marked_for_later,
                admin_notifications,
            }),
            theme,
            font_family,
            text_alignment,
            content_size,
            paragraph_layout,
            font_size,
        })
    }

    pub fn login(&self) -> Option<&User> {
        self.login.as_ref().map(|l| &l.user)
    }
}

impl From<PersistentData> for PersistentTemplate {
    fn from(data: PersistentData) -> PersistentTemplate {
        PersistentTemplate {
            site: data.site,
            login: data.login.map(|l| LoginUserTemplate {
                username: l.user.username,
                display_name: l.user.display_name,
                has_unread_events: l.has_unread_events,
                unread_notifications_count: l.unread_notifications_count,
                marked_for_later: l.marked_for_later,
                admin_notifications: l.admin_notifications,
            }),
            theme: data.theme,
            font_family: data.font_family,
            text_alignment: data.text_alignment,
            content_size: data.content_size,
            paragraph_layout: data.paragraph_layout,
            font_size: data.font_size,
        }
    }
}

impl From<&PersistentData> for PersistentTemplate {
    fn from(data: &PersistentData) -> PersistentTemplate {
        PersistentTemplate {
            site: data.site.clone(),
            login: data.login.as_ref().map(|l| LoginUserTemplate {
                username: l.user.username.clone(),
                display_name: l.user.display_name.clone(),
                has_unread_events: l.has_unread_events,
                unread_notifications_count: l.unread_notifications_count,
                marked_for_later: l.marked_for_later,
                admin_notifications: l.admin_notifications,
            }),
            theme: data.theme.clone(),
            font_family: data.font_family.clone(),
            text_alignment: data.text_alignment.clone(),
            content_size: data.content_size.clone(),
            paragraph_layout: data.paragraph_layout.clone(),
            font_size: data.font_size,
        }
    }
}

impl PersistentTemplate {
    pub fn into_logged_out(self) -> PersistentTemplate {
        PersistentTemplate {
            site: self.site,
            login: None,
            theme: self.theme,
            font_family: self.font_family,
            text_alignment: self.text_alignment,
            content_size: self.content_size,
            paragraph_layout: self.paragraph_layout,
            font_size: self.font_size,
        }
    }

    pub fn get_classes(&self) -> String {
        let mut classes = vec![];
        match self.theme {
            PersistentTheme::Light => {}
            PersistentTheme::Dark => classes.push("theme-dark"),
        };
        match self.font_family {
            PersistentFontFamily::Serif => {}
            PersistentFontFamily::SansSerif => classes.push("font-sans-serif"),
        };
        match self.text_alignment {
            PersistentTextAlignment::Left => {}
            PersistentTextAlignment::Justify => classes.push("text-justify"),
        };
        match self.content_size {
            PersistentContentSize::Wide => {}
            PersistentContentSize::Narrow => classes.push("content-narrow"),
        };
        match self.paragraph_layout {
            PersistentParagraphLayout::Classic => {}
            PersistentParagraphLayout::Modern => classes.push("paragraph-style-modern"),
        };
        format!(
            "class=\"{}\" style=\"{}\"",
            classes.join(" "),
            match self.font_size {
                DEFAULT_FONT_SIZE => String::new(),
                font_size => format!("font-size: {}px", font_size),
            }
        )
    }

    pub fn is_dark(&self) -> bool {
        self.theme == PersistentTheme::Dark
    }

    pub fn is_sans_serif(&self) -> bool {
        self.font_family == PersistentFontFamily::SansSerif
    }

    pub fn is_left_aligned(&self) -> bool {
        self.text_alignment == PersistentTextAlignment::Left
    }

    pub fn is_justify(&self) -> bool {
        self.text_alignment == PersistentTextAlignment::Justify
    }

    pub fn is_content_wide(&self) -> bool {
        self.content_size == PersistentContentSize::Wide
    }

    pub fn is_content_narrow(&self) -> bool {
        self.content_size == PersistentContentSize::Narrow
    }

    pub fn is_paragraph_layout_classic(&self) -> bool {
        self.paragraph_layout == PersistentParagraphLayout::Classic
    }

    pub fn is_paragraph_layout_modern(&self) -> bool {
        self.paragraph_layout == PersistentParagraphLayout::Modern
    }

    pub fn allow_smaller(&self) -> bool {
        self.font_size > 12
    }

    pub fn allow_bigger(&self) -> bool {
        self.font_size < 22
    }

    pub fn as_leading_qmark(&self) -> String {
        if self.login.is_some() {
            // Do not serialize an url, it is part of the user's data
            return String::new();
        }

        let text = Self::t_value(
            &self.theme,
            &self.font_family,
            &self.text_alignment,
            &self.content_size,
            &self.paragraph_layout,
            self.font_size,
        );
        if text.is_empty() {
            return String::new();
        }
        format!("?t={}", text)
    }

    pub fn as_leading_amp(&self) -> String {
        if self.login.is_some() {
            // Do not serialize an url, it is part of the user's data
            return String::new();
        }

        let text = Self::t_value(
            &self.theme,
            &self.font_family,
            &self.text_alignment,
            &self.content_size,
            &self.paragraph_layout,
            self.font_size,
        );
        if text.is_empty() {
            return String::new();
        }
        format!("&t={}", text)
    }

    pub fn as_inputs(&self) -> String {
        if self.login.is_some() {
            // Do not serialize inputs, it is part of the user's data
            return String::new();
        }

        let text = Self::t_value(
            &self.theme,
            &self.font_family,
            &self.text_alignment,
            &self.content_size,
            &self.paragraph_layout,
            self.font_size,
        );
        if text.is_empty() {
            return String::new();
        }
        format!("<input type=\"hidden\" name=\"t\" value=\"{}\" />", text)
    }

    pub fn as_light_qmark(&self) -> String {
        format!(
            "?t={}",
            Self::explicit_t_value(
                &PersistentTheme::Light,
                &self.font_family,
                &self.text_alignment,
                &self.content_size,
                &self.paragraph_layout,
                self.font_size,
            )
        )
    }

    pub fn as_dark_qmark(&self) -> String {
        format!(
            "?t={}",
            Self::explicit_t_value(
                &PersistentTheme::Dark,
                &self.font_family,
                &self.text_alignment,
                &self.content_size,
                &self.paragraph_layout,
                self.font_size,
            )
        )
    }

    pub fn as_serif_qmark(&self) -> String {
        format!(
            "?t={}",
            Self::explicit_t_value(
                &self.theme,
                &PersistentFontFamily::Serif,
                &self.text_alignment,
                &self.content_size,
                &self.paragraph_layout,
                self.font_size,
            )
        )
    }

    pub fn as_sans_serif_qmark(&self) -> String {
        format!(
            "?t={}",
            Self::explicit_t_value(
                &self.theme,
                &PersistentFontFamily::SansSerif,
                &self.text_alignment,
                &self.content_size,
                &self.paragraph_layout,
                self.font_size,
            )
        )
    }

    pub fn as_left_aligned_qmark(&self) -> String {
        format!(
            "?t={}",
            Self::explicit_t_value(
                &self.theme,
                &self.font_family,
                &PersistentTextAlignment::Left,
                &self.content_size,
                &self.paragraph_layout,
                self.font_size,
            )
        )
    }

    pub fn as_justify_qmark(&self) -> String {
        format!(
            "?t={}",
            Self::explicit_t_value(
                &self.theme,
                &self.font_family,
                &PersistentTextAlignment::Justify,
                &self.content_size,
                &self.paragraph_layout,
                self.font_size,
            )
        )
    }

    pub fn as_wide_content_qmark(&self) -> String {
        format!(
            "?t={}",
            Self::explicit_t_value(
                &self.theme,
                &self.font_family,
                &self.text_alignment,
                &PersistentContentSize::Wide,
                &self.paragraph_layout,
                self.font_size,
            )
        )
    }

    pub fn as_narrow_content_qmark(&self) -> String {
        format!(
            "?t={}",
            Self::explicit_t_value(
                &self.theme,
                &self.font_family,
                &self.text_alignment,
                &PersistentContentSize::Narrow,
                &self.paragraph_layout,
                self.font_size,
            )
        )
    }

    pub fn as_classic_paragraph_layout_qmark(&self) -> String {
        format!(
            "?t={}",
            Self::explicit_t_value(
                &self.theme,
                &self.font_family,
                &self.text_alignment,
                &self.content_size,
                &PersistentParagraphLayout::Classic,
                self.font_size,
            )
        )
    }

    pub fn as_modern_paragraph_layout_qmark(&self) -> String {
        format!(
            "?t={}",
            Self::explicit_t_value(
                &self.theme,
                &self.font_family,
                &self.text_alignment,
                &self.content_size,
                &PersistentParagraphLayout::Modern,
                self.font_size,
            )
        )
    }

    pub fn as_smaller_font(&self) -> String {
        format!(
            "?t={}",
            Self::explicit_t_value(
                &self.theme,
                &self.font_family,
                &self.text_alignment,
                &self.content_size,
                &self.paragraph_layout,
                std::cmp::max(self.font_size - 2, 12),
            )
        )
    }

    pub fn as_bigger_font(&self) -> String {
        format!(
            "?t={}",
            Self::explicit_t_value(
                &self.theme,
                &self.font_family,
                &self.text_alignment,
                &self.content_size,
                &self.paragraph_layout,
                std::cmp::min(self.font_size + 2, 22),
            )
        )
    }

    fn t_value(
        theme: &PersistentTheme,
        font_family: &PersistentFontFamily,
        text_alignment: &PersistentTextAlignment,
        content_size: &PersistentContentSize,
        paragraph_layout: &PersistentParagraphLayout,
        font_size: u32,
    ) -> String {
        match (
            theme,
            font_family,
            text_alignment,
            content_size,
            paragraph_layout,
            font_size,
        ) {
            (
                PersistentTheme::Light,
                PersistentFontFamily::Serif,
                PersistentTextAlignment::Left,
                PersistentContentSize::Wide,
                PersistentParagraphLayout::Modern,
                DEFAULT_FONT_SIZE,
            ) => String::new(),
            (theme, font_family, text_alignment, content_size, paragraph_layout, font_size) => {
                Self::explicit_t_value(
                    theme,
                    font_family,
                    text_alignment,
                    content_size,
                    paragraph_layout,
                    font_size,
                )
            }
        }
    }

    fn explicit_t_value(
        theme: &PersistentTheme,
        font_family: &PersistentFontFamily,
        text_alignment: &PersistentTextAlignment,
        content_size: &PersistentContentSize,
        paragraph_layout: &PersistentParagraphLayout,
        font_size: u32,
    ) -> String {
        format!(
            "{}{}{}{}{}{}",
            theme.as_str(),
            font_family.as_str(),
            text_alignment.as_str(),
            font_size,
            content_size.as_str(),
            paragraph_layout.as_str(),
        )
    }
}

async fn identity_user(id: Identity, data: &AppState) -> Result<Option<User>, AppError> {
    match id.identity() {
        None => Ok(None),
        Some(IdentityData { user_id, token_id }) => {
            let user = web::block({
                let conn = data
                    .pool
                    .get()
                    .map_err(|e| e.into_app(&PersistentData::from_no_id(data)))?;
                move || {
                    intertextual::actions::users::find_user_for_token_and_update_time_if_needed(
                        user_id, token_id, &conn,
                    )
                }
            })
            .await
            .map_err(|e| e.into_app(&PersistentData::from_no_id(data)))?;
            if user.is_none() {
                log::info!(
                    "Identity cookie contains invalid token pair (no match in database): {} / {}",
                    user_id,
                    token_id
                );
                id.forget();
            }
            Ok(user)
        }
    }
}

fn parse_theme(
    theme_str: &str,
) -> (
    PersistentTheme,
    PersistentFontFamily,
    PersistentTextAlignment,
    PersistentContentSize,
    PersistentParagraphLayout,
    u32,
) {
    let theme = match theme_str.get(0..1) {
        Some("d") => PersistentTheme::Dark,
        Some("l") => PersistentTheme::Light,
        _ => PersistentTheme::Light,
    };
    let font_family = match theme_str.get(1..2) {
        Some("n") => PersistentFontFamily::SansSerif,
        Some("s") => PersistentFontFamily::Serif,
        _ => PersistentFontFamily::Serif,
    };
    let text_alignment = match theme_str.get(2..3) {
        Some("j") => PersistentTextAlignment::Justify,
        Some("l") => PersistentTextAlignment::Left,
        _ => PersistentTextAlignment::Left,
    };
    let mut font_size = theme_str
        .get(3..5)
        .and_then(|s| s.parse::<u32>().ok())
        .unwrap_or(DEFAULT_FONT_SIZE);
    font_size = std::cmp::max(font_size, 12);
    font_size = std::cmp::min(font_size, 22);
    let content_size = match theme_str.get(5..6) {
        Some("w") => PersistentContentSize::Wide,
        Some("n") => PersistentContentSize::Narrow,
        _ => PersistentContentSize::Wide,
    };
    let paragraph_layout = match theme_str.get(6..7) {
        Some("c") => PersistentParagraphLayout::Classic,
        Some("m") => PersistentParagraphLayout::Modern,
        _ => PersistentParagraphLayout::Classic,
    };
    (
        theme,
        font_family,
        text_alignment,
        content_size,
        paragraph_layout,
        font_size,
    )
}
