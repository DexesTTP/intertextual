pub mod common_pages;
pub mod identity;
pub mod multipart;
pub mod persistent;
pub mod sitemap;
pub mod validation;

pub fn is_unique_violation<T>(res: &Result<T, crate::error::AppError>) -> bool {
    matches!(
        res,
        Err(crate::error::AppError::IntertextualError {
            persistent: _,
            error: intertextual::models::error::IntertextualError::DBError(
                diesel::result::Error::DatabaseError(
                    diesel::result::DatabaseErrorKind::UniqueViolation,
                    _,
                ),
            ),
        })
    )
}
