use regex::Regex;

use intertextual::models::error::IntertextualError;
use intertextual::models::formats::*;

pub fn validate_username(
    username: &str,
    form_field_name: &'static str,
) -> Result<(), IntertextualError> {
    const MAX_CHARACTERS_FOR_USERNAME: usize = 32;

    if username.len() > MAX_CHARACTERS_FOR_USERNAME {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name,
            message: format!(
                "Usernames should be at most {} characters long.",
                MAX_CHARACTERS_FOR_USERNAME
            ),
        });
    }

    let re = Regex::new(r"^[a-zA-Z0-9_]+$").unwrap();
    if !re.is_match(username) {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name,
            message: "Usernames can only contain uppercase and lowercase characters, numbers and underscores. Use a simpler username, and set any other characters as your display name.".to_string(),
        });
    }

    Ok(())
}

pub fn validate_password(
    password: &str,
    form_field_name: &'static str,
) -> Result<(), IntertextualError> {
    if password.is_empty() {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name,
            message: "Your password cannot be empty".to_string(),
        });
    }

    Ok(())
}

pub fn validate_display_name(
    display_name: &str,
    form_field_name: &'static str,
) -> Result<(), IntertextualError> {
    const MAX_CHARACTERS_FOR_DISPLAY_NAME: usize = 64;

    if display_name.trim().is_empty() {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name,
            message: "Your display name cannot be empty or only made of whitespace.".to_string(),
        });
    }

    if display_name.trim().len() >= MAX_CHARACTERS_FOR_DISPLAY_NAME {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name,
            message: format!(
                "Your display name should be at most {} characters long.",
                MAX_CHARACTERS_FOR_DISPLAY_NAME
            ),
        });
    }

    Ok(())
}

pub fn validate_story_url(
    story_url: &str,
    form_field_name: &'static str,
) -> Result<(), IntertextualError> {
    const MAX_CHARACTERS_FOR_STORY_URL: usize = 32;

    if story_url.is_empty() {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name,
            message: "Story URLs cannot be empty.".to_string(),
        });
    }

    if story_url.len() > MAX_CHARACTERS_FOR_STORY_URL {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name,
            message: format!(
                "Story URLs should be at most {} characters long.",
                MAX_CHARACTERS_FOR_STORY_URL
            ),
        });
    }

    let re = Regex::new(r"^[a-zA-Z0-9_]+$").unwrap();
    if !re.is_match(story_url) {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name,
            message:  "Story URLs can only contain uppercase and lowercase characters, numbers and underscores".to_string(),
        });
    }

    Ok(())
}

pub fn validate_story_title(
    title: &str,
    form_field_name: &'static str,
) -> Result<(), IntertextualError> {
    const MAX_CHARACTERS_FOR_STORY_NAME: usize = 128;

    if title.trim().is_empty() {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name,
            message: "Story titles cannot be empty or only made of whitespace.".to_string(),
        });
    }

    if title.trim().len() >= MAX_CHARACTERS_FOR_STORY_NAME {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name,
            message: format!(
                "Story titles should be at most {} characters long.",
                MAX_CHARACTERS_FOR_STORY_NAME
            ),
        });
    }

    Ok(())
}

pub fn validate_story_description(
    description: &RichTagline,
    form_field_name: &'static str,
) -> Result<(), IntertextualError> {
    const MAX_CHARACTERS_FOR_DESCRIPTION: usize = 280;

    if description.inner.trim().chars().count() > MAX_CHARACTERS_FOR_DESCRIPTION {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name,
            message: format!(
                "Descriptions should be at most {} characters long.",
                MAX_CHARACTERS_FOR_DESCRIPTION
            ),
        });
    }

    Ok(())
}

pub fn validate_email(email: &str, form_field_name: &'static str) -> Result<(), IntertextualError> {
    let re = Regex::new(r"^\S+@\S+$").unwrap();
    if !re.is_match(email) {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name,
            message: "Email addresses must contain an \"@\" symbol.".to_string(),
        });
    }

    Ok(())
}

pub fn validate_tag_name(tag_name: &str) -> Result<(), IntertextualError> {
    static MAX_CHARACTERS_FOR_TAG_NAME: usize = 100;
    if tag_name.len() > MAX_CHARACTERS_FOR_TAG_NAME {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Tag",
            message: format!(
                "Tag names cannot exceed {} characters.",
                MAX_CHARACTERS_FOR_TAG_NAME
            ),
        });
    }

    static EXCLUDED_TAG_CHARACTERS: &str = "#,;!@$%^&* ";
    for excluded_char in EXCLUDED_TAG_CHARACTERS.chars() {
        if tag_name.contains(excluded_char) {
            return Err(IntertextualError::FormFieldFormatError {
                form_field_name: "Tag",
                message: format!(
                    "The tag '{}' cannot contain any of the symbols {} and cannot contain spaces.",
                    tag_name, EXCLUDED_TAG_CHARACTERS
                ),
            });
        }
    }
    Ok(())
}

pub fn validate_user_details(
    details: &SanitizedHtml,
    form_field_name: &'static str,
) -> Result<(), IntertextualError> {
    const MAX_CHARACTERS_FOR_USER_DETAILS: usize = 20000;

    if details.text().chars().count() > MAX_CHARACTERS_FOR_USER_DETAILS {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name,
            message: format!(
                "User details should be at most {} characters long.",
                MAX_CHARACTERS_FOR_USER_DETAILS
            ),
        });
    }

    Ok(())
}

pub fn validate_admin_message(
    message: &str,
    form_field_name: &'static str,
) -> Result<(), IntertextualError> {
    const MIN_ADMIN_MESSAGE_CHARACTERS: usize = 5;

    if message.chars().count() < MIN_ADMIN_MESSAGE_CHARACTERS {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name,
            message: format!(
                "Administration messages must have {} characters or more",
                MIN_ADMIN_MESSAGE_CHARACTERS
            ),
        });
    }

    Ok(())
}

pub fn validate_age_verification(
    age_verification: &Option<String>,
    form_field_name: &'static str,
) -> Result<(), IntertextualError> {
    match age_verification {
        Some(value) if value == "verified" => Ok(()),
        _ => Err(IntertextualError::FormFieldFormatError {
            form_field_name,
            message: "To use this site, you need to be 18 years old or older.".to_string(),
        }),
    }
}

pub fn validate_comment(
    comment: &RichBlock,
    form_field_name: &'static str,
) -> Result<(), IntertextualError> {
    const MAX_CHARACTERS_FOR_COMMENT: usize = 2000;

    if comment.inner.trim().is_empty() {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name,
            message: "Comments cannot be empty or only made of whitespace.".to_string(),
        });
    }

    if comment.inner.chars().count() > MAX_CHARACTERS_FOR_COMMENT {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name,
            message: format!(
                "Comments should be at most {} characters long.",
                MAX_CHARACTERS_FOR_COMMENT
            ),
        });
    }

    Ok(())
}

pub fn validate_rich_modmail(
    modmail: &RichBlock,
    form_field_name: &'static str,
) -> Result<(), IntertextualError> {
    validate_modmail(&modmail.inner, form_field_name)
}

pub fn validate_modmail(
    modmail: &str,
    form_field_name: &'static str,
) -> Result<(), IntertextualError> {
    const MAX_CHARACTERS_FOR_MODMAIL: usize = 2000;

    if modmail.trim().is_empty() {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name,
            message: "Messages cannot be empty or only made of whitespace.".to_string(),
        });
    }

    if modmail.chars().count() > MAX_CHARACTERS_FOR_MODMAIL {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name,
            message: format!(
                "Messages should be at most {} characters long.",
                MAX_CHARACTERS_FOR_MODMAIL
            ),
        });
    }

    Ok(())
}

pub fn validate_comment_report(
    report: &str,
    form_field_name: &'static str,
) -> Result<(), IntertextualError> {
    const MAX_CHARACTERS_FOR_COMMENT_REPORT: usize = 2000;

    if report.trim().is_empty() {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name,
            message: "Reports cannot be empty or only made of whitespace.".to_string(),
        });
    }

    if report.chars().count() > MAX_CHARACTERS_FOR_COMMENT_REPORT {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name,
            message: format!(
                "Reports should be at most {} characters long.",
                MAX_CHARACTERS_FOR_COMMENT_REPORT
            ),
        });
    }

    Ok(())
}
