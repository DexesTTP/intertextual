//! # Comment retriever
//!
//! This module contains methods used to retrieve comment informations
use diesel::PgConnection;

use crate::actions;
use crate::models;
use crate::models::error::IntertextualError;
use crate::prelude::*;

/// Gets the information linked to the given `comment_id`
pub fn get_comment(
    comment_id: uuid::Uuid,
    filter_mode: &FilterMode,
    conn: &PgConnection,
) -> Result<
    (
        Vec<models::users::User>,
        models::stories::Story,
        models::stories::Chapter,
        models::users::User,
        models::comments::Comment,
    ),
    IntertextualError,
> {
    let (comment, user, chapter, story) = actions::comments::find_comment(comment_id, conn)?
        .ok_or(IntertextualError::CommentNotFound { comment_id })?;
    let authors = actions::stories::find_authors_by_story(story.id, filter_mode, conn)?;
    Ok((authors, story, chapter, user, comment))
}
