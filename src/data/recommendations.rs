//! # Recommendations retriever
//!
//! This module contains methods used to retrieve informations for the recommendations
use diesel::PgConnection;
use models::error::IntertextualError;

use crate::actions;
use crate::models;

/// Gets the data linked to a recommendation based on the URL components (recommender, author, story URL) and respecting the given `filter_mode`
pub fn find_author_recommendation_by_url(
    recommender_username: String,
    author_username: String,
    story_url: String,
    filter_mode: &models::filter::FilterMode,
    conn: &PgConnection,
) -> Result<
    (
        models::users::User,
        models::users::User,
        models::stories::Story,
        models::recommendations::Recommendation,
    ),
    IntertextualError,
> {
    let recommender =
        actions::users::find_user_by_username(&recommender_username, filter_mode, conn)?;
    let recommender = match recommender {
        Some(recommender) => recommender,
        None => {
            return Err(IntertextualError::UserNotFound {
                username: recommender_username,
            })
        }
    };
    let author = actions::users::find_user_by_username(&author_username, filter_mode, conn)?;
    let author = match author {
        Some(author) => author,
        None => {
            return Err(IntertextualError::UserNotFound {
                username: author_username,
            })
        }
    };
    let story =
        actions::stories::find_story_by_author_and_url(author.id, &story_url, filter_mode, conn)?;
    let story = match story {
        Some(story) => story,
        None => {
            return Err(IntertextualError::AuthorStoryNotFound {
                author_username,
                url_fragment: story_url,
            })
        }
    };
    let recommendation = actions::recommendations::find_recommendation_by_user_and_story(
        recommender.id,
        story.id,
        conn,
    )?;
    let recommendation = match recommendation {
        Some(recommendation) => recommendation,
        None => {
            return Err(IntertextualError::AuthorRecommendationNotFound {
                recommender_username,
                author_username,
                url_fragment: story_url,
            })
        }
    };

    Ok((recommender, author, story, recommendation))
}

/// Gets the data linked to a recommendation based on the URL components (recommender, story URL) and respecting the given `filter_mode`
pub fn find_collaboration_recommendation_by_url(
    recommender_username: String,
    story_url: String,
    filter_mode: &models::filter::FilterMode,
    conn: &PgConnection,
) -> Result<
    (
        models::users::User,
        Vec<models::users::User>,
        models::stories::Story,
        models::recommendations::Recommendation,
    ),
    IntertextualError,
> {
    let recommender =
        actions::users::find_user_by_username(&recommender_username, filter_mode, conn)?;
    let recommender = match recommender {
        Some(recommender) => recommender,
        None => {
            return Err(IntertextualError::UserNotFound {
                username: recommender_username,
            })
        }
    };
    let story = actions::stories::find_collaboration_story_by_url(&story_url, filter_mode, conn)?;
    let story = match story {
        Some(story) => story,
        None => {
            return Err(IntertextualError::CollaborationStoryNotFound {
                url_fragment: story_url,
            })
        }
    };
    let recommendation = actions::recommendations::find_recommendation_by_user_and_story(
        recommender.id,
        story.id,
        conn,
    )?;
    let recommendation = match recommendation {
        Some(recommendation) => recommendation,
        None => {
            return Err(IntertextualError::CollaborationRecommendationNotFound {
                recommender_username,
                url_fragment: story_url,
            })
        }
    };

    let authors = actions::stories::find_authors_by_story(story.id, filter_mode, conn)?;

    Ok((recommender, authors, story, recommendation))
}
