//! # Reports retriever
//!
//! This module contains methods used to retrieve informations for the chapter, comment and recommendation reports
use diesel::PgConnection;

use crate::actions;
use crate::models;
use crate::models::{error::IntertextualError, formats::SanitizedHtml};
use crate::prelude::*;

/// List of all the information linked to a comment's report
pub struct ReportedComment {
    /// The authors of the story with the comment
    pub authors: Vec<models::users::User>,
    /// The story with the comment
    pub story: models::stories::Story,
    /// The chapter with the comment
    pub chapter: models::stories::Chapter,
    /// The author of the comment
    pub comment_author: models::users::User,
    /// The contents of the comment
    pub comment: models::comments::Comment,
    /// The user who reported the comment
    pub reporter: models::users::User,
    /// The report information
    pub report: models::comments::CommentReport,
}

/// Gets the comment report information
pub fn get_reported_comment(
    comment_id: uuid::Uuid,
    conn: &PgConnection,
) -> Result<ReportedComment, IntertextualError> {
    let (report, reporter, comment, chapter, story) =
        actions::comments::admin::get_reported_comment(comment_id, conn)?
            .ok_or(IntertextualError::CommentNotFound { comment_id })?;
    let authors =
        actions::stories::find_authors_by_story(story.id, &FilterMode::BypassFilters, conn)?;
    let comment_author_id = comment.commenter_id;
    let comment_author = actions::users::find_user_by_id(comment_author_id, conn)?
        .ok_or(IntertextualError::CommentNotFound { comment_id })?;
    Ok(ReportedComment {
        authors,
        story,
        chapter,
        comment_author,
        comment,
        reporter,
        report,
    })
}

/// List of all the information linked to a chapter's report
pub struct ChapterReport {
    /// The user who wrote the report
    pub reporter: models::users::User,
    /// The reported story
    pub story: models::stories::Story,
    /// Whether this story is standalone (has a single chapter)
    pub story_is_standalone: bool,
    /// The author component of the story (or the collaboration string if this is a collaboration)
    pub story_author_path: String,
    /// The reported chapter
    pub chapter: models::stories::Chapter,
    /// The contents of the report
    pub report: models::stories::ChapterReport,
    /// If the report was handled, the moderator who handled the report
    pub moderator_who_handled_report: Option<models::users::User>,
}

impl ChapterReport {
    /// Compile a chapter report based on the reporter, chapter and report raw data
    pub fn get(
        (user, chapter, report): (
            models::users::User,
            models::stories::Chapter,
            models::stories::ChapterReport,
        ),
        conn: &PgConnection,
    ) -> Result<ChapterReport, IntertextualError> {
        let story = actions::stories::admin::find_story_by_id(chapter.story_id, conn)?;
        let story_is_standalone = actions::stories::story_is_standalone(
            chapter.story_id,
            &crate::models::filter::FilterMode::BypassFilters,
            conn,
        )?;
        let story_authors = actions::stories::find_authors_by_story(
            chapter.story_id,
            &FilterMode::BypassFilters,
            conn,
        )?;
        let story_author_path = story.author_path(&story_authors);
        let moderator_who_handled_report = match report.handled_by_moderator_id {
            Some(id) => actions::users::find_user_by_id(id, conn)?,
            None => None,
        };
        Ok(ChapterReport {
            reporter: user,
            story,
            story_is_standalone,
            story_author_path,
            chapter,
            report,
            moderator_who_handled_report,
        })
    }
}

/// List of all the information linked to a recommendation report
pub struct RecommendationReport {
    /// The user who wrote the report
    pub reporter: models::users::User,
    /// The reported recommendation
    pub recommendation: models::recommendations::Recommendation,
    /// The author of the reported recommendation
    pub recommendation_author: models::users::User,
    /// The reported recommendation, formatted and ready to be printed on screen
    pub formatted_recommendation: SanitizedHtml,
    /// The story the recommendation is for
    pub story: models::stories::Story,
    /// The author component of the story (or the collaboration string if this is a collaboration)
    pub story_author_path: String,
    /// The contents of the report
    pub report: models::recommendations::RecommendationReport,
    /// If the report was handled, the moderator who handled the report
    pub moderator_who_handled_report: Option<models::users::User>,
}

impl RecommendationReport {
    /// Compile a recommendation report based on the reporter, recommendation and report raw data
    pub fn get(
        (user, recommendation, report): (
            models::users::User,
            models::recommendations::Recommendation,
            models::recommendations::RecommendationReport,
        ),
        conn: &PgConnection,
    ) -> Result<RecommendationReport, IntertextualError> {
        let recommendation_author = actions::users::find_user_by_id(recommendation.user_id, conn)?;
        let recommendation_author =
            recommendation_author.ok_or(IntertextualError::PageDoesNotExist)?;
        let story = actions::stories::admin::find_story_by_id(recommendation.story_id, conn)?;
        let story_authors = actions::stories::find_authors_by_story(
            recommendation.story_id,
            &FilterMode::BypassFilters,
            conn,
        )?;
        let story_author_path = story.author_path(&story_authors);
        let moderator_who_handled_report = match report.handled_by_moderator_id {
            Some(id) => actions::users::find_user_by_id(id, conn)?,
            None => None,
        };
        let formatted_recommendation = recommendation.description.html(conn);
        Ok(RecommendationReport {
            reporter: user,
            recommendation,
            recommendation_author,
            formatted_recommendation,
            story,
            story_author_path,
            report,
            moderator_who_handled_report,
        })
    }
}

/// List of all the information linked to a comment report
pub struct CommentReport {
    /// The user who wrote the report
    pub reporter: models::users::User,
    /// The reported comment
    pub comment: models::comments::Comment,
    /// The author of the reported comment
    pub comment_author: models::users::User,
    /// The chapter the comment is for
    pub chapter: models::stories::Chapter,
    /// The story the comment is for
    pub story: models::stories::Story,
    /// The author component of the story (or the collaboration string if this is a collaboration)
    pub story_author_path: String,
    /// The contents of the report
    pub report: models::comments::CommentReport,
    /// If the report was handled, the moderator who handled the report
    pub moderator_who_handled_report: Option<models::users::User>,
}

impl CommentReport {
    /// Compile a comment report based on the reporter, comment and report raw data
    pub fn get(
        (user, comment, report): (
            models::users::User,
            models::comments::Comment,
            models::comments::CommentReport,
        ),
        conn: &PgConnection,
    ) -> Result<CommentReport, IntertextualError> {
        let comment_author = actions::users::find_user_by_id(comment.commenter_id, conn)?;
        let comment_author = comment_author.ok_or(IntertextualError::PageDoesNotExist)?;
        let chapter = actions::stories::admin::find_chapter_by_id(comment.chapter_id, conn)?;
        let story = actions::stories::admin::find_story_by_id(chapter.story_id, conn)?;
        let story_authors = actions::stories::find_authors_by_story(
            comment.chapter_id,
            &FilterMode::BypassFilters,
            conn,
        )?;
        let story_author_path = story.author_path(&story_authors);
        let moderator_who_handled_report = match report.handled_by_moderator_id {
            Some(id) => actions::users::find_user_by_id(id, conn)?,
            None => None,
        };
        Ok(CommentReport {
            reporter: user,
            comment,
            comment_author,
            chapter,
            story,
            story_author_path,
            report,
            moderator_who_handled_report,
        })
    }
}
