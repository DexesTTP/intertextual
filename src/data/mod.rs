//! # Data Access Layer
//!
//! Regroups some complex access operations on the database, can yield access errors as needed.

pub mod comments;
pub mod recommendations;
pub mod reports;
pub mod stories;
