use clap::Clap;
use diesel::Connection;
use diesel::PgConnection;

mod commands;

#[derive(Clap)]
struct Opts {
    /// Use once to suppress info output, use twice to suppress warnings
    #[clap(short, long, parse(from_occurrences))]
    quiet: u8,

    /// If defined, this database URL will be used instead of the environment variable one.
    #[clap(short, long)]
    database: Option<String>,

    /// The subcommand to use
    #[clap(subcommand)]
    command: SubCommand,
}

#[derive(Clap)]
enum SubCommand {
    /// Changes an existing user account
    User(commands::users::UserParams),
    /// Handles the registration limits
    Registrations(commands::registrations::RegistrationsParameters),
    /// Import a series of stories
    Import(commands::import::ImportParameters),
    /// Utilities, used to manipulate the database contents in various ways.
    Utilities(commands::utils::UtilsParameters),
    /// Sets up the database with all the account and stories needed for a stress test
    ///
    /// WARNING : This will pretty much nuke your database, and there is no easy way to clean it up afterwards.
    PrepareStressTest(commands::stress_test::StressTestParameters),
}

fn main() {
    let opts = Opts::parse();
    dotenv::dotenv().ok();

    let filter = match opts.quiet {
        0 => log::LevelFilter::Info,
        1 => log::LevelFilter::Warn,
        _ => log::LevelFilter::Error,
    };
    let stdout = log4rs::append::console::ConsoleAppender::builder().build();
    let log_config = log4rs::config::Config::builder()
        .appender(log4rs::config::Appender::builder().build("stdout", Box::new(stdout)))
        .build(
            log4rs::config::Root::builder()
                .appender("stdout")
                .build(filter),
        )
        .unwrap();
    log4rs::init_config(log_config).unwrap();

    let database_url = match opts.database {
        Some(db_url) => db_url,
        None => std::env::var("DATABASE_URL")
            .expect("Use the -d option or set the DATABASE_URL environment variable"),
    };
    let conn = PgConnection::establish(&database_url).expect("Error connecting to database");

    match opts.command {
        SubCommand::User(params) => commands::users::handle(params, &conn),
        SubCommand::Registrations(params) => commands::registrations::registrations(params, &conn),
        SubCommand::Import(params) => commands::import::import(params, &conn),
        SubCommand::Utilities(params) => commands::utils::handle(params, &conn),
        SubCommand::PrepareStressTest(params) => commands::stress_test::handle(params, &conn),
    }
    .expect("Error while executing the method")
}
