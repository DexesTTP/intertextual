use clap::Clap;
use diesel::PgConnection;

use intertextual::models::error::IntertextualError;

#[derive(Clap)]
pub struct RegistrationsParameters {
    /// The subcommand to use
    #[clap(subcommand)]
    command: RegistrationsSubCommand,
}

#[derive(Clap)]
enum RegistrationsSubCommand {
    /// Show the status of the registration handler
    Show,
    /// Disable all registrations until they are manually re-enabled
    Disable,
    /// Manually re-enable registrations, while conserving any previously set limit
    Enable,
    /// Change the parameters of the registration limits
    Limit(RegistrationsLimitParameters),
}

#[derive(Clap)]
struct RegistrationsLimitParameters {
    /// The subcommand to use
    #[clap(subcommand)]
    command: RegistrationsLimitSubCommand,
}

#[derive(Clap)]
enum RegistrationsLimitSubCommand {
    /// Manually re-enable registrations and remove all registration limits
    Remove,
    /// Manually re-enable registrations and set the limit to the given value
    SetLimit(RegistrationsSetLimitParameters),
}

#[derive(Clap)]
struct RegistrationsSetLimitParameters {
    /// New limit to use. Must be a positive number.
    pub new_limit: i64,
}

pub fn registrations(
    params: RegistrationsParameters,
    conn: &PgConnection,
) -> Result<(), IntertextualError> {
    use intertextual::actions::users::admin::registration_limit;
    use intertextual::models::admin::NewModerationAction;

    match params.command {
        RegistrationsSubCommand::Show => {
            let status = registration_limit::get_registration_limit(conn)?;
            match status {
                registration_limit::RegistrationLimit::RegistrationsDisabled => {
                    log::info!("Registrations are manually disabled")
                }
                registration_limit::RegistrationLimit::RegistrationsEnabledWithoutLimit => {
                    log::info!("Registrations are enabled without limit")
                }
                registration_limit::RegistrationLimit::RegistrationsEnabledWithLimit {
                    current_registrations,
                    hours,
                    max_registrations,
                } => log::info!(
                    "Registrations are enabled with a maximum of {} new users every {} hours. Currently : {} users ({}% of limit)",
                    max_registrations,
                    hours,
                    current_registrations,
                    (current_registrations * 100) / max_registrations
                ),
            }
        }
        RegistrationsSubCommand::Disable => {
            let action = NewModerationAction {
                username: "system".to_string(),
                action_type: "Manually disabled registrations".to_string(),
                message: "System action from command line".to_string(),
            };
            registration_limit::set_registration_limit(
                registration_limit::NewRegistrationLimit::RegistrationsDisabled,
                action,
                conn,
            )?;
            log::info!("Manually disabled all registrations");
        }
        RegistrationsSubCommand::Enable => {
            let action = NewModerationAction {
                username: "system".to_string(),
                action_type: "Manually re-enabled registrations".to_string(),
                message: "System action from command line".to_string(),
            };
            registration_limit::enable_registrations(action, conn)?;
            log::info!("Manually re-enabled all registrations");
        }
        RegistrationsSubCommand::Limit(params) => match params.command {
            RegistrationsLimitSubCommand::Remove => {
                let action = NewModerationAction {
                    username: "system".to_string(),
                    action_type: "Removed the registration limit".to_string(),
                    message: "System action from command line".to_string(),
                };
                registration_limit::set_registration_limit(
                    registration_limit::NewRegistrationLimit::RegistrationsEnabledWithoutLimit,
                    action,
                    conn,
                )?;
                log::info!("Removed the registration limit");
            }
            RegistrationsLimitSubCommand::SetLimit(params) => {
                let action = NewModerationAction {
                    username: "system".to_string(),
                    action_type: format!("Set the registration limit to {}", params.new_limit),
                    message: "System action from command line".to_string(),
                };
                registration_limit::set_registration_limit(
                    registration_limit::NewRegistrationLimit::RegistrationsEnabledWithLimit {
                        hours: 24,
                        max_registrations: params.new_limit,
                    },
                    action,
                    conn,
                )?;
                log::info!(
                    "Set the registration limit to {} users every 24 hours",
                    params.new_limit
                );
            }
        },
    }

    Ok(())
}
