use actix_web::{post, web, HttpResponse};
use serde::{Deserialize, Serialize};

use intertextual::actions;
use intertextual::data;
use intertextual::models;
use intertextual::models::error::IntertextualError;
use intertextual::models::filter::FilterMode;
use intertextual::models::shared::AppState;

use crate::app::identity::Identity;
use crate::app::is_unique_violation;
use crate::app::persistent::*;
use crate::error::*;

#[derive(Serialize, Deserialize)]
pub struct UserApprovalQuery {
    pub return_to_last_chapter: Option<String>,
    pub t: Option<String>,
}

#[post("/add/@{user}/{story}/")]
async fn handle_author_add_user_approval(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    url_params: web::Query<UserApprovalQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let (authors, story) = web::block({
        let mode = FilterMode::from_login_opt(&login);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    let return_to_last_chapter = url_params
        .return_to_last_chapter
        .filter(|s| s == "true")
        .is_some();
    handle_shared_add_user_approval(data, persistent, authors, story, return_to_last_chapter).await
}

#[post("/add/collaboration/{story}/")]
async fn handle_collaboration_add_user_approval(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<UserApprovalQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let (authors, story) = web::block({
        let mode = FilterMode::from_login_opt(&login);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    let return_to_last_chapter = url_params
        .return_to_last_chapter
        .filter(|s| s == "true")
        .is_some();
    handle_shared_add_user_approval(data, persistent, authors, story, return_to_last_chapter).await
}

#[post("/remove/@{user}/{story}/")]
async fn handle_author_remove_user_approval(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    url_params: web::Query<UserApprovalQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let (authors, story) = web::block({
        let mode = FilterMode::from_login_opt(&login);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    let return_to_last_chapter = url_params
        .return_to_last_chapter
        .filter(|s| s == "true")
        .is_some();
    handle_shared_remove_user_approval(data, persistent, authors, story, return_to_last_chapter)
        .await
}

#[post("/remove/collaboration/{story}/")]
async fn handle_collaboration_remove_user_approval(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<UserApprovalQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let (authors, story) = web::block({
        let mode = FilterMode::from_login_opt(&login);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    let return_to_last_chapter = url_params
        .return_to_last_chapter
        .filter(|s| s == "true")
        .is_some();
    handle_shared_remove_user_approval(data, persistent, authors, story, return_to_last_chapter)
        .await
}

async fn handle_shared_add_user_approval(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    return_to_last_chapter: bool,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();

    let story_id = story.id;
    let chapters = web::block({
        let filter_mode = FilterMode::from_login_opt(&login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::stories::find_chapters_metadata_by_story_id(story_id, &filter_mode, &conn)
    })
    .await
    .map_err_app(&persistent)?;
    let last_chapter = chapters
        .last()
        .ok_or_else(|| IntertextualError::CollaborationChapterNotFound {
            url_fragment: story.url_fragment.clone(),
            chapter_number: 0,
        })
        .map_err_app(&persistent)?;

    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;
    login_user
        .require_approval_rights_for(&authors, &story)
        .map_err_app(&persistent)?;

    let user_id = login_user.id;
    let story_id = story.id;
    let kudo_added_result = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::user_approvals::modifications::add_user_approval(user_id, story_id, &conn)
    })
    .await
    .map_err_app(&persistent);
    if is_unique_violation(&kudo_added_result) {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Snaps",
            message: "You already gave a snap to this story !".to_string(),
        }
        .into_app(&persistent));
    }

    let return_url = if return_to_last_chapter {
        format!(
            "/{}/{}/{}/{}#story-actions-list",
            story.author_path(&authors),
            story.url_fragment,
            last_chapter.chapter_number,
            PersistentTemplate::from(&persistent).as_leading_qmark()
        )
    } else {
        format!(
            "/{}/{}/{}#story-actions-list",
            story.author_path(&authors),
            story.url_fragment,
            PersistentTemplate::from(&persistent).as_leading_qmark()
        )
    };

    Ok(HttpResponse::SeeOther()
        .header("Location", return_url)
        .finish())
}

async fn handle_shared_remove_user_approval(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    return_to_last_chapter: bool,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();

    let story_id = story.id;
    let chapters = web::block({
        let filter_mode = FilterMode::from_login_opt(&login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::stories::find_chapters_metadata_by_story_id(story_id, &filter_mode, &conn)
    })
    .await
    .map_err_app(&persistent)?;
    let last_chapter = chapters
        .last()
        .ok_or_else(|| IntertextualError::CollaborationChapterNotFound {
            url_fragment: story.url_fragment.clone(),
            chapter_number: 0,
        })
        .map_err_app(&persistent)?;

    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;
    let user_id = login_user.id;
    let story_id = story.id;
    let _ignore_errors = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            actions::user_approvals::modifications::remove_user_approval(user_id, story_id, &conn)
        }
    })
    .await;

    let return_url = if return_to_last_chapter {
        format!(
            "/{}/{}/{}/{}#story-actions-list",
            story.author_path(&authors),
            story.url_fragment,
            last_chapter.chapter_number,
            PersistentTemplate::from(&persistent).as_leading_qmark()
        )
    } else {
        format!(
            "/{}/{}/{}#story-actions-list",
            story.author_path(&authors),
            story.url_fragment,
            PersistentTemplate::from(&persistent).as_leading_qmark()
        )
    };

    Ok(HttpResponse::SeeOther()
        .header("Location", return_url)
        .finish())
}
