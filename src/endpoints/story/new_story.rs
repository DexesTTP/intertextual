use std::str::FromStr;

use actix_web::{get, post, web, HttpResponse};
use askama::Template;
use futures::TryStreamExt;

use intertextual::actions::stories::modifications;
use intertextual::models;
use intertextual::models::stories::RecommendationsState;
use intertextual::utils::word_count::count_words_from_html;

use crate::app::multipart;
use crate::prelude::*;

#[derive(Template)]
#[template(path = "story/new_story.html")]
struct StorySubmitTemplate {
    persistent: PersistentTemplate,
    current_username: String,
    default_comments_enabled: bool,
    default_recommendations_enabled: RecommendationsState,
    default_foreword: SanitizedHtml,
    default_afterword: SanitizedHtml,
}

#[get("/submit/")]
async fn main_page(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let user_settings = login_user.get_settings();

    let current_username = login_user.username.clone();
    let s = StorySubmitTemplate {
        persistent: PersistentTemplate::from(&persistent),
        current_username,
        default_comments_enabled: user_settings.preferred_comments_enabled,
        default_recommendations_enabled: user_settings.preferred_recommendations_enabled,
        default_foreword: login_user.default_foreword.clone(),
        default_afterword: login_user.default_afterword.clone(),
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/submit/")]
async fn handle_submit(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<PersistentQuery>,
    mut payload: actix_multipart::Multipart,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let mut title: Option<String> = None;
    let mut url_fragment: Option<String> = None;
    let mut description: Option<RichTagline> = None;
    let mut foreword: SanitizedHtml = SanitizedHtml::new();
    let mut input_choice: Option<String> = None;
    let mut html_from_editor: SanitizedHtml = SanitizedHtml::new();
    let mut html_from_file: SanitizedHtml = SanitizedHtml::new();
    let mut afterword: SanitizedHtml = SanitizedHtml::new();
    let mut comment_mode: Option<String> = None;
    let mut recommendation_mode: Option<String> = None;
    let mut is_multi_chapter: Option<String> = None;
    let mut other_fields: Vec<(String, String)> = vec![];
    while let Ok(Some(mut field)) = payload.try_next().await {
        let content_disposition = field
            .content_disposition()
            .ok_or_else(|| IntertextualError::FormFieldFormatError {
                form_field_name: "Unknown",
                message: "The data sent to the server is invalid".to_string(),
            })
            .map_err_app(&persistent)?;
        if !content_disposition.is_form_data() {
            return Err(IntertextualError::FormFieldFormatError {
                form_field_name: "Unknown",
                message: "The data sent to the server is invalid".to_string(),
            }
            .into_app(&persistent));
        }

        let content_disposition_name = content_disposition
            .get_name()
            .ok_or_else(|| IntertextualError::FormFieldFormatError {
                form_field_name: "Unknown",
                message: "The data sent to the server is invalid".to_string(),
            })
            .map_err_app(&persistent)?;

        match content_disposition_name {
            "title" => {
                title = Some(
                    multipart::process_utf8_field_into_string(
                        &persistent,
                        &mut field,
                        multipart::FIELD_SIZE_LIMIT,
                    )
                    .await?,
                );
            }
            "url_fragment" => {
                url_fragment = Some(
                    multipart::process_utf8_field_into_string(
                        &persistent,
                        &mut field,
                        multipart::FIELD_SIZE_LIMIT,
                    )
                    .await?,
                );
            }
            "description" => {
                description = Some(RichTagline {
                    inner: multipart::process_utf8_field_into_string(
                        &persistent,
                        &mut field,
                        multipart::FIELD_SIZE_LIMIT,
                    )
                    .await?,
                });
            }
            "foreword" => {
                foreword = multipart::process_utf8_field_into_sanitized_html(
                    &persistent,
                    &mut field,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
            }
            "input_choice" => {
                input_choice = Some(
                    multipart::process_utf8_field_into_string(
                        &persistent,
                        &mut field,
                        multipart::FIELD_SIZE_LIMIT,
                    )
                    .await?,
                );
            }
            "file" => {
                html_from_file = multipart::process_file_stream_into_sanitized_html(
                    &persistent,
                    &mut field,
                    content_disposition,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
            }
            "content" => {
                html_from_editor = multipart::process_utf8_field_into_sanitized_html(
                    &persistent,
                    &mut field,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
            }
            "afterword" => {
                afterword = multipart::process_utf8_field_into_sanitized_html(
                    &persistent,
                    &mut field,
                    multipart::CONTENT_SIZE_LIMIT,
                )
                .await?;
            }
            "comment_mode" => {
                comment_mode = Some(
                    multipart::process_utf8_field_into_string(
                        &persistent,
                        &mut field,
                        multipart::FIELD_SIZE_LIMIT,
                    )
                    .await?,
                );
            }
            "recommendation_mode" => {
                recommendation_mode = Some(
                    multipart::process_utf8_field_into_string(
                        &persistent,
                        &mut field,
                        multipart::FIELD_SIZE_LIMIT,
                    )
                    .await?,
                );
            }
            "is_multi_chapter" => {
                is_multi_chapter = Some(
                    multipart::process_utf8_field_into_string(
                        &persistent,
                        &mut field,
                        multipart::FIELD_SIZE_LIMIT,
                    )
                    .await?,
                );
            }
            _ => {
                other_fields.push((
                    content_disposition_name.to_string(),
                    multipart::process_utf8_field_into_string(
                        &persistent,
                        &mut field,
                        multipart::FIELD_SIZE_LIMIT,
                    )
                    .await?,
                ));
            }
        }
    }

    let title = title
        .ok_or_else(|| IntertextualError::FormFieldFormatError {
            form_field_name: "Story Title",
            message: "This form field was missing from the input data".to_string(),
        })
        .map_err_app(&persistent)?;
    let url_fragment = url_fragment
        .ok_or_else(|| IntertextualError::FormFieldFormatError {
            form_field_name: "Story URL",
            message: "This form field was missing from the input data".to_string(),
        })
        .map_err_app(&persistent)?;
    let description = description
        .ok_or_else(|| IntertextualError::FormFieldFormatError {
            form_field_name: "Description",
            message: "This form field was missing from the input data".to_string(),
        })
        .map_err_app(&persistent)?;
    let comment_mode = comment_mode
        .ok_or_else(|| IntertextualError::FormFieldFormatError {
            form_field_name: "Comment mode",
            message: "This form field was missing from the input data".to_string(),
        })
        .map_err_app(&persistent)?;
    let recommendation_mode = recommendation_mode
        .ok_or_else(|| IntertextualError::FormFieldFormatError {
            form_field_name: "Recommendation mode",
            message: "This form field was missing from the input data".to_string(),
        })
        .map_err_app(&persistent)?;

    let content = match input_choice.as_deref() {
        Some("direct") => html_from_editor,
        Some("file") => html_from_file,
        _ => {
            return Err(IntertextualError::FormFieldFormatError {
                form_field_name: "Input mode",
                message: "An invalid input mode was selected".to_string(),
            }
            .into_app(&persistent));
        }
    };

    let comments_enabled = comment_mode.as_str() == "enable";
    let recommendations_enabled =
        models::stories::RecommendationsState::from_str(&recommendation_mode)
            .map_err_app(&persistent)?;
    let word_count = count_words_from_html(&content);
    crate::app::validation::validate_story_url(&url_fragment, "Story URL")
        .map_err_app(&persistent)?;
    crate::app::validation::validate_story_title(&title, "Title").map_err_app(&persistent)?;
    crate::app::validation::validate_story_description(&description, "Description")
        .map_err_app(&persistent)?;

    let is_multi_chapter = matches!(is_multi_chapter.as_deref(), Some("on"));

    let new_story = models::stories::NewSingleAuthorStory {
        author_id: login_user.id,
        title,
        url_fragment,
        description,
        foreword,
        content,
        afterword,
        recommendations_enabled,
        comments_enabled,
        word_count,
        is_multi_chapter,
    };
    let result = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || modifications::create_new_single_author_story(new_story, &conn)
    })
    .await
    .map_err_app(&persistent);

    if crate::app::is_unique_violation(&result) {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Story URL",
            message: "This story URL is already taken.".to_string(),
        }
        .into_app(&persistent));
    }
    let (story, _chapter) = result?;

    Ok(HttpResponse::SeeOther()
        .header(
            "Location",
            format!(
                "/{}/{}/0/settings_tags/?story_creation=true&confirm=new_story&{}#select_tags",
                story.author_path(std::slice::from_ref(login_user)),
                story.url_fragment,
                PersistentTemplate::from(&persistent).as_leading_amp()
            ),
        )
        .finish())
}
