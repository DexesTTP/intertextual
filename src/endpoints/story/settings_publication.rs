use actix_web::{get, post, web, HttpResponse};
use askama::Template;
use serde::{Deserialize, Serialize};

use intertextual::actions;
use intertextual::actions::stories::modifications;
use intertextual::data;
use intertextual::models;
use intertextual::models::error::IntertextualError;
use intertextual::models::filter::FilterMode;
use intertextual::models::shared::AppState;
use intertextual::utils::publication::get_publication_mode;
use intertextual::utils::publication::PublicationMode;

use crate::app::identity::Identity;
use crate::app::persistent::*;
use crate::error::*;

#[derive(Template)]
#[template(path = "story/publication.html")]
struct PublicationTemplate {
    persistent: PersistentTemplate,
    authors: Vec<models::users::User>,
    author_path: String,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
    is_standalone: bool,
    default_publication_mode: DefaultPublicationMode,
    next_publication_date: String,
    show_publication_confirm_box: bool,
    show_tag_edition_confirm_box: bool,
}

#[derive(Eq, PartialEq)]
pub enum DefaultPublicationMode {
    Now,
    Later,
    DoNotPublish,
}

#[derive(Serialize, Deserialize)]
pub struct PublicationSettingsQuery {
    pub t: Option<String>,
    pub confirm: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct PublicationParams {
    publication_mode: Option<String>,
    publication_time: Option<String>,
}

#[get("/@{user}/{story}/{chapter}/publication/")]
async fn main_page_author(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, i32)>,
    url_params: web::Query<PublicationSettingsQuery>,
) -> Result<HttpResponse, AppError> {
    let params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &params.t).await?;
    let login_user = persistent.login();

    let (username, url_fragment, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let mode = FilterMode::from_login_opt(&login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            data::stories::find_author_chapter_by_url(
                username,
                url_fragment,
                chapter_number,
                &mode,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    main_page_shared(data, persistent, authors, story, chapter, params.confirm).await
}

#[get("/collaboration/{story}/{chapter}/publication/")]
async fn main_page_collaboration(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, i32)>,
    url_params: web::Query<PublicationSettingsQuery>,
) -> Result<HttpResponse, AppError> {
    let params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &params.t).await?;
    let login_user = persistent.login();

    let (url_fragment, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let mode = FilterMode::from_login_opt(&login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            data::stories::find_collaboration_chapter_by_url(
                url_fragment,
                chapter_number,
                &mode,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    main_page_shared(data, persistent, authors, story, chapter, params.confirm).await
}

async fn main_page_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
    confirm: Option<String>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        let story_id = story.id;
        let user_id = login_user.id;
        move || actions::access::require_management_rights_for(story_id, user_id, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    let story_id = story.id;
    let is_standalone = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        let filter_mode = FilterMode::from_login(login_user);
        move || actions::stories::story_is_standalone(story_id, &filter_mode, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    let user_settings = login_user.get_settings();
    let default_publication_mode = if chapter.is_published() {
        DefaultPublicationMode::Now
    } else {
        match user_settings.preferred_publication_mode {
            models::users::PreferredPublicationMode::PublishNow => DefaultPublicationMode::Now,
            models::users::PreferredPublicationMode::PublishLater => DefaultPublicationMode::Later,
            models::users::PreferredPublicationMode::DoNotPublish => {
                DefaultPublicationMode::DoNotPublish
            }
        }
    };
    let default_publication_date =
        user_settings.get_next_publication_date(chrono::Utc::now().naive_utc());

    let author_path = story.author_path(&authors);
    let s = PublicationTemplate {
        persistent: PersistentTemplate::from(&persistent),
        authors,
        author_path,
        story,
        chapter,
        is_standalone,
        default_publication_mode,
        next_publication_date: default_publication_date
            .format("%Y-%m-%d %H:%M")
            .to_string(),
        show_publication_confirm_box: matches!(&confirm, Some(v) if v == "publication"),
        show_tag_edition_confirm_box: matches!(&confirm, Some(v) if v == "tag_edition"),
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/@{user}/{story}/{chapter}/publication/")]
async fn handle_chapter_publication_author(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, i32)>,
    form: web::Form<PublicationParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();

    let (username, url_fragment, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let mode = FilterMode::from_login_opt(&login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            data::stories::find_author_chapter_by_url(
                username,
                url_fragment,
                chapter_number,
                &mode,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    handle_chapter_publication_shared(data, persistent, authors, story, chapter, form).await
}

#[post("/collaboration/{story}/{chapter}/publication/")]
async fn handle_chapter_publication_collaboration(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, i32)>,
    form: web::Form<PublicationParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();

    let (url_fragment, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let mode = FilterMode::from_login_opt(&login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            data::stories::find_collaboration_chapter_by_url(
                url_fragment,
                chapter_number,
                &mode,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    handle_chapter_publication_shared(data, persistent, authors, story, chapter, form).await
}

async fn handle_chapter_publication_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
    form: web::Form<PublicationParams>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;
    let form = form.into_inner();

    web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        let story_id = story.id;
        let user_id = login_user.id;
        move || actions::access::require_management_rights_for(story_id, user_id, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    let publication_mode = form
        .publication_mode
        .ok_or_else(|| IntertextualError::FormFieldFormatError {
            form_field_name: "Publication mode",
            message: "This form field was missing from the input data".to_string(),
        })
        .map_err_app(&persistent)?;
    let publication_time = form
        .publication_time
        .ok_or_else(|| IntertextualError::FormFieldFormatError {
            form_field_name: "Publication time",
            message: "This form field was missing from the input data".to_string(),
        })
        .map_err_app(&persistent)?;

    let publication =
        get_publication_mode(&publication_mode, &publication_time).map_err_app(&persistent)?;

    let return_url = format!(
        "/{}/{}/{}/?confirm=publication{}",
        story.author_path(&authors),
        story.url_fragment,
        chapter.chapter_number,
        PersistentTemplate::from(&persistent).as_leading_amp()
    );

    match publication {
        PublicationMode::PublishNow => {
            let publication_date = chrono::Utc::now().naive_utc();
            web::block({
                let conn = data.pool.get().map_err_app(&persistent)?;
                move || modifications::set_public_after(chapter, Some(publication_date), &conn)
            })
            .await
            .map_err_app(&persistent)?;
        }
        PublicationMode::PublishLater(publication_date) => {
            web::block({
                let conn = data.pool.get().map_err_app(&persistent)?;
                move || modifications::set_public_after(chapter, Some(publication_date), &conn)
            })
            .await
            .map_err_app(&persistent)?;
        }
        PublicationMode::DoNotPublish => {
            web::block({
                let conn = data.pool.get().map_err_app(&persistent)?;
                move || modifications::set_public_after(chapter, None, &conn)
            })
            .await
            .map_err_app(&persistent)?;
        }
    }

    Ok(HttpResponse::SeeOther()
        .header("Location", return_url)
        .finish())
}
