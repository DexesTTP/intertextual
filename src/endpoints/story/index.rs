use actix_web::{get, web, HttpRequest, HttpResponse};
use askama::Template;
use log::error;
use serde::{Deserialize, Serialize};

use intertextual::actions;
use intertextual::data;
use intertextual::models;
use intertextual::models::filter::FilterMode;
use intertextual::models::stories::StoryEntry;
use intertextual::models::stories::StoryQueryExtraInfos;
use intertextual::utils::hits::should_add_hit;
use intertextual::utils::page_list::PageListing;
use intertextual::utils::text_trim::limit_bytes_with_ellipses_if_needed;

use crate::prelude::*;

const COMMENTS_PER_PAGE: i64 = 20;

#[derive(Template)]
#[template(path = "story/index.html")]
struct StoryIndexTemplate {
    persistent: PersistentTemplate,
    story_entry: StoryEntry,
    story: models::stories::Story,
    chapters: Vec<models::stories::Chapter>,
    can_edit: bool,
    can_manage: bool,
    is_administrator: bool,
    can_see_recommendations: bool,
    can_see_recommendations_bypass: bool,
    can_write_recommendation: bool,
    recommendations_count: usize,
    user_approval_status: UserApprovalStatus,
    follow_status: FollowStatus,
    can_report_story: bool,
    has_reported_story: bool,
    report_chapter_number: i32,
    w3cdtf_formatted_first_publication: String,
    publication_year: String,
    current_year: String,
    trimmed_description_for_card: String,
    show_edit_chapter_confirm_box: bool,
    show_new_chapter_confirm_box: bool,
    show_delete_chapter_confirm_box: bool,
    show_publication_confirm_box: bool,
    show_tag_edition_confirm_box: bool,
}

#[derive(Template)]
#[template(path = "story/chapter.html")]
struct ChapterTemplate {
    persistent: PersistentTemplate,
    story_entry: StoryEntry,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
    can_edit: bool,
    can_manage: bool,
    is_administrator: bool,
    is_standalone: bool,
    previous_chapter: Option<models::stories::Chapter>,
    next_chapter: Option<models::stories::Chapter>,
    can_see_recommendations: bool,
    can_see_recommendations_bypass: bool,
    can_write_recommendation: bool,
    recommendations_count: usize,
    user_approval_status: UserApprovalStatus,
    follow_status: FollowStatus,
    can_report_chapter: bool,
    has_reported_chapter: bool,
    publication_year: String,
    current_year: String,
    trimmed_description_for_card: String,
    comment_data: CommentData,
    show_edit_chapter_confirm_box: bool,
    show_new_chapter_confirm_box: bool,
    show_delete_chapter_confirm_box: bool,
    show_publication_confirm_box: bool,
    show_tag_edition_confirm_box: bool,
}

pub struct CommentData {
    comments_enabled: bool,
    comments_enabled_bypass: bool,
    can_write_comment: bool,
    default_comment_text: Option<String>,
    show_comments: bool,
    total_comment_count: i64,
    comments: Vec<CommentEntry>,
    page_listing: PageListing,
}

pub struct CommentEntry {
    id: uuid::Uuid,
    user_username: String,
    user_display_name: String,
    created: String,
    edited: Option<String>,
    formatted_comment: SanitizedHtml,
    can_edit: bool,
    can_reply: bool,
    can_delete: bool,
    is_already_reported: bool,
    can_report: bool,
}

pub struct UserApprovalStatus {
    show: bool,
    is_logged_out: bool,
    user_can_approve: bool,
    user_has_given_approval: bool,
    approval_count: i64,
}

impl UserApprovalStatus {
    pub async fn new(
        data: &AppState,
        persistent: &PersistentData,
        show: bool,
        authors: &[models::users::User],
        story_id: uuid::Uuid,
        user: Option<&models::users::User>,
    ) -> Result<UserApprovalStatus, AppError> {
        match user {
            Some(user) => {
                let user_id = user.id;
                let (approval_count, user_has_given_approval) = web::block({
                    let conn = data.pool.get().map_err_app(persistent)?;
                    move || -> Result<(i64, bool), IntertextualError> {
                        Ok((
                            actions::user_approvals::total_user_approval_count_for_story(
                                story_id, &conn,
                            )?,
                            actions::user_approvals::has_given_user_approval(
                                user_id, story_id, &conn,
                            )?,
                        ))
                    }
                })
                .await
                .map_err_app(persistent)?;
                Ok(UserApprovalStatus {
                    show,
                    is_logged_out: false,
                    user_can_approve: authors.iter().all(|a| a.id != user_id),
                    user_has_given_approval,
                    approval_count,
                })
            }
            None => {
                let approval_count = web::block({
                    let conn = data.pool.get().map_err_app(persistent)?;
                    move || {
                        actions::user_approvals::total_user_approval_count_for_story(
                            story_id, &conn,
                        )
                    }
                })
                .await
                .map_err_app(persistent)?;
                Ok(UserApprovalStatus {
                    show,
                    is_logged_out: true,
                    user_can_approve: false,
                    user_has_given_approval: false,
                    approval_count,
                })
            }
        }
    }
}

pub struct FollowStatus {
    show: bool,
    user_is_following_author: bool,
    user_is_following_story: bool,
}

impl FollowStatus {
    pub async fn new(
        data: &AppState,
        persistent: &PersistentData,
        show: bool,
        authors: &[models::users::User],
        story_id: uuid::Uuid,
        user: Option<&models::users::User>,
    ) -> Result<FollowStatus, AppError> {
        match user {
            Some(user) => {
                let user_id = user.id;
                match authors.first() {
                    Some(author) => {
                        let author_id = author.id;
                        let (user_is_following_author, user_is_following_story) = web::block({
                            let conn = data.pool.get().map_err_app(persistent)?;
                            move || -> Result<(bool, bool), IntertextualError> {
                                Ok((
                                    actions::follows::is_following_author(
                                        user_id, author_id, &conn,
                                    )?,
                                    actions::follows::is_following_story(user_id, story_id, &conn)?,
                                ))
                            }
                        })
                        .await
                        .map_err_app(persistent)?;
                        Ok(FollowStatus {
                            show: show && authors.iter().all(|a| a.id != user_id),
                            user_is_following_author,
                            user_is_following_story,
                        })
                    }
                    None => Ok(FollowStatus {
                        show: show && authors.iter().all(|a| a.id != user_id),
                        user_is_following_author: false,
                        user_is_following_story: false,
                    }),
                }
            }
            None => Ok(FollowStatus {
                show: false,
                user_is_following_author: false,
                user_is_following_story: false,
            }),
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct StoryPageQuery {
    pub t: Option<String>,
    pub confirm: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct ChapterPageQuery {
    pub t: Option<String>,
    pub confirm: Option<String>,
    pub comments_start: Option<String>,
    pub reply_to: Option<String>,
}

#[get("/@{user}/{story}/")]
async fn author_story_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    url_params: web::Query<StoryPageQuery>,
) -> Result<HttpResponse, AppError> {
    let params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &params.t).await?;
    let login_user = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let (authors, story) = web::block({
        let mode = FilterMode::from_login_opt(&login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    story_page_shared(request, data, persistent, authors, story, params.confirm).await
}

#[get("/collaboration/{story}/")]
async fn collaboration_story_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<StoryPageQuery>,
) -> Result<HttpResponse, AppError> {
    let params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &params.t).await?;
    let login_user = persistent.login();

    let url_fragment = path.into_inner();
    let (authors, story) = web::block({
        let mode = FilterMode::from_login_opt(&login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    story_page_shared(request, data, persistent, authors, story, params.confirm).await
}

#[get("/@{user}/{story}/{chapter}/")]
async fn author_chapter_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, i32)>,
    url_params: web::Query<ChapterPageQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let login_user = persistent.login();

    let (username, url_fragment, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let mode = FilterMode::from_login_opt(&login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            data::stories::find_author_chapter_by_url(
                username,
                url_fragment,
                chapter_number,
                &mode,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    chapter_page_shared(
        request,
        data,
        &persistent,
        authors,
        story,
        chapter,
        url_params,
    )
    .await
}

#[get("/collaboration/{story}/{chapter}/")]
async fn collaboration_chapter_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, i32)>,
    url_params: web::Query<ChapterPageQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let login_user = persistent.login();

    let (url_fragment, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let mode = FilterMode::from_login_opt(&login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            data::stories::find_collaboration_chapter_by_url(
                url_fragment,
                chapter_number,
                &mode,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    chapter_page_shared(
        request,
        data,
        &persistent,
        authors,
        story,
        chapter,
        url_params,
    )
    .await
}

async fn story_page_shared(
    request: HttpRequest,
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    confirm: Option<String>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let filter_mode = FilterMode::from_login_opt(&login_user);

    let story_id = story.id;
    let url_fragment = story.url_fragment.clone();
    let mut chapters = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        let filter_mode = filter_mode.clone();
        move || actions::stories::find_chapters_by_story_id(story_id, &filter_mode, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    let recommendations = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::recommendations::find_recommendations_by_story(story_id, &conn)
    })
    .await
    .map_err_app(&persistent)?;
    let recommendations_count = recommendations.len();
    let can_see_recommendations =
        models::stories::RecommendationsState::Enabled.matches(&story.recommendations_enabled);
    let can_write_recommendation = match login_user {
        None => false,
        Some(login_user) => {
            login_user.can_write_recommendation_for(&authors, &story, &recommendations)
        }
    };
    let displayed_chapter_count = chapters.len();
    let user_approval_status =
        UserApprovalStatus::new(&data, &persistent, true, &authors, story_id, login_user).await?;
    let follow_status =
        FollowStatus::new(&data, &persistent, true, &authors, story_id, login_user).await?;

    let (can_edit, can_manage) = match login_user {
        Some(login_user) => web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            let story_id = story.id;
            let user_id = login_user.id;
            move || -> Result<(bool, bool), IntertextualError> {
                let creative_role = actions::access::get_creative_role(story_id, user_id, &conn)?;
                Ok((creative_role.can_edit(), creative_role.can_manage()))
            }
        })
        .await
        .map_err_app(&persistent)?,
        None => (false, false),
    };
    let is_administrator = login_user.iter().any(|u| u.administrator);
    let can_see_recommendations_bypass = can_manage || is_administrator;
    if displayed_chapter_count == 0 {
        return Err(match (story.is_collaboration, authors.as_slice()) {
            (false, [author]) => IntertextualError::AuthorStoryNotFound {
                url_fragment,
                author_username: author.username.clone(),
            },
            _ => IntertextualError::CollaborationStoryNotFound { url_fragment },
        }
        .into_app(&persistent));
    }

    let (can_report_chapter, has_reported_chapter, report_chapter_number) =
        match (chapters.last(), login_user) {
            (Some(chapter), Some(user)) => {
                let has_reported_chapter = web::block({
                    let user_id = user.id;
                    let chapter_id = chapter.id;
                    let conn = data.pool.get().map_err_app(&persistent)?;
                    move || actions::reports::user_has_reported_chapter(user_id, chapter_id, &conn)
                })
                .await
                .map_err_app(&persistent)?;
                (
                    !has_reported_chapter,
                    has_reported_chapter,
                    chapter.chapter_number,
                )
            }
            _ => (false, false, 1),
        };

    let story_entry = web::block({
        let user_info = StoryQueryExtraInfos::from_maybe_user(&data.site, &login_user);
        let story = story.clone();
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || StoryEntry::from_database(story, &user_info, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    if displayed_chapter_count == 1 && !story.is_multi_chapter {
        let chapter = chapters.pop();
        let chapter = match chapter {
            Some(chapter) => chapter,
            None => {
                error!(
                    "story={} Displaying one chapter but could not unwrap the first chapter.",
                    story.id
                );
                return Err(IntertextualError::InternalServerError.into_app(&persistent));
            }
        };
        if should_add_hit(request, &login_user, &authors, &chapter, &data.pool) {
            let chapter_id = chapter.id;
            web::block({
                let conn = data.pool.get().map_err_app(&persistent)?;
                move || actions::hits::modifications::add_hit(chapter_id, &conn)
            })
            .await
            .map_err_app(&persistent)?;
        }
        let publication_year = chapter
            .official_creation_date
            .unwrap_or_else(|| chrono::Utc::now().naive_utc())
            .format("%Y")
            .to_string();
        let current_year = chrono::Utc::now().naive_utc().format("%Y").to_string();
        let trimmed_description_for_card =
            limit_bytes_with_ellipses_if_needed(&story.description.inner, 200);
        let comment_data = get_comment_data(
            data,
            &persistent,
            &authors,
            &story,
            &chapter,
            login_user,
            None,
            None,
        )
        .await?;
        let s = ChapterTemplate {
            persistent: PersistentTemplate::from(&persistent),
            story_entry,
            story,
            chapter,
            can_edit,
            can_manage,
            is_administrator,
            is_standalone: true,
            previous_chapter: None,
            next_chapter: None,
            user_approval_status,
            follow_status,
            can_see_recommendations,
            can_see_recommendations_bypass,
            can_write_recommendation,
            recommendations_count,
            can_report_chapter,
            has_reported_chapter,
            publication_year,
            current_year,
            trimmed_description_for_card,
            comment_data,
            show_edit_chapter_confirm_box: matches!(&confirm, Some(v) if v == "edit_chapter"),
            show_new_chapter_confirm_box: matches!(&confirm, Some(v) if v == "new_chapter"),
            show_delete_chapter_confirm_box: matches!(&confirm, Some(v) if v == "delete_chapter"),
            show_publication_confirm_box: matches!(&confirm, Some(v) if v == "publication"),
            show_tag_edition_confirm_box: matches!(&confirm, Some(v) if v == "tag_edition"),
        }
        .render()
        .map_err_app(&persistent)?;
        Ok(HttpResponse::Ok().content_type("text/html").body(s))
    } else {
        let first_publication = chapters
            .get(0)
            .and_then(|c| c.official_creation_date)
            .unwrap_or_else(|| chrono::Utc::now().naive_utc());
        let w3cdtf_formatted_first_publication = first_publication.format("%Y-%m-%d").to_string();
        let publication_year = first_publication.format("%Y").to_string();
        let current_year = chrono::Utc::now().naive_utc().format("%Y").to_string();
        let trimmed_description_for_card =
            limit_bytes_with_ellipses_if_needed(&story.description.inner, 200);
        let s = StoryIndexTemplate {
            persistent: PersistentTemplate::from(&persistent),
            story_entry,
            story,
            chapters,
            can_edit,
            can_manage,
            is_administrator,
            can_see_recommendations,
            can_see_recommendations_bypass,
            can_write_recommendation,
            recommendations_count,
            user_approval_status,
            follow_status,
            can_report_story: can_report_chapter,
            has_reported_story: has_reported_chapter,
            report_chapter_number,
            w3cdtf_formatted_first_publication,
            publication_year,
            current_year,
            trimmed_description_for_card,
            show_edit_chapter_confirm_box: matches!(&confirm, Some(v) if v == "edit_chapter"),
            show_new_chapter_confirm_box: matches!(&confirm, Some(v) if v == "new_chapter"),
            show_delete_chapter_confirm_box: matches!(&confirm, Some(v) if v == "delete_chapter"),
            show_publication_confirm_box: matches!(&confirm, Some(v) if v == "publication"),
            show_tag_edition_confirm_box: matches!(&confirm, Some(v) if v == "tag_edition"),
        }
        .render()
        .map_err_app(&persistent)?;
        Ok(HttpResponse::Ok().content_type("text/html").body(s))
    }
}

async fn chapter_page_shared(
    request: HttpRequest,
    data: web::Data<AppState>,
    persistent: &PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
    url_params: ChapterPageQuery,
) -> Result<HttpResponse, AppError> {
    let comments_start = url_params.comments_start;
    let reply_to = url_params.reply_to;
    let confirm = url_params.confirm;
    let login_user = persistent.login();
    let filter_mode = FilterMode::from_login_opt(&login_user);

    let chapter_number = chapter.chapter_number;
    let story_id = story.id;
    let (
        is_standalone,
        previous_chapter,
        next_chapter
    ) = web::block({
        let conn = data.pool.get().map_err_app(persistent)?;
        let filter_mode = filter_mode.clone();
        move || -> Result<(bool, Option<models::stories::Chapter>, Option<models::stories::Chapter>), IntertextualError> {
            Ok((
                actions::stories::story_is_standalone(story_id, &filter_mode, &conn)?,
                actions::stories::find_previous_chapter(story_id, chapter_number, &filter_mode, &conn)?,
                actions::stories::find_next_chapter(story_id, chapter_number, &filter_mode, &conn)?,
            ))
        }
    })
    .await.map_err_app(persistent)?;

    if should_add_hit(request, &login_user, &authors, &chapter, &data.pool) {
        let chapter_id = chapter.id;
        web::block({
            let conn = data.pool.get().map_err_app(persistent)?;
            move || actions::hits::modifications::add_hit(chapter_id, &conn)
        })
        .await
        .map_err_app(persistent)?;
    }

    let (can_write_recommendation, recommendations_count) = match next_chapter {
        Some(_) => (false, 0),
        None => {
            let recommendations = web::block({
                let conn = data.pool.get().map_err_app(persistent)?;
                move || actions::recommendations::find_recommendations_by_story(story_id, &conn)
            })
            .await
            .map_err_app(persistent)?;
            let recommendations_count = recommendations.len();
            let can_write_recommendation = match login_user {
                None => false,
                Some(login_user) => {
                    login_user.can_write_recommendation_for(&authors, &story, &recommendations)
                }
            };
            (can_write_recommendation, recommendations_count)
        }
    };
    let can_see_recommendations =
        models::stories::RecommendationsState::Enabled.matches(&story.recommendations_enabled);

    let (can_report_chapter, has_reported_chapter) = match login_user {
        Some(user) => {
            let has_reported_chapter = web::block({
                let user_id = user.id;
                let chapter_id = chapter.id;
                let conn = data.pool.get().map_err_app(persistent)?;
                move || actions::reports::user_has_reported_chapter(user_id, chapter_id, &conn)
            })
            .await
            .map_err_app(persistent)?;
            (!has_reported_chapter, has_reported_chapter)
        }
        None => (false, false),
    };

    let is_last_chapter = next_chapter.is_none();
    let user_approval_status = UserApprovalStatus::new(
        &data,
        persistent,
        is_last_chapter,
        &authors,
        story_id,
        login_user,
    )
    .await?;
    let follow_status = FollowStatus::new(
        &data,
        persistent,
        is_last_chapter,
        &authors,
        story_id,
        login_user,
    )
    .await?;
    let (can_edit, can_manage) = match login_user {
        Some(login_user) => web::block({
            let conn = data.pool.get().map_err_app(persistent)?;
            let story_id = story.id;
            let user_id = login_user.id;
            move || -> Result<(bool, bool), IntertextualError> {
                let creative_role = actions::access::get_creative_role(story_id, user_id, &conn)?;
                Ok((creative_role.can_edit(), creative_role.can_manage()))
            }
        })
        .await
        .map_err_app(persistent)?,
        None => (false, false),
    };
    let is_administrator = login_user.iter().any(|u| u.administrator);
    let can_see_recommendations_bypass = can_manage || is_administrator;
    let publication_year = chapter
        .official_creation_date
        .unwrap_or_else(|| chrono::Utc::now().naive_utc())
        .format("%Y")
        .to_string();
    let current_year = chrono::Utc::now().naive_utc().format("%Y").to_string();
    let trimmed_description_for_card =
        limit_bytes_with_ellipses_if_needed(&story.description.inner, 200);
    let story_entry = web::block({
        let user_info = StoryQueryExtraInfos::from_maybe_user(&data.site, &login_user);
        let story = story.clone();
        let conn = data.pool.get().map_err_app(persistent)?;
        move || StoryEntry::from_database(story, &user_info, &conn)
    })
    .await
    .map_err_app(persistent)?;
    let comment_data = get_comment_data(
        data,
        persistent,
        &authors,
        &story,
        &chapter,
        login_user,
        comments_start,
        reply_to.map(|u| format!("@{} ", u)),
    )
    .await?;
    let s = ChapterTemplate {
        persistent: PersistentTemplate::from(persistent),
        story_entry,
        story,
        chapter,
        can_edit,
        can_manage,
        is_administrator,
        is_standalone,
        previous_chapter,
        next_chapter,
        user_approval_status,
        follow_status,
        can_see_recommendations,
        can_see_recommendations_bypass,
        can_write_recommendation,
        recommendations_count,
        can_report_chapter,
        has_reported_chapter,
        publication_year,
        current_year,
        trimmed_description_for_card,
        comment_data,
        show_edit_chapter_confirm_box: matches!(&confirm, Some(v) if v == "edit_chapter"),
        show_new_chapter_confirm_box: matches!(&confirm, Some(v) if v == "new_chapter"),
        show_delete_chapter_confirm_box: matches!(&confirm, Some(v) if v == "delete_chapter"),
        show_publication_confirm_box: matches!(&confirm, Some(v) if v == "publication"),
        show_tag_edition_confirm_box: matches!(&confirm, Some(v) if v == "tag_edition"),
    }
    .render()
    .map_err_app(persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[allow(clippy::too_many_arguments)]
async fn get_comment_data(
    data: web::Data<AppState>,
    persistent: &PersistentData,
    authors: &[models::users::User],
    story: &intertextual::models::stories::Story,
    chapter: &intertextual::models::stories::Chapter,
    login_user: Option<&intertextual::models::users::User>,
    comments_start: Option<String>,
    default_comment_text: Option<String>,
) -> Result<CommentData, AppError> {
    let comments_enabled = story.comments_enabled;
    let comments_enabled_bypass = match login_user {
        Some(login_user) => login_user.require_see_comments_for(authors, story).is_ok(),
        _ => false,
    };

    if !comments_enabled && !comments_enabled_bypass {
        return Ok(CommentData {
            comments_enabled,
            comments_enabled_bypass,
            can_write_comment: false,
            default_comment_text,
            total_comment_count: 0,
            show_comments: false,
            comments: vec![],
            page_listing: PageListing::empty(),
        });
    }

    let can_write_comment = login_user
        .filter(|&l| l.can_write_comment_for(authors, story))
        .is_some();
    let total_comment_count = web::block({
        let chapter_id = chapter.id;
        let conn = data.pool.get().map_err_app(persistent)?;
        move || actions::comments::find_comment_quantity_for_chapter(chapter_id, &conn)
    })
    .await
    .map_err_app(persistent)?;

    if comments_start.is_none() {
        return Ok(CommentData {
            comments_enabled,
            comments_enabled_bypass,
            can_write_comment,
            default_comment_text,
            total_comment_count,
            show_comments: false,
            comments: vec![],
            page_listing: PageListing::empty(),
        });
    }

    let start_index = comments_start
        .and_then(|s| s.parse::<i64>().ok())
        .unwrap_or(0);
    let comment_list = web::block({
        let chapter_id = chapter.id;
        let conn = data.pool.get().map_err_app(persistent)?;
        move || {
            actions::comments::find_comments_for_chapter(
                chapter_id,
                start_index,
                COMMENTS_PER_PAGE,
                &conn,
            )
        }
    })
    .await
    .map_err_app(persistent)?;

    let mut comments: Vec<CommentEntry> = vec![];
    comments.reserve(comment_list.len());
    for (comment, user) in comment_list {
        let can_edit = match login_user {
            Some(login_user) => login_user
                .require_edit_comment_rights_for(authors, story, &user)
                .is_ok(),
            _ => false,
        };
        let can_reply = match login_user {
            Some(login_user) => login_user.id != user.id,
            _ => false,
        };
        let can_delete = match login_user {
            Some(login_user) => login_user
                .require_delete_comment_rights_for(authors, story, &user)
                .is_ok(),
            _ => false,
        };
        let can_report = match login_user {
            Some(login_user) => login_user
                .require_report_comment_rights_for(authors, story, &user)
                .is_ok(),
            _ => false,
        };
        let is_already_reported = match login_user {
            Some(login_user) => web::block({
                let login_user_id = login_user.id;
                let comment_id = comment.id;
                let conn = data.pool.get().map_err_app(persistent)?;
                move || {
                    actions::reports::user_has_reported_comment(login_user_id, comment_id, &conn)
                }
            })
            .await
            .map_err_app(persistent)?,
            _ => false,
        };
        let formatted_comment = richblock_to_html(&comment.message, &data.pool, persistent).await?;
        comments.push(CommentEntry {
            id: comment.id,
            created: comment
                .created
                .format("%Y-%m-%d at %H:%M (UTC+00)")
                .to_string(),
            edited: comment
                .updated
                .map(|t| t.format("%Y-%m-%d at %H:%M (UTC+00)").to_string()),
            user_username: user.username,
            user_display_name: user.display_name,
            formatted_comment,
            can_edit,
            can_reply,
            can_delete,
            is_already_reported,
            can_report,
        });
    }

    let page_listing =
        PageListing::get_from_count(total_comment_count, start_index, COMMENTS_PER_PAGE);
    Ok(CommentData {
        comments_enabled,
        comments_enabled_bypass,
        can_write_comment,
        default_comment_text,
        total_comment_count,
        show_comments: true,
        comments,
        page_listing,
    })
}
