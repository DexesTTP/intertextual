const COMMENTS_PER_PAGE: i64 = 20;

pub mod list {
    use actix_web::{get, web, HttpResponse};
    use askama::Template;
    use serde::{Deserialize, Serialize};

    use intertextual::actions;
    use intertextual::models;
    use intertextual::utils::page_list::PageListing;

    use crate::prelude::*;

    use super::COMMENTS_PER_PAGE;

    #[derive(Template)]
    #[template(path = "story/comments.html")]
    struct CommentsTemplate {
        persistent: PersistentTemplate,
        authors: Vec<models::users::ShortUserEntry>,
        author_path: String,
        story: models::stories::Story,
        chapter: models::stories::Chapter,
        comments: Vec<CommentEntry>,
        page_listing: PageListing,
        can_write_comment: bool,
        is_administrator: bool,
    }

    pub struct CommentEntry {
        id: uuid::Uuid,
        user_username: String,
        user_display_name: String,
        created: String,
        edited: Option<String>,
        formatted_comment: SanitizedHtml,
        can_edit: bool,
        can_reply: bool,
        can_delete: bool,
        is_already_reported: bool,
        can_report: bool,
    }

    #[derive(Serialize, Deserialize)]
    struct CommentsQueryParam {
        start: Option<String>,
        t: Option<String>,
    }

    #[get("/@{author}/{story}/{chapter}/comments/")]
    async fn get_author(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(String, String, i32)>,
        url_params: web::Query<CommentsQueryParam>,
    ) -> Result<HttpResponse, AppError> {
        let url_params = url_params.into_inner();
        let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
        let login_user = persistent.login();
        let filter_mode = intertextual::models::filter::FilterMode::from_login_opt(&login_user);

        let (username, url_fragment, chapter_number) = path.into_inner();
        let (authors, story, chapter) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                intertextual::data::stories::find_author_chapter_by_url(
                    username,
                    url_fragment,
                    chapter_number,
                    &filter_mode,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        get_shared(data, persistent, authors, story, chapter, url_params.start).await
    }

    #[get("/collaboration/{story}/{chapter}/comments/")]
    async fn get_collaboration(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(String, i32)>,
        url_params: web::Query<CommentsQueryParam>,
    ) -> Result<HttpResponse, AppError> {
        let url_params = url_params.into_inner();
        let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
        let login_user = persistent.login();
        let filter_mode = intertextual::models::filter::FilterMode::from_login_opt(&login_user);

        let (url_fragment, chapter_number) = path.into_inner();
        let (authors, story, chapter) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                intertextual::data::stories::find_collaboration_chapter_by_url(
                    url_fragment,
                    chapter_number,
                    &filter_mode,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        get_shared(data, persistent, authors, story, chapter, url_params.start).await
    }

    async fn get_shared(
        data: web::Data<AppState>,
        persistent: PersistentData,
        authors: Vec<models::users::User>,
        story: models::stories::Story,
        chapter: models::stories::Chapter,
        start: Option<String>,
    ) -> Result<HttpResponse, AppError> {
        let login_user = persistent.login();

        if !story.comments_enabled {
            // Check if the current user can see the comments
            match login_user {
                Some(login_user) => login_user
                    .require_see_comments_for(&authors, &story)
                    .map_err_app(&persistent)?,
                None => {
                    return Err(
                        IntertextualError::AccessRightsRequiredObfuscated.into_app(&persistent)
                    )
                }
            }
        }

        let start_index = start.and_then(|s| s.parse::<i64>().ok()).unwrap_or(0);

        let chapter_id = chapter.id;
        let (comment_list, comment_count) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || -> Result<(Vec<(models::comments::Comment, models::users::User)>, i64), IntertextualError> {
                Ok((
                    actions::comments::find_comments_for_chapter(chapter_id, start_index, COMMENTS_PER_PAGE, &conn)?,
                    actions::comments::find_comment_quantity_for_chapter(chapter_id, &conn)?,
                ))
            }
        })
        .await.map_err_app(&persistent)?;

        let page_listing =
            PageListing::get_from_count(comment_count, start_index, COMMENTS_PER_PAGE);

        let mut comments: Vec<CommentEntry> = vec![];
        comments.reserve(comment_list.len());
        for (comment, user) in comment_list {
            let can_edit = match login_user {
                Some(login_user) => login_user
                    .require_edit_comment_rights_for(&authors, &story, &user)
                    .is_ok(),
                _ => false,
            };
            let can_reply = match login_user {
                Some(login_user) => login_user.id != user.id,
                _ => false,
            };
            let can_delete = match login_user {
                Some(login_user) => login_user
                    .require_delete_comment_rights_for(&authors, &story, &user)
                    .is_ok(),
                _ => false,
            };
            let can_report = match login_user {
                Some(login_user) => login_user
                    .require_report_comment_rights_for(&authors, &story, &user)
                    .is_ok(),
                _ => false,
            };
            let is_already_reported = match login_user {
                Some(login_user) => {
                    let login_user_id = login_user.id;
                    let comment_id = comment.id;
                    web::block({
                        let conn = data.pool.get().map_err_app(&persistent)?;
                        move || {
                            actions::reports::user_has_reported_comment(
                                login_user_id,
                                comment_id,
                                &conn,
                            )
                        }
                    })
                    .await
                    .map_err_app(&persistent)?
                }
                _ => false,
            };
            let formatted_comment =
                richblock_to_html(&comment.message, &data.pool, &persistent).await?;
            comments.push(CommentEntry {
                id: comment.id,
                created: comment
                    .created
                    .format("%Y-%m-%d at %H:%M (UTC+00)")
                    .to_string(),
                edited: comment
                    .updated
                    .map(|t| t.format("%Y-%m-%d at %H:%M (UTC+00)").to_string()),
                user_username: user.username,
                user_display_name: user.display_name,
                formatted_comment,
                can_edit,
                can_reply,
                can_delete,
                is_already_reported,
                can_report,
            });
        }

        let can_write_comment = login_user
            .filter(|&l| l.can_write_comment_for(&authors, &story))
            .is_some();

        let is_administrator = login_user.iter().any(|u| u.administrator);
        let author_path = story.author_path(&authors);
        let s = CommentsTemplate {
            persistent: PersistentTemplate::from(&persistent),
            authors: authors
                .iter()
                .map(models::users::ShortUserEntry::from)
                .collect(),
            author_path,
            story,
            chapter,
            comments,
            page_listing,
            can_write_comment,
            is_administrator,
        }
        .render()
        .map_err_app(&persistent)?;
        Ok(HttpResponse::Ok().content_type("text/html").body(s))
    }
}

pub mod single {
    use actix_web::{get, web, HttpResponse};

    use intertextual::actions;
    use intertextual::models;

    use crate::prelude::*;

    use super::COMMENTS_PER_PAGE;

    #[get("/@{author}/{story}/{chapter}/comments/{id}/")]
    async fn get_author(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(String, String, i32, uuid::Uuid)>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let filter_mode = intertextual::models::filter::FilterMode::from_login_opt(&login_user);

        let (username, url_fragment, chapter_number, comment_id) = path.into_inner();
        let (authors, story, chapter) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                intertextual::data::stories::find_author_chapter_by_url(
                    username,
                    url_fragment,
                    chapter_number,
                    &filter_mode,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        get_shared(data, persistent, authors, story, chapter, comment_id).await
    }

    #[get("/collaboration/{story}/{chapter}/comments/{id}/")]
    async fn get_collaboration(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(String, i32, uuid::Uuid)>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let filter_mode = intertextual::models::filter::FilterMode::from_login_opt(&login_user);

        let (url_fragment, chapter_number, comment_id) = path.into_inner();
        let (authors, story, chapter) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                intertextual::data::stories::find_collaboration_chapter_by_url(
                    url_fragment,
                    chapter_number,
                    &filter_mode,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        get_shared(data, persistent, authors, story, chapter, comment_id).await
    }

    async fn get_shared(
        data: web::Data<AppState>,
        persistent: PersistentData,
        authors: Vec<models::users::User>,
        story: models::stories::Story,
        chapter: models::stories::Chapter,
        comment_id: uuid::Uuid,
    ) -> Result<HttpResponse, AppError> {
        let login_user = persistent.login();

        if !story.comments_enabled {
            // Check if the current user can see the comments
            match login_user {
                Some(login_user) => login_user
                    .require_see_comments_for(&authors, &story)
                    .map_err_app(&persistent)?,
                None => {
                    return Err(
                        IntertextualError::AccessRightsRequiredObfuscated.into_app(&persistent)
                    )
                }
            }
        }

        let (comment, start_index) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || -> Result<(models::comments::Comment, i64), IntertextualError> {
                let (comment, _, _, _) = actions::comments::find_comment(comment_id, &conn)?
                    .ok_or(IntertextualError::CommentNotFound { comment_id })?;
                let index = actions::comments::find_comment_index_for_story(&comment, &conn)?;
                Ok((comment, index))
            }
        })
        .await
        .map_err_app(&persistent)?;

        let return_url = format!(
            "/{}/{}/{}/?comments_start={}{}#comment_{}",
            story.author_path(&authors),
            story.url_fragment,
            chapter.chapter_number,
            COMMENTS_PER_PAGE * (start_index / COMMENTS_PER_PAGE),
            PersistentTemplate::from(&persistent).as_leading_amp(),
            comment.id,
        );

        Ok(HttpResponse::SeeOther()
            .header("Location", return_url)
            .finish())
    }
}

pub mod reply {
    use actix_web::{get, web, HttpResponse};

    use intertextual::actions;
    use intertextual::models;
    use intertextual::models::error::IntertextualError;
    use intertextual::models::shared::AppState;

    use crate::app::identity::Identity;
    use crate::app::persistent::*;
    use crate::error::*;

    #[get("/@{author}/{story}/{chapter}/comments/reply/{id}/")]
    async fn get_author(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(String, String, i32, uuid::Uuid)>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let filter_mode = intertextual::models::filter::FilterMode::from_login_opt(&login_user);

        let (username, url_fragment, chapter_number, comment_id) = path.into_inner();
        let (authors, story, chapter) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                intertextual::data::stories::find_author_chapter_by_url(
                    username,
                    url_fragment,
                    chapter_number,
                    &filter_mode,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        get_shared(data, persistent, authors, story, chapter, comment_id).await
    }

    #[get("/collaboration/{story}/{chapter}/comments/reply/{id}/")]
    async fn get_collaboration(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(String, i32, uuid::Uuid)>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let filter_mode = intertextual::models::filter::FilterMode::from_login_opt(&login_user);

        let (url_fragment, chapter_number, comment_id) = path.into_inner();
        let (authors, story, chapter) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                intertextual::data::stories::find_collaboration_chapter_by_url(
                    url_fragment,
                    chapter_number,
                    &filter_mode,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        get_shared(data, persistent, authors, story, chapter, comment_id).await
    }

    async fn get_shared(
        data: web::Data<AppState>,
        persistent: PersistentData,
        authors: Vec<models::users::User>,
        story: models::stories::Story,
        chapter: models::stories::Chapter,
        comment_id: uuid::Uuid,
    ) -> Result<HttpResponse, AppError> {
        let login_user = persistent.login();

        if !story.comments_enabled {
            // Check if the current user can see the comments
            match login_user {
                Some(login_user) => login_user
                    .require_see_comments_for(&authors, &story)
                    .map_err_app(&persistent)?,
                None => {
                    return Err(
                        IntertextualError::AccessRightsRequiredObfuscated.into_app(&persistent)
                    )
                }
            }
        }

        let comment_user = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || -> Result<models::users::User, IntertextualError> {
                let (_, comment_user, _, _) =
                    actions::comments::find_comment(comment_id, &conn)?
                        .ok_or(IntertextualError::CommentNotFound { comment_id })?;
                Ok(comment_user)
            }
        })
        .await
        .map_err_app(&persistent)?;

        let return_url = format!(
            "/{}/{}/{}/?reply_to={}&comments_start=0{}#new_comment_text_field",
            story.author_path(&authors),
            story.url_fragment,
            chapter.chapter_number,
            comment_user.username,
            PersistentTemplate::from(&persistent).as_leading_amp(),
        );

        Ok(HttpResponse::SeeOther()
            .header("Location", return_url)
            .finish())
    }
}

pub mod new {
    use actix_web::{post, web, HttpResponse};
    use serde::{Deserialize, Serialize};

    use intertextual::actions;
    use intertextual::models;

    use crate::prelude::*;

    use super::COMMENTS_PER_PAGE;

    #[derive(Serialize, Deserialize)]
    pub struct CommentCreateFormParams {
        message: RichBlock,
    }

    #[post("/@{author}/{story}/{chapter}/comments/new/")]
    async fn post_author(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(String, String, i32)>,
        form: web::Form<CommentCreateFormParams>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let filter_mode = intertextual::models::filter::FilterMode::from_login_opt(&login_user);

        let (username, url_fragment, chapter_number) = path.into_inner();
        let (authors, story, chapter) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                intertextual::data::stories::find_author_chapter_by_url(
                    username,
                    url_fragment,
                    chapter_number,
                    &filter_mode,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        post_shared(data, persistent, authors, story, chapter, form).await
    }

    #[post("/collaboration/{story}/{chapter}/comments/new/")]
    async fn post_collaboration(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(String, i32)>,
        form: web::Form<CommentCreateFormParams>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let filter_mode = intertextual::models::filter::FilterMode::from_login_opt(&login_user);

        let (url_fragment, chapter_number) = path.into_inner();
        let (authors, story, chapter) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                intertextual::data::stories::find_collaboration_chapter_by_url(
                    url_fragment,
                    chapter_number,
                    &filter_mode,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        post_shared(data, persistent, authors, story, chapter, form).await
    }

    async fn post_shared(
        data: web::Data<AppState>,
        persistent: PersistentData,
        authors: Vec<models::users::User>,
        story: models::stories::Story,
        chapter: models::stories::Chapter,
        form: web::Form<CommentCreateFormParams>,
    ) -> Result<HttpResponse, AppError> {
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequired)
            .map_err_app(&persistent)?;
        login_user
            .require_see_comments_for(&authors, &story)
            .map_err_app(&persistent)?;

        if !login_user.can_write_comment_for(&authors, &story) {
            return Err(IntertextualError::AccessRightsRequired.into_app(&persistent));
        }

        let form = form.into_inner();
        crate::app::validation::validate_comment(&form.message, "Comment")
            .map_err_app(&persistent)?;

        let new_comment = models::comments::NewComment {
            commenter_id: login_user.id,
            chapter_id: chapter.id,
            message: form.message,
        };

        let (comment, start_index) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || -> Result<(models::comments::Comment, i64), IntertextualError> {
                let comment =
                    actions::comments::modifications::add_new_comment(new_comment, &conn)?;
                let index = actions::comments::find_comment_index_for_story(&comment, &conn)?;
                Ok((comment, index))
            }
        })
        .await
        .map_err_app(&persistent)?;
        let return_url = format!(
            "/{}/{}/{}/?comments_start={}{}#comment_{}",
            story.author_path(&authors),
            story.url_fragment,
            chapter.chapter_number,
            COMMENTS_PER_PAGE * (start_index / COMMENTS_PER_PAGE),
            PersistentTemplate::from(&persistent).as_leading_amp(),
            comment.id,
        );

        Ok(HttpResponse::SeeOther()
            .header("Location", return_url)
            .finish())
    }
}

pub mod edit {
    use actix_web::{get, post, web, HttpResponse};
    use askama::Template;
    use serde::{Deserialize, Serialize};

    use intertextual::actions;
    use intertextual::models;

    use crate::prelude::*;

    use super::COMMENTS_PER_PAGE;

    #[derive(Template)]
    #[template(path = "story/comment_edit.html")]
    struct EditCommentTemplate {
        persistent: PersistentTemplate,
        authors: Vec<models::users::User>,
        author_path: String,
        story: models::stories::Story,
        chapter: models::stories::Chapter,
        comment_id: uuid::Uuid,
        comment_created: String,
        comment_edited: Option<String>,
        comment_user_username: String,
        comment_user_display_name: String,
        contents: RichBlock,
    }

    #[derive(Serialize, Deserialize)]
    pub struct CommentEditFormParams {
        message: RichBlock,
    }

    #[get("/@{author}/{story}/{chapter}/comments/edit/{id}/")]
    async fn get_author(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(String, String, i32, uuid::Uuid)>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let filter_mode = intertextual::models::filter::FilterMode::from_login_opt(&login_user);

        let (username, url_fragment, chapter_number, comment_id) = path.into_inner();
        let (authors, story, chapter) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                intertextual::data::stories::find_author_chapter_by_url(
                    username,
                    url_fragment,
                    chapter_number,
                    &filter_mode,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        get_shared(data, persistent, authors, story, chapter, comment_id).await
    }

    #[get("/collaboration/{story}/{chapter}/comments/edit/{id}/")]
    async fn get_collaboration(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(String, i32, uuid::Uuid)>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let filter_mode = intertextual::models::filter::FilterMode::from_login_opt(&login_user);

        let (url_fragment, chapter_number, comment_id) = path.into_inner();
        let (authors, story, chapter) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                intertextual::data::stories::find_collaboration_chapter_by_url(
                    url_fragment,
                    chapter_number,
                    &filter_mode,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        get_shared(data, persistent, authors, story, chapter, comment_id).await
    }

    #[post("/@{author}/{story}/{chapter}/comments/edit/{id}/")]
    async fn post_author(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(String, String, i32, uuid::Uuid)>,
        form: web::Form<CommentEditFormParams>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let filter_mode = intertextual::models::filter::FilterMode::from_login_opt(&login_user);

        let (username, url_fragment, chapter_number, comment_id) = path.into_inner();
        let (authors, story, chapter) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                intertextual::data::stories::find_author_chapter_by_url(
                    username,
                    url_fragment,
                    chapter_number,
                    &filter_mode,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        post_shared(data, persistent, authors, story, chapter, comment_id, form).await
    }

    #[post("/collaboration/{story}/{chapter}/comments/edit/{id}/")]
    async fn post_collaboration(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(String, i32, uuid::Uuid)>,
        form: web::Form<CommentEditFormParams>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let filter_mode = intertextual::models::filter::FilterMode::from_login_opt(&login_user);

        let (url_fragment, chapter_number, comment_id) = path.into_inner();
        let (authors, story, chapter) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                intertextual::data::stories::find_collaboration_chapter_by_url(
                    url_fragment,
                    chapter_number,
                    &filter_mode,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        post_shared(data, persistent, authors, story, chapter, comment_id, form).await
    }

    async fn get_shared(
        data: web::Data<AppState>,
        persistent: PersistentData,
        authors: Vec<models::users::User>,
        story: models::stories::Story,
        chapter: models::stories::Chapter,
        comment_id: uuid::Uuid,
    ) -> Result<HttpResponse, AppError> {
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequired)
            .map_err_app(&persistent)?;
        login_user
            .require_see_comments_for(&authors, &story)
            .map_err_app(&persistent)?;

        let (comment, user, _, _) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || actions::comments::find_comment(comment_id, &conn)
        })
        .await
        .map_err_app(&persistent)?
        .ok_or(IntertextualError::CommentNotFound { comment_id })
        .map_err_app(&persistent)?;

        login_user
            .require_edit_comment_rights_for(&authors, &story, &user)
            .map_err_app(&persistent)?;

        if user.id != login_user.id {
            return Err(IntertextualError::AccessRightsRequired.into_app(&persistent));
        }

        let author_path = story.author_path(&authors);
        let s = EditCommentTemplate {
            persistent: PersistentTemplate::from(&persistent),
            authors,
            author_path,
            story,
            chapter,
            comment_id: comment.id,
            comment_created: comment
                .created
                .format("%Y-%m-%d at %H:%M (UTC+00)")
                .to_string(),
            comment_edited: comment
                .updated
                .map(|t| t.format("%Y-%m-%d at %H:%M (UTC+00)").to_string()),
            comment_user_username: user.username,
            comment_user_display_name: user.display_name,
            contents: comment.message,
        }
        .render()
        .map_err_app(&persistent)?;
        Ok(HttpResponse::Ok().content_type("text/html").body(s))
    }

    async fn post_shared(
        data: web::Data<AppState>,
        persistent: PersistentData,
        authors: Vec<models::users::User>,
        story: models::stories::Story,
        chapter: models::stories::Chapter,
        comment_id: uuid::Uuid,
        form: web::Form<CommentEditFormParams>,
    ) -> Result<HttpResponse, AppError> {
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequired)
            .map_err_app(&persistent)?;
        login_user
            .require_see_comments_for(&authors, &story)
            .map_err_app(&persistent)?;

        let (comment, user, _, _) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || actions::comments::find_comment(comment_id, &conn)
        })
        .await
        .map_err_app(&persistent)?
        .ok_or(IntertextualError::CommentNotFound { comment_id })
        .map_err_app(&persistent)?;

        login_user
            .require_edit_comment_rights_for(&authors, &story, &user)
            .map_err_app(&persistent)?;

        if user.id != login_user.id {
            return Err(IntertextualError::AccessRightsRequired.into_app(&persistent));
        }

        let form = form.into_inner();
        crate::app::validation::validate_comment(&form.message, "Comment")
            .map_err_app(&persistent)?;

        let (comment, start_index) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || -> Result<(models::comments::Comment, i64), IntertextualError> {
                let comment = actions::comments::modifications::edit_comment(
                    comment.id,
                    form.message,
                    &conn,
                )?;
                let index = actions::comments::find_comment_index_for_story(&comment, &conn)?;
                Ok((comment, index))
            }
        })
        .await
        .map_err_app(&persistent)?;

        let return_url = format!(
            "/{}/{}/{}/?comments_start={}{}#comment_{}",
            story.author_path(&authors),
            story.url_fragment,
            chapter.chapter_number,
            COMMENTS_PER_PAGE * (start_index / COMMENTS_PER_PAGE),
            PersistentTemplate::from(&persistent).as_leading_amp(),
            comment.id,
        );
        Ok(HttpResponse::SeeOther()
            .header("Location", return_url)
            .finish())
    }
}

pub mod report {
    use actix_web::{get, post, web, HttpResponse};
    use askama::Template;
    use serde::{Deserialize, Serialize};

    use intertextual::actions;
    use intertextual::models;

    use crate::prelude::*;

    use super::COMMENTS_PER_PAGE;

    #[derive(Template)]
    #[template(path = "story/comment_report.html")]
    struct ReportCommentTemplate {
        persistent: PersistentTemplate,
        authors: Vec<models::users::User>,
        author_path: String,
        story: models::stories::Story,
        chapter: models::stories::Chapter,
        comment_id: uuid::Uuid,
        comment_created: String,
        comment_edited: Option<String>,
        comment_user_username: String,
        comment_user_display_name: String,
        formatted_comment: SanitizedHtml,
    }

    #[derive(Serialize, Deserialize)]
    pub struct CommentReportFormParams {
        reason: Option<String>,
        other_content: Option<String>,
    }

    #[get("/@{author}/{story}/{chapter}/comments/report/{id}/")]
    async fn get_author(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(String, String, i32, uuid::Uuid)>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let filter_mode = intertextual::models::filter::FilterMode::from_login_opt(&login_user);

        let (username, url_fragment, chapter_number, comment_id) = path.into_inner();
        let (authors, story, chapter) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                intertextual::data::stories::find_author_chapter_by_url(
                    username,
                    url_fragment,
                    chapter_number,
                    &filter_mode,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        get_shared(data, persistent, authors, story, chapter, comment_id).await
    }

    #[get("/collaboration/{story}/{chapter}/comments/report/{id}/")]
    async fn get_collaboration(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(String, i32, uuid::Uuid)>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let filter_mode = intertextual::models::filter::FilterMode::from_login_opt(&login_user);

        let (url_fragment, chapter_number, comment_id) = path.into_inner();
        let (authors, story, chapter) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                intertextual::data::stories::find_collaboration_chapter_by_url(
                    url_fragment,
                    chapter_number,
                    &filter_mode,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        get_shared(data, persistent, authors, story, chapter, comment_id).await
    }

    #[post("/@{author}/{story}/{chapter}/comments/report/{id}/")]
    async fn post_author(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(String, String, i32, uuid::Uuid)>,
        form: web::Form<CommentReportFormParams>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let filter_mode = intertextual::models::filter::FilterMode::from_login_opt(&login_user);

        let (username, url_fragment, chapter_number, comment_id) = path.into_inner();
        let (authors, story, chapter) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                intertextual::data::stories::find_author_chapter_by_url(
                    username,
                    url_fragment,
                    chapter_number,
                    &filter_mode,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        post_shared(data, persistent, authors, story, chapter, comment_id, form).await
    }

    #[post("/collaboration/{story}/{chapter}/comments/report/{id}/")]
    async fn post_collaboration(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(String, i32, uuid::Uuid)>,
        form: web::Form<CommentReportFormParams>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let filter_mode = intertextual::models::filter::FilterMode::from_login_opt(&login_user);

        let (url_fragment, chapter_number, comment_id) = path.into_inner();
        let (authors, story, chapter) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                intertextual::data::stories::find_collaboration_chapter_by_url(
                    url_fragment,
                    chapter_number,
                    &filter_mode,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        post_shared(data, persistent, authors, story, chapter, comment_id, form).await
    }

    async fn get_shared(
        data: web::Data<AppState>,
        persistent: PersistentData,
        authors: Vec<models::users::User>,
        story: models::stories::Story,
        chapter: models::stories::Chapter,
        comment_id: uuid::Uuid,
    ) -> Result<HttpResponse, AppError> {
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequired)
            .map_err_app(&persistent)?;

        login_user
            .require_see_comments_for(&authors, &story)
            .map_err_app(&persistent)?;

        let (comment, user, _, _) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || actions::comments::find_comment(comment_id, &conn)
        })
        .await
        .map_err_app(&persistent)?
        .ok_or(IntertextualError::CommentNotFound { comment_id })
        .map_err_app(&persistent)?;

        login_user
            .require_report_comment_rights_for(&authors, &story, &user)
            .map_err_app(&persistent)?;

        let author_path = story.author_path(&authors);
        let formatted_comment =
            richblock_to_html(&comment.message, &data.pool, &persistent).await?;
        let s = ReportCommentTemplate {
            persistent: PersistentTemplate::from(&persistent),
            authors,
            author_path,
            story,
            chapter,
            comment_id,
            comment_created: comment
                .created
                .format("%Y-%m-%d at %H:%M (UTC+00)")
                .to_string(),
            comment_edited: comment
                .updated
                .map(|t| t.format("%Y-%m-%d at %H:%M (UTC+00)").to_string()),
            comment_user_username: user.username,
            comment_user_display_name: user.display_name,
            formatted_comment,
        }
        .render()
        .map_err_app(&persistent)?;
        Ok(HttpResponse::Ok().content_type("text/html").body(s))
    }

    async fn post_shared(
        data: web::Data<AppState>,
        persistent: PersistentData,
        authors: Vec<models::users::User>,
        story: models::stories::Story,
        chapter: models::stories::Chapter,
        comment_id: uuid::Uuid,
        form: web::Form<CommentReportFormParams>,
    ) -> Result<HttpResponse, AppError> {
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequired)
            .map_err_app(&persistent)?;
        login_user
            .require_see_comments_for(&authors, &story)
            .map_err_app(&persistent)?;

        let (comment, user, _, _) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || actions::comments::find_comment(comment_id, &conn)
        })
        .await
        .map_err_app(&persistent)?
        .ok_or(IntertextualError::CommentNotFound { comment_id })
        .map_err_app(&persistent)?;

        login_user
            .require_report_comment_rights_for(&authors, &story, &user)
            .map_err_app(&persistent)?;

        let form = form.into_inner();
        let message = match form.reason {
            Some(m) if m.as_str() == "spam" => "Reported for spam".to_string(),
            Some(m) if m.as_str() == "tos" => "Reported for TOS violation".to_string(),
            Some(m) if m.as_str() == "offensive" => "Reported for offensive language".to_string(),
            Some(m) if m.as_str() == "harassment" => "Reported for harassment".to_string(),
            _ => form.other_content.unwrap_or_else(String::new),
        };

        crate::app::validation::validate_comment_report(&message, "Report message")
            .map_err_app(&persistent)?;

        let login_id = login_user.id;
        let report_result = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || -> Result<(models::comments::CommentReport, i64), IntertextualError> {
                let report = actions::reports::modifications::report_comment(
                    login_id, comment.id, message, &conn,
                )?;
                let index = actions::comments::find_comment_index_for_story(&comment, &conn)?;
                Ok((report, index))
            }
        })
        .await
        .map_err_app(&persistent);
        if crate::app::is_unique_violation(&report_result) {
            return Err(IntertextualError::FormFieldFormatError {
                form_field_name: "Report",
                message: "You can only report a comment once".to_string(),
            }
            .into_app(&persistent));
        }
        let (_report_result, start_index) = report_result?;
        let return_url = format!(
            "/{}/{}/{}/?comments_start={}{}#comment_{}",
            story.author_path(&authors),
            story.url_fragment,
            chapter.chapter_number,
            COMMENTS_PER_PAGE * (start_index / COMMENTS_PER_PAGE),
            PersistentTemplate::from(&persistent).as_leading_amp(),
            comment_id,
        );

        Ok(HttpResponse::SeeOther()
            .header("Location", return_url)
            .finish())
    }
}

pub mod unreport {
    use actix_web::{post, web, HttpResponse};

    use intertextual::actions;
    use intertextual::models;
    use intertextual::models::error::IntertextualError;
    use intertextual::models::shared::AppState;

    use crate::app::identity::Identity;
    use crate::app::persistent::*;
    use crate::error::*;

    use super::*;

    #[post("/@{author}/{story}/{chapter}/comments/unreport/{id}/")]
    async fn post_author(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(String, String, i32, uuid::Uuid)>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let filter_mode = intertextual::models::filter::FilterMode::from_login_opt(&login_user);

        let (username, url_fragment, chapter_number, comment_id) = path.into_inner();
        let (authors, story, chapter) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                intertextual::data::stories::find_author_chapter_by_url(
                    username,
                    url_fragment,
                    chapter_number,
                    &filter_mode,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        post_shared(data, persistent, authors, story, chapter, comment_id).await
    }

    #[post("/collaboration/{story}/{chapter}/comments/unreport/{id}/")]
    async fn post_collaboration(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(String, i32, uuid::Uuid)>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let filter_mode = intertextual::models::filter::FilterMode::from_login_opt(&login_user);

        let (url_fragment, chapter_number, comment_id) = path.into_inner();
        let (authors, story, chapter) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                intertextual::data::stories::find_collaboration_chapter_by_url(
                    url_fragment,
                    chapter_number,
                    &filter_mode,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        post_shared(data, persistent, authors, story, chapter, comment_id).await
    }

    async fn post_shared(
        data: web::Data<AppState>,
        persistent: PersistentData,
        authors: Vec<models::users::User>,
        story: models::stories::Story,
        chapter: models::stories::Chapter,
        comment_id: uuid::Uuid,
    ) -> Result<HttpResponse, AppError> {
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequired)
            .map_err_app(&persistent)?;
        login_user
            .require_see_comments_for(&authors, &story)
            .map_err_app(&persistent)?;

        let (comment, user, _, _) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || actions::comments::find_comment(comment_id, &conn)
        })
        .await
        .map_err_app(&persistent)?
        .ok_or(IntertextualError::CommentNotFound { comment_id })
        .map_err_app(&persistent)?;

        login_user
            .require_report_comment_rights_for(&authors, &story, &user)
            .map_err_app(&persistent)?;

        let (_unreported_report, start_index) = web::block({
            let login_id = login_user.id;
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || -> Result<(Option<models::comments::CommentReport>, i64), IntertextualError> {
                let unreport =
                    actions::reports::modifications::unreport_comment(login_id, comment.id, &conn)?;
                let index = actions::comments::find_comment_index_for_story(&comment, &conn)?;
                Ok((unreport, index))
            }
        })
        .await
        .map_err_app(&persistent)?;
        let return_url = format!(
            "/{}/{}/{}/?comments_start={}{}#comment_{}",
            story.author_path(&authors),
            story.url_fragment,
            chapter.chapter_number,
            COMMENTS_PER_PAGE * (start_index / COMMENTS_PER_PAGE),
            PersistentTemplate::from(&persistent).as_leading_amp(),
            comment_id,
        );

        Ok(HttpResponse::SeeOther()
            .header("Location", return_url)
            .finish())
    }
}

pub mod delete {
    use actix_web::{get, post, web, HttpResponse};
    use askama::Template;

    use intertextual::actions;
    use intertextual::models;

    use crate::prelude::*;

    #[derive(Template)]
    #[template(path = "story/comment_delete.html")]
    struct DeleteCommentTemplate {
        persistent: PersistentTemplate,
        authors: Vec<models::users::User>,
        author_path: String,
        story: models::stories::Story,
        chapter: models::stories::Chapter,
        comment_id: uuid::Uuid,
        comment_created: String,
        comment_edited: Option<String>,
        comment_user_username: String,
        comment_user_display_name: String,
        formatted_comment: SanitizedHtml,
    }

    #[get("/@{author}/{story}/{chapter}/comments/delete/{id}/")]
    async fn get_author(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(String, String, i32, uuid::Uuid)>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let filter_mode = intertextual::models::filter::FilterMode::from_login_opt(&login_user);

        let (username, url_fragment, chapter_number, comment_id) = path.into_inner();
        let (authors, story, chapter) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                intertextual::data::stories::find_author_chapter_by_url(
                    username,
                    url_fragment,
                    chapter_number,
                    &filter_mode,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        get_shared(data, persistent, authors, story, chapter, comment_id).await
    }

    #[get("/collaboration/{story}/{chapter}/comments/delete/{id}/")]
    async fn get_collaboration(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(String, i32, uuid::Uuid)>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let filter_mode = intertextual::models::filter::FilterMode::from_login_opt(&login_user);

        let (url_fragment, chapter_number, comment_id) = path.into_inner();
        let (authors, story, chapter) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                intertextual::data::stories::find_collaboration_chapter_by_url(
                    url_fragment,
                    chapter_number,
                    &filter_mode,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        get_shared(data, persistent, authors, story, chapter, comment_id).await
    }

    #[post("/@{author}/{story}/{chapter}/comments/delete/{id}/")]
    async fn post_author(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(String, String, i32, uuid::Uuid)>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let filter_mode = intertextual::models::filter::FilterMode::from_login_opt(&login_user);

        let (username, url_fragment, chapter_number, comment_id) = path.into_inner();
        let (authors, story, chapter) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                intertextual::data::stories::find_author_chapter_by_url(
                    username,
                    url_fragment,
                    chapter_number,
                    &filter_mode,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        post_shared(data, persistent, authors, story, chapter, comment_id).await
    }

    #[post("/collaboration/{story}/{chapter}/comments/delete/{id}/")]
    async fn post_collaboration(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(String, i32, uuid::Uuid)>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let filter_mode = intertextual::models::filter::FilterMode::from_login_opt(&login_user);

        let (url_fragment, chapter_number, comment_id) = path.into_inner();
        let (authors, story, chapter) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                intertextual::data::stories::find_collaboration_chapter_by_url(
                    url_fragment,
                    chapter_number,
                    &filter_mode,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        post_shared(data, persistent, authors, story, chapter, comment_id).await
    }

    async fn get_shared(
        data: web::Data<AppState>,
        persistent: PersistentData,
        authors: Vec<models::users::User>,
        story: models::stories::Story,
        chapter: models::stories::Chapter,
        comment_id: uuid::Uuid,
    ) -> Result<HttpResponse, AppError> {
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequired)
            .map_err_app(&persistent)?;
        login_user
            .require_see_comments_for(&authors, &story)
            .map_err_app(&persistent)?;

        let (comment, user, _, _) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || actions::comments::find_comment(comment_id, &conn)
        })
        .await
        .map_err_app(&persistent)?
        .ok_or(IntertextualError::CommentNotFound { comment_id })
        .map_err_app(&persistent)?;

        login_user
            .require_delete_comment_rights_for(&authors, &story, &user)
            .map_err_app(&persistent)?;

        let author_path = story.author_path(&authors);
        let formatted_comment =
            richblock_to_html(&comment.message, &data.pool, &persistent).await?;
        let s = DeleteCommentTemplate {
            persistent: PersistentTemplate::from(&persistent),
            authors,
            author_path,
            story,
            chapter,
            comment_id,
            comment_created: comment
                .created
                .format("%Y-%m-%d at %H:%M (UTC+00)")
                .to_string(),
            comment_edited: comment
                .updated
                .map(|t| t.format("%Y-%m-%d at %H:%M (UTC+00)").to_string()),
            comment_user_username: user.username,
            comment_user_display_name: user.display_name,
            formatted_comment,
        }
        .render()
        .map_err_app(&persistent)?;
        Ok(HttpResponse::Ok().content_type("text/html").body(s))
    }

    async fn post_shared(
        data: web::Data<AppState>,
        persistent: PersistentData,
        authors: Vec<models::users::User>,
        story: models::stories::Story,
        chapter: models::stories::Chapter,
        comment_id: uuid::Uuid,
    ) -> Result<HttpResponse, AppError> {
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequired)
            .map_err_app(&persistent)?;
        login_user
            .require_see_comments_for(&authors, &story)
            .map_err_app(&persistent)?;

        let (comment, user, _, _) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || actions::comments::find_comment(comment_id, &conn)
        })
        .await
        .map_err_app(&persistent)?
        .ok_or(IntertextualError::CommentNotFound { comment_id })
        .map_err_app(&persistent)?;

        login_user
            .require_delete_comment_rights_for(&authors, &story, &user)
            .map_err_app(&persistent)?;

        web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || actions::comments::modifications::delete_comment(comment, &conn)
        })
        .await
        .map_err_app(&persistent)?;

        let return_url = format!(
            "/{}/{}/{}/?start_comments=0{}#comments",
            story.author_path(&authors),
            story.url_fragment,
            chapter.chapter_number,
            PersistentTemplate::from(&persistent).as_leading_amp()
        );

        Ok(HttpResponse::SeeOther()
            .header("Location", return_url)
            .finish())
    }
}
