use actix_web::{get, post, web, HttpResponse};
use askama::Template;
use serde::{Deserialize, Serialize};

use intertextual::actions;
use intertextual::data;
use intertextual::models;
use intertextual::models::error::IntertextualError;
use intertextual::models::filter::FilterMode;
use intertextual::models::shared::AppState;

use crate::app::identity::Identity;
use crate::app::persistent::*;
use crate::error::*;

#[derive(Template)]
#[template(path = "story/chapter_report.html")]
struct ReportChapterTemplate {
    persistent: PersistentTemplate,
    authors: Vec<models::users::User>,
    author_path: String,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
    is_standalone: bool,
}

#[derive(Serialize, Deserialize)]
pub struct CommentReportFormParams {
    reason: Option<String>,
    other_content: Option<String>,
}

#[get("/@{author}/{story}/{chapter}/report/")]
async fn get_report_author(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, i32)>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();

    let (username, url_fragment, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let mode = FilterMode::from_login_opt(&login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            data::stories::find_author_chapter_by_url(
                username,
                url_fragment,
                chapter_number,
                &mode,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    get_report_shared(data, persistent, authors, story, chapter).await
}

#[get("/collaboration/{story}/{chapter}/report/")]
async fn get_report_collaboration(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, i32)>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();

    let (url_fragment, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let mode = FilterMode::from_login_opt(&login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            data::stories::find_collaboration_chapter_by_url(
                url_fragment,
                chapter_number,
                &mode,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    get_report_shared(data, persistent, authors, story, chapter).await
}

async fn get_report_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;
    let author_path = story.author_path(&authors);

    login_user
        .require_report_chapter_rights_for(&authors, &story, &chapter)
        .map_err_app(&persistent)?;

    let is_standalone = web::block({
        let story_id = story.id;
        let filter_mode = FilterMode::from_login(login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::stories::story_is_standalone(story_id, &filter_mode, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    let s = ReportChapterTemplate {
        persistent: PersistentTemplate::from(&persistent),
        authors,
        author_path,
        story,
        chapter,
        is_standalone,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/@{author}/{story}/{chapter}/report/")]
async fn post_report_author(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, i32)>,
    form: web::Form<CommentReportFormParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();

    let (username, url_fragment, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let mode = FilterMode::from_login_opt(&login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            data::stories::find_author_chapter_by_url(
                username,
                url_fragment,
                chapter_number,
                &mode,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    post_report_shared(data, persistent, authors, story, chapter, form).await
}

#[post("/collaboration/{story}/{chapter}/report/")]
async fn post_report_collaboration(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, i32)>,
    form: web::Form<CommentReportFormParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();

    let (url_fragment, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let mode = FilterMode::from_login_opt(&login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            data::stories::find_collaboration_chapter_by_url(
                url_fragment,
                chapter_number,
                &mode,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    post_report_shared(data, persistent, authors, story, chapter, form).await
}

async fn post_report_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
    form: web::Form<CommentReportFormParams>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    login_user
        .require_report_chapter_rights_for(&authors, &story, &chapter)
        .map_err_app(&persistent)?;

    let form = form.into_inner();
    let message = match form.reason {
        Some(m) if m.as_str() == "spam" => "Reported for spam".to_string(),
        Some(m) if m.as_str() == "tos" => "Reported for TOS violation".to_string(),
        Some(m) if m.as_str() == "offensive" => "Reported for offensive language".to_string(),
        Some(m) if m.as_str() == "harassment" => "Reported for harassment".to_string(),
        _ => form.other_content.unwrap_or_else(String::new),
    };

    crate::app::validation::validate_comment_report(&message, "Report message")
        .map_err_app(&persistent)?;

    let report_result = web::block({
        let login_id = login_user.id;
        let chapter_id = chapter.id;
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            actions::reports::modifications::report_chapter(login_id, chapter_id, message, &conn)
        }
    })
    .await
    .map_err_app(&persistent);
    if crate::app::is_unique_violation(&report_result) {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Report",
            message: "You can only report a chapter once".to_string(),
        }
        .into_app(&persistent));
    }
    let return_url = format!(
        "/{}/{}/{}",
        story.author_path(&authors),
        story.url_fragment,
        PersistentTemplate::from(&persistent).as_leading_qmark(),
    );

    Ok(HttpResponse::SeeOther()
        .header("Location", return_url)
        .finish())
}

#[post("/@{author}/{story}/{chapter}/unreport/")]
async fn post_unreport_author(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, i32)>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();

    let (username, url_fragment, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let mode = FilterMode::from_login_opt(&login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            data::stories::find_author_chapter_by_url(
                username,
                url_fragment,
                chapter_number,
                &mode,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    post_unreport_shared(data, persistent, authors, story, chapter).await
}

#[post("/collaboration/{story}/{chapter}/unreport/")]
async fn post_unreport_collaboration(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, i32)>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();

    let (url_fragment, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let mode = FilterMode::from_login_opt(&login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            data::stories::find_collaboration_chapter_by_url(
                url_fragment,
                chapter_number,
                &mode,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    post_unreport_shared(data, persistent, authors, story, chapter).await
}

async fn post_unreport_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    login_user
        .require_report_chapter_rights_for(&authors, &story, &chapter)
        .map_err_app(&persistent)?;

    let _unreported_report = web::block({
        let login_id = login_user.id;
        let chapter_id = chapter.id;
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::reports::modifications::unreport_chapter(login_id, chapter_id, &conn)
    })
    .await
    .map_err_app(&persistent)?;
    let return_url = format!(
        "/{}/{}/{}",
        story.author_path(&authors),
        story.url_fragment,
        PersistentTemplate::from(&persistent).as_leading_qmark(),
    );

    Ok(HttpResponse::SeeOther()
        .header("Location", return_url)
        .finish())
}
