use actix_web::{get, web, HttpResponse};
use askama::Template;

use intertextual::actions::static_pages;

use crate::prelude::*;

#[derive(Template)]
#[template(path = "about.html")]
struct AboutPageTemplate {
    persistent: PersistentTemplate,
    has_contact: bool,
    has_faq: bool,
    has_legal: bool,
}

#[derive(Template)]
#[template(path = "static_page.html")]
struct StaticPageTemplate {
    persistent: PersistentTemplate,
    title: String,
    content: SanitizedHtml,
}

#[get("/about/")]
async fn about_page(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let (has_contact, has_faq, has_legal) = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || -> Result<(bool, bool, bool), IntertextualError> {
            let contact =
                static_pages::get_static_page(static_pages::StaticPageKind::Contact, &conn)?;
            let faq = static_pages::get_static_page(static_pages::StaticPageKind::Faq, &conn)?;
            let legal = static_pages::get_static_page(static_pages::StaticPageKind::Legal, &conn)?;
            Ok((!contact.is_empty(), !faq.is_empty(), !legal.is_empty()))
        }
    })
    .await
    .map_err_app(&persistent)?;
    let s = AboutPageTemplate {
        persistent: PersistentTemplate::from(&persistent),
        has_contact,
        has_faq,
        has_legal,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/contact/")]
async fn contact_page(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    static_page_shared(id, static_pages::StaticPageKind::Contact, data, url_params).await
}

#[get("/faq/")]
async fn faq_page(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    static_page_shared(id, static_pages::StaticPageKind::Faq, data, url_params).await
}

#[get("/legal/")]
async fn legal_page(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    static_page_shared(id, static_pages::StaticPageKind::Legal, data, url_params).await
}

#[get("/rules/")]
async fn rules_page(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    static_page_shared(id, static_pages::StaticPageKind::Rules, data, url_params).await
}

#[get("/terms_of_service/")]
async fn tos_page(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    static_page_shared(
        id,
        static_pages::StaticPageKind::TermsOfService,
        data,
        url_params,
    )
    .await
}

async fn static_page_shared(
    id: Identity,
    kind: static_pages::StaticPageKind,
    data: web::Data<AppState>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;

    let content = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || static_pages::get_static_page(kind, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    let s = StaticPageTemplate {
        persistent: PersistentTemplate::from(&persistent),
        title: kind.title().to_string(),
        content,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
