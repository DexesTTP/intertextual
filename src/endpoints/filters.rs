pub fn san(h: &intertextual::models::formats::SanitizedHtml) -> askama::Result<&str> {
    Ok(h.as_str())
}
