use actix_web::{get, web, HttpResponse};
use askama::Template;
use itertools::Itertools;
use serde::Serialize;
use std::sync::Arc;

use intertextual::actions;
use intertextual::models;
use intertextual::models::shared::SharedSiteData;
use intertextual::models::stories::StoryEntry;
use intertextual::models::stories::StoryQueryExtraInfos;

use crate::prelude::*;

#[derive(Template)]
#[template(path = "whatsnew.html")]
struct IndexTemplate {
    persistent: PersistentTemplate,
    whatsnew_title: String,
    stories: Vec<(StoryEntry, ExtraStoryInformation)>,
    excluded_stories: Vec<(StoryEntry, ExtraStoryInformation)>,
    whatsnew_description: String,
    next_end_date: Option<String>,
    current_end_date: String,
    previous_end_date: Option<String>,
}

#[derive(Template)]
#[template(path = "whatsnew.txt")]
struct TextIndexTemplate {
    site: Arc<SharedSiteData>,
    whatsnew_title: String,
    stories: Vec<(StoryEntry, ExtraStoryInformation)>,
}

#[derive(Serialize)]
struct WhatsNewJSONTemplate {
    pub stories: Vec<WhatsNewJSONEntry>,
}

#[derive(Serialize)]
struct WhatsNewJSONEntry {
    pub url: String,
    pub title: String,
    pub status: String,
    pub authors: Vec<String>,
    pub major_tags: Vec<String>,
    pub description: RichTagline,
}

struct ExtraStoryInformation {
    status: String,
    is_update: bool,
}

impl ExtraStoryInformation {
    pub fn from(chapters: &[models::stories::Chapter]) -> Self {
        let new_words_count: i32 = chapters.iter().map(|c| c.word_count).sum();
        if chapters.iter().any(|c| c.chapter_number == 1) {
            Self {
                status: format!("New story, {} words", new_words_count),
                is_update: false,
            }
        } else if chapters.len() == 1 {
            Self {
                status: format!("{} new chapter, {} words", chapters.len(), new_words_count),
                is_update: true,
            }
        } else {
            Self {
                status: format!("{} new chapters, {} words", chapters.len(), new_words_count),
                is_update: true,
            }
        }
    }
}

#[get("/whatsnew/")]
async fn html_page(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let start_date = data
        .default_publication_date
        .get_last_publication_date(chrono::Utc::now().naive_utc());
    html_page_shared(data, persistent, start_date).await
}

#[get("/whatsnew/{date}/")]
async fn specific_html_page(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let selected_time = parse_time(&data, path).map_err_app(&persistent)?;
    html_page_shared(data, persistent, selected_time).await
}

#[get("/whatsnew.txt/")]
async fn txt_page(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let start_date = data
        .default_publication_date
        .get_last_publication_date(chrono::Utc::now().naive_utc());
    txt_page_shared(data, persistent, start_date).await
}

#[get("/whatsnew/{date}/whatsnew.txt/")]
async fn specific_txt_page(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let selected_time = parse_time(&data, path).map_err_app(&persistent)?;
    txt_page_shared(data, persistent, selected_time).await
}

#[get("/whatsnew.json/")]
async fn json_page(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<PersistentQuery>,
) -> Result<web::Json<WhatsNewJSONTemplate>, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let start_date = data
        .default_publication_date
        .get_last_publication_date(chrono::Utc::now().naive_utc());
    json_page_shared(data, persistent, start_date).await
}

#[get("/whatsnew/{date}/whatsnew.json/")]
async fn specific_json_page(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<PersistentQuery>,
) -> Result<web::Json<WhatsNewJSONTemplate>, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let selected_time = parse_time(&data, path).map_err_app(&persistent)?;
    json_page_shared(data, persistent, selected_time).await
}

fn parse_time(
    data: &web::Data<AppState>,
    path: web::Path<String>,
) -> Result<chrono::NaiveDateTime, IntertextualError> {
    let date = path.into_inner();
    Ok(chrono::NaiveDate::parse_from_str(&date, "%Y-%m-%d")
        .map_err(|_| IntertextualError::FormFieldFormatError {
            form_field_name: "Date",
            message: "The date isn't in the right format. Expected format : 2020-08-03".to_string(),
        })?
        .and_hms(data.default_publication_date.hour, 00, 00))
}

async fn html_page_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    selected_time: chrono::NaiveDateTime,
) -> Result<HttpResponse, AppError> {
    let login = persistent.login();

    let start_date = data
        .default_publication_date
        .get_last_publication_date(selected_time);
    let end_date = data
        .default_publication_date
        .get_next_publication_date(selected_time);

    let current_time = chrono::Utc::now().naive_utc();
    if end_date > current_time {
        return Err(IntertextualError::PageDoesNotExist.into_app(&persistent));
    }

    let stories = web::block({
        let user_info = StoryQueryExtraInfos::from_maybe_user(&data.site, &login);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || -> Result<Vec<(StoryEntry, ExtraStoryInformation)>, IntertextualError> {
            let stories = actions::stories::find_stories_in_range(
                start_date,
                end_date,
                &user_info.filter_mode,
                &conn,
            )?;
            let mut search_results = Vec::with_capacity(stories.len());
            for (story, chapters) in stories {
                let entry = StoryEntry::from_database(story, &user_info, &conn)?;
                let mut chapters = chapters;
                chapters.sort_by_key(|c| c.chapter_number);
                let chapters: Vec<models::stories::Chapter> =
                    chapters.into_iter().dedup_by(|a, b| a.id == b.id).collect();
                let extra_informations = ExtraStoryInformation::from(&chapters);
                search_results.push((entry, extra_informations));
            }
            Ok(search_results)
        }
    })
    .await
    .map_err_app(&persistent)?;

    let next_end_date = end_date + chrono::Duration::days(7);
    let previous_end_date = end_date - chrono::Duration::days(7);
    let whatsnew_description = format!(
        "{} new stories and {} updated stories",
        stories.iter().filter(|(_, e)| !e.is_update).count(),
        stories.iter().filter(|(_, e)| e.is_update).count(),
    );

    let (excluded_stories, stories) = stories.into_iter().partition(|(s, _)| s.is_excluded());

    let s = IndexTemplate {
        persistent: PersistentTemplate::from(&persistent),
        whatsnew_title: format!(
            "What's new ? From {} to {}",
            start_date.format("%Y-%m-%d"),
            end_date.format("%Y-%m-%d"),
        ),
        whatsnew_description,
        current_end_date: end_date.format("%Y-%m-%d").to_string(),
        next_end_date: if next_end_date > current_time {
            None
        } else {
            Some(next_end_date.format("%Y-%m-%d").to_string())
        },
        previous_end_date: Some(previous_end_date.format("%Y-%m-%d").to_string()),
        stories,
        excluded_stories,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

async fn txt_page_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    selected_time: chrono::NaiveDateTime,
) -> Result<HttpResponse, AppError> {
    let login = persistent.login();

    let start_date = data
        .default_publication_date
        .get_last_publication_date(selected_time);
    let end_date = data
        .default_publication_date
        .get_next_publication_date(selected_time);

    let current_time = chrono::Utc::now().naive_utc();
    if end_date > current_time {
        return Err(IntertextualError::PageDoesNotExist.into_app(&persistent));
    }

    let stories = web::block({
        let user_info = StoryQueryExtraInfos::from_maybe_user(&data.site, &login);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || -> Result<Vec<(StoryEntry, ExtraStoryInformation)>, IntertextualError> {
            let stories = actions::stories::find_stories_in_range(
                start_date,
                end_date,
                &user_info.filter_mode,
                &conn,
            )?;
            let mut search_results = Vec::with_capacity(stories.len());
            for (story, chapters) in stories {
                let entry = StoryEntry::from_database(story, &user_info, &conn)?;
                let extra_informations = ExtraStoryInformation::from(&chapters);
                search_results.push((entry, extra_informations));
            }
            Ok(search_results)
        }
    })
    .await
    .map_err_app(&persistent)?;

    let s = TextIndexTemplate {
        site: data.site.clone(),
        whatsnew_title: format!(
            "What's new ? From {} to {}",
            start_date.format("%Y-%m-%d"),
            end_date.format("%Y-%m-%d"),
        ),
        stories,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/plain").body(s))
}

async fn json_page_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    selected_time: chrono::NaiveDateTime,
) -> Result<web::Json<WhatsNewJSONTemplate>, AppError> {
    let login = persistent.login();

    let start_date = data
        .default_publication_date
        .get_last_publication_date(selected_time);
    let end_date = data
        .default_publication_date
        .get_next_publication_date(selected_time);

    let current_time = chrono::Utc::now().naive_utc();
    if end_date > current_time {
        return Err(IntertextualError::PageDoesNotExist.into_app(&persistent));
    }

    let stories = web::block({
        let user_info = StoryQueryExtraInfos::from_maybe_user(&data.site, &login);
        let base_url = data.site.base_url.clone();
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || -> Result<Vec<WhatsNewJSONEntry>, IntertextualError> {
            let stories = actions::stories::find_stories_in_range(
                start_date,
                end_date,
                &user_info.filter_mode,
                &conn,
            )?;
            let mut search_results = Vec::with_capacity(stories.len());
            for (story, chapters) in stories {
                let entry = StoryEntry::from_database(story, &user_info, &conn)?;
                let extra_informations = ExtraStoryInformation::from(&chapters);
                search_results.push(WhatsNewJSONEntry {
                    title: entry.title,
                    url: format!("{}/{}/{}", base_url, entry.author_path, entry.url_fragment),
                    description: entry.description,
                    status: extra_informations.status,
                    authors: entry.authors.into_iter().map(|a| a.display_name).collect(),
                    major_tags: entry
                        .major_tags
                        .into_iter()
                        .map(|(_c, t)| t.display_name)
                        .collect(),
                });
            }
            Ok(search_results)
        }
    })
    .await
    .map_err_app(&persistent)?;

    Ok(web::Json(WhatsNewJSONTemplate { stories }))
}
