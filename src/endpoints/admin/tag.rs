use actix_web::{get, post, web, HttpResponse};
use askama::Template;
use serde::Deserialize;

use intertextual::actions;
use intertextual::models::admin::NewModerationAction;
use intertextual::models::error::IntertextualError;
use intertextual::models::shared::AppState;

use crate::app::identity::Identity;
use crate::app::persistent::*;
use crate::app::validation::validate_tag_name;
use crate::error::*;

#[derive(Template)]
#[template(path = "admin/tag.html")]
struct AdminTagTemplate {
    persistent: PersistentTemplate,
    categories: Vec<AdminCategoryEntry>,
}

struct AdminCategoryEntry {
    id: uuid::Uuid,
    display_name: String,
    description: String,
    order_index: i32,
    is_always_displayed: bool,
    allow_user_defined_tags: bool,
    quick_select_for_included_tag_search: bool,
    quick_select_for_excluded_tag_search: bool,
    tags: Vec<AdminCanonicalTagEntry>,
}

struct AdminCanonicalTagEntry {
    id: uuid::Uuid,
    display_name: String,
    description: String,
    is_checkable: bool,
    show_in_popularity_listing: bool,
    story_count: i64,
    linked_tags: Vec<AdminInternalTagEntry>,
}

struct AdminInternalTagEntry {
    id: uuid::Uuid,
    name: String,
}

#[derive(Deserialize)]
struct NewCategoryParams {
    name: String,
    description: String,
    is_always_displayed: Option<String>,
    allow_user_defined_tags: Option<String>,
}

#[derive(Deserialize)]
struct EditCategoryParams {
    name: String,
    description: String,
    order_index: String,
    is_always_displayed: Option<String>,
    allow_user_defined_tags: Option<String>,
    quick_select_for_included_tag_search: Option<String>,
    quick_select_for_excluded_tag_search: Option<String>,
}

#[derive(Deserialize)]
struct DeleteCategoryParams {
    confirm_deletion: Option<String>,
}

#[derive(Deserialize)]
struct NewTagParams {
    name: String,
    description: String,
    is_checkable: Option<String>,
}

#[derive(Deserialize)]
struct EditTagParams {
    category_id: uuid::Uuid,
    description: String,
    is_checkable: Option<String>,
    show_in_popularity_listing: Option<String>,
}

#[derive(Deserialize)]
struct LinkParams {
    canonical_tag: String,
}

#[get("/admin/tags/")]
async fn admin_page_tags(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let categories = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || -> Result<Vec<AdminCategoryEntry>, IntertextualError> {
            let all_categories = actions::tags::get_all_categories(&conn)?;
            let mut result = Vec::with_capacity(all_categories.len());
            for category in all_categories {
                let all_canonical_tags = actions::tags::get_tags_by_category(category.id, &conn)?;
                let mut tags = Vec::with_capacity(all_canonical_tags.len());
                for tag in all_canonical_tags {
                    let internal_tags =
                        actions::tags::find_internal_tags_equivalent_to(&tag, &conn)?;
                    let story_count = internal_tags
                        .iter()
                        .map(|t| i64::from(t.tagged_stories))
                        .sum();
                    tags.push(AdminCanonicalTagEntry {
                        id: tag.id,
                        display_name: tag.display_name,
                        description: tag.description.unwrap_or_else(String::new),
                        is_checkable: tag.is_checkable,
                        show_in_popularity_listing: tag.show_in_popularity_listing,
                        story_count,
                        linked_tags: internal_tags
                            .into_iter()
                            .map(|t| AdminInternalTagEntry {
                                id: t.internal_id,
                                name: t.internal_name,
                            })
                            .collect(),
                    })
                }

                tags.sort_by(|tag1, tag2| tag1.display_name.cmp(&tag2.display_name));

                result.push(AdminCategoryEntry {
                    id: category.id,
                    display_name: category.display_name,
                    description: category.description,
                    order_index: category.order_index,
                    is_always_displayed: category.is_always_displayed,
                    allow_user_defined_tags: category.allow_user_defined_tags,
                    quick_select_for_included_tag_search: category
                        .quick_select_for_included_tag_search,
                    quick_select_for_excluded_tag_search: category
                        .quick_select_for_excluded_tag_search,
                    tags,
                })
            }

            Ok(result)
        }
    })
    .await
    .map_err_app(&persistent)?;

    let s = AdminTagTemplate {
        persistent: PersistentTemplate::from(&persistent),
        categories,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/admin/tags/create_category/")]
async fn create_category(
    id: Identity,
    data: web::Data<AppState>,
    form: web::Form<NewCategoryParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let form = form.into_inner();
    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!("Created category '{}'", &form.name),
        message: String::from("<No message required for this action>"),
    };
    let new_category = actions::tags::admin::NewTagCategory {
        display_name: form.name,
        description: form.description,
        allow_user_defined_tags: matches!(&form.allow_user_defined_tags, Some(value) if value == "on"),
        is_always_displayed: matches!(&form.is_always_displayed, Some(value) if value == "on"),
    };
    let (new_category, _) = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::tags::admin::add_new_category(new_category, action, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    Ok(HttpResponse::SeeOther()
        .header(
            "Location",
            format!(
                "/admin/tags/{}#category_{}",
                PersistentTemplate::from(&persistent).as_leading_qmark(),
                new_category.id,
            ),
        )
        .finish())
}

#[post("/admin/tags/edit_category/{category_id}/")]
async fn edit_category(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<uuid::Uuid>,
    form: web::Form<EditCategoryParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let category_id = path.into_inner();
    let mut category = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::tags::admin::find_category_by_id(category_id, &conn)
    })
    .await
    .map_err_app(&persistent)?
    .ok_or(IntertextualError::PageDoesNotExist)
    .map_err_app(&persistent)?;

    let form = form.into_inner();
    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!(
            "Updated category '{}' (new name : '{}')",
            &category.display_name, &form.name
        ),
        message: String::from("<No message required for this action>"),
    };
    category.display_name = form.name;
    category.description = form.description;
    category.order_index = form
        .order_index
        .parse::<i32>()
        .unwrap_or(category.order_index);
    category.allow_user_defined_tags =
        matches!(&form.allow_user_defined_tags, Some(value) if value == "on");
    category.is_always_displayed =
        matches!(&form.is_always_displayed, Some(value) if value == "on");
    category.quick_select_for_included_tag_search =
        matches!(&form.quick_select_for_included_tag_search, Some(value) if value == "on");
    category.quick_select_for_excluded_tag_search =
        matches!(&form.quick_select_for_excluded_tag_search, Some(value) if value == "on");

    let (new_category, _) = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::tags::admin::update_category(category, action, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    Ok(HttpResponse::SeeOther()
        .header(
            "Location",
            format!(
                "/admin/tags/{}#category_{}",
                PersistentTemplate::from(&persistent).as_leading_qmark(),
                new_category.id,
            ),
        )
        .finish())
}

#[post("/admin/tags/delete_category/{category_id}/")]
async fn delete_category(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<uuid::Uuid>,
    form: web::Form<DeleteCategoryParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let category_id = path.into_inner();
    let category = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::tags::admin::find_category_by_id(category_id, &conn)
    })
    .await
    .map_err_app(&persistent)?
    .ok_or(IntertextualError::PageDoesNotExist)
    .map_err_app(&persistent)?;

    let form = form.into_inner();
    if !matches!(&form.confirm_deletion, Some(value) if value == "on") {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Category delete",
            message: "You need to check the checkbox first".to_string(),
        }
        .into_app(&persistent));
    }

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!("Deleted category '{}'", &category.display_name,),
        message: String::from("<No message required for this action>"),
    };

    let _ = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::tags::admin::delete_category(category, action, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    Ok(HttpResponse::SeeOther()
        .header(
            "Location",
            format!(
                "/admin/tags/{}",
                PersistentTemplate::from(&persistent).as_leading_qmark(),
            ),
        )
        .finish())
}

#[post("/admin/tags/create_tag/{category_id}/")]
async fn create_tag(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<uuid::Uuid>,
    form: web::Form<NewTagParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let category_id = path.into_inner();

    let form = form.into_inner();
    let new_tag_name = form.name.clone();
    validate_tag_name(&new_tag_name).map_err_app(&persistent)?;

    let (_, mut new_tag) = web::block({
        let category_id = category_id;
        let new_tag_name = new_tag_name.clone();
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::tags::modifications::get_or_insert_tag(category_id, &new_tag_name, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    new_tag.description = Some(form.description.trim().to_string()).filter(|t| !t.is_empty());
    new_tag.is_checkable = matches!(&form.is_checkable, Some(value) if value == "on");

    let new_tag_id = new_tag.id;
    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!("Created tag '{}'", &new_tag_name),
        message: String::from("<No message required for this action>"),
    };
    let _ = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::tags::admin::update_tag(new_tag, action, &conn)
    })
    .await
    .map_err_app(&persistent)?;
    Ok(HttpResponse::SeeOther()
        .header(
            "Location",
            format!(
                "/admin/tags/{}#tag_{}",
                PersistentTemplate::from(&persistent).as_leading_qmark(),
                new_tag_id,
            ),
        )
        .finish())
}

#[post("/admin/tags/edit/{tag_id}/")]
async fn edit_tag(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<uuid::Uuid>,
    form: web::Form<EditTagParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let tag_id = path.into_inner();
    let mut tag = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::tags::find_canonical_tag_by_id(tag_id, &conn)
    })
    .await
    .map_err_app(&persistent)?
    .ok_or_else(|| IntertextualError::FormFieldFormatError {
        form_field_name: "Canonical tag",
        message: format!("There is no existing canonical tag with the id {}", tag_id),
    })
    .map_err_app(&persistent)?;

    let form = form.into_inner();
    tag.category_id = form.category_id;
    tag.description = Some(form.description.trim().to_string()).filter(|t| !t.is_empty());
    tag.is_checkable = matches!(&form.is_checkable, Some(value) if value == "on");
    tag.show_in_popularity_listing =
        matches!(&form.show_in_popularity_listing, Some(value) if value == "on");

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!("Edited tag '{}'", tag.display_name),
        message: String::from("<No message required for this action>"),
    };
    let _ = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::tags::admin::update_tag(tag, action, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    Ok(HttpResponse::SeeOther()
        .header(
            "Location",
            format!(
                "/admin/tags/{}#tag_{}",
                PersistentTemplate::from(&persistent).as_leading_qmark(),
                tag_id,
            ),
        )
        .finish())
}

#[post("/admin/tags/link/{internal_id}/")]
async fn link_tag(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<uuid::Uuid>,
    form: web::Form<LinkParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let internal_id = path.into_inner();
    let internal_tag = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::tags::admin::find_internal_tag_by_id(internal_id, &conn)
    })
    .await
    .map_err_app(&persistent)?
    .ok_or_else(|| IntertextualError::FormFieldFormatError {
        form_field_name: "Internal tag",
        message: format!(
            "There is no existing internal tag with the id {}",
            internal_id
        ),
    })
    .map_err_app(&persistent)?;

    let form = form.into_inner();
    let (_, canonical_tag) = web::block({
        let canonical_tag = form.canonical_tag.clone();
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::tags::find_tag_by_name(&canonical_tag, &conn)
    })
    .await
    .map_err_app(&persistent)?
    .ok_or_else(|| IntertextualError::FormFieldFormatError {
        form_field_name: "Canonical tag",
        message: format!(
            "There is no existing parent tag with the name {}",
            &form.canonical_tag
        ),
    })
    .map_err_app(&persistent)?;

    if canonical_tag.display_name == internal_tag.internal_name {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Canonical tag",
            message: format!(
                "Cannot link tag {} with itself (use unlink for this)",
                &internal_tag.internal_name
            ),
        }
        .into_app(&persistent));
    }

    let tag_id = canonical_tag.id;
    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!(
            "Made tag '{}' a child of tag '{}'",
            &internal_tag.internal_name, &canonical_tag.display_name
        ),
        message: String::from("<No message required for this action>"),
    };
    let _ = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::tags::admin::link_tag(internal_tag, canonical_tag, action, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    Ok(HttpResponse::SeeOther()
        .header(
            "Location",
            format!(
                "/admin/tags/{}#tag_{}",
                PersistentTemplate::from(&persistent).as_leading_qmark(),
                tag_id,
            ),
        )
        .finish())
}

#[post("/admin/tags/unlink/{internal_id}/")]
async fn unlink_tag(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<uuid::Uuid>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let internal_id = path.into_inner();
    let internal_tag = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::tags::admin::find_internal_tag_by_id(internal_id, &conn)
    })
    .await
    .map_err_app(&persistent)?
    .ok_or_else(|| IntertextualError::FormFieldFormatError {
        form_field_name: "Internal tag",
        message: format!(
            "There is no existing internal tag with the id {}",
            internal_id
        ),
    })
    .map_err_app(&persistent)?;

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!(
            "Unlinked tag '{}' into its own parent tag",
            &internal_tag.internal_name
        ),
        message: String::from("<No message required for this action>"),
    };
    let new_canonical_tag = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::tags::admin::unlink_tag(internal_tag, action, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    Ok(HttpResponse::SeeOther()
        .header(
            "Location",
            format!(
                "/admin/tags/{}#tag_{}",
                PersistentTemplate::from(&persistent).as_leading_qmark(),
                new_canonical_tag.id,
            ),
        )
        .finish())
}

#[post("/admin/tags/delete/{internal_id}/")]
async fn delete_tag(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<uuid::Uuid>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let internal_id = path.into_inner();
    let internal_tag = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::tags::admin::find_internal_tag_by_id(internal_id, &conn)
    })
    .await
    .map_err_app(&persistent)?
    .ok_or_else(|| IntertextualError::FormFieldFormatError {
        form_field_name: "Internal tag",
        message: format!(
            "There is no existing internal tag with the id {}",
            internal_id
        ),
    })
    .map_err_app(&persistent)?;

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!("Deleted tag '{}'", &internal_tag.internal_name),
        message: String::from("<No message required for this action>"),
    };
    let _ = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::tags::admin::delete_tag(internal_tag, action, &conn)
    })
    .await
    .map_err_app(&persistent)?;
    Ok(HttpResponse::SeeOther()
        .header(
            "Location",
            format!(
                "/admin/tags/{}",
                PersistentTemplate::from(&persistent).as_leading_qmark()
            ),
        )
        .finish())
}
