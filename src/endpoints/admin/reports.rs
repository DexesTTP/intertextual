use actix_web::{get, web, HttpResponse};
use askama::Template;
use serde::Deserialize;

use intertextual::actions;
use intertextual::actions::reports::admin::ListMode;
use intertextual::data::reports::ChapterReport;
use intertextual::data::reports::CommentReport;
use intertextual::data::reports::RecommendationReport;
use intertextual::models;
use intertextual::utils::page_list::PageListing;

use crate::prelude::*;

const ITEMS_PER_PAGE: i64 = 50;

#[derive(Template)]
#[template(path = "admin/report_list.html")]
struct ReportListTemplate {
    persistent: PersistentTemplate,
    report_page_title: String,
    current_fragment: String,
    reports: Vec<ReportEntry>,
    page_listing: PageListing,
}

struct ReportEntry {
    context: String,
    trimmed_message: String,
    username: String,
    url_fragment: String,
    moderator_who_handled_report: Option<models::users::User>,
}

impl ReportEntry {
    fn from_chapter(report: ChapterReport) -> ReportEntry {
        ReportEntry {
            context: if report.story_is_standalone {
                format!("Report for story {}", report.story.title)
            } else {
                format!(
                    "Report for chapter {} of story {}",
                    report.chapter.get_title(&report.story, &false),
                    report.story.title
                )
            },
            username: report.reporter.username,
            moderator_who_handled_report: report.moderator_who_handled_report,
            url_fragment: format!(
                "chapter/{}/{}",
                report.reporter.id, report.report.chapter_id
            ),
            trimmed_message:
                intertextual::utils::text_trim::limit_graphemes_with_ellipses_if_needed(
                    &report.report.reason,
                    30,
                ),
        }
    }

    fn from_recommendation(report: RecommendationReport) -> ReportEntry {
        ReportEntry {
            context: format!(
                "Report for suggestion from user {} of story {} ",
                report.recommendation_author.username, report.story.title,
            ),
            username: report.reporter.username,
            moderator_who_handled_report: report.moderator_who_handled_report,
            url_fragment: format!(
                "recommendation/{}/{}/{}",
                report.reporter.id, report.report.user_id, report.report.story_id
            ),
            trimmed_message:
                intertextual::utils::text_trim::limit_graphemes_with_ellipses_if_needed(
                    &report.report.reason,
                    30,
                ),
        }
    }
    fn from_comment(report: CommentReport) -> Self {
        ReportEntry {
            context: format!(
                "Report for comment from user @{} on story {}",
                report.comment_author.username, report.story.title,
            ),
            username: report.reporter.username,
            moderator_who_handled_report: report.moderator_who_handled_report,
            url_fragment: format!(
                "comment/{}/{}",
                report.reporter.id, report.report.comment_id
            ),
            trimmed_message:
                intertextual::utils::text_trim::limit_graphemes_with_ellipses_if_needed(
                    &report.report.reason,
                    30,
                ),
        }
    }
}

#[derive(Deserialize)]
pub struct ModmailQuery {
    t: Option<String>,
    start: Option<i64>,
}

#[get("/admin/report_list/new/")]
async fn report_list_unhandled(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<ModmailQuery>,
) -> Result<HttpResponse, AppError> {
    report_list_shared(
        id,
        data,
        url_params,
        "Unhandled reports".to_string(),
        "new".to_string(),
        ListMode::New,
    )
    .await
}

#[get("/admin/report_list/all/")]
async fn report_list_all(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<ModmailQuery>,
) -> Result<HttpResponse, AppError> {
    report_list_shared(
        id,
        data,
        url_params,
        "All reports".to_string(),
        "all".to_string(),
        ListMode::All,
    )
    .await
}

async fn report_list_shared(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<ModmailQuery>,
    title: String,
    current_fragment: String,
    mode: ListMode,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let start = url_params.start.unwrap_or(0);

    let (reports, total_reports_count) = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || -> Result<(Vec<ReportEntry>, i64), IntertextualError> {
            let chapter_reports_count = {
                let mode = mode.clone();
                actions::reports::admin::get_chapter_reports_count(mode, &conn)?
            };
            let recommendation_reports_count = {
                let mode = mode.clone();
                actions::reports::admin::get_recommendation_reports_count(mode, &conn)?
            };
            let comment_reports_count = {
                let mode = mode.clone();
                actions::reports::admin::get_recommendation_reports_count(mode, &conn)?
            };
            let chapter_reports = {
                let mode = mode.clone();
                actions::reports::admin::get_chapter_reports(mode, start, ITEMS_PER_PAGE, &conn)?
            };
            let recommendation_reports = {
                let mode = mode.clone();
                actions::reports::admin::get_recommendation_reports(
                    mode,
                    start,
                    ITEMS_PER_PAGE,
                    &conn,
                )?
            };
            let comment_reports = {
                let mode = mode.clone();
                actions::reports::admin::get_comment_reports(mode, start, ITEMS_PER_PAGE, &conn)?
            };
            let mut result = Vec::with_capacity(comment_reports.len());
            for report in chapter_reports {
                result.push(ReportEntry::from_chapter(ChapterReport::get(
                    report, &conn,
                )?));
            }
            for report in recommendation_reports {
                result.push(ReportEntry::from_recommendation(RecommendationReport::get(
                    report, &conn,
                )?));
            }
            for report in comment_reports {
                result.push(ReportEntry::from_comment(CommentReport::get(
                    report, &conn,
                )?));
            }
            Ok((
                result,
                chapter_reports_count + recommendation_reports_count + comment_reports_count,
            ))
        }
    })
    .await
    .map_err_app(&persistent)?;

    let page_listing = PageListing::get_from_count(total_reports_count, start, ITEMS_PER_PAGE);

    let s = ReportListTemplate {
        persistent: PersistentTemplate::from(&persistent),
        report_page_title: title,
        current_fragment,
        reports,
        page_listing,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

pub mod chapter {
    use actix_web::{get, post, web, HttpResponse};
    use askama::Template;

    use intertextual::actions;
    use intertextual::data::reports::ChapterReport;
    use intertextual::models::admin::NewModerationAction;
    use intertextual::models::error::IntertextualError;

    use crate::app::identity::Identity;
    use crate::app::persistent::*;
    use crate::error::*;
    use crate::AppState;

    #[derive(Template)]
    #[template(path = "admin/report_chapter.html")]
    struct ReportChapterTemplate {
        persistent: PersistentTemplate,
        report: ChapterReport,
    }

    #[get("/admin/report/chapter/{reporter_id}/{chapter_id}/")]
    async fn get(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(uuid::Uuid, uuid::Uuid)>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequiredObfuscated)
            .map_err_app(&persistent)?;
        login_user
            .require_administrator_level()
            .map_err_app(&persistent)?;

        let (reporter_id, chapter_id) = path.into_inner();
        let report = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || -> Result<Option<ChapterReport>, IntertextualError> {
                let raw_report =
                    actions::reports::admin::get_chapter_report(reporter_id, chapter_id, &conn)?;
                match raw_report {
                    Some(raw_report) => Ok(Some(ChapterReport::get(raw_report, &conn)?)),
                    None => Ok(None),
                }
            }
        })
        .await
        .map_err_app(&persistent)?
        .ok_or(IntertextualError::PageDoesNotExist)
        .map_err_app(&persistent)?;

        let s = ReportChapterTemplate {
            persistent: PersistentTemplate::from(&persistent),
            report,
        }
        .render()
        .map_err_app(&persistent)?;
        Ok(HttpResponse::Ok().content_type("text/html").body(s))
    }

    #[post("/admin/report/chapter/{reporter_id}/{chapter_id}/handled/")]
    async fn set_handled(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(uuid::Uuid, uuid::Uuid)>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequiredObfuscated)
            .map_err_app(&persistent)?;
        login_user
            .require_administrator_level()
            .map_err_app(&persistent)?;

        let (reporter_id, chapter_id) = path.into_inner();
        let (user, chapter, raw_report) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || actions::reports::admin::get_chapter_report(reporter_id, chapter_id, &conn)
        })
        .await
        .map_err_app(&persistent)?
        .ok_or(IntertextualError::PageDoesNotExist)
        .map_err_app(&persistent)?;

        let action = NewModerationAction {
            username: login_user.username.clone(),
            action_type: format!(
                "Set report by user id={} for chapter id={} as handled",
                user.id, chapter.id
            ),
            message: "No message required".to_string(),
        };
        let _ = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            let moderator_id = login_user.id;
            move || {
                actions::reports::admin::set_chapter_report_handled(
                    moderator_id,
                    raw_report,
                    action,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        Ok(HttpResponse::SeeOther()
            .header(
                "Location",
                format!(
                    "/admin/report_list/all/{}",
                    PersistentTemplate::from(&persistent).as_leading_qmark()
                ),
            )
            .finish())
    }

    #[post("/admin/report/chapter/{reporter_id}/{chapter_id}/unhandled/")]
    async fn set_unhandled(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(uuid::Uuid, uuid::Uuid)>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequiredObfuscated)
            .map_err_app(&persistent)?;
        login_user
            .require_administrator_level()
            .map_err_app(&persistent)?;

        let (reporter_id, chapter_id) = path.into_inner();
        let (user, chapter, raw_report) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || actions::reports::admin::get_chapter_report(reporter_id, chapter_id, &conn)
        })
        .await
        .map_err_app(&persistent)?
        .ok_or(IntertextualError::PageDoesNotExist)
        .map_err_app(&persistent)?;

        let action = NewModerationAction {
            username: login_user.username.clone(),
            action_type: format!(
                "Set report by user id={} for chapter id={} as handled",
                user.id, chapter.id
            ),
            message: "No message required".to_string(),
        };
        let _ = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || actions::reports::admin::set_chapter_report_unhandled(raw_report, action, &conn)
        })
        .await
        .map_err_app(&persistent)?;

        Ok(HttpResponse::SeeOther()
            .header(
                "Location",
                format!(
                    "/admin/report_list/all/{}",
                    PersistentTemplate::from(&persistent).as_leading_qmark()
                ),
            )
            .finish())
    }
}

pub mod recommendation {
    use actix_web::{get, post, web, HttpResponse};
    use askama::Template;

    use intertextual::actions;
    use intertextual::data::reports::RecommendationReport;
    use intertextual::models::admin::NewModerationAction;

    use crate::prelude::*;

    #[derive(Template)]
    #[template(path = "admin/report_recommendation.html")]
    struct ReportRecommendationTemplate {
        persistent: PersistentTemplate,
        report: RecommendationReport,
        formatted_recommendation: SanitizedHtml,
    }

    #[get("/admin/report/recommendation/{reporter_id}/{recommender_id}/{story_id}/")]
    async fn get(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(uuid::Uuid, uuid::Uuid, uuid::Uuid)>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequiredObfuscated)
            .map_err_app(&persistent)?;
        login_user
            .require_administrator_level()
            .map_err_app(&persistent)?;

        let (reporter_id, recommender_id, story_id) = path.into_inner();
        let report = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || -> Result<Option<RecommendationReport>, IntertextualError> {
                let raw_report = actions::reports::admin::get_recommendation_report(
                    reporter_id,
                    recommender_id,
                    story_id,
                    &conn,
                )?;
                match raw_report {
                    Some(raw_report) => Ok(Some(RecommendationReport::get(raw_report, &conn)?)),
                    None => Ok(None),
                }
            }
        })
        .await
        .map_err_app(&persistent)?
        .ok_or(IntertextualError::PageDoesNotExist)
        .map_err_app(&persistent)?;

        let formatted_recommendation =
            richblock_to_html(&report.recommendation.description, &data.pool, &persistent).await?;
        let s = ReportRecommendationTemplate {
            persistent: PersistentTemplate::from(&persistent),
            report,
            formatted_recommendation,
        }
        .render()
        .map_err_app(&persistent)?;
        Ok(HttpResponse::Ok().content_type("text/html").body(s))
    }

    #[post("/admin/report/recommendation/{reporter_id}/{recommender_id}/{story_id}/handled/")]
    async fn set_handled(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(uuid::Uuid, uuid::Uuid, uuid::Uuid)>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequiredObfuscated)
            .map_err_app(&persistent)?;
        login_user
            .require_administrator_level()
            .map_err_app(&persistent)?;

        let (reporter_id, recommender_id, story_id) = path.into_inner();
        let (user, recommendation, raw_report) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                actions::reports::admin::get_recommendation_report(
                    reporter_id,
                    recommender_id,
                    story_id,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?
        .ok_or(IntertextualError::PageDoesNotExist)
        .map_err_app(&persistent)?;

        let action = NewModerationAction {
            username: login_user.username.clone(),
            action_type: format!(
                "Set report by user id={} of recommendation by user id={} for story id={} as unhandled",
                user.id, recommendation.user_id, recommendation.story_id,
            ),
            message: "No message required".to_string(),
        };
        let _ = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            let moderator_id = login_user.id;
            move || {
                actions::reports::admin::set_recommendation_report_handled(
                    moderator_id,
                    raw_report,
                    action,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        Ok(HttpResponse::SeeOther()
            .header(
                "Location",
                format!(
                    "/admin/report_list/all/{}",
                    PersistentTemplate::from(&persistent).as_leading_qmark()
                ),
            )
            .finish())
    }

    #[post("/admin/report/recommendation/{reporter_id}/{recommender_id}/{story_id}/unhandled/")]
    async fn set_unhandled(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(uuid::Uuid, uuid::Uuid, uuid::Uuid)>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequiredObfuscated)
            .map_err_app(&persistent)?;
        login_user
            .require_administrator_level()
            .map_err_app(&persistent)?;

        let (reporter_id, recommender_id, story_id) = path.into_inner();
        let (user, recommendation, raw_report) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                actions::reports::admin::get_recommendation_report(
                    reporter_id,
                    recommender_id,
                    story_id,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?
        .ok_or(IntertextualError::PageDoesNotExist)
        .map_err_app(&persistent)?;

        let action = NewModerationAction {
            username: login_user.username.clone(),
            action_type: format!(
                "Set report by user id={} of recommendation by user id={} for story id={} as handled",
                user.id, recommendation.user_id, recommendation.story_id,
            ),
            message: "No message required".to_string(),
        };
        let _ = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                actions::reports::admin::set_recommendation_report_unhandled(
                    raw_report, action, &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        Ok(HttpResponse::SeeOther()
            .header(
                "Location",
                format!(
                    "/admin/report_list/all/{}",
                    PersistentTemplate::from(&persistent).as_leading_qmark()
                ),
            )
            .finish())
    }
}

pub mod comment {
    use actix_web::{get, post, web, HttpResponse};
    use askama::Template;

    use intertextual::actions;
    use intertextual::data::reports::CommentReport;
    use intertextual::models::admin::NewModerationAction;

    use crate::prelude::*;

    #[derive(Template)]
    #[template(path = "admin/report_comment.html")]
    struct ReportCommentTemplate {
        persistent: PersistentTemplate,
        report: CommentReport,
        formatted_comment: SanitizedHtml,
    }

    #[get("/admin/report/comment/{reporter_id}/{comment_id}/")]
    async fn get(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(uuid::Uuid, uuid::Uuid)>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequiredObfuscated)
            .map_err_app(&persistent)?;
        login_user
            .require_administrator_level()
            .map_err_app(&persistent)?;

        let (reporter_id, comment_id) = path.into_inner();
        let report = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || -> Result<Option<CommentReport>, IntertextualError> {
                let raw_report =
                    actions::reports::admin::get_comment_report(reporter_id, comment_id, &conn)?;
                match raw_report {
                    Some(raw_report) => Ok(Some(CommentReport::get(raw_report, &conn)?)),
                    None => Ok(None),
                }
            }
        })
        .await
        .map_err_app(&persistent)?
        .ok_or(IntertextualError::PageDoesNotExist)
        .map_err_app(&persistent)?;

        let formatted_comment =
            richblock_to_html(&report.comment.message, &data.pool, &persistent).await?;
        let s = ReportCommentTemplate {
            persistent: PersistentTemplate::from(&persistent),
            report,
            formatted_comment,
        }
        .render()
        .map_err_app(&persistent)?;
        Ok(HttpResponse::Ok().content_type("text/html").body(s))
    }

    #[post("/admin/report/comment/{reporter_id}/{comment_id}/handled/")]
    async fn set_handled(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(uuid::Uuid, uuid::Uuid)>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequiredObfuscated)
            .map_err_app(&persistent)?;
        login_user
            .require_administrator_level()
            .map_err_app(&persistent)?;

        let (reporter_id, comment_id) = path.into_inner();
        let (user, comment, raw_report) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || actions::reports::admin::get_comment_report(reporter_id, comment_id, &conn)
        })
        .await
        .map_err_app(&persistent)?
        .ok_or(IntertextualError::PageDoesNotExist)
        .map_err_app(&persistent)?;

        let action = NewModerationAction {
            username: login_user.username.clone(),
            action_type: format!(
                "Set report by user id={} for comment id={} as handled",
                user.id, comment.id
            ),
            message: "No message required".to_string(),
        };
        let _ = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            let moderator_id = login_user.id;
            move || {
                actions::reports::admin::set_comment_report_handled(
                    moderator_id,
                    raw_report,
                    action,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;

        Ok(HttpResponse::SeeOther()
            .header(
                "Location",
                format!(
                    "/admin/report_list/all/{}",
                    PersistentTemplate::from(&persistent).as_leading_qmark()
                ),
            )
            .finish())
    }

    #[post("/admin/report/comment/{reporter_id}/{comment_id}/unhandled/")]
    async fn set_unhandled(
        id: Identity,
        data: web::Data<AppState>,
        path: web::Path<(uuid::Uuid, uuid::Uuid)>,
        url_params: web::Query<PersistentQuery>,
    ) -> Result<HttpResponse, AppError> {
        let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequiredObfuscated)
            .map_err_app(&persistent)?;
        login_user
            .require_administrator_level()
            .map_err_app(&persistent)?;

        let (reporter_id, comment_id) = path.into_inner();
        let (user, comment, raw_report) = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || actions::reports::admin::get_comment_report(reporter_id, comment_id, &conn)
        })
        .await
        .map_err_app(&persistent)?
        .ok_or(IntertextualError::PageDoesNotExist)
        .map_err_app(&persistent)?;

        let action = NewModerationAction {
            username: login_user.username.clone(),
            action_type: format!(
                "Set report by user id={} for comment id={} as handled",
                user.id, comment.id
            ),
            message: "No message required".to_string(),
        };
        let _ = web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || actions::reports::admin::set_comment_report_unhandled(raw_report, action, &conn)
        })
        .await
        .map_err_app(&persistent)?;

        Ok(HttpResponse::SeeOther()
            .header(
                "Location",
                format!(
                    "/admin/report_list/all/{}",
                    PersistentTemplate::from(&persistent).as_leading_qmark()
                ),
            )
            .finish())
    }
}
