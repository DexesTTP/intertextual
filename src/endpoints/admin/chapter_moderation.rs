use actix_web::{get, post, web, HttpResponse};
use askama::Template;
use models::filter::FilterMode;
use serde::{Deserialize, Serialize};

use intertextual::actions;
use intertextual::actions::stories::admin;
use intertextual::data;
use intertextual::models;
use intertextual::models::admin::NewModerationAction;

use crate::app::validation::validate_admin_message;
use crate::app::validation::validate_modmail;
use crate::prelude::*;

#[derive(Template)]
#[template(path = "admin/chapter_moderation.html")]
struct ChapterModerationTemplate {
    persistent: PersistentTemplate,
    authors: Vec<models::users::User>,
    author_path: String,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
    tag_categories: Vec<TagCategoryEntry>,
}

struct TagCategoryEntry {
    name: String,
    checkable_tags: Vec<CheckableTagEntry>,
}

struct CheckableTagEntry {
    id: uuid::Uuid,
    name: String,
    description: String,
    checked: bool,
}

#[derive(Serialize, Deserialize)]
pub struct LockAdminActionParams {
    message: String,
    locking_message: String,
    lock_specific_chapter: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct AdminActionParams {
    message: String,
}

#[derive(Serialize, Deserialize)]
pub struct TagAdminActionParams {
    #[serde(flatten)]
    all_fields: std::collections::BTreeMap<String, String>,
}

#[get("/admin/@{user}/{story}/{chapter}/")]
async fn main_page_single_author(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, i32)>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let (author_username, url_fragment, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            data::stories::find_author_chapter_by_url(
                author_username,
                url_fragment,
                chapter_number,
                &FilterMode::BypassFilters,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    main_page_shared(data, persistent, authors, story, chapter).await
}

#[get("/admin/collaboration/{story}/{chapter}/")]
async fn main_page_collaboration(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, i32)>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let (url_fragment, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            intertextual::data::stories::find_collaboration_chapter_by_url(
                url_fragment,
                chapter_number,
                &FilterMode::BypassFilters,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    main_page_shared(data, persistent, authors, story, chapter).await
}

async fn main_page_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
) -> Result<HttpResponse, AppError> {
    let tag_categories = web::block({
        let story_id = story.id;
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || -> Result<Vec<TagCategoryEntry>, IntertextualError> {
            let story_tags = actions::tags::find_internal_tags_by_story_id(story_id, &conn)?;
            let raw_categories = actions::tags::get_all_categories(&conn)?;
            let mut categories = Vec::<TagCategoryEntry>::with_capacity(raw_categories.len());
            for category in raw_categories {
                if !category.is_always_displayed {
                    // We want a simple list of checkable mandatory tags
                    // The tags that aren't "always displayed" don't really matter
                    continue;
                }

                let category_tags = story_tags
                    .iter()
                    .filter(|(cat, _, _, _)| cat.id == category.id)
                    .collect::<Vec<&(
                        models::tags::TagCategory,
                        models::tags::CanonicalTag,
                        models::tags::InternalTagAssociation,
                        models::tags::StoryToInternalTagAssociation,
                    )>>();
                let raw_checkable_tags =
                    actions::tags::get_checkable_tags_by_category(category.id, &conn)?;
                let mut checkable_tags =
                    Vec::<CheckableTagEntry>::with_capacity(raw_checkable_tags.len());

                for tag in raw_checkable_tags {
                    let tag_id = tag.id;
                    let checked = category_tags.iter().any(|(_, t, _, _)| t.id == tag_id);
                    checkable_tags.push(CheckableTagEntry {
                        id: tag_id,
                        name: tag.display_name,
                        description: tag.description.unwrap_or_else(String::new),
                        checked,
                    });
                }

                categories.push(TagCategoryEntry {
                    name: category.display_name,
                    checkable_tags,
                });
            }
            Ok(categories)
        }
    })
    .await
    .map_err_app(&persistent)?;

    let author_path = story.author_path(&authors);
    let s = ChapterModerationTemplate {
        persistent: PersistentTemplate::from(&persistent),
        authors,
        author_path,
        tag_categories,
        story,
        chapter,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/admin/@{user}/{story}/{chapter}/edit_tags/")]
async fn handle_story_edit_tags_author(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, i32)>,
    form: web::Form<TagAdminActionParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let (author_username, story_url, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            data::stories::find_author_chapter_by_url(
                author_username,
                story_url,
                chapter_number,
                &FilterMode::BypassFilters,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    handle_story_edit_tags_shared(data, persistent, authors, story, chapter, form).await
}

#[post("/admin/collaboration/{story}/{chapter}/edit_tags/")]
async fn handle_story_edit_tags_collaboration(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, i32)>,
    form: web::Form<TagAdminActionParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let (url_fragment, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            intertextual::data::stories::find_collaboration_chapter_by_url(
                url_fragment,
                chapter_number,
                &FilterMode::BypassFilters,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    handle_story_edit_tags_shared(data, persistent, authors, story, chapter, form).await
}

async fn handle_story_edit_tags_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
    form: web::Form<TagAdminActionParams>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;

    let form = form.into_inner();

    let mut message = String::new();
    let mut selected_tag_ids = Vec::new();

    for (name, value) in form.all_fields {
        if name == "message" {
            message = value;
            continue;
        }

        if let Some(tag_id) = name.strip_prefix("tag_") {
            if let Ok(tag_id) = uuid::Uuid::parse_str(tag_id) {
                selected_tag_ids.push(tag_id);
            }

            continue;
        }
    }

    log::info!("Test: {:?}", selected_tag_ids);

    validate_admin_message(&message, "Justification").map_err_app(&persistent)?;

    let redirect_url = format!(
        "/admin/{}/{}/{}/{}",
        &story.author_path(&authors),
        &story.url_fragment,
        &chapter.chapter_number,
        PersistentTemplate::from(&persistent).as_leading_qmark()
    );
    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!("Changed tags for story id={}", &story.id),
        message,
    };

    let _ = web::block({
        let moderator =
            models::users::ModeratorUserWrapper::from_user(login_user).map_err_app(&persistent)?;
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            admin::add_and_remove_always_displayed_checkable_tags(
                moderator,
                &authors,
                &story,
                &selected_tag_ids,
                action,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    Ok(HttpResponse::SeeOther()
        .header("Location", redirect_url)
        .finish())
}

#[post("/admin/@{user}/{story}/{chapter}/disable_collab/")]
async fn handle_story_disable_collab_author(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, i32)>,
    form: web::Form<AdminActionParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let (author_username, story_url, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            data::stories::find_author_chapter_by_url(
                author_username,
                story_url,
                chapter_number,
                &FilterMode::BypassFilters,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    handle_story_disable_collab_shared(data, persistent, authors, story, chapter, form).await
}

#[post("/admin/collaboration/{story}/{chapter}/disable_collab/")]
async fn handle_story_disable_collab_collaboration(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, i32)>,
    form: web::Form<AdminActionParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let (url_fragment, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            intertextual::data::stories::find_collaboration_chapter_by_url(
                url_fragment,
                chapter_number,
                &FilterMode::BypassFilters,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    handle_story_disable_collab_shared(data, persistent, authors, story, chapter, form).await
}

async fn handle_story_disable_collab_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
    form: web::Form<AdminActionParams>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;

    let form = form.into_inner();

    let message = form.message;
    validate_admin_message(&message, "Justification").map_err_app(&persistent)?;

    let redirect_url = match authors.first() {
        Some(author) => format!(
            "/admin/{}/{}/{}/{}",
            author.username,
            &story.url_fragment,
            &chapter.chapter_number,
            PersistentTemplate::from(&persistent).as_leading_qmark()
        ),
        None => format!(
            "/admin/{}",
            PersistentTemplate::from(&persistent).as_leading_qmark()
        ),
    };
    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!("Locked story id={}", story.id),
        message,
    };

    let _ = web::block({
        let moderator =
            models::users::ModeratorUserWrapper::from_user(login_user).map_err_app(&persistent)?;
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || admin::disable_collaboration_link(moderator, &authors, story, action, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    Ok(HttpResponse::SeeOther()
        .header("Location", redirect_url)
        .finish())
}

#[post("/admin/@{user}/{story}/{chapter}/lock/")]
async fn handle_story_lock_author(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, i32)>,
    form: web::Form<LockAdminActionParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let (author_username, story_url, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            data::stories::find_author_chapter_by_url(
                author_username,
                story_url,
                chapter_number,
                &FilterMode::BypassFilters,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    handle_story_lock_shared(data, persistent, authors, story, chapter, form).await
}

#[post("/admin/collaboration/{story}/{chapter}/lock/")]
async fn handle_story_lock_collaboration(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, i32)>,
    form: web::Form<LockAdminActionParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let (url_fragment, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            intertextual::data::stories::find_collaboration_chapter_by_url(
                url_fragment,
                chapter_number,
                &FilterMode::BypassFilters,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    handle_story_lock_shared(data, persistent, authors, story, chapter, form).await
}

async fn handle_story_lock_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
    form: web::Form<LockAdminActionParams>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;

    let chapter_list = web::block({
        let story_id = story.id;
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            actions::stories::find_chapters_by_story_id(story_id, &FilterMode::BypassFilters, &conn)
        }
    })
    .await
    .map_err_app(&persistent)?;

    let form = form.into_inner();

    let message = form.message;
    validate_admin_message(&message, "Justification").map_err_app(&persistent)?;

    if !form.locking_message.is_empty() {
        validate_modmail(&form.locking_message, "Message").map_err_app(&persistent)?;
    }

    let redirect_url = format!(
        "/admin/{}/{}/{}/{}",
        &story.author_path(&authors),
        &story.url_fragment,
        &chapter.chapter_number,
        PersistentTemplate::from(&persistent).as_leading_qmark()
    );
    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!("Locked story id={}", story.id),
        message,
    };

    let author_ids: Vec<uuid::Uuid> = authors.iter().map(|a| a.id).collect();
    let _ = web::block({
        let moderator =
            models::users::ModeratorUserWrapper::from_user(login_user).map_err_app(&persistent)?;
        let story_title = story.title;
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            admin::lock_chapter_list(
                moderator,
                &authors,
                &chapter_list,
                &story_title,
                action,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    if !form.locking_message.is_empty() {
        for author_id in author_ids {
            let _ = web::block({
                let moderator = models::users::ModeratorUserWrapper::from_user(login_user)
                    .map_err_app(&persistent)?;
                let new_modmail = models::modmail::NewModmail {
                    user_id: author_id,
                    message: models::formats::RichBlock::from_raw_text(&form.locking_message),
                };
                let conn = data.pool.get().map_err_app(&persistent)?;
                move || actions::modmail::admins::send_new_modmail(moderator, new_modmail, &conn)
            })
            .await
            .map_err_app(&persistent)?;
        }
    }

    Ok(HttpResponse::SeeOther()
        .header("Location", redirect_url)
        .finish())
}

#[post("/admin/@{user}/{story}/{chapter}/unlock/")]
async fn handle_story_unlock_author(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, i32)>,
    form: web::Form<AdminActionParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let (author_username, story_url, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            data::stories::find_author_chapter_by_url(
                author_username,
                story_url,
                chapter_number,
                &FilterMode::BypassFilters,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    handle_story_unlock_shared(data, persistent, authors, story, chapter, form).await
}

#[post("/admin/collaboration/{story}/{chapter}/unlock/")]
async fn handle_story_unlock_collaboration(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, i32)>,
    form: web::Form<AdminActionParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let (url_fragment, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            intertextual::data::stories::find_collaboration_chapter_by_url(
                url_fragment,
                chapter_number,
                &FilterMode::BypassFilters,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    handle_story_unlock_shared(data, persistent, authors, story, chapter, form).await
}

async fn handle_story_unlock_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
    form: web::Form<AdminActionParams>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;

    let chapter_list = web::block({
        let story_id = story.id;
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            actions::stories::find_chapters_by_story_id(story_id, &FilterMode::BypassFilters, &conn)
        }
    })
    .await
    .map_err_app(&persistent)?;

    let message = form.into_inner().message;
    validate_admin_message(&message, "Justification").map_err_app(&persistent)?;

    let author_url = story.author_path(&authors);
    let story_url = story.url_fragment.clone();
    let chapter_number = chapter.chapter_number;
    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!(
            "Unlocked chapter id={} of story id={}",
            &chapter.id, &story.id
        ),
        message,
    };

    let _ = web::block({
        let moderator =
            models::users::ModeratorUserWrapper::from_user(login_user).map_err_app(&persistent)?;
        let story_title = story.title;
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            admin::unlock_chapter_list(
                moderator,
                &authors,
                &chapter_list,
                &story_title,
                action,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    Ok(HttpResponse::SeeOther()
        .header(
            "Location",
            format!(
                "/admin/{}/{}/{}/{}",
                author_url,
                story_url,
                chapter_number,
                PersistentTemplate::from(&persistent).as_leading_qmark()
            ),
        )
        .finish())
}

#[post("/admin/@{user}/{story}/{chapter}/delete/")]
async fn handle_story_delete_author(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, i32)>,
    form: web::Form<AdminActionParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let (author_username, story_url, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            data::stories::find_author_chapter_by_url(
                author_username,
                story_url,
                chapter_number,
                &FilterMode::BypassFilters,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    handle_story_delete_shared(data, persistent, authors, story, chapter, form).await
}

#[post("/admin/collaboration/{story}/{chapter}/delete/")]
async fn handle_story_delete_collaboration(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, i32)>,
    form: web::Form<AdminActionParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let (url_fragment, chapter_number) = path.into_inner();
    let (authors, story, chapter) = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            intertextual::data::stories::find_collaboration_chapter_by_url(
                url_fragment,
                chapter_number,
                &FilterMode::BypassFilters,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    handle_story_delete_shared(data, persistent, authors, story, chapter, form).await
}

async fn handle_story_delete_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    chapter: models::stories::Chapter,
    form: web::Form<AdminActionParams>,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;

    let message = form.into_inner().message;
    validate_admin_message(&message, "Justification").map_err_app(&persistent)?;

    let author_url = story.author_path(&authors);
    let story_url = story.url_fragment.clone();
    let chapter_number = chapter.chapter_number;
    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!("Deleted story id={}", story.id),
        message,
    };

    let _ = web::block({
        let moderator =
            models::users::ModeratorUserWrapper::from_user(login_user).map_err_app(&persistent)?;
        let story_title = story.title;
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            admin::delete_chapter_administrator(
                moderator,
                &authors,
                chapter,
                &story_title,
                action,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    Ok(HttpResponse::SeeOther()
        .header(
            "Location",
            format!(
                "/admin/@{}/{}/{}/{}",
                author_url,
                story_url,
                chapter_number,
                PersistentTemplate::from(&persistent).as_leading_qmark()
            ),
        )
        .finish())
}
