use actix_web::{get, web, HttpResponse};
use askama::Template;
use serde::Deserialize;

use intertextual::actions;
use intertextual::models::error::IntertextualError;
use intertextual::models::filter::FilterMode;
use intertextual::models::shared::AppState;

use crate::app::identity::Identity;
use crate::app::persistent::*;
use crate::endpoints::statistics::graphs::*;
use crate::error::*;

#[derive(Template)]
#[template(path = "admin/statistics.html")]
struct AdminStatisticsTemplate {
    persistent: PersistentTemplate,
    range_query: AdminQueryRangeMode,
    stats: GlobalStatistics,
    user_registration: Option<TimeSeriesGraph>,
    story_creation: Option<TimeSeriesGraph>,
    story_hits: Option<TimeSeriesGraph>,
    user_approvals: Option<TimeSeriesGraph>,
    comments: Option<TimeSeriesGraph>,
    recommendations: Option<TimeSeriesGraph>,
}

pub struct GlobalStatistics {
    visible_user_count: i64,
    user_count: i64,
    story_count: i64,
    admin_story_count: i64,
    hit_count: i64,
    user_approval_count: i64,
    comment_count: i64,
    recommendation_count: i64,
}

pub struct GraphEntries {
    pub users: Vec<chrono::NaiveDateTime>,
    pub stories: Vec<chrono::NaiveDateTime>,
    pub hits: Vec<chrono::NaiveDateTime>,
    pub user_approvals: Vec<chrono::NaiveDateTime>,
    pub comments: Vec<chrono::NaiveDateTime>,
    pub recommendations: Vec<chrono::NaiveDateTime>,
}

#[derive(Deserialize)]
pub struct AdminStatisticsQuery {
    pub t: Option<String>,
    pub range: Option<String>,
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum AdminQueryRangeMode {
    Daily,
    Weekly,
    Monthly,
    Last100,
}

impl AdminQueryRangeMode {
    pub fn parse_query(url: &Option<String>) -> AdminQueryRangeMode {
        match url.as_deref() {
            Some("daily") => AdminQueryRangeMode::Daily,
            Some("weekly") => AdminQueryRangeMode::Weekly,
            Some("monthly") => AdminQueryRangeMode::Monthly,
            Some("100") => AdminQueryRangeMode::Last100,
            _ => AdminQueryRangeMode::Daily,
        }
    }

    pub fn duration(&self) -> chrono::Duration {
        match self {
            AdminQueryRangeMode::Daily => chrono::Duration::hours(24),
            AdminQueryRangeMode::Weekly => chrono::Duration::days(7),
            AdminQueryRangeMode::Monthly => chrono::Duration::days(30),
            AdminQueryRangeMode::Last100 => chrono::Duration::days(100),
        }
    }

    pub fn range(&self) -> GraphRangeMode {
        match self {
            AdminQueryRangeMode::Daily => GraphRangeMode::Hourly { hours: 24 },
            AdminQueryRangeMode::Weekly => GraphRangeMode::Daily { days: 7 },
            AdminQueryRangeMode::Monthly => GraphRangeMode::Daily { days: 30 },
            AdminQueryRangeMode::Last100 => GraphRangeMode::Daily { days: 100 },
        }
    }

    pub fn description(&self) -> &'static str {
        match self {
            AdminQueryRangeMode::Daily => "Last 24 hours",
            AdminQueryRangeMode::Weekly => "Last 7 days",
            AdminQueryRangeMode::Monthly => "Last 30 days",
            AdminQueryRangeMode::Last100 => "Last 100 days",
        }
    }
}

#[get("/admin/statistics/")]
async fn main_page(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<AdminStatisticsQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let range_query = AdminQueryRangeMode::parse_query(&url_params.range);

    let end = chrono::Utc::now().naive_utc();
    let start = end - range_query.duration();

    let (stats, total_graphs) = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || -> Result<(GlobalStatistics, GraphEntries), IntertextualError> {
            let global_statistics = GlobalStatistics {
                visible_user_count: actions::users::admin::find_all_visible_users_count(&conn)?,
                user_count: actions::users::admin::find_all_users_count(&conn)?,
                story_count: actions::stories::find_stories_quantity(&FilterMode::Standard, &conn)?,
                admin_story_count: actions::stories::find_stories_quantity(
                    &FilterMode::BypassFilters,
                    &conn,
                )?,
                hit_count: actions::hits::admin::find_total_hit_count(&conn)?,
                user_approval_count:
                    actions::user_approvals::admin::find_total_user_approval_count(&conn)?,
                comment_count: actions::comments::admin::find_total_comments_count(&conn)?,
                recommendation_count:
                    actions::recommendations::admin::find_total_recommendations_count(&conn)?,
            };
            let graph_entries = GraphEntries {
                users: actions::users::admin::find_all_user_registration_times_between(
                    start, end, &conn,
                )?,
                stories: actions::stories::admin::find_all_story_internal_creation_times_between(
                    start, end, &conn,
                )?,
                hits: actions::hits::admin::find_all_hit_times_between(start, end, &conn)?,
                user_approvals: actions::user_approvals::admin::find_user_approval_times_between(
                    start, end, &conn,
                )?,
                comments: actions::comments::admin::find_all_comment_times_between(
                    start, end, &conn,
                )?,
                recommendations:
                    actions::recommendations::admin::find_all_recommendation_times_between(
                        start, end, &conn,
                    )?,
            };
            Ok((global_statistics, graph_entries))
        }
    })
    .await
    .map_err_app(&persistent)?;

    let range = range_query.range();
    let s = AdminStatisticsTemplate {
        persistent: PersistentTemplate::from(&persistent),
        range_query,
        stats,
        user_registration: create_graph_from_time_series(&total_graphs.users, range),
        story_creation: create_graph_from_time_series(&total_graphs.stories, range),
        story_hits: create_graph_from_time_series(&total_graphs.hits, range),
        user_approvals: create_graph_from_time_series(&total_graphs.user_approvals, range),
        comments: create_graph_from_time_series(&total_graphs.comments, range),
        recommendations: create_graph_from_time_series(&total_graphs.recommendations, range),
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
