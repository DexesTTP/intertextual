use actix_web::{get, web, HttpResponse};
use askama::Template;
use serde::Deserialize;

use intertextual::actions;
use intertextual::models;
use intertextual::models::error::IntertextualError;
use intertextual::models::filter::FilterMode;
use intertextual::models::shared::AppState;
use intertextual::models::stories::StoryEntry;
use intertextual::models::stories::StoryQueryExtraInfos;
use intertextual::utils::page_list::PageListing;

use crate::app::identity::Identity;
use crate::app::persistent::*;
use crate::error::*;

const STORIES_PER_PAGE: i64 = 20;

#[derive(Template)]
#[template(path = "admin/story_list.html")]
struct AdminStoryListTemplate {
    persistent: PersistentTemplate,
    sort_mode: SortByMode,
    stories: Vec<StoryEntry>,
    page_listing: PageListing,
}

#[derive(Deserialize)]
pub struct StoryListQuery {
    pub t: Option<String>,
    pub start: Option<String>,
    pub sort_by: Option<String>,
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum SortByMode {
    TitleAsc,
    TitleDesc,
    CreationAsc,
    CreationDesc,
    PublicationAsc,
    PublicationDesc,
    UpdateAsc,
    UpdateDesc,
}

impl SortByMode {
    pub fn parse(sort_by: &Option<String>) -> SortByMode {
        match sort_by.as_deref() {
            Some(t) if t == "title_asc" => SortByMode::TitleAsc,
            Some(t) if t == "title_desc" => SortByMode::TitleDesc,
            Some(t) if t == "creation_asc" => SortByMode::CreationAsc,
            Some(t) if t == "creation_desc" => SortByMode::CreationDesc,
            Some(t) if t == "publication_asc" => SortByMode::PublicationAsc,
            Some(t) if t == "publication_desc" => SortByMode::PublicationDesc,
            Some(t) if t == "update_asc" => SortByMode::UpdateAsc,
            Some(t) if t == "update_desc" => SortByMode::UpdateDesc,
            _ => SortByMode::TitleAsc,
        }
    }

    pub fn to_story_sort_mode(self) -> models::stories::StorySortMode {
        use models::stories::StorySortMode;
        match self {
            SortByMode::TitleAsc => StorySortMode::TitleAsc,
            SortByMode::TitleDesc => StorySortMode::TitleDesc,
            SortByMode::CreationAsc => StorySortMode::InternalCreationAsc,
            SortByMode::CreationDesc => StorySortMode::InternalCreationDesc,
            SortByMode::PublicationAsc => StorySortMode::PublicationAsc,
            SortByMode::PublicationDesc => StorySortMode::PublicationDesc,
            SortByMode::UpdateAsc => StorySortMode::LastUpdateAsc,
            SortByMode::UpdateDesc => StorySortMode::LastUpdateDesc,
        }
    }

    pub fn next_title_sort(&self) -> &'static str {
        match self {
            Self::TitleAsc => "title_desc",
            _ => "title_asc",
        }
    }

    pub fn title_symbol(&self) -> &'static str {
        match self {
            Self::TitleAsc => "↓",
            Self::TitleDesc => "↑",
            _ => "",
        }
    }

    pub fn next_creation_sort(&self) -> &'static str {
        match self {
            Self::CreationDesc => "creation_asc",
            _ => "creation_desc",
        }
    }

    pub fn creation_symbol(&self) -> &'static str {
        match self {
            Self::CreationAsc => "↑",
            Self::CreationDesc => "↓",
            _ => "",
        }
    }

    pub fn next_publication_sort(&self) -> &'static str {
        match self {
            Self::PublicationAsc => "publication_asc",
            _ => "publication_desc",
        }
    }

    pub fn publication_symbol(&self) -> &'static str {
        match self {
            Self::PublicationAsc => "↑",
            Self::PublicationDesc => "↓",
            _ => "",
        }
    }

    pub fn next_update_sort(&self) -> &'static str {
        match self {
            Self::UpdateDesc => "update_asc",
            _ => "update_desc",
        }
    }

    pub fn update_symbol(&self) -> &'static str {
        match self {
            Self::UpdateAsc => "↑",
            Self::UpdateDesc => "↓",
            _ => "",
        }
    }

    pub fn current_amp(&self) -> &'static str {
        match self {
            SortByMode::TitleAsc => "",
            SortByMode::TitleDesc => "&sort_by=title_desc",
            SortByMode::CreationAsc => "&sort_by=creation_asc",
            SortByMode::CreationDesc => "&sort_by=creation_desc",
            SortByMode::PublicationAsc => "&sort_by=publication_asc",
            SortByMode::PublicationDesc => "&sort_by=publication_desc",
            SortByMode::UpdateAsc => "&sort_by=update_asc",
            SortByMode::UpdateDesc => "&sort_by=update_desc",
        }
    }
}

#[get("/admin/stories/")]
async fn admin_page_stories(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<StoryListQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let start = url_params
        .start
        .and_then(|s| s.parse::<i64>().ok())
        .unwrap_or(0);
    let sort_mode = SortByMode::parse(&url_params.sort_by);
    let filter_mode = FilterMode::from_login(login_user);

    let (stories, story_quantity) = web::block({
        let sort_mode = sort_mode.to_story_sort_mode();
        let user_info = StoryQueryExtraInfos::from_user(&data.site, login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || -> Result<(Vec<models::stories::StoryEntry>, i64), IntertextualError> {
            let stories = actions::stories::find_stories_by_sort_mode(
                start,
                STORIES_PER_PAGE,
                sort_mode,
                &user_info.filter_mode,
                &conn,
            )?;
            let mut result: Vec<models::stories::StoryEntry> = Vec::with_capacity(stories.len());
            for story in stories {
                let entry = models::stories::StoryEntry::from_database(story, &user_info, &conn)?;
                result.push(entry);
            }
            Ok((
                result,
                actions::stories::find_stories_quantity(&filter_mode, &conn)?,
            ))
        }
    })
    .await
    .map_err_app(&persistent)?;

    let s = AdminStoryListTemplate {
        persistent: PersistentTemplate::from(&persistent),
        stories,
        page_listing: PageListing::get_from_count(story_quantity, start, STORIES_PER_PAGE),
        sort_mode,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
