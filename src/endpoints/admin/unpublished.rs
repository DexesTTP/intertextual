use actix_web::{get, web, HttpResponse};
use askama::Template;

use intertextual::actions;
use intertextual::models;

use crate::prelude::*;

#[derive(Template)]
#[template(path = "admin/story_unpublished.html")]
struct AdminChaptersUnpublishedTemplate {
    persistent: PersistentTemplate,
    results: Vec<AdminChapterResult>,
}

struct AdminChapterResult {
    pub url_fragment: String,
    pub title: String,
    pub chapter_number: i32,
    pub authors: Vec<models::users::ShortUserEntry>,
    pub author_path: String,
    pub tags: Vec<models::tags::CanonicalTag>,
    pub published_after_date: String,
    pub locked: bool,
}

impl AdminChapterResult {
    pub fn from(
        story: models::stories::Story,
        chapter: models::stories::Chapter,
        authors: &[models::users::User],
        tags: Vec<(models::tags::TagCategory, models::tags::CanonicalTag)>,
    ) -> Self {
        let author_path = story.author_path(authors);
        Self {
            title: story.title,
            url_fragment: story.url_fragment,
            chapter_number: chapter.chapter_number,
            authors: authors
                .iter()
                .map(models::users::ShortUserEntry::from)
                .collect(),
            author_path,
            tags: tags.into_iter().map(|(_, t)| t).collect(),
            locked: chapter.moderator_locked,
            published_after_date: chapter
                .show_publicly_after_date
                .map_or_else(String::new, |d| {
                    d.format("%Y-%m-%d %H:%M (UTC+00)").to_string()
                }),
        }
    }
}

#[get("/admin/unpublished/")]
async fn unpublished_stories(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let stories = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::stories::admin::find_all_unpublished_chapters(&conn)
    })
    .await
    .map_err_app(&persistent)?;

    let mut results = Vec::<AdminChapterResult>::new();
    results.reserve(stories.len());
    for (story, chapter) in stories {
        let authors = web::block({
            let story_id = story.id;
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                actions::stories::find_authors_by_story(story_id, &FilterMode::BypassFilters, &conn)
            }
        })
        .await
        .map_err_app(&persistent)?;
        let tags = web::block({
            let story_id = story.id;
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || actions::tags::find_all_canonical_tags_by_story_id(story_id, &conn)
        })
        .await
        .map_err_app(&persistent)?;
        results.push(AdminChapterResult::from(story, chapter, &authors, tags));
    }

    let s = AdminChaptersUnpublishedTemplate {
        persistent: PersistentTemplate::from(&persistent),
        results,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
