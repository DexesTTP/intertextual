use actix_web::{get, post, web, HttpResponse};
use askama::Template;
use serde::{Deserialize, Serialize};

use intertextual::actions;
use intertextual::constants::RECOMMENDATION_URL;
use intertextual::models;
use intertextual::models::admin::NewModerationAction;
use intertextual::models::stories::StoryEntry;
use intertextual::models::stories::StoryQueryExtraInfos;
use intertextual::utils::page_list::PageListing;

use crate::app::validation::validate_admin_message;

use crate::prelude::*;

const RECOMMENDATIONS_PER_PAGE: i64 = 20;

#[derive(Template)]
#[template(path = "admin/recommendation_list.html")]
struct AdminRecommendationsListTemplate {
    persistent: PersistentTemplate,
    sort_mode: SortByMode,
    recommendations: Vec<RecommendationEntry>,
    page_listing: PageListing,
}

struct RecommendationEntry {
    recommendation: models::recommendations::Recommendation,
    author: models::users::User,
    story: StoryEntry,
}

#[derive(Template)]
#[template(path = "admin/delete_recommendation.html")]
struct AdminDeleteRecommendationTemplate {
    persistent: PersistentTemplate,
    user: models::users::User,
    authors: Vec<models::users::User>,
    author_path: String,
    story: models::stories::Story,
}

#[derive(Serialize, Deserialize)]
pub struct AdminActionParams {
    message: String,
}

#[derive(Deserialize)]
pub struct RecommendationListQuery {
    pub t: Option<String>,
    pub start: Option<String>,
    pub sort_by: Option<String>,
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum SortByMode {
    CreationAsc,
    CreationDesc,
}

impl SortByMode {
    pub fn parse(sort_by: &Option<String>) -> SortByMode {
        match sort_by.as_deref() {
            Some(t) if t == "creation_asc" => SortByMode::CreationAsc,
            Some(t) if t == "creation_desc" => SortByMode::CreationDesc,
            _ => SortByMode::CreationDesc,
        }
    }

    pub fn to_recommendation_sort_mode(
        self,
    ) -> actions::recommendations::admin::RecommendationsSortMode {
        use actions::recommendations::admin::RecommendationsSortMode;
        match self {
            SortByMode::CreationAsc => RecommendationsSortMode::CreationAsc,
            SortByMode::CreationDesc => RecommendationsSortMode::CreationDesc,
        }
    }

    pub fn next_creation_sort(&self) -> &'static str {
        match self {
            Self::CreationDesc => "creation_asc",
            _ => "creation_desc",
        }
    }

    pub fn creation_symbol(&self) -> &'static str {
        match self {
            Self::CreationAsc => "↑",
            Self::CreationDesc => "↓",
        }
    }

    pub fn current_amp(&self) -> &'static str {
        match self {
            SortByMode::CreationAsc => "&sort_by=creation_asc",
            SortByMode::CreationDesc => "",
        }
    }
}

#[get("/admin/recommendations/")]
async fn admin_recommendations_list(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<RecommendationListQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let start = url_params
        .start
        .and_then(|s| s.parse::<i64>().ok())
        .unwrap_or(0);
    let sort_mode = SortByMode::parse(&url_params.sort_by);

    let (recommendations, recommendations_quantity) = web::block({
        let sort_mode = sort_mode.to_recommendation_sort_mode();
        let user_info = StoryQueryExtraInfos::from_user(&data.site, login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || -> Result<(Vec<RecommendationEntry>, i64), IntertextualError> {
            let recommendation =
                actions::recommendations::admin::find_all_recommendations_by_sort_mode(
                    start,
                    RECOMMENDATIONS_PER_PAGE,
                    sort_mode,
                    &conn,
                )?;
            let mut result: Vec<RecommendationEntry> = Vec::with_capacity(recommendation.len());
            for recommendation in recommendation {
                if let Some(story) =
                    actions::stories::find_story_by_id(recommendation.story_id, &conn)?
                {
                    if let Some(author) =
                        actions::users::find_user_by_id(recommendation.user_id, &conn)?
                    {
                        let story = StoryEntry::from_database(story, &user_info, &conn)?;
                        result.push(RecommendationEntry {
                            recommendation,
                            author,
                            story,
                        });
                    }
                }
            }
            Ok((
                result,
                actions::recommendations::admin::find_total_recommendations_count(&conn)?,
            ))
        }
    })
    .await
    .map_err_app(&persistent)?;

    let s = AdminRecommendationsListTemplate {
        persistent: PersistentTemplate::from(&persistent),
        recommendations,
        page_listing: PageListing::get_from_count(
            recommendations_quantity,
            start,
            RECOMMENDATIONS_PER_PAGE,
        ),
        sort_mode,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/admin/recommendations/@{user}/collaboration/{story}/")]
async fn main_page_collaboration(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let (user_name, url_fragment) = path.into_inner();
    let (recommender, authors, story, _recommendation) = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            intertextual::data::recommendations::find_collaboration_recommendation_by_url(
                user_name,
                url_fragment,
                &models::filter::FilterMode::BypassFilters,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    let s = AdminDeleteRecommendationTemplate {
        persistent: PersistentTemplate::from(&persistent),
        user: recommender,
        authors,
        author_path: "collaboration".to_string(),
        story,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/admin/recommendations/@{user}/@{author}/{story}/")]
async fn main_page_single_author(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, String)>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let (user_name, author_name, url_fragment) = path.into_inner();
    let (recommender, author, story, _recommendation) = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            intertextual::data::recommendations::find_author_recommendation_by_url(
                user_name,
                author_name,
                url_fragment,
                &models::filter::FilterMode::BypassFilters,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    let author_path = format!("@{}", author.username);
    let s = AdminDeleteRecommendationTemplate {
        persistent: PersistentTemplate::from(&persistent),
        user: recommender,
        authors: vec![author],
        author_path,
        story,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/admin/recommendations/@{user}/collaboration/{story}/delete/")]
async fn handle_delete_collaboration(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    form: web::Form<AdminActionParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let (user_name, url_fragment) = path.into_inner();
    let (recommender, _authors, story, recommendation) = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            intertextual::data::recommendations::find_collaboration_recommendation_by_url(
                user_name,
                url_fragment,
                &models::filter::FilterMode::BypassFilters,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    let message = form.into_inner().message;
    validate_admin_message(&message, "Justification").map_err_app(&persistent)?;

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!(
            "Deleted recommendation by id={} for collaboration story id={}",
            recommender.id, story.id
        ),
        message,
    };

    web::block({
        let moderator =
            models::users::ModeratorUserWrapper::from_user(login_user).map_err_app(&persistent)?;
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            actions::recommendations::admin::delete_recommendation_administrator(
                moderator,
                recommendation,
                action,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    Ok(HttpResponse::SeeOther()
        .header(
            "Location",
            format!(
                "/{}/@{}/{}",
                RECOMMENDATION_URL,
                recommender.username,
                PersistentTemplate::from(&persistent).as_leading_qmark()
            ),
        )
        .finish())
}

#[post("/admin/recommendations/@{user}/@{author}/{story}/delete/")]
async fn handle_delete_single_author(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, String)>,
    form: web::Form<AdminActionParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequiredObfuscated)
        .map_err_app(&persistent)?;
    login_user
        .require_administrator_level()
        .map_err_app(&persistent)?;

    let (user_name, author_name, url_fragment) = path.into_inner();
    let (recommender, author, story, recommendation) = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            intertextual::data::recommendations::find_author_recommendation_by_url(
                user_name,
                author_name,
                url_fragment,
                &models::filter::FilterMode::BypassFilters,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    let message = form.into_inner().message;
    validate_admin_message(&message, "Justification").map_err_app(&persistent)?;

    let action = NewModerationAction {
        username: login_user.username.clone(),
        action_type: format!(
            "Deleted recommendation by id={} for story id={} of user id={}",
            recommender.id, story.id, author.id
        ),
        message,
    };

    web::block({
        let moderator =
            models::users::ModeratorUserWrapper::from_user(login_user).map_err_app(&persistent)?;
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            actions::recommendations::admin::delete_recommendation_administrator(
                moderator,
                recommendation,
                action,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    Ok(HttpResponse::SeeOther()
        .header(
            "Location",
            format!(
                "/{}/@{}/{}",
                RECOMMENDATION_URL,
                recommender.username,
                PersistentTemplate::from(&persistent).as_leading_qmark()
            ),
        )
        .finish())
}
