use actix_web::{get, web, HttpResponse};
use askama::Template;

use intertextual::actions;
use intertextual::data;
use intertextual::models;
use intertextual::models::error::IntertextualError;
use intertextual::models::filter::FilterMode;
use intertextual::models::shared::AppState;

use crate::app::identity::Identity;
use crate::app::persistent::*;
use crate::endpoints::statistics::graphs::*;
use crate::endpoints::statistics::shared::*;
use crate::error::*;

#[derive(Template)]
#[template(path = "statistics/story.html")]
struct StoryStatisticsTemplate {
    persistent: PersistentTemplate,
    range_query: QueryRangeMode,
    sort_mode: StorySortByMode,
    authors: Vec<models::users::ShortUserEntry>,
    author_path: String,
    story: models::stories::Story,
    stats: StoryStatistics,
    chapters: Vec<ChapterStatistics>,
    story_hits: Option<TimeSeriesGraph>,
    user_approvals: Option<TimeSeriesGraph>,
    comments: Option<TimeSeriesGraph>,
    recommendations: Option<TimeSeriesGraph>,
}

#[get("/statistics/@{user}/{story}/")]
async fn story_author_page(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    url_params: web::Query<StatisticsQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let (authors, story) = web::block({
        let mode = FilterMode::from_login_opt(&login);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    story_page_shared(
        persistent,
        data,
        authors,
        story,
        url_params.range(),
        url_params.story_sort_by(),
    )
    .await
}

#[get("/statistics/collaboration/{story}/")]
async fn story_collaboration_page(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<StatisticsQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let (authors, story) = web::block({
        let mode = FilterMode::from_login_opt(&login);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    story_page_shared(
        persistent,
        data,
        authors,
        story,
        url_params.range(),
        url_params.story_sort_by(),
    )
    .await
}

async fn story_page_shared(
    persistent: PersistentData,
    data: web::Data<AppState>,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
    range_query: QueryRangeMode,
    sort_mode: StorySortByMode,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        let story_id = story.id;
        let user_id = login_user.id;
        move || actions::access::require_management_rights_for(story_id, user_id, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    let (stats, mut chapters, graphs) = web::block({
        let story = story.clone();
        let filter_mode = FilterMode::from_login(login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || get_story_statistics(&story, &filter_mode, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    sort_mode.sort_list(&mut chapters);

    let range = range_query.range(graphs.min_time());
    let story_hits = create_graph_from_time_series(&graphs.hits, range);
    let user_approvals = create_graph_from_time_series(&graphs.user_approvals, range);
    let comments = create_graph_from_time_series(&graphs.comments, range);
    let recommendations = create_graph_from_time_series(&graphs.recommendations, range);

    let author_path = story.author_path(&authors);
    let s = StoryStatisticsTemplate {
        persistent: PersistentTemplate::from(&persistent),
        range_query,
        sort_mode,
        authors: authors
            .iter()
            .map(models::users::ShortUserEntry::from)
            .collect(),
        author_path,
        stats,
        story,
        chapters,
        story_hits,
        user_approvals,
        recommendations,
        comments,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
