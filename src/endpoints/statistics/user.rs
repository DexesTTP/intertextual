use actix_web::{get, web, HttpResponse};
use askama::Template;

use intertextual::actions;
use intertextual::models;
use intertextual::models::error::IntertextualError;
use intertextual::models::filter::FilterMode;
use intertextual::models::shared::AppState;

use crate::app::identity::Identity;
use crate::app::persistent::*;
use crate::endpoints::statistics::graphs::*;
use crate::endpoints::statistics::shared::*;
use crate::error::*;

#[derive(Template)]
#[template(path = "statistics/user.html")]
struct UserStatisticsTemplate {
    persistent: PersistentTemplate,
    range_query: QueryRangeMode,
    sort_mode: UserSortByMode,
    user: models::users::User,
    stats: AuthorStatistics,
    story_hits: Option<TimeSeriesGraph>,
    user_approvals: Option<TimeSeriesGraph>,
    comments: Option<TimeSeriesGraph>,
    recommendations: Option<TimeSeriesGraph>,
    stories: Vec<(ShortStoryData, StoryStatistics)>,
}

#[get("/statistics/")]
async fn main_page(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<StatisticsQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let username = {
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequired)
            .map_err_app(&persistent)?;
        login_user.username.clone()
    };
    user_page_shared(
        data,
        persistent,
        username,
        url_params.range(),
        url_params.user_sort_by(),
    )
    .await
}

#[get("/statistics/@{user}/")]
async fn user_specific_page(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<StatisticsQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let username = path.into_inner();
    user_page_shared(
        data,
        persistent,
        username,
        url_params.range(),
        url_params.user_sort_by(),
    )
    .await
}

async fn user_page_shared(
    data: web::Data<AppState>,
    persistent: PersistentData,
    username: String,
    range_query: QueryRangeMode,
    sort_mode: UserSortByMode,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let filter_mode = FilterMode::from_login(login_user);

    let user = {
        if login_user.username == username {
            web::block({
                let id = login_user.id;
                let conn = data.pool.get().map_err_app(&persistent)?;
                move || actions::users::find_user_by_id(id, &conn)
            })
            .await
            .map_err_app(&persistent)?
            .ok_or(IntertextualError::UserNotFound { username })
            .map_err_app(&persistent)?
        } else {
            web::block({
                let username = username.clone();
                let filter_mode = filter_mode.clone();
                let conn = data.pool.get().map_err_app(&persistent)?;
                move || actions::users::find_user_by_username(&username, &filter_mode, &conn)
            })
            .await
            .map_err_app(&persistent)?
            .ok_or(IntertextualError::UserNotFound { username })
            .map_err_app(&persistent)?
        }
    };

    login_user
        .require_see_statistics_rights_for(std::slice::from_ref(&user))
        .map_err_app(&persistent)?;

    let (user_statistics, mut stories, total_graphs) = web::block({
        let user_id = user.id;
        let filter_mode = filter_mode.clone();
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || get_author_statistics(user_id, &filter_mode, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    sort_mode.sort_list(&mut stories);

    let range = range_query.range(total_graphs.min_time());
    let s = UserStatisticsTemplate {
        persistent: PersistentTemplate::from(&persistent),
        range_query,
        sort_mode,
        user,
        stats: user_statistics,
        stories,
        story_hits: create_graph_from_time_series(&total_graphs.hits, range),
        user_approvals: create_graph_from_time_series(&total_graphs.user_approvals, range),
        comments: create_graph_from_time_series(&total_graphs.comments, range),
        recommendations: create_graph_from_time_series(&total_graphs.recommendations, range),
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
