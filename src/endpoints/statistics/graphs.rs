use std::convert::TryFrom;

use chrono::Timelike;

const GRAPH_START_X: i64 = 10;
const GRAPH_START_Y: i64 = 90;
const GRAPH_WIDTH: i64 = 650;
const GRAPH_HEIGHT: i64 = 80;
const PIXELS_BETWEEN_LEGENDS: i64 = 10;

pub struct TimeSeriesGraph {
    pub data: Vec<TimeSeriesEntry>,
    pub max: i64,
}

pub struct TimeSeriesEntry {
    pub prev_x: i64,
    pub prev_y: i64,
    pub x: i64,
    pub y: i64,
    pub name: String,
    pub show_legend: bool,
}

#[derive(Clone, Copy)]
pub enum GraphRangeMode {
    Hourly { hours: i64 },
    Daily { days: i64 },
    DailyExact { start_date: chrono::NaiveDate },
}

pub fn create_graph_from_time_series(
    raw_entries: &[chrono::NaiveDateTime],
    range: GraphRangeMode,
) -> Option<TimeSeriesGraph> {
    match range {
        GraphRangeMode::Hourly { hours } => {
            let start_time = chrono::Utc::now().naive_utc() - chrono::Duration::hours(hours);
            create_hourly_graph_from_time_series(raw_entries, start_time)
        }
        GraphRangeMode::Daily { days } => {
            let start_date = (chrono::Utc::now().naive_utc() - chrono::Duration::days(days)).date();
            create_daily_graph_from_time_series(raw_entries, start_date)
        }
        GraphRangeMode::DailyExact { start_date } => {
            create_daily_graph_from_time_series(raw_entries, start_date)
        }
    }
}

pub fn create_daily_graph_from_time_series(
    raw_entries: &[chrono::NaiveDateTime],
    start_day: chrono::NaiveDate,
) -> Option<TimeSeriesGraph> {
    let end_day = chrono::Utc::now().naive_utc().date();
    let range = end_day.signed_duration_since(start_day);
    let mut entries = Vec::<(chrono::NaiveDate, i64)>::with_capacity(
        usize::try_from(range.num_days()).unwrap_or(0),
    );

    let mut day = start_day;
    while day <= end_day {
        entries.push((
            day,
            i64::try_from(raw_entries.iter().filter(|h| h.date() == day).count()).unwrap_or(0),
        ));
        day += chrono::Duration::days(1);
    }

    let max: i64 = entries
        .iter()
        .max_by_key(|(_, entry)| entry)
        .map(|(_, entry)| entry)
        .cloned()
        .unwrap_or(0);
    let max_hits_divider = if max == 0 { 1 } else { max };

    let mut data = Vec::with_capacity(entries.len());
    let length = i64::try_from(entries.len()).unwrap_or(0);
    let step_width = if length > 1 {
        GRAPH_WIDTH as f32 / (length - 1) as f32
    } else {
        GRAPH_WIDTH as f32
    };
    let mut prev_y = None;
    let mut running_x = (GRAPH_START_X as f32) - step_width;
    let mut prev_x = None;
    let mut last_legend_x = None;
    for (day, entry_value) in entries {
        let height = (entry_value * GRAPH_HEIGHT) / max_hits_divider;
        running_x += step_width;
        let x = running_x;
        let y = GRAPH_START_Y - height;
        let show_legend = if let Some(last_position) = last_legend_x {
            last_position + (PIXELS_BETWEEN_LEGENDS as f32) < x
        } else {
            true
        };
        data.push(TimeSeriesEntry {
            prev_x: prev_x.unwrap_or(x) as i64,
            prev_y: prev_y.unwrap_or(y),
            x: x as i64,
            y,
            name: day.format("%Y-%m-%d").to_string(),
            show_legend,
        });
        if show_legend {
            last_legend_x = Some(x);
        }
        prev_x = Some(x);
        prev_y = Some(y);
    }

    Some(TimeSeriesGraph { data, max })
}

pub fn create_hourly_graph_from_time_series(
    raw_entries: &[chrono::NaiveDateTime],
    start_time: chrono::NaiveDateTime,
) -> Option<TimeSeriesGraph> {
    let end_time = chrono::Utc::now().naive_utc();

    let range = end_time.signed_duration_since(start_time);
    let mut entries: Vec<(chrono::NaiveDateTime, i64)> =
        Vec::with_capacity(usize::try_from(range.num_hours()).unwrap_or(0));

    let mut current_hour = start_time;
    while current_hour <= end_time {
        let entry_count_for_hour = raw_entries
            .iter()
            .filter(|hit_time| {
                hit_time.date() == current_hour.date() && hit_time.hour() == current_hour.hour()
            })
            .count();
        entries.push((
            current_hour,
            i64::try_from(entry_count_for_hour).unwrap_or(0),
        ));
        current_hour += chrono::Duration::hours(1);
    }

    let max: i64 = entries
        .iter()
        .max_by_key(|(_, entry)| entry)
        .map(|(_, entry)| entry)
        .cloned()
        .unwrap_or(0);
    let max_hits_divider = if max == 0 { 1 } else { max };

    let mut data = Vec::with_capacity(entries.len());
    let length = i64::try_from(entries.len()).unwrap_or(0);
    let step_width = if length > 1 {
        GRAPH_WIDTH as f32 / (length - 1) as f32
    } else {
        GRAPH_WIDTH as f32
    };
    let mut prev_y = None;
    let mut running_x = (GRAPH_START_X as f32) - step_width;
    let mut prev_x = None;
    let mut last_legend_x = None;
    for (hour, entry_value) in entries {
        let height = (entry_value * GRAPH_HEIGHT) / max_hits_divider;
        running_x += step_width;
        let x = running_x;
        let y = GRAPH_START_Y - height;
        let show_legend = if let Some(last_position) = last_legend_x {
            last_position + (PIXELS_BETWEEN_LEGENDS as f32) < x
        } else {
            true
        };
        data.push(TimeSeriesEntry {
            prev_x: prev_x.unwrap_or(x) as i64,
            prev_y: prev_y.unwrap_or(y),
            x: x as i64,
            y,
            name: if hour.hour() == 0 {
                hour.format("%Y-%m-%d %H:00").to_string()
            } else {
                hour.format("%H:00").to_string()
            },
            show_legend,
        });
        if show_legend {
            last_legend_x = Some(x);
        }
        prev_x = Some(x);
        prev_y = Some(y);
    }

    Some(TimeSeriesGraph { data, max })
}
