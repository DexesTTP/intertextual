use actix_web::{get, post, web, HttpResponse};
use askama::Template;
use serde::{Deserialize, Serialize};

use intertextual::actions;
use intertextual::models::error::IntertextualError;
use intertextual::models::shared::AppState;

use crate::app::identity::Identity;
use crate::app::identity::IdentityData;
use crate::app::identity::IdentityRememberMode;
use crate::app::persistent::*;
use crate::app::validation::validate_age_verification;
use crate::app::validation::validate_password;
use crate::app::validation::validate_username;
use crate::error::*;

#[derive(Template)]
#[template(path = "user/register.html")]
struct RegisterTemplate {
    persistent: PersistentTemplate,
    allow_register: bool,
}

#[derive(Serialize, Deserialize)]
struct NewUserParams {
    username: String,
    password: String,
    password_confirm: String,
    rules_verification: Option<String>,
    age_verification: Option<String>,
}

#[get("/register/")]
async fn register_page(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    match persistent.login() {
        Some(user) => Ok(HttpResponse::Found()
            .header(
                "Location",
                format!(
                    "/@{}/{}",
                    user.username,
                    PersistentTemplate::from(&persistent).as_leading_qmark()
                ),
            )
            .finish()),
        None => {
            let allow_register = web::block({
                let conn = data.pool.get().map_err_app(&persistent)?;
                move || actions::users::get_registrations_enabled(&conn)
            })
            .await
            .map_err_app(&persistent)?;
            let s = RegisterTemplate {
                persistent: PersistentTemplate::from(&persistent),
                allow_register,
            }
            .render()
            .map_err_app(&persistent)?;
            Ok(HttpResponse::Ok().content_type("text/html").body(s))
        }
    }
}

#[post("/register/")]
async fn handle_register(
    id: Identity,
    data: web::Data<AppState>,
    params: web::Form<NewUserParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent =
        PersistentData::from_query(&data, id.clone(), &url_params.into_inner()).await?;

    let allow_register = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::users::get_registrations_enabled(&conn)
    })
    .await
    .map_err_app(&persistent)?;
    if !allow_register {
        return Err(IntertextualError::AccessRightsRequired.into_app(&persistent));
    }

    let NewUserParams {
        username,
        password,
        password_confirm,
        rules_verification,
        age_verification,
    } = params.into_inner();

    // Check if the username is valid
    validate_username(&username, "Username").map_err_app(&persistent)?;

    // Check if the username is not already in use
    let existing_user = web::block({
        let username = username.clone();
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::users::find_user_by_username_for_login(&username, &conn)
    })
    .await
    .map_err_app(&persistent)?;
    if existing_user.is_some() {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Username",
            message: "This username is already in use".to_string(),
        })
        .map_err_app(&persistent);
    }

    // Check if the password has the right format
    validate_password(&password, "Password").map_err_app(&persistent)?;

    // Check if the repeated password matches the first one
    if password != password_confirm {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Password",
            message: "The passwords do not match".to_string(),
        }
        .into_app(&persistent));
    }

    // Check if the rules checkbox is checked
    validate_age_verification(&rules_verification, "Rules not accepted")
        .map_err_app(&persistent)?;

    // Check if the age checkbox is checked
    let persistent_data = PersistentTemplate::from(&persistent);
    if persistent_data.site.is_adult {
        validate_age_verification(&age_verification, "Age verification")
            .map_err_app(&persistent)?;
    }

    let result = web::block({
        let username = username.clone();
        let display_name = username.clone();
        let password_secret_key = data.password_secret_key.to_string();
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            actions::users::modifications::insert_user(
                username,
                display_name,
                password,
                password_secret_key,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent);
    if crate::app::is_unique_violation(&result) {
        return Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Username",
            message: format!("The username \"{}\" is already taken", username),
        }
        .into_app(&persistent));
    }
    let user = result?;

    let user_id = user.id;
    let token_id = web::block({
        let valid_until = chrono::Utc::now().naive_utc() + chrono::Duration::days(1);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            actions::users::modifications::add_auth_token_for_user(
                user_id,
                "First log-in".to_string(),
                valid_until,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    id.remember(
        IdentityData { user_id, token_id },
        IdentityRememberMode::Session,
    );

    Ok(HttpResponse::SeeOther()
        .header(
            "Location",
            format!(
                "/settings/@{}/{}",
                user.username,
                persistent_data.as_leading_qmark()
            ),
        )
        .finish())
}
