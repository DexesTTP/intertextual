use actix_web::{get, post, web, HttpResponse};
use askama::Template;
use serde::Deserialize;

use intertextual::actions;
use intertextual::actions::marked_for_later::MarkedForLaterSortMode;
use intertextual::data;
use intertextual::models;
use intertextual::models::filter::FilterMode;
use intertextual::models::stories::StoryEntry;
use intertextual::models::stories::StoryQueryExtraInfos;
use intertextual::utils::page_list::PageListing;

use crate::prelude::*;

const STORIES_PER_PAGE: i64 = 20;

#[derive(Template)]
#[template(path = "user/marked_for_later.html")]
struct MarkedForLaterTemplate {
    persistent: PersistentTemplate,
    user: models::users::User,
    stories: Vec<StoryEntry>,
    excluded_stories: Vec<StoryEntry>,
    page_listing: PageListing,
    sorting_mode: String,
}

#[derive(Deserialize)]
struct MarkedForLaterParams {
    t: Option<String>,
    start: Option<String>,
    sort_by: Option<String>,
}

#[get("/marked_for_later/")]
async fn main_page(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<MarkedForLaterParams>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let username = {
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequired)
            .map_err_app(&persistent)?;
        login_user.username.clone()
    };
    let start_index = url_params
        .start
        .and_then(|s| s.parse::<i64>().ok())
        .unwrap_or(0);
    let sorting_mode = MarkedForLaterSortMode::parse_url(url_params.sort_by.as_deref())
        .unwrap_or(MarkedForLaterSortMode::NewestFirst);
    shared_page(data, persistent, username, start_index, sorting_mode).await
}

#[get("/marked_for_later/@{user}/")]
async fn specific_page(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<MarkedForLaterParams>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let username = path.into_inner();
    let start_index = url_params
        .start
        .and_then(|s| s.parse::<i64>().ok())
        .unwrap_or(0);
    let sorting_mode = MarkedForLaterSortMode::parse_url(url_params.sort_by.as_deref())
        .unwrap_or(MarkedForLaterSortMode::NewestFirst);
    shared_page(data, persistent, username, start_index, sorting_mode).await
}

async fn shared_page(
    data: web::Data<AppState>,
    persistent: PersistentData,
    username: String,
    start_index: i64,
    sorting_mode: MarkedForLaterSortMode,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let filter_mode = FilterMode::from_login(login_user);

    let user = {
        if login_user.username == username {
            web::block({
                let id = login_user.id;
                let conn = data.pool.get().map_err_app(&persistent)?;
                move || actions::users::find_user_by_id(id, &conn)
            })
            .await
            .map_err_app(&persistent)?
            .ok_or(IntertextualError::UserNotFound { username })
            .map_err_app(&persistent)?
        } else {
            web::block({
                let username = username.clone();
                let filter_mode = filter_mode.clone();
                let conn = data.pool.get().map_err_app(&persistent)?;
                move || actions::users::find_user_by_username(&username, &filter_mode, &conn)
            })
            .await
            .map_err_app(&persistent)?
            .ok_or(IntertextualError::UserNotFound { username })
            .map_err_app(&persistent)?
        }
    };

    login_user
        .require_see_statistics_rights_for(std::slice::from_ref(&user))
        .map_err_app(&persistent)?;

    let (story_count, stories) = web::block({
        let user_id = user.id;
        let user_info = StoryQueryExtraInfos::from_user(&data.site, login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || -> Result<(i64, Vec<StoryEntry>), IntertextualError> {
            let story_count =
                actions::marked_for_later::total_marked_for_later_count_given_by_user(
                    user_id, &conn,
                )?;
            let raw_stories = actions::marked_for_later::marked_for_later_by_user(
                user_id,
                start_index,
                STORIES_PER_PAGE,
                sorting_mode,
                &conn,
            )?;
            let mut stories =
                Vec::with_capacity(std::cmp::min(raw_stories.len(), STORIES_PER_PAGE as usize));
            for (story, _time) in raw_stories {
                let story_entry = StoryEntry::from_database(story, &user_info, &conn)?;
                if story_entry.chapter_count == 0 {
                    // This story isn't actually visible to you :/
                    // FIXME : This is a bit of a hackish way to detect this. Try doing it another way...
                    continue;
                }
                stories.push(story_entry);
            }
            Ok((story_count, stories))
        }
    })
    .await
    .map_err_app(&persistent)?;

    let page_listing = PageListing::get_from_count(story_count, start_index, STORIES_PER_PAGE);

    let (excluded_stories, stories) = stories.into_iter().partition(StoryEntry::is_excluded);

    let s = MarkedForLaterTemplate {
        persistent: PersistentTemplate::from(&persistent),
        user,
        stories,
        excluded_stories,
        page_listing,
        sorting_mode: sorting_mode.to_string(),
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/@{user}/{story}/0/mark_for_later/")]
async fn handle_author_mark_for_later(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let (authors, story) = web::block({
        let mode = FilterMode::from_login_opt(&login);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    handle_shared_mark_for_later(data, persistent, authors, story).await
}

#[post("/collaboration/{story}/0/mark_for_later/")]
async fn handle_collaboration_mark_for_later(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let (authors, story) = web::block({
        let mode = FilterMode::from_login_opt(&login);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    handle_shared_mark_for_later(data, persistent, authors, story).await
}

#[post("/@{user}/{story}/0/remove_marked_for_later/")]
async fn handle_author_remove_marked_for_later(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let (authors, story) = web::block({
        let mode = FilterMode::from_login_opt(&login);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    handle_shared_remove_marked_for_later(data, persistent, authors, story).await
}

#[post("/collaboration/{story}/0/remove_marked_for_later/")]
async fn handle_collaboration_remove_marked_for_later(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let (authors, story) = web::block({
        let mode = FilterMode::from_login_opt(&login);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await
    .map_err_app(&persistent)?;
    handle_shared_remove_marked_for_later(data, persistent, authors, story).await
}

async fn handle_shared_mark_for_later(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;
    login_user
        .require_approval_rights_for(&authors, &story)
        .map_err_app(&persistent)?;

    // Do not error out if this fails.
    let _ignore_errors = web::block({
        let user_id = login_user.id;
        let story_id = story.id;
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::marked_for_later::modifications::mark_for_later(user_id, story_id, &conn)
    })
    .await;

    Ok(HttpResponse::SeeOther()
        .header(
            "Location",
            format!(
                "/marked_for_later/{}",
                PersistentTemplate::from(&persistent).as_leading_qmark()
            ),
        )
        .finish())
}

async fn handle_shared_remove_marked_for_later(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;
    login_user
        .require_approval_rights_for(&authors, &story)
        .map_err_app(&persistent)?;

    // Do not error out if this fails.
    let _ignore_errors = web::block({
        let user_id = login_user.id;
        let story_id = story.id;
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::marked_for_later::modifications::unmark_for_later(user_id, story_id, &conn)
    })
    .await;

    Ok(HttpResponse::SeeOther()
        .header(
            "Location",
            format!(
                "/marked_for_later/{}",
                PersistentTemplate::from(&persistent).as_leading_qmark()
            ),
        )
        .finish())
}
