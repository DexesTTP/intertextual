use std::convert::TryFrom;

use actix_web::{get, web, HttpResponse};
use serde::Serialize;

use intertextual::actions;
use intertextual::constants::RECOMMENDATION_URL;
use intertextual::models::stories::RecommendationsState;
use intertextual::models::tags::TagAssociationType;

use crate::prelude::*;

#[derive(Serialize)]
struct DataContainer {
    pub user_info: UserInfoContainer,
    pub stories: Vec<StoryContainer>,
    pub recommendations: Vec<RecommendationContainer>,
    pub comments: Vec<CommentContainer>,
    pub user_approvals: Vec<SimpleStoryInfoContainer>,
    pub marked_for_later: Vec<SimpleStoryInfoContainer>,
    pub author_follows: Vec<FollowContainer>,
    pub story_follows: Vec<FollowContainer>,
    pub recommendations_follows: Vec<FollowContainer>,
    pub reports: Vec<ReportContainer>,
    pub moderation_messages: Vec<ModmailContainer>,
}

#[derive(Serialize)]
struct UserInfoContainer {
    pub username: String,
    pub display_name: String,
    pub account_created_at: String,
    pub email: Option<String>,
    pub description: SanitizedHtml,
    pub theme: Option<String>,
    pub settings: Option<serde_json::Value>,
}

#[derive(Serialize)]
struct StoryContainer {
    pub url: String,
    pub title: String,
    pub created_at: String,
    pub description: RichTagline,
    pub is_collaboration: bool,
    pub recommendations_enabled: String,
    pub comments_enabled: bool,
    pub tags: Vec<TagContainer>,
    pub chapters: Vec<ChapterContainer>,
}

#[derive(Serialize)]
struct TagContainer {
    pub tag_category: String,
    pub tag_name: String,
    pub internal_tag_name: String,
    pub association: String,
}

#[derive(Serialize)]
struct ChapterContainer {
    pub url: String,
    pub title: Option<String>,
    pub created_at: String,
    pub published_at: Option<String>,
    pub first_published_at: Option<String>,
    pub edited_at: Option<String>,
    pub foreword: SanitizedHtml,
    pub contents: SanitizedHtml,
    pub afterword: SanitizedHtml,
}

#[derive(Serialize)]
struct SimpleStoryInfoContainer {
    pub url: String,
    pub created_at: String,
}

#[derive(Serialize)]
struct RecommendationContainer {
    pub url: String,
    pub created_at: String,
    pub contents: RichBlock,
    pub is_featured: bool,
}

#[derive(Serialize)]
struct CommentContainer {
    pub url: String,
    pub created_at: String,
    pub updated_at: Option<String>,
    pub contents: RichBlock,
}

#[derive(Serialize)]
struct FollowContainer {
    pub followed: String,
    pub follow_date: String,
}

#[derive(Serialize)]
struct ReportContainer {
    pub reported_url: String,
    pub report_date: String,
    pub report: String,
}

#[derive(Serialize)]
struct ModmailContainer {
    pub created_at: String,
    pub message: String,
}

const DATE_FORMAT: &str = "%Y-%m-%d %H:%M:%S";

#[get("/gdpr_data_request/get_zip/")]
async fn api_get_data(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<PersistentQuery>,
) -> Result<actix_web::HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let json_data = web::block({
        let user_id = login_user.id;
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || -> Result<DataContainer, IntertextualError> {
            let login_user = actions::users::find_user_by_id(user_id, &conn)?;
            let login_user = login_user.ok_or(IntertextualError::InternalServerError)?;
            let mut result = DataContainer {
                user_info: UserInfoContainer {
                    username: login_user.username.clone(),
                    display_name: login_user.display_name.clone(),
                    account_created_at: login_user
                        .account_created_at
                        .format(DATE_FORMAT)
                        .to_string(),
                    email: login_user.email.clone(),
                    theme: login_user.theme.clone(),
                    description: match &login_user.details {
                        Some(html) => html.clone(),
                        None => SanitizedHtml::new(),
                    },
                    settings: login_user.settings.clone(),
                },
                stories: vec![],
                user_approvals: vec![],
                marked_for_later: vec![],
                recommendations: vec![],
                comments: vec![],
                author_follows: vec![],
                story_follows: vec![],
                recommendations_follows: vec![],
                reports: vec![],
                moderation_messages: vec![],
            };

            let mode = FilterMode::from_login(&login_user);
            for story in actions::stories::find_all_stories_by_author(login_user.id, &mode, &conn)?
            {
                let author_path = story.author_path(std::slice::from_ref(&login_user));
                let story_path = format!("/{}/{}/", author_path, story.url_fragment);
                let mut chapters = vec![];
                let mode = FilterMode::from_login(&login_user);
                for chapter in actions::stories::find_chapters_by_story_id(story.id, &mode, &conn)?
                {
                    chapters.push(ChapterContainer {
                        url: format!("{}/{}/", story_path, chapter.chapter_number),
                        title: chapter.title,
                        foreword: chapter.foreword,
                        contents: chapter.content,
                        afterword: chapter.afterword,
                        created_at: chapter
                            .chapter_internally_created_at
                            .format(DATE_FORMAT)
                            .to_string(),
                        first_published_at: chapter
                            .official_creation_date
                            .map(|d| d.format(DATE_FORMAT).to_string()),
                        published_at: chapter
                            .show_publicly_after_date
                            .map(|d| d.format(DATE_FORMAT).to_string()),
                        edited_at: chapter
                            .last_edition_date
                            .map(|d| d.format(DATE_FORMAT).to_string()),
                    })
                }
                let mut tags = vec![];
                for (category, canonical, tag, association) in
                    actions::tags::find_internal_tags_by_story_id(story.id, &conn)?
                {
                    tags.push(TagContainer {
                        tag_category: category.display_name.to_string(),
                        tag_name: canonical.display_name.to_string(),
                        internal_tag_name: tag.internal_name.to_string(),
                        association: match TagAssociationType::try_from(association.tag_type) {
                            Ok(TagAssociationType::Major) => "Major".to_string(),
                            Ok(TagAssociationType::Standard) => "Standard".to_string(),
                            Ok(TagAssociationType::Spoiler) => "Spoiler".to_string(),
                            Err(_) => "Invalid data stored".to_string(),
                        },
                    })
                }
                result.stories.push(StoryContainer {
                    url: story_path,
                    title: story.title,
                    created_at: story
                        .story_internally_created_at
                        .format(DATE_FORMAT)
                        .to_string(),
                    description: story.description,
                    is_collaboration: story.is_collaboration,
                    comments_enabled: story.comments_enabled,
                    recommendations_enabled: match RecommendationsState::try_from(
                        story.recommendations_enabled,
                    ) {
                        Ok(RecommendationsState::Enabled) => "Enabled".to_string(),
                        Ok(RecommendationsState::Disabled) => "Disabled".to_string(),
                        Ok(RecommendationsState::UserOnly) => "User only".to_string(),
                        Err(_) => "Invalid value stored".to_string(),
                    },
                    tags,
                    chapters,
                })
            }

            for (story, date) in
                actions::user_approvals::gdpr::all_user_approvals_from_user(user_id, &conn)?
            {
                let authors = actions::stories::find_authors_by_story(story.id, &mode, &conn)?;
                result.user_approvals.push(SimpleStoryInfoContainer {
                    url: format!("/{}/{}/", story.author_path(&authors), story.url_fragment),
                    created_at: date.format(DATE_FORMAT).to_string(),
                })
            }

            for (story, date) in
                actions::marked_for_later::gdpr::all_user_story_for_later_from_user(user_id, &conn)?
            {
                let authors = actions::stories::find_authors_by_story(story.id, &mode, &conn)?;
                result.marked_for_later.push(SimpleStoryInfoContainer {
                    url: format!("/{}/{}/", story.author_path(&authors), story.url_fragment),
                    created_at: date.format(DATE_FORMAT).to_string(),
                })
            }

            for (story, recommendation) in
                actions::recommendations::find_recommendations_by_user(user_id, &conn)?
            {
                let authors = actions::stories::find_authors_by_story(story.id, &mode, &conn)?;
                result.recommendations.push(RecommendationContainer {
                    url: format!(
                        "/{}/@{}/{}/{}/",
                        RECOMMENDATION_URL,
                        login_user.username,
                        story.author_path(&authors),
                        story.url_fragment
                    ),
                    created_at: recommendation
                        .recommendation_date
                        .format(DATE_FORMAT)
                        .to_string(),
                    contents: recommendation.description,
                    is_featured: recommendation.featured_by_user,
                })
            }

            for (story, chapter, comment) in
                actions::comments::gdpr::all_comments_from_user(user_id, &conn)?
            {
                let authors = actions::stories::find_authors_by_story(story.id, &mode, &conn)?;
                result.comments.push(CommentContainer {
                    url: format!(
                        "/{}/{}/{}/comments/{}",
                        story.author_path(&authors),
                        story.url_fragment,
                        chapter.chapter_number,
                        comment.id
                    ),
                    created_at: comment.created.format(DATE_FORMAT).to_string(),
                    updated_at: comment.updated.map(|u| u.format(DATE_FORMAT).to_string()),
                    contents: comment.message,
                });
            }

            for (author, date) in
                actions::follows::private::get_user_author_follows(user_id, &conn)?
            {
                result.author_follows.push(FollowContainer {
                    followed: format!("/@{}/", author.username),
                    follow_date: date.format(DATE_FORMAT).to_string(),
                });
            }

            for (story, date) in actions::follows::private::get_user_story_follows(user_id, &conn)?
            {
                let authors = actions::stories::find_authors_by_story(story.id, &mode, &conn)?;
                result.story_follows.push(FollowContainer {
                    followed: format!("/{}/{}/", story.author_path(&authors), story.url_fragment),
                    follow_date: date.format(DATE_FORMAT).to_string(),
                });
            }

            for (author, date) in
                actions::follows::private::get_user_recommender_follows(user_id, &conn)?
            {
                result.recommendations_follows.push(FollowContainer {
                    followed: format!("/@{}/", author.username),
                    follow_date: date.format(DATE_FORMAT).to_string(),
                });
            }

            for (story, chapter, report) in
                actions::reports::gdpr::all_chapter_reports_for_user(user_id, &conn)?
            {
                let authors = actions::stories::find_authors_by_story(story.id, &mode, &conn)?;
                result.reports.push(ReportContainer {
                    reported_url: format!(
                        "/{}/{}/{}/",
                        story.author_path(&authors),
                        story.url_fragment,
                        chapter.chapter_number,
                    ),
                    report_date: report.report_date.format(DATE_FORMAT).to_string(),
                    report: report.reason,
                });
            }

            for (user, story, report) in
                actions::reports::gdpr::all_recommendation_reports_for_user(user_id, &conn)?
            {
                let authors = actions::stories::find_authors_by_story(story.id, &mode, &conn)?;
                result.reports.push(ReportContainer {
                    reported_url: format!(
                        "/{}/@{}/{}/{}/",
                        RECOMMENDATION_URL,
                        user.username,
                        story.author_path(&authors),
                        story.url_fragment
                    ),
                    report_date: report.report_date.format(DATE_FORMAT).to_string(),
                    report: report.reason,
                });
            }

            for (story, chapter, comment, report) in
                actions::reports::gdpr::all_comment_reports_for_user(user_id, &conn)?
            {
                let authors = actions::stories::find_authors_by_story(story.id, &mode, &conn)?;
                result.reports.push(ReportContainer {
                    reported_url: format!(
                        "/{}/{}/{}/comments/{}",
                        story.author_path(&authors),
                        story.url_fragment,
                        chapter.chapter_number,
                        comment.id,
                    ),
                    report_date: report.report_date.format(DATE_FORMAT).to_string(),
                    report: report.reason,
                });
            }

            for modmail in actions::modmail::gdpr::all_modmail_for_user(user_id, &conn)? {
                result.moderation_messages.push(ModmailContainer {
                    created_at: modmail.message_date.format(DATE_FORMAT).to_string(),
                    message: modmail.message.inner,
                });
            }

            Ok(result)
        }
    })
    .await
    .map_err_app(&persistent)?;

    Ok(HttpResponse::Ok()
        .set_header(
            "Content-Disposition",
            format!("attachment; filename=\"data-{}.json\"", login_user.username),
        )
        .json(json_data))
}
