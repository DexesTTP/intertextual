use actix_web::{get, post, web, HttpRequest, HttpResponse};
use argonautica::Verifier;
use askama::Template;
use log::{info, warn};
use serde::{Deserialize, Serialize};

use intertextual::actions;
use intertextual::models::error::IntertextualError;
use intertextual::models::shared::AppState;

use crate::app::identity::Identity;
use crate::app::identity::IdentityData;
use crate::app::identity::IdentityRememberMode;
use crate::app::persistent::*;
use crate::error::*;

#[derive(Template)]
#[template(path = "user/login.html")]
struct LoginTemplate {
    persistent: PersistentTemplate,
    default_device_name: String,
}

#[derive(Serialize, Deserialize)]
pub struct LoginParams {
    username: String,
    password: String,
    remember_me: Option<String>,
    device_name: Option<String>,
}

#[get("/login/")]
async fn login_page(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    match persistent.login() {
        Some(user) => Ok(HttpResponse::SeeOther()
            .header(
                "Location",
                format!(
                    "/@{}/{}",
                    user.username,
                    PersistentTemplate::from(&persistent).as_leading_qmark()
                ),
            )
            .finish()),
        None => {
            let mut default_device_name = request
                .headers()
                .get(actix_web::http::header::USER_AGENT)
                .and_then(|v| v.to_str().map(String::from).ok())
                .unwrap_or_else(String::new);
            if !default_device_name.is_empty() {
                let parser = woothee::parser::Parser::new();
                let result = parser.parse(&default_device_name);
                if let Some(result) = result {
                    default_device_name = format!("{} on {}", result.name, result.os);
                }
            }
            if default_device_name.is_empty() || !default_device_name.is_ascii() {
                default_device_name = String::from("Unknown device");
            }
            let s = LoginTemplate {
                persistent: PersistentTemplate::from(&persistent),
                default_device_name,
            }
            .render()
            .map_err_app(&persistent)?;
            Ok(HttpResponse::Ok().content_type("text/html").body(s))
        }
    }
}

#[post("/login/")]
async fn handle_login(
    request: HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    params: web::Form<LoginParams>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent =
        PersistentData::from_query(&data, id.clone(), &url_params.into_inner()).await?;

    let params = params.into_inner();
    let username = params.username;
    let user = web::block({
        let username = username.clone();
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::users::find_user_by_username_for_login(&username, &conn)
    })
    .await
    .map_err_app(&persistent)?
    .ok_or(IntertextualError::UserNotFound { username })
    .map_err_app(&persistent)?;
    let password = params.password.clone();
    if password.is_empty() {
        return Err(IntertextualError::InvalidPassword.into_app(&persistent));
    }
    let is_valid = Verifier::default()
        .with_hash(user.pw_hash)
        .with_password(password)
        .with_secret_key(data.password_secret_key.as_str())
        .verify()
        .unwrap();
    if !is_valid {
        if let Some(addr) = request.connection_info().realip_remote_addr() {
            info!("Failed login attempt from {}", addr);
        } else {
            warn!("Failed login attempt, remote address could not be determined");
        }
        return Err(IntertextualError::InvalidPassword.into_app(&persistent));
    }

    let (remember_mode, token_name, token_duration) = match params.remember_me.as_deref() {
        Some("on") => {
            let duration = chrono::Duration::days(20 * 365); // Stay logged-in for 20 years
            let mut device_name = params.device_name.unwrap_or_else(String::new);
            if device_name.is_empty() || !device_name.is_ascii() {
                device_name = String::from("Unnamed device");
            }
            (
                IdentityRememberMode::MaxAge {
                    duration: duration.to_std().unwrap(),
                },
                device_name,
                duration,
            )
        }
        _ => (
            IdentityRememberMode::Session,
            String::from("Session"),
            chrono::Duration::days(1), // Token is valid for 1 day
        ),
    };

    let user_id = user.id;
    let token_id = web::block({
        let valid_until = chrono::Utc::now().naive_utc() + token_duration;
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            actions::users::modifications::add_auth_token_for_user(
                user_id,
                token_name,
                valid_until,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    id.remember(IdentityData { user_id, token_id }, remember_mode);

    if user.password_reset_required {
        Ok(HttpResponse::SeeOther()
            // Note : we purposefully don't include the persistent URL, since the theme should now come with your user settings.
            .header("Location", format!("/password_reset/@{}/", user.username))
            .finish())
    } else {
        Ok(HttpResponse::SeeOther()
            // Note : we purposefully don't include the persistent URL, since the theme should now come with your user settings.
            .header("Location", format!("/@{}/", user.username))
            .finish())
    }
}

#[post("/logout/")]
async fn logout(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent =
        PersistentData::from_query(&data, id.clone(), &url_params.into_inner()).await?;
    if let Some(identity) = id.identity() {
        web::block({
            let conn = data.pool.get().map_err_app(&persistent)?;
            move || {
                actions::users::modifications::remove_auth_token_from_id(
                    identity.user_id,
                    identity.token_id,
                    &conn,
                )
            }
        })
        .await
        .map_err_app(&persistent)?;
    }

    id.forget();
    Ok(HttpResponse::SeeOther()
        .header(
            "Location",
            format!(
                "/login/{}",
                PersistentTemplate::from(&persistent)
                    .into_logged_out()
                    .as_leading_qmark()
            ),
        )
        .finish())
}
