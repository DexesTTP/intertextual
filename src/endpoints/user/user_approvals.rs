use actix_web::{get, web, HttpResponse};
use askama::Template;
use serde::Deserialize;

use intertextual::actions;
use intertextual::models;
use intertextual::models::filter::FilterMode;
use intertextual::models::stories::StoryEntry;
use intertextual::models::stories::StoryQueryExtraInfos;
use intertextual::utils::page_list::PageListing;

use crate::prelude::*;

const STORIES_PER_PAGE: i64 = 20;

#[derive(Template)]
#[template(path = "user/user_approval_list.html")]
struct UserApprovalsTemplate {
    persistent: PersistentTemplate,
    user: models::users::User,
    stories: Vec<StoryEntry>,
    excluded_stories: Vec<StoryEntry>,
    page_listing: PageListing,
}

#[derive(Deserialize)]
struct UserApprovalsParams {
    t: Option<String>,
    start: Option<String>,
}

#[get("/")]
async fn main_page(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<UserApprovalsParams>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let username = {
        let login_user = persistent.login();
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequired)
            .map_err_app(&persistent)?;
        login_user.username.clone()
    };
    let start_index = url_params
        .start
        .and_then(|s| s.parse::<i64>().ok())
        .unwrap_or(0);
    shared_page(data, persistent, username, start_index).await
}

#[get("/@{user}/")]
async fn specific_page(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<UserApprovalsParams>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let username = path.into_inner();
    let start_index = url_params
        .start
        .and_then(|s| s.parse::<i64>().ok())
        .unwrap_or(0);
    shared_page(data, persistent, username, start_index).await
}

async fn shared_page(
    data: web::Data<AppState>,
    persistent: PersistentData,
    username: String,
    start_index: i64,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let filter_mode = FilterMode::from_login(login_user);

    let user = {
        if login_user.username == username {
            web::block({
                let id = login_user.id;
                let conn = data.pool.get().map_err_app(&persistent)?;
                move || actions::users::find_user_by_id(id, &conn)
            })
            .await
            .map_err_app(&persistent)?
            .ok_or(IntertextualError::UserNotFound { username })
            .map_err_app(&persistent)?
        } else {
            web::block({
                let username = username.clone();
                let filter_mode = filter_mode.clone();
                let conn = data.pool.get().map_err_app(&persistent)?;
                move || actions::users::find_user_by_username(&username, &filter_mode, &conn)
            })
            .await
            .map_err_app(&persistent)?
            .ok_or(IntertextualError::UserNotFound { username })
            .map_err_app(&persistent)?
        }
    };

    login_user
        .require_see_statistics_rights_for(std::slice::from_ref(&user))
        .map_err_app(&persistent)?;

    let (story_count, stories) = web::block({
        let user_id = user.id;
        let user_info = StoryQueryExtraInfos::from_user(&data.site, login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || -> Result<(i64, Vec<StoryEntry>), IntertextualError> {
            let story_count =
                actions::user_approvals::total_user_approval_count_given_by_user(user_id, &conn)?;
            let raw_stories = actions::user_approvals::user_approvals_by_user(
                user_id,
                start_index,
                STORIES_PER_PAGE,
                &conn,
            )?;
            let mut stories =
                Vec::with_capacity(std::cmp::min(raw_stories.len(), STORIES_PER_PAGE as usize));
            for (story, _time) in raw_stories {
                let story_entry = StoryEntry::from_database(story, &user_info, &conn)?;
                if story_entry.chapter_count == 0 {
                    // This story isn't actually visible to you :/
                    // FIXME : This is a bit of a hackish way to detect this. Try doing it another way...
                    continue;
                }
                stories.push(story_entry);
            }
            Ok((story_count, stories))
        }
    })
    .await
    .map_err_app(&persistent)?;

    let page_listing = PageListing::get_from_count(story_count, start_index, STORIES_PER_PAGE);

    let (excluded_stories, stories) = stories.into_iter().partition(StoryEntry::is_excluded);

    let s = UserApprovalsTemplate {
        persistent: PersistentTemplate::from(&persistent),
        user,
        stories,
        excluded_stories,
        page_listing,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
