use actix_web::{get, web, HttpResponse};
use askama::Template;
use serde::Deserialize;

use intertextual::actions;
use intertextual::models::error::IntertextualError;
use intertextual::models::filter::FilterMode;
use intertextual::models::shared::AppState;
use intertextual::utils::page_list::PageListing;

use crate::app::identity::Identity;
use crate::app::persistent::*;
use crate::error::*;

const MAX_USERS_PER_PAGE: i64 = 50;

#[derive(Template)]
#[template(path = "user/author_list.html")]
struct UserListTemplate {
    persistent: PersistentTemplate,
    page_listing: PageListing,
    authors: Vec<AuthorEntry>,
}

struct AuthorEntry {
    pub username: String,
    pub display_name: String,
    pub story_count: i64,
}

#[derive(Deserialize)]
struct UserListParams {
    t: Option<String>,
    start: Option<String>,
}

#[get("/authors/")]
async fn authors_list_page(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<UserListParams>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let login_user = persistent.login();

    let start_index = url_params
        .start
        .and_then(|s| s.parse::<i64>().ok())
        .unwrap_or(0);

    let (authors, author_count) = web::block({
        let start_index = start_index;
        let mode = FilterMode::from_login_opt(&login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || -> Result<(Vec<AuthorEntry>, i64), IntertextualError> {
            let author_count = actions::users::find_total_author_count(&mode, &conn)?;
            let author_list = actions::users::find_authors_in_range(
                start_index,
                MAX_USERS_PER_PAGE,
                &mode,
                &conn,
            )?;
            let mut authors: Vec<AuthorEntry> = Vec::with_capacity(std::cmp::min(
                author_list.len(),
                MAX_USERS_PER_PAGE as usize,
            ));
            for author in author_list {
                let story_count =
                    actions::stories::find_story_count_by_author(author.id, &mode, &conn)?;
                authors.push(AuthorEntry {
                    username: author.username,
                    display_name: author.display_name,
                    story_count,
                });
            }
            Ok((authors, author_count))
        }
    })
    .await
    .map_err_app(&persistent)?;

    let page_listing = PageListing::get_from_count(author_count, start_index, MAX_USERS_PER_PAGE);

    let s = UserListTemplate {
        persistent: PersistentTemplate::from(&persistent),
        page_listing,
        authors,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
