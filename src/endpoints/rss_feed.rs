use std::sync::Arc;

use actix_web::{get, web, HttpResponse};
use askama::Template;

use intertextual::models::shared::SharedSiteData;
use intertextual::models::stories::StoryQueryExtraInfos;
use intertextual::models::stories::{Chapter, Story, StoryEntry};
use intertextual::{actions, data};

use crate::prelude::*;
use intertextual::models::users::User;

#[derive(Template)]
#[template(path = "latest.xml")]
struct IndexTemplate {
    site: Arc<SharedSiteData>,
    last_update: chrono::NaiveDateTime,
    stories: Vec<StoryEntry>,
}

#[derive(Template)]
#[template(path = "story/latest.xml")]
struct StoryFeedTemplate {
    site: Arc<SharedSiteData>,
    last_update: chrono::NaiveDateTime,
    authors: Vec<User>,
    story: Story,
    chapters: Vec<Chapter>,
}

const STORIES_IN_RSS_FEED: usize = 10;

#[get("/latest.rss/")]
async fn rss_feed(data: web::Data<AppState>) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_no_id(&data);
    let stories = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        let user_info = StoryQueryExtraInfos::from_maybe_user(&data.site, &None);
        move || -> Result<Vec<StoryEntry>, IntertextualError> {
            let story_to_new_chapters_association = actions::latest_stories::find_latest_stories(
                actions::latest_stories::LatestStoriesOptions {
                    entries_count: STORIES_IN_RSS_FEED,
                    aggregate_same_author_after_count: None,
                },
                &user_info.filter_mode,
                &conn,
            )?;
            let mut search_results = Vec::<StoryEntry>::with_capacity(STORIES_IN_RSS_FEED as usize);
            for entry in story_to_new_chapters_association {
                match entry {
                    actions::latest_stories::LatestStoryResult::SingleUpdate(entry) => {
                        search_results.push(StoryEntry::from_database(
                            entry.story,
                            &user_info,
                            &conn,
                        )?);
                    }
                    actions::latest_stories::LatestStoryResult::AggregatedUpdate(aggregated) => {
                        for entry in aggregated.entries {
                            search_results.push(StoryEntry::from_database(
                                entry.story,
                                &user_info,
                                &conn,
                            )?);
                        }
                    }
                }
            }
            Ok(search_results)
        }
    })
    .await
    .map_err_app(&persistent)?;
    let last_update = stories
        .iter()
        .map(|c| c.last_update)
        .max()
        .unwrap_or_else(|| chrono::Utc::now().naive_utc());
    let s = IndexTemplate {
        site: data.site.clone(),
        last_update,
        stories,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok()
        .content_type("application/rss+xml")
        .body(s))
}

#[get("/feed/@{user}/{story}/")]
async fn author_story_feed(
    state: web::Data<AppState>,
    path: web::Path<(String, String)>,
) -> Result<HttpResponse, AppError> {
    let (username, url_fragment) = path.into_inner();
    let persistent = PersistentData::from_no_id(&state);

    let (authors, story) = web::block({
        let filter_mode = StoryQueryExtraInfos::from_maybe_user(&state.site, &None).filter_mode;
        let conn = state.pool.get().map_err_app(&persistent)?;
        move || data::stories::find_author_story_by_url(username, url_fragment, &filter_mode, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    let last_update = story.cached_last_update_date;
    let story_id = story.id;

    let chapters = web::block({
        let filter_mode = StoryQueryExtraInfos::from_maybe_user(&state.site, &None).filter_mode;
        let conn = state.pool.get().map_err_app(&persistent)?;
        move || actions::stories::find_chapters_by_story_id(story_id, &filter_mode, &conn)
    })
    .await
    .map_err_app(&persistent)?;

    let s = StoryFeedTemplate {
        site: state.site.clone(),
        last_update,
        authors,
        story,
        chapters,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok()
        .content_type("application/rss+xml")
        .body(s))
}
