use actix_web::{get, web, HttpResponse};
use askama::Template;

use intertextual::actions;
use intertextual::models;
use intertextual::models::filter::FilterMode;
use intertextual::models::stories::StoryQueryExtraInfos;

use crate::prelude::*;

#[derive(Template)]
#[template(path = "recommendation/index.html")]
struct UserSingleRecommendationTemplate {
    persistent: PersistentTemplate,
    user: models::users::User,
    recommendation: RecommendationEntry,
    is_self: bool,
    is_administrator: bool,
}

pub struct RecommendationEntry {
    story: models::stories::StoryEntry,
    formatted_recommendation: SanitizedHtml,
    can_report: bool,
    is_already_reported: bool,
}

#[get("/@{user}/@{author}/{story}/")]
async fn single_page_author(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, String)>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let filter_mode = FilterMode::from_login_opt(&login_user);

    let (recommender_username, author_username, story_url_fragment) = path.into_inner();
    let (recommender, author, story, recommendation) = web::block({
        let filter_mode = filter_mode.clone();
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            intertextual::data::recommendations::find_author_recommendation_by_url(
                recommender_username,
                author_username,
                story_url_fragment,
                &filter_mode,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    let can_report = match login_user {
        Some(login_user) => login_user
            .require_report_recommendation_rights_for(
                &recommender,
                std::slice::from_ref(&author),
                &story,
            )
            .is_ok(),
        _ => false,
    };
    let is_already_reported = match login_user {
        Some(login_user) => {
            let login_user_id = login_user.id;
            let recommender_id = recommender.id;
            let story_id = story.id;
            web::block({
                let conn = data.pool.get().map_err_app(&persistent)?;
                move || {
                    actions::reports::user_has_reported_recommendation(
                        login_user_id,
                        recommender_id,
                        story_id,
                        &conn,
                    )
                }
            })
            .await
            .map_err_app(&persistent)?
        }
        _ => false,
    };

    let story_entry = web::block({
        let user_info = StoryQueryExtraInfos::from_maybe_user(&data.site, &login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || models::stories::StoryEntry::from_database(story, &user_info, &conn)
    })
    .await
    .map_err_app(&persistent)?;
    let recommendation = RecommendationEntry {
        story: story_entry,
        formatted_recommendation: richblock_to_html(
            &recommendation.description,
            &data.pool,
            &persistent,
        )
        .await?,
        can_report,
        is_already_reported,
    };

    let is_self = login_user.iter().any(|u| u.id == recommender.id);
    let is_administrator = login_user.iter().any(|u| u.administrator);
    let s = UserSingleRecommendationTemplate {
        persistent: PersistentTemplate::from(&persistent),
        user: recommender,
        recommendation,
        is_self,
        is_administrator,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/@{user}/collaboration/{story}/")]
async fn single_page_collaboration(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let filter_mode = FilterMode::from_login_opt(&login_user);

    let (recommender_username, story_url_fragment) = path.into_inner();
    let (recommender, authors, story, recommendation) = web::block({
        let filter_mode = filter_mode.clone();
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            intertextual::data::recommendations::find_collaboration_recommendation_by_url(
                recommender_username,
                story_url_fragment,
                &filter_mode,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    let can_report = match login_user {
        Some(login_user) => login_user
            .require_report_recommendation_rights_for(&recommender, &authors, &story)
            .is_ok(),
        _ => false,
    };
    let is_already_reported = match login_user {
        Some(login_user) => {
            let login_user_id = login_user.id;
            let recommender_id = recommender.id;
            let story_id = story.id;
            web::block({
                let conn = data.pool.get().map_err_app(&persistent)?;
                move || {
                    actions::reports::user_has_reported_recommendation(
                        login_user_id,
                        recommender_id,
                        story_id,
                        &conn,
                    )
                }
            })
            .await
            .map_err_app(&persistent)?
        }
        _ => false,
    };

    let story_entry = web::block({
        let user_info = StoryQueryExtraInfos::from_maybe_user(&data.site, &login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || models::stories::StoryEntry::from_database(story, &user_info, &conn)
    })
    .await
    .map_err_app(&persistent)?;
    let recommendation = RecommendationEntry {
        story: story_entry,
        formatted_recommendation: richblock_to_html(
            &recommendation.description,
            &data.pool,
            &persistent,
        )
        .await?,
        can_report,
        is_already_reported,
    };

    let is_self = login_user.iter().any(|u| u.id == recommender.id);
    let is_administrator = login_user.iter().any(|u| u.administrator);
    let s = UserSingleRecommendationTemplate {
        persistent: PersistentTemplate::from(&persistent),
        user: recommender,
        recommendation,
        is_self,
        is_administrator,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
