use actix_web::{get, web, HttpResponse};
use askama::Template;

use intertextual::actions;
use intertextual::data;
use intertextual::models;
use intertextual::models::filter::FilterMode;
use models::stories::RecommendationsState;

use crate::prelude::*;

#[derive(Template)]
#[template(path = "recommendation/story_list.html")]
struct StoryRecommendationsTemplate {
    persistent: PersistentTemplate,
    authors: Vec<models::users::ShortUserEntry>,
    author_path: String,
    story: models::stories::Story,
    recommendations: Vec<RecommendationEntry>,
    is_administrator: bool,
}

pub struct RecommendationEntry {
    username: String,
    display_name: String,
    formatted_recommendation: SanitizedHtml,
    can_report: bool,
    is_self: bool,
    is_already_reported: bool,
}

#[get("/story/@{author}/{story}/")]
async fn main_author_page(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &params.t).await?;
    let login = persistent.login();

    let (username, url_fragment) = path.into_inner();
    let (authors, story) = web::block({
        let mode = FilterMode::from_login_opt(&login);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || data::stories::find_author_story_by_url(username, url_fragment, &mode, &conn)
    })
    .await
    .map_err_app(&persistent)?;
    main_shared_page(data, persistent, authors, story).await
}

#[get("/story/collaboration/{story}/")]
async fn main_collaboration_page(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &params.t).await?;
    let login = persistent.login();

    let url_fragment = path.into_inner();
    let (authors, story) = web::block({
        let mode = FilterMode::from_login_opt(&login);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || data::stories::find_collaboration_story_by_url(url_fragment, &mode, &conn)
    })
    .await
    .map_err_app(&persistent)?;
    main_shared_page(data, persistent, authors, story).await
}

async fn main_shared_page(
    data: web::Data<AppState>,
    persistent: PersistentData,
    authors: Vec<models::users::User>,
    story: models::stories::Story,
) -> Result<HttpResponse, AppError> {
    let login_user = persistent.login();

    if !RecommendationsState::Enabled.matches(&story.recommendations_enabled) {
        let login_user = login_user
            .ok_or(IntertextualError::LoginRequiredObfuscated)
            .map_err_app(&persistent)?;
        login_user
            .require_recommendation_see_from_story_rights_for(&authors, &story)
            .map_err_app(&persistent)?;
    }

    let author_path = story.author_path(&authors);
    let short_authors = authors
        .iter()
        .map(models::users::ShortUserEntry::from)
        .collect();
    let recommendations = web::block({
        let login_user_id = login_user.map(|u| u.id);
        let story = story.clone();
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || -> Result<Vec<RecommendationEntry>, IntertextualError> {
            let login_user = match login_user_id {
                Some(login_user_id) => actions::users::find_user_by_id(login_user_id, &conn)?,
                None => None,
            };

            let raw_recommendations =
                actions::recommendations::find_recommendations_by_story(story.id, &conn)?;
            let mut recommendations: Vec<RecommendationEntry> = vec![];
            for (user, recommendation) in raw_recommendations {
                let can_report = match &login_user {
                    Some(login_user) => login_user
                        .require_report_recommendation_rights_for(&user, &authors, &story)
                        .is_ok(),
                    _ => false,
                };
                let is_self = matches!(login_user_id, Some(id) if id == user.id);
                let is_already_reported = match &login_user {
                    Some(login_user) => actions::reports::user_has_reported_recommendation(
                        login_user.id,
                        user.id,
                        story.id,
                        &conn,
                    )?,
                    _ => false,
                };
                recommendations.push(RecommendationEntry {
                    username: user.username,
                    display_name: user.display_name,
                    formatted_recommendation: recommendation.description.html(&conn),
                    can_report,
                    is_self,
                    is_already_reported,
                })
            }
            Ok(recommendations)
        }
    })
    .await
    .map_err_app(&persistent)?;

    let is_administrator = login_user.iter().any(|u| u.administrator);
    let s = StoryRecommendationsTemplate {
        persistent: PersistentTemplate::from(&persistent),
        authors: short_authors,
        author_path,
        story,
        recommendations,
        is_administrator,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
