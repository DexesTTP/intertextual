use actix_web::{post, web, HttpResponse};

use intertextual::actions;
use intertextual::constants::RECOMMENDATION_URL;
use intertextual::models;
use intertextual::models::error::IntertextualError;
use intertextual::models::shared::AppState;

use crate::app::identity::Identity;
use crate::app::persistent::*;
use crate::error::*;

#[post("/follow/@{user}/")]
async fn handle_follow_author_recommendations(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let author_username = path.into_inner();
    let author = web::block({
        let author_username = author_username.clone();
        let mode = models::filter::FilterMode::from_login(login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::users::find_user_by_username(&author_username, &mode, &conn)
    })
    .await
    .map_err_app(&persistent)?
    .ok_or(IntertextualError::UserNotFound {
        username: author_username,
    })
    .map_err_app(&persistent)?;

    let _ignore_errors = web::block({
        let user_id = login_user.id;
        let author_id = author.id;
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            actions::follows::modifications::follow_author_recommendations(
                user_id, author_id, &conn,
            )
        }
    })
    .await;

    let return_url = format!(
        "/{}/@{}/{}#follow",
        RECOMMENDATION_URL,
        author.username,
        PersistentTemplate::from(&persistent).as_leading_qmark()
    );

    Ok(HttpResponse::SeeOther()
        .header("Location", return_url)
        .finish())
}

#[post("/unfollow/@{user}/")]
async fn handle_unfollow_author_recommendations(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let author_username = path.into_inner();
    let author = web::block({
        let username = author_username.clone();
        let mode = models::filter::FilterMode::from_login(login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || actions::users::find_user_by_username(&username, &mode, &conn)
    })
    .await
    .map_err_app(&persistent)?
    .ok_or(IntertextualError::UserNotFound {
        username: author_username,
    })
    .map_err_app(&persistent)?;

    let _ignore_errors = web::block({
        let user_id = login_user.id;
        let author_id = author.id;
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            actions::follows::modifications::unfollow_author_recommendations(
                user_id, author_id, &conn,
            )
        }
    })
    .await;

    let return_url = format!(
        "/{}/@{}/{}#follow",
        RECOMMENDATION_URL,
        author.username,
        PersistentTemplate::from(&persistent).as_leading_qmark()
    );

    Ok(HttpResponse::SeeOther()
        .header("Location", return_url)
        .finish())
}
