use actix_web::{get, post, web, HttpResponse};
use askama::Template;
use serde::{Deserialize, Serialize};

use intertextual::actions;
use intertextual::constants::RECOMMENDATION_URL;
use intertextual::models;

use crate::prelude::*;

#[derive(Template)]
#[template(path = "recommendation/edit.html")]
struct EditUserRecommendationTemplate {
    persistent: PersistentTemplate,
    user: models::users::User,
    authors: Vec<models::users::ShortUserEntry>,
    author_path: String,
    story: models::stories::Story,
    recommendation_description: RichBlock,
}

#[derive(Serialize, Deserialize)]
pub struct EditRecommendationParams {
    content: String,
    feature_recommendation: Option<String>,
}

#[get("/@{user}/@{author}/{story}/edit/")]
async fn main_author_page(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, String)>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let (recommender_username, author_username, url_fragment) = path.into_inner();
    let (recommender, author, story, recommendation) = web::block({
        let mode = FilterMode::from_login(login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            intertextual::data::recommendations::find_author_recommendation_by_url(
                recommender_username,
                author_username,
                url_fragment,
                &mode,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    login_user
        .require_recommendation_edit_rights_for(&recommender)
        .map_err_app(&persistent)?;

    let s = EditUserRecommendationTemplate {
        persistent: PersistentTemplate::from(&persistent),
        user: recommender,
        authors: vec![models::users::ShortUserEntry::from(&author)],
        author_path: format!("@{}", author.username),
        story,
        recommendation_description: recommendation.description,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/@{user}/collaboration/{story}/edit/")]
async fn main_collaboration_page(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let (recommender_username, url_fragment) = path.into_inner();
    let (recommender, authors, story, recommendation) = web::block({
        let mode = FilterMode::from_login(login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            intertextual::data::recommendations::find_collaboration_recommendation_by_url(
                recommender_username,
                url_fragment,
                &mode,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    login_user
        .require_recommendation_edit_rights_for(&recommender)
        .map_err_app(&persistent)?;

    let s = EditUserRecommendationTemplate {
        persistent: PersistentTemplate::from(&persistent),
        user: recommender,
        authors: authors
            .iter()
            .map(models::users::ShortUserEntry::from)
            .collect(),
        author_path: "collaboration".to_string(),
        story,
        recommendation_description: recommendation.description,
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[post("/@{user}/@{author}/{story}/edit/")]
async fn handle_author_edit(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, String)>,
    url_params: web::Query<PersistentQuery>,
    form: web::Form<EditRecommendationParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let (recommender_username, author_username, url_fragment) = path.into_inner();
    let (recommender, _author, _story, recommendation) = web::block({
        let mode = FilterMode::from_login(login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            intertextual::data::recommendations::find_author_recommendation_by_url(
                recommender_username,
                author_username,
                url_fragment,
                &mode,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    login_user
        .require_recommendation_edit_rights_for(&recommender)
        .map_err_app(&persistent)?;

    let form = form.into_inner();
    let featured_by_user = form.feature_recommendation.filter(|o| o == "on").is_some();
    let description = String::from(form.content.trim());

    let recommendation = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            actions::recommendations::modifications::update_recommendation_description(
                recommendation,
                description,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;
    web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            actions::recommendations::modifications::update_recommendation_featured_by_user(
                recommendation,
                featured_by_user,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    Ok(HttpResponse::SeeOther()
        .header(
            "Location",
            format!(
                "/{}/@{}/?confirm=recommendation_edit{}",
                RECOMMENDATION_URL,
                recommender.username,
                PersistentTemplate::from(&persistent).as_leading_amp()
            ),
        )
        .finish())
}

#[post("/@{user}/collaboration/{story}/edit/")]
async fn handle_collaboration_edit(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    url_params: web::Query<PersistentQuery>,
    form: web::Form<EditRecommendationParams>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let (recommender_username, url_fragment) = path.into_inner();
    let (recommender, _authors, _story, recommendation) = web::block({
        let mode = FilterMode::from_login(login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            intertextual::data::recommendations::find_collaboration_recommendation_by_url(
                recommender_username,
                url_fragment,
                &mode,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    login_user
        .require_recommendation_edit_rights_for(&recommender)
        .map_err_app(&persistent)?;

    let form = form.into_inner();
    let featured_by_user = form.feature_recommendation.filter(|o| o == "on").is_some();
    let description = String::from(form.content.trim());

    let recommendation = web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            actions::recommendations::modifications::update_recommendation_description(
                recommendation,
                description,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;
    web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            actions::recommendations::modifications::update_recommendation_featured_by_user(
                recommendation,
                featured_by_user,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    Ok(HttpResponse::SeeOther()
        .header(
            "Location",
            format!(
                "/{}/@{}/?confirm=recommendation_edit{}",
                RECOMMENDATION_URL,
                recommender.username,
                PersistentTemplate::from(&persistent).as_leading_amp()
            ),
        )
        .finish())
}

#[post("/@{recommender_username}/@{author}/{story}/delete/")]
async fn handle_author_delete(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String, String)>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let (recommender_username, author_username, url_fragment) = path.into_inner();
    let (recommender, _author, _story, recommendation) = web::block({
        let mode = FilterMode::from_login(login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            intertextual::data::recommendations::find_author_recommendation_by_url(
                recommender_username,
                author_username,
                url_fragment,
                &mode,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    login_user
        .require_recommendation_edit_rights_for(&recommender)
        .map_err_app(&persistent)?;

    web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            actions::recommendations::modifications::delete_recommendation(recommendation, &conn)
        }
    })
    .await
    .map_err_app(&persistent)?;

    Ok(HttpResponse::SeeOther()
        .header(
            "Location",
            format!(
                "/{}/@{}/?confirm=recommendation_delete{}",
                RECOMMENDATION_URL,
                recommender.username,
                PersistentTemplate::from(&persistent).as_leading_amp()
            ),
        )
        .finish())
}

#[post("/@{recommender_username}/collaboration/{story}/delete/")]
async fn handle_collaboration_delete(
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<(String, String)>,
    url_params: web::Query<PersistentQuery>,
) -> Result<HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let login_user = persistent.login();
    let login_user = login_user
        .ok_or(IntertextualError::LoginRequired)
        .map_err_app(&persistent)?;

    let (recommender_username, url_fragment) = path.into_inner();
    let (recommender, _authors, _story, recommendation) = web::block({
        let mode = FilterMode::from_login(login_user);
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            intertextual::data::recommendations::find_collaboration_recommendation_by_url(
                recommender_username,
                url_fragment,
                &mode,
                &conn,
            )
        }
    })
    .await
    .map_err_app(&persistent)?;

    login_user
        .require_recommendation_edit_rights_for(&recommender)
        .map_err_app(&persistent)?;

    web::block({
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || {
            actions::recommendations::modifications::delete_recommendation(recommendation, &conn)
        }
    })
    .await
    .map_err_app(&persistent)?;

    Ok(HttpResponse::SeeOther()
        .header(
            "Location",
            format!(
                "/{}/@{}/?confirm=recommendation_delete{}",
                RECOMMENDATION_URL,
                recommender.username,
                PersistentTemplate::from(&persistent).as_leading_amp()
            ),
        )
        .finish())
}
