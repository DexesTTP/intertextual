use actix_web::{get, web, HttpResponse};
use askama::Template;
use itertools::Itertools;
use serde::Deserialize;

use intertextual::actions;
use intertextual::models::filter::FilterMode;
use intertextual::models::stories::ChapterMetadata;
use intertextual::models::stories::StoryEntry;
use intertextual::models::stories::StoryQueryExtraInfos;
use intertextual::models::users::User;

use crate::prelude::*;

const STORY_ENTRIES_ON_INDEX_PAGE: usize = 20;
const MAX_ENTRIES_PER_AUTHOR_ON_INDEX_PAGE: usize = 2;
const MAX_UPDATES_BY_AGGREGATED_CARD: usize = 5;

#[derive(Template)]
#[template(path = "index.html")]
struct IndexTemplate {
    persistent: PersistentTemplate,
    stories: Vec<StoryCardData>,
    is_administrator: bool,
    view_mode: String,
    display_mode: String,
}

struct ExtraStoryInformation {
    entry_update: Option<chrono::NaiveDateTime>,
    is_new_story: bool,
    chapter_count: usize,
    word_count: i32,
}

enum StoryCardData {
    SingleEntry(StoryEntry, ExtraStoryInformation),
    CombinedEntry(Vec<User>, Vec<(StoryEntry, ExtraStoryInformation)>),
}

impl ExtraStoryInformation {
    pub fn from(story: &StoryEntry, chapters: &[ChapterMetadata]) -> Self {
        let new_words_count: i32 = chapters.iter().map(|c| c.word_count).sum();
        let entry_update = chapters
            .iter()
            .filter_map(|c| c.official_creation_date)
            .max();
        let first_publication = story.first_publication;
        let is_new_story = chapters.iter().any(|c| {
            c.official_creation_date
                .iter()
                .any(|c| c == &first_publication)
        });
        Self {
            entry_update,
            is_new_story,
            chapter_count: chapters.len(),
            word_count: new_words_count,
        }
    }

    pub fn entry_update_formatted(&self) -> String {
        match &self.entry_update {
            Some(entry_update) => entry_update.format("%Y-%m-%d").to_string(),
            None => String::from("Not published yet"),
        }
    }
}

#[derive(Deserialize)]
struct IndexParams {
    t: Option<String>,
    view_mode: Option<String>,
    display_mode: Option<String>,
}

#[derive(Clone, Copy)]
pub enum DisplayMode {
    /// Sort by featured stories first, then by publication date (newest first)
    Card,
    /// Sort by publication date (oldest first)
    List,
}

impl std::fmt::Display for DisplayMode {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                DisplayMode::Card => "card",
                DisplayMode::List => "list",
            }
        )
    }
}

impl DisplayMode {
    fn parse_url(url_param: Option<&str>) -> Option<DisplayMode> {
        match url_param {
            Some(s) if s == "card" => Some(DisplayMode::Card),
            Some(s) if s == "list" => Some(DisplayMode::List),
            _ => None,
        }
    }
}

#[get("/")]
async fn index(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<IndexParams>,
) -> Result<HttpResponse, AppError> {
    let url_params = url_params.into_inner();
    let persistent = PersistentData::from_raw_query_data(&data, id, &url_params.t).await?;
    let login = persistent.login();
    let filter_mode = match url_params.view_mode.as_deref() {
        Some(t) if t == "admin" => FilterMode::from_login_opt(&login),
        Some(t) if t == "author" => FilterMode::from_login_opt_without_bypass(&login),
        _ => FilterMode::Standard,
    };

    let mut display_mode = DisplayMode::parse_url(url_params.display_mode.as_deref());
    if let Some(login_user) = login {
        let new_settings = login_user.get_settings();
        if let Some(display_mode) = &display_mode {
            let mut new_settings = new_settings;
            new_settings.card_display_mode = Some(display_mode.to_string());
            let _ = web::block({
                let login_user_id = login_user.id;
                let serialized_settings = new_settings
                    .to_serializable()
                    .map_err(|_| IntertextualError::InternalServerError)
                    .map_err_app(&persistent)?;
                let conn = data.pool.get().map_err_app(&persistent)?;
                move || {
                    actions::users::modifications::update_user_settings(
                        login_user_id,
                        Some(serialized_settings),
                        &conn,
                    )
                }
            })
            .await
            .map_err_app(&persistent)?;
        } else {
            display_mode = DisplayMode::parse_url(new_settings.card_display_mode.as_deref());
        }
    }
    let display_mode = display_mode.unwrap_or(DisplayMode::Card);

    let stories = web::block({
        let mut user_info = StoryQueryExtraInfos::from_maybe_user(&data.site, &login);
        user_info.filter_mode = filter_mode.clone();
        let conn = data.pool.get().map_err_app(&persistent)?;
        move || -> Result<Vec<StoryCardData>, IntertextualError> {
            let story_to_new_chapters_association = actions::latest_stories::find_latest_stories(
                actions::latest_stories::LatestStoriesOptions {
                    entries_count: STORY_ENTRIES_ON_INDEX_PAGE,
                    aggregate_same_author_after_count: Some(MAX_ENTRIES_PER_AUTHOR_ON_INDEX_PAGE),
                },
                &filter_mode,
                &conn,
            )?;
            let mut search_results = Vec::with_capacity(STORY_ENTRIES_ON_INDEX_PAGE);
            for entry in story_to_new_chapters_association {
                match entry {
                    actions::latest_stories::LatestStoryResult::SingleUpdate(entry) => {
                        let story_entry =
                            StoryEntry::from_database(entry.story, &user_info, &conn)?;
                        if story_entry.is_excluded() {
                            // Never show stories with excluded tags on the site index
                            continue;
                        }
                        let mut chapters = entry.chapters;
                        chapters.sort_by_key(|c| c.chapter_number);
                        let chapters: Vec<ChapterMetadata> =
                            chapters.into_iter().dedup_by(|a, b| a.id == b.id).collect();
                        let extra_informations =
                            ExtraStoryInformation::from(&story_entry, &chapters);
                        search_results
                            .push(StoryCardData::SingleEntry(story_entry, extra_informations));
                    }
                    actions::latest_stories::LatestStoryResult::AggregatedUpdate(aggregated) => {
                        let story_list_size =
                            usize::min(aggregated.entries.len(), MAX_UPDATES_BY_AGGREGATED_CARD);
                        let mut story_list = Vec::with_capacity(story_list_size);
                        for (index, entry) in aggregated.entries.into_iter().enumerate() {
                            if index > story_list_size {
                                break;
                            }
                            let story_entry =
                                StoryEntry::from_database(entry.story, &user_info, &conn)?;
                            if story_entry.is_excluded() {
                                // Never show stories with excluded tags on the site index
                                continue;
                            }
                            let mut chapters = entry.chapters;
                            chapters.sort_by_key(|c| c.chapter_number);
                            let chapters: Vec<ChapterMetadata> =
                                chapters.into_iter().dedup_by(|a, b| a.id == b.id).collect();
                            let extra_informations =
                                ExtraStoryInformation::from(&story_entry, &chapters);
                            story_list.push((story_entry, extra_informations));
                        }
                        let mut authors = Vec::with_capacity(aggregated.author_ids.len());
                        for author_id in aggregated.author_ids {
                            if let Some(user) = actions::users::find_user_by_id(author_id, &conn)? {
                                authors.push(user);
                            }
                        }
                        search_results.push(StoryCardData::CombinedEntry(authors, story_list));
                    }
                }
            }
            Ok(search_results)
        }
    })
    .await
    .map_err_app(&persistent)?;

    let is_administrator = login.iter().any(|u| u.administrator);
    let s = IndexTemplate {
        persistent: PersistentTemplate::from(&persistent),
        stories,
        is_administrator,
        display_mode: display_mode.to_string(),
        view_mode: url_params.view_mode.unwrap_or_else(String::new),
    }
    .render()
    .map_err_app(&persistent)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
