use std::clone::Clone;
use std::path::PathBuf;
use std::sync::Arc;

use actix_web::{get, guard, middleware, web, App, HttpServer};
use diesel::{r2d2::ConnectionManager, r2d2::Pool, PgConnection};
use intertextual::models::shared::DefaultColor;
use log::info;

use intertextual::models::shared::AppState;
use intertextual::models::shared::DbPool;
use intertextual::models::shared::SharedSiteData;
use intertextual::utils::publication::PublicationDate;

use crate::app::identity::Identity;
use crate::app::identity::IntertextualIdentityCookiePolicy;
use crate::app::identity::IntertextualIdentityService;
use crate::app::persistent::*;
use crate::error::*;

mod app;
mod endpoints;
mod error;

mod prelude {
    pub use intertextual::prelude::*;

    pub use crate::app::identity::Identity;
    pub use crate::app::persistent::*;
    pub use crate::endpoints::filters;
    pub use crate::error::*;

    pub async fn richblock_to_html(
        rb: &RichBlock,
        pool: &DbPool,
        persistent: &PersistentData,
    ) -> Result<SanitizedHtml, AppError> {
        let rb = rb.clone();
        let pool = pool.clone();
        actix_web::web::block(move || -> Result<SanitizedHtml, diesel::r2d2::PoolError> {
            let conn = pool.get()?;
            Ok(rb.html(&conn))
        })
        .await
        .map_err_app(persistent)
    }
}

const PUBLISH_DAY: PublicationDate = PublicationDate {
    weekday: chrono::Weekday::Sat,
    hour: 16, /* h UTC */
};

const ALLOWED_STATIC_FILES: [&str; 4] = [
    "base.css",
    "icon-dark-mode.png",
    "icon-light-mode.png",
    "card-image.png",
];

pub fn pool_from_env() -> DbPool {
    let connspec = std::env::var("DATABASE_URL").expect("DATABASE_URL needs to be set");
    let manager = ConnectionManager::<PgConnection>::new(connspec);
    Pool::builder()
        .build(manager)
        .expect("Failed to create pool")
}

fn get_static_dir() -> PathBuf {
    PathBuf::from(std::env::var("STATIC_DIR").unwrap_or("static".to_string()))
}

fn get_theme_dir() -> PathBuf {
    PathBuf::from(std::env::var("THEME_DIR").unwrap_or("theme".to_string()))
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    dotenv::dotenv().ok();
    std::env::set_var("RUST_LOG", "actix_web=info");
    log4rs::init_file(
        std::env::var("LOG4RS_PATH").expect("LOG4RS_PATH needs to be set"),
        log4rs::file::Deserializers::default(),
    )
    .unwrap();

    info!("Retrieving listen address.");
    let listen = std::env::var("LISTEN").expect("LISTEN needs to be set");

    info!("Retrieving password secret.");
    let pw_sk = std::env::var("PASSWORD_SECRET_KEY").expect("PASSWORD_SECRET_KEY needs to be set");

    info!("Retrieving cookie secret.");
    let cookie_privkey = base64::decode(
        std::env::var("COOKIE_PRIVATE_KEY").expect("COOKIE_PRIVATE_KEY needs to be set"),
    )
    .expect("base64 decoding error on COOKIE_PRIVATE_KEY");

    info!("Retrieving cookie options");
    let insecure_cookies = matches!(
        std::env::var("USE_INSECURE_COOKIES"),
        Ok(v) if v.as_str() == "true"
    );
    let secure_cookies = !insecure_cookies;
    info!("Creating database connection.");
    let pool = pool_from_env();

    let account_deletion_request_duration_days = 2;

    actix_rt::spawn({
        info!("Initializing 1hr background thread (dedup wiper task, account deletion task, tag recompute task)");
        let pool = pool.clone();
        async move {
            let mut task = actix_rt::time::interval(std::time::Duration::from_secs(3600));
            loop {
                task.tick().await;
                intertextual::utils::hits::clear_hit_dedup_cache();
                if let Ok(conn) = pool.get() {
                    let _ =
                        intertextual::actions::users::automated_actions::delete_pending_accounts(
                            account_deletion_request_duration_days,
                            &conn,
                        );
                }
                if let Ok(conn) = pool.get() {
                    let _ =
                        intertextual::actions::users::automated_actions::clean_expired_auth_tokens(
                            &conn,
                        );
                }
                if let Ok(conn) = pool.get() {
                    let _ =
                        intertextual::actions::tags::automated_actions::recompute_tag_counts(&conn);
                }
            }
        }
    });

    actix_rt::spawn({
        info!("Initializing 30s worker task (notification, story cached values)");
        let pool = pool.clone();
        let task_worker_index = 1;
        async move {
            let mut task = actix_rt::time::interval(std::time::Duration::from_secs(30));
            loop {
                task.tick().await;
                if let Ok(conn) = pool.get() {
                    while let Ok(Some(task)) =
                        intertextual::actions::notifications::automated_actions::get_next_request(
                            task_worker_index,
                            &conn,
                        )
                    {
                        log::info!("Processing notification task {}", task.id);
                        let _ =
                            intertextual::actions::notifications::automated_actions::handle_request(
                                task.id,
                                task_worker_index,
                                &conn,
                            );
                        log::info!("End of notification task {}", task.id);
                    }
                    let _ =
                        intertextual::actions::stories::automated_actions::recompute_cached_story_values(
                            chrono::Utc::now().naive_utc(),
                            &conn,
                        );
                }
            }
        }
    });

    let default_tag_highlight_rules = match std::env::var("SITEWIDE_HIGHLIGHT_RULE_TEMPLATE") {
        Ok(template_name) if !template_name.is_empty() => {
            log::info!(
                "Retrieving the site-wide default highlight rules template '{}'",
                &template_name
            );
            let conn = pool
                .get()
                .expect("Could not get a database connection for the template request");
            intertextual::actions::tags::get_tag_highlight_rule_template_by_name(
                &template_name,
                &conn,
            )
            .expect("Could not find the requested template in the database")
        }
        _ => vec![],
    };

    let arc_data = Arc::new(SharedSiteData {
        base_url: std::env::var("BASE_URL")
            .expect("BASE_URL needs to be set")
            .trim_end_matches('/')
            .to_string(),
        secure_cookies,
        allowed_theme_files: std::env::var("ALLOWED_THEME_FILES").map_or_else(
            |_| Vec::new(),
            |t| {
                t.split(':')
                    .filter(|f| !f.is_empty())
                    .map(String::from)
                    .collect::<Vec<String>>()
            },
        ),
        name: std::env::var("SITE_NAME").expect("SITE_NAME needs to be set"),
        description: std::env::var("SITE_DESCRIPTION").unwrap_or_default(),
        tags: std::env::var("SITE_TAGS").map_or_else(
            |_| Vec::new(),
            |t| {
                t.split(',')
                    .filter(|f| !f.is_empty())
                    .map(String::from)
                    .collect::<Vec<String>>()
            },
        ),
        account_deletion_request_duration_days,
        is_adult: matches!(
            std::env::var("SITE_IS_ADULT"),
            Ok(v) if v.as_str() == "true"
        ),
        default_tag_highlight_rules: Arc::new(default_tag_highlight_rules),
        default_colors: Arc::new(vec![
            DefaultColor {
                name: "red",
                rgba: "rgba(255, 0, 0, 0.3)",
            },
            DefaultColor {
                name: "green",
                rgba: "rgba(0, 255, 0, 0.3)",
            },
            DefaultColor {
                name: "blue",
                rgba: "rgba(0, 0, 255, 0.3)",
            },
            DefaultColor {
                name: "yellow",
                rgba: "rgba(255, 255, 0, 0.3)",
            },
            DefaultColor {
                name: "magenta",
                rgba: "rgba(255, 0, 255, 0.3)",
            },
            DefaultColor {
                name: "cyan",
                rgba: "rgba(0, 255, 255, 0.3)",
            },
            DefaultColor {
                name: "purple",
                rgba: "rgba(127, 0, 255, 0.3)",
            },
            DefaultColor {
                name: "orange",
                rgba: "rgba(255, 127, 0, 0.3)",
            },
            DefaultColor {
                name: "teal",
                rgba: "rgba(0, 255, 127, 0.3)",
            },
        ]),
    });

    info!("Starting server on {}", &listen);
    HttpServer::new(move || {
        App::new()
            .data(AppState {
                site: arc_data.clone(),
                pool: pool.clone(),
                password_secret_key: pw_sk.clone(),
                default_publication_date: PUBLISH_DAY,
            })
            .wrap(IntertextualIdentityService::new({
                IntertextualIdentityCookiePolicy::new(cookie_privkey.as_slice())
                    .name("auth")
                    .http_only(true)
                    .same_site(actix_web::cookie::SameSite::Lax)
                    .secure(secure_cookies)
            }))
            .wrap(middleware::Logger::new("\"%r\" %s %T"))
            .service(handle_favicon)
            .service(actix_files::Files::new(
                "/js/tinymce",
                std::env::var("TINYMCE_DIR").unwrap_or("node_modules/tinymce".to_string()),
            ))
            .service(actix_files::Files::new("/js", get_static_dir().join("js")))
            .service(handle_theme)
            .service(
                web::scope("")
                    .wrap(middleware::NormalizePath::default())
                    .service(endpoints::admin::chapter_moderation::handle_story_delete_author)
                    .service(endpoints::admin::chapter_moderation::handle_story_delete_collaboration)
                    .service(endpoints::admin::chapter_moderation::handle_story_disable_collab_author)
                    .service(endpoints::admin::chapter_moderation::handle_story_disable_collab_collaboration)
                    .service(endpoints::admin::chapter_moderation::handle_story_edit_tags_author)
                    .service(endpoints::admin::chapter_moderation::handle_story_edit_tags_collaboration)
                    .service(endpoints::admin::chapter_moderation::handle_story_lock_author)
                    .service(endpoints::admin::chapter_moderation::handle_story_lock_collaboration)
                    .service(endpoints::admin::chapter_moderation::handle_story_unlock_author)
                    .service(endpoints::admin::chapter_moderation::handle_story_unlock_collaboration)
                    .service(endpoints::admin::chapter_moderation::main_page_collaboration)
                    .service(endpoints::admin::chapter_moderation::main_page_single_author)
                    .service(endpoints::admin::comments::admin_comments_list)
                    .service(endpoints::admin::comments::handle_delete)
                    .service(endpoints::admin::comments::main_page)
                    .service(endpoints::admin::comments::report_page)
                    .service(endpoints::admin::index::admin_page)
                    .service(endpoints::admin::moderation_log::main_page)
                    .service(endpoints::admin::modmail::get_all)
                    .service(endpoints::admin::modmail::get_new)
                    .service(endpoints::admin::modmail::get_user)
                    .service(endpoints::admin::modmail::handle_send)
                    .service(endpoints::admin::modmail::set_handled)
                    .service(endpoints::admin::modmail::set_unhandled)
                    .service(endpoints::admin::recommendations::admin_recommendations_list)
                    .service(endpoints::admin::recommendations::handle_delete_collaboration)
                    .service(endpoints::admin::recommendations::handle_delete_single_author)
                    .service(endpoints::admin::recommendations::main_page_collaboration)
                    .service(endpoints::admin::recommendations::main_page_single_author)
                    .service(endpoints::admin::registrations::handle_disable)
                    .service(endpoints::admin::registrations::handle_enable)
                    .service(endpoints::admin::registrations::handle_limit)
                    .service(endpoints::admin::registrations::main_page)
                    .service(endpoints::admin::reports::chapter::get)
                    .service(endpoints::admin::reports::chapter::set_handled)
                    .service(endpoints::admin::reports::chapter::set_unhandled)
                    .service(endpoints::admin::reports::comment::get)
                    .service(endpoints::admin::reports::comment::set_handled)
                    .service(endpoints::admin::reports::comment::set_unhandled)
                    .service(endpoints::admin::reports::recommendation::get)
                    .service(endpoints::admin::reports::recommendation::set_handled)
                    .service(endpoints::admin::reports::recommendation::set_unhandled)
                    .service(endpoints::admin::reports::report_list_all)
                    .service(endpoints::admin::reports::report_list_unhandled)
                    .service(endpoints::admin::static_pages::get_page)
                    .service(endpoints::admin::static_pages::update_page)
                    .service(endpoints::admin::statistics::main_page)
                    .service(endpoints::admin::stories::admin_page_stories)
                    .service(endpoints::admin::tag_highlight_rules_templates::handle_add_new_rule)
                    .service(endpoints::admin::tag_highlight_rules_templates::handle_create_template)
                    .service(endpoints::admin::tag_highlight_rules_templates::handle_delete_rule)
                    .service(endpoints::admin::tag_highlight_rules_templates::handle_delete_template)
                    .service(endpoints::admin::tag_highlight_rules_templates::handle_edit_rule)
                    .service(endpoints::admin::tag_highlight_rules_templates::handle_update_template)
                    .service(endpoints::admin::tag_highlight_rules_templates::main_page)
                    .service(endpoints::admin::tag::admin_page_tags)
                    .service(endpoints::admin::tag::create_category)
                    .service(endpoints::admin::tag::create_tag)
                    .service(endpoints::admin::tag::delete_category)
                    .service(endpoints::admin::tag::delete_tag)
                    .service(endpoints::admin::tag::edit_category)
                    .service(endpoints::admin::tag::edit_tag)
                    .service(endpoints::admin::tag::link_tag)
                    .service(endpoints::admin::tag::unlink_tag)
                    .service(endpoints::admin::unpublished::unpublished_stories)
                    .service(endpoints::admin::users::account_ban)
                    .service(endpoints::admin::users::account_cancel_delete)
                    .service(endpoints::admin::users::account_delete)
                    .service(endpoints::admin::users::account_demote)
                    .service(endpoints::admin::users::account_mute)
                    .service(endpoints::admin::users::account_promote)
                    .service(endpoints::admin::users::account_reset_password)
                    .service(endpoints::admin::users::account_unban)
                    .service(endpoints::admin::users::account_unmute)
                    .service(endpoints::admin::users::account_unwarn)
                    .service(endpoints::admin::users::account_warn)
                    .service(endpoints::admin::users::admin_page_user_details)
                    .service(endpoints::admin::users::admin_page_users)
                    .service(endpoints::api::api_authors)
                    .service(endpoints::api::api_story_url_available)
                    .service(endpoints::api::api_tags)
                    .service(endpoints::api::api_username_available)
                    .service(endpoints::api::api_users)
                    .service(endpoints::crawlers::robots_txt)
                    .service(endpoints::crawlers::sitemap_xml)
                    .service(endpoints::index::index)
                    .service(endpoints::rss_feed::rss_feed)
                    .service(endpoints::search::advanced_search_page)
                    .service(endpoints::search::search_page)
                    .service(endpoints::static_pages::about_page)
                    .service(endpoints::static_pages::contact_page)
                    .service(endpoints::static_pages::faq_page)
                    .service(endpoints::static_pages::legal_page)
                    .service(endpoints::static_pages::rules_page)
                    .service(endpoints::static_pages::tos_page)
                    .service(endpoints::statistics::story::story_author_page)
                    .service(endpoints::statistics::story::story_collaboration_page)
                    .service(endpoints::statistics::tag::main_page)
                    .service(endpoints::statistics::tag::user_specific_page)
                    .service(endpoints::statistics::user::main_page)
                    .service(endpoints::statistics::user::user_specific_page)
                    .service(endpoints::story::comments::delete::get_author)
                    .service(endpoints::story::comments::delete::get_collaboration)
                    .service(endpoints::story::comments::delete::post_author)
                    .service(endpoints::story::comments::delete::post_collaboration)
                    .service(endpoints::story::comments::edit::get_author)
                    .service(endpoints::story::comments::edit::get_collaboration)
                    .service(endpoints::story::comments::edit::post_author)
                    .service(endpoints::story::comments::edit::post_collaboration)
                    .service(endpoints::story::comments::list::get_author)
                    .service(endpoints::story::comments::list::get_collaboration)
                    .service(endpoints::story::comments::new::post_author)
                    .service(endpoints::story::comments::new::post_collaboration)
                    .service(endpoints::story::comments::reply::get_author)
                    .service(endpoints::story::comments::reply::get_collaboration)
                    .service(endpoints::story::comments::report::get_author)
                    .service(endpoints::story::comments::report::get_collaboration)
                    .service(endpoints::story::comments::report::post_author)
                    .service(endpoints::story::comments::report::post_collaboration)
                    .service(endpoints::story::comments::single::get_author)
                    .service(endpoints::story::comments::single::get_collaboration)
                    .service(endpoints::story::comments::unreport::post_author)
                    .service(endpoints::story::comments::unreport::post_collaboration)
                    .service(endpoints::story::edit_chapter::handle_chapter_delete_author)
                    .service(endpoints::story::edit_chapter::handle_chapter_delete_collaboration)
                    .service(endpoints::story::edit_chapter::handle_chapter_edit_author)
                    .service(endpoints::story::edit_chapter::handle_chapter_edit_collaboration)
                    .service(endpoints::story::edit_chapter::main_page_author)
                    .service(endpoints::story::edit_chapter::main_page_collaboration)
                    .service(endpoints::story::index::author_chapter_page)
                    .service(endpoints::rss_feed::author_story_feed)
                    .service(endpoints::story::index::author_story_page)
                    .service(endpoints::story::index::collaboration_chapter_page)
                    .service(endpoints::story::index::collaboration_story_page)
                    .service(endpoints::story::new_chapter::handle_submit_author)
                    .service(endpoints::story::new_chapter::handle_submit_collaboration)
                    .service(endpoints::story::new_chapter::main_page_author)
                    .service(endpoints::story::new_chapter::main_page_collaboration)
                    .service(endpoints::story::new_story::handle_submit)
                    .service(endpoints::story::new_story::main_page)
                    .service(endpoints::story::reports::get_report_author)
                    .service(endpoints::story::reports::get_report_collaboration)
                    .service(endpoints::story::reports::post_report_author)
                    .service(endpoints::story::reports::post_report_collaboration)
                    .service(endpoints::story::reports::post_unreport_author)
                    .service(endpoints::story::reports::post_unreport_collaboration)
                    .service(endpoints::story::settings_access::handle_story_access_remove_author)
                    .service(endpoints::story::settings_access::handle_story_access_remove_collaboration)
                    .service(endpoints::story::settings_access::handle_story_authors_add_author)
                    .service(endpoints::story::settings_access::handle_story_authors_add_collaboration)
                    .service(endpoints::story::settings_access::handle_story_authors_set_self_public_author)
                    .service(endpoints::story::settings_access::handle_story_authors_set_self_public_collaboration)
                    .service(endpoints::story::settings_access::handle_story_authors_unset_self_public_author)
                    .service(endpoints::story::settings_access::handle_story_authors_unset_self_public_collaboration)
                    .service(endpoints::story::settings_access::handle_story_interaction_edit_author)
                    .service(endpoints::story::settings_access::handle_story_interaction_edit_collaboration)
                    .service(endpoints::story::settings_access::handle_story_readwrite_add_author)
                    .service(endpoints::story::settings_access::handle_story_readwrite_add_collaboration)
                    .service(endpoints::story::settings_access::handle_story_readonly_add_author)
                    .service(endpoints::story::settings_access::handle_story_readonly_add_collaboration)
                    .service(endpoints::story::settings_access::handle_story_set_collab_author)
                    .service(endpoints::story::settings_access::handle_story_set_collab_collaboration)
                    .service(endpoints::story::settings_access::handle_story_unset_collab_author)
                    .service(endpoints::story::settings_access::handle_story_unset_collab_collaboration)
                    .service(endpoints::story::settings_access::main_page_author)
                    .service(endpoints::story::settings_access::main_page_collaboration)
                    .service(endpoints::story::settings_publication::handle_chapter_publication_author)
                    .service(endpoints::story::settings_publication::handle_chapter_publication_collaboration)
                    .service(endpoints::story::settings_publication::main_page_author)
                    .service(endpoints::story::settings_publication::main_page_collaboration)
                    .service(endpoints::story::settings_tags::handle_tag_edit_author)
                    .service(endpoints::story::settings_tags::handle_tag_edit_collaboration)
                    .service(endpoints::story::settings_tags::handle_tag_select_author)
                    .service(endpoints::story::settings_tags::handle_tag_select_collaboration)
                    .service(endpoints::story::settings_tags::main_page_author)
                    .service(endpoints::story::settings_tags::main_page_collaboration)
                    .service(endpoints::story::settings::handle_chapter_move_author)
                    .service(endpoints::story::settings::handle_chapter_move_collaboration)
                    .service(endpoints::story::settings::handle_story_edit_author)
                    .service(endpoints::story::settings::handle_story_edit_collaboration)
                    .service(endpoints::story::settings::handle_story_multi_chapter_edit_author)
                    .service(endpoints::story::settings::handle_story_multi_chapter_edit_collaboration)
                    .service(endpoints::story::settings::main_page_author)
                    .service(endpoints::story::settings::main_page_collaboration)
                    .service(endpoints::story::story_list::main_page)
                    .service(endpoints::tags::popularity_tags_page)
                    .service(endpoints::tags::tag_list_page)
                    .service(endpoints::user::author_list::authors_list_page)
                    .service(endpoints::user::follow::handle_follow_author)
                    .service(endpoints::user::follow::handle_follow_collaboration_story)
                    .service(endpoints::user::follow::handle_follow_story)
                    .service(endpoints::user::follow::handle_unfollow_author)
                    .service(endpoints::user::follow::handle_unfollow_collaboration_story)
                    .service(endpoints::user::follow::handle_unfollow_story)
                    .service(endpoints::user::follow::main_page)
                    .service(endpoints::user::follow::specific_page)
                    .service(endpoints::user::gdpr_data_request::api_get_data)
                    .service(endpoints::user::index::handle_author_feature_story_toggle)
                    .service(endpoints::user::index::handle_collaboration_feature_story_toggle)
                    .service(endpoints::user::index::user_page)
                    .service(endpoints::user::login::handle_login)
                    .service(endpoints::user::login::login_page)
                    .service(endpoints::user::login::logout)
                    .service(endpoints::user::marked_for_later::handle_author_mark_for_later)
                    .service(endpoints::user::marked_for_later::handle_author_remove_marked_for_later)
                    .service(endpoints::user::marked_for_later::handle_collaboration_mark_for_later)
                    .service(endpoints::user::marked_for_later::handle_collaboration_remove_marked_for_later)
                    .service(endpoints::user::marked_for_later::main_page)
                    .service(endpoints::user::marked_for_later::specific_page)
                    .service(endpoints::user::modmail::handle_send)
                    .service(endpoints::user::modmail::main_page)
                    .service(endpoints::user::notifications::all_page)
                    .service(endpoints::user::notifications::handle_access)
                    .service(endpoints::user::notifications::handle_mark_all_read)
                    .service(endpoints::user::notifications::handle_mark_read)
                    .service(endpoints::user::notifications::handle_mark_unread)
                    .service(endpoints::user::notifications::main_page)
                    .service(endpoints::user::register::handle_register)
                    .service(endpoints::user::register::register_page)
                    .service(endpoints::user::settings::handle_activate_account)
                    .service(endpoints::user::settings::handle_cancel_delete_account)
                    .service(endpoints::user::settings::handle_deactivate_account)
                    .service(endpoints::user::settings::handle_delete_account)
                    .service(endpoints::user::settings::handle_delete_token)
                    .service(endpoints::user::settings::handle_exclude_author)
                    .service(endpoints::user::settings::handle_reset_password)
                    .service(endpoints::user::settings::handle_unexclude_author)
                    .service(endpoints::user::settings::handle_update_default_interaction)
                    .service(endpoints::user::settings::handle_update_default_notes)
                    .service(endpoints::user::settings::handle_update_default_publication)
                    .service(endpoints::user::settings::handle_update_excluded_tags)
                    .service(endpoints::user::settings::handle_update_information)
                    .service(endpoints::user::settings::handle_update_notifications)
                    .service(endpoints::user::settings::handle_update_password)
                    .service(endpoints::user::settings::main_page)
                    .service(endpoints::user::settings::password_reset_page)
                    .service(endpoints::user::settings::specific_page)
                    .service(endpoints::user::settings::tag_highlight::handle_create_rule)
                    .service(endpoints::user::settings::tag_highlight::handle_delete_rule)
                    .service(endpoints::user::settings::tag_highlight::handle_edit_rule)
                    .service(endpoints::user::settings::tag_highlight::handle_select_template)
                    .service(endpoints::whatsnew::html_page)
                    .service(endpoints::whatsnew::json_page)
                    .service(endpoints::whatsnew::specific_html_page)
                    .service(endpoints::whatsnew::specific_json_page)
                    .service(endpoints::whatsnew::specific_txt_page)
                    .service(endpoints::whatsnew::txt_page)
                    .service(
                        web::scope(intertextual::constants::RECOMMENDATION_URL)
                            .service(endpoints::recommendation::edit::handle_author_delete)
                            .service(endpoints::recommendation::edit::handle_author_edit)
                            .service(endpoints::recommendation::edit::handle_collaboration_delete)
                            .service(endpoints::recommendation::edit::handle_collaboration_edit)
                            .service(endpoints::recommendation::edit::main_author_page)
                            .service(endpoints::recommendation::edit::main_collaboration_page)
                            .service(endpoints::recommendation::follow::handle_follow_author_recommendations)
                            .service(endpoints::recommendation::follow::handle_unfollow_author_recommendations)
                            .service(endpoints::recommendation::index::single_page_author)
                            .service(endpoints::recommendation::index::single_page_collaboration)
                            .service(endpoints::recommendation::new::handle_author_submit)
                            .service(endpoints::recommendation::new::handle_collaboration_submit)
                            .service(endpoints::recommendation::new::main_author_page)
                            .service(endpoints::recommendation::new::main_collaboration_page)
                            .service(endpoints::recommendation::report::get_report_author)
                            .service(endpoints::recommendation::report::get_report_collaboration)
                            .service(endpoints::recommendation::report::post_report_author)
                            .service(endpoints::recommendation::report::post_report_collaboration)
                            .service(endpoints::recommendation::report::post_unreport_author)
                            .service(endpoints::recommendation::report::post_unreport_collaboration)
                            .service(endpoints::recommendation::story_list::main_author_page)
                            .service(endpoints::recommendation::story_list::main_collaboration_page)
                            .service(endpoints::recommendation::user_list::main_page)
                            .service(endpoints::recommendation::user_list::specific_page)
                    )
                    .service(
                        web::scope(intertextual::constants::USER_APPROVAL_URL)
                            .service(endpoints::story::user_approvals::handle_author_add_user_approval)
                            .service(endpoints::story::user_approvals::handle_author_remove_user_approval)
                            .service(endpoints::story::user_approvals::handle_collaboration_add_user_approval)
                            .service(endpoints::story::user_approvals::handle_collaboration_remove_user_approval)
                            .service(endpoints::user::user_approvals::main_page)
                            .service(endpoints::user::user_approvals::specific_page)
                    )
                    .default_service(
                        web::route()
                            .guard(guard::Not(guard::Get()))
                            .to(handle_not_found),
                    ),
            )
    })
    .bind(&listen)?
    .run()
    .await
}

async fn handle_not_found(
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<PersistentQuery>,
) -> Result<actix_web::HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    Ok(crate::app::common_pages::not_found(
        PersistentTemplate::from(&persistent),
        None,
    ))
}

#[get("/favicon.ico")]
async fn handle_favicon(
    req: actix_web::HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    url_params: web::Query<PersistentQuery>,
) -> Result<actix_web::HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let file = actix_files::NamedFile::open(get_theme_dir().join("favicon.ico"));
    let file = file
        .map_err(|_| intertextual::models::error::IntertextualError::PageDoesNotExist)
        .map_err(|e| e.into_app(&persistent))?;
    let response = file.into_response(&req);
    let response = response
        .map_err(|_| intertextual::models::error::IntertextualError::PageDoesNotExist)
        .map_err(|e| e.into_app(&persistent))?;
    Ok(response)
}

#[get("/theme/{filename}")]
async fn handle_theme(
    req: actix_web::HttpRequest,
    id: Identity,
    data: web::Data<AppState>,
    path: web::Path<String>,
    url_params: web::Query<PersistentQuery>,
) -> Result<actix_web::HttpResponse, AppError> {
    let persistent = PersistentData::from_query(&data, id, &url_params.into_inner()).await?;
    let filename = path.into_inner();
    if !ALLOWED_STATIC_FILES.contains(&filename.as_str())
        && !data.site.allowed_theme_files.contains(&filename)
    {
        return Err(intertextual::models::error::IntertextualError::PageDoesNotExist)
            .map_err(|e| e.into_app(&persistent));
    }

    let file = actix_files::NamedFile::open(get_theme_dir().join(&filename));
    let file = file.map_err(|_| intertextual::models::error::IntertextualError::PageDoesNotExist);
    let response = file.and_then(|f| {
        f.into_response(&req)
            .map_err(|_| intertextual::models::error::IntertextualError::PageDoesNotExist)
    });
    if let Ok(response) = response {
        return Ok(response);
    }
    let file = actix_files::NamedFile::open(get_static_dir().join("default_theme").join(&filename));
    let file = file
        .map_err(|_| intertextual::models::error::IntertextualError::PageDoesNotExist)
        .map_err(|e| e.into_app(&persistent))?;
    let response = file.into_response(&req);
    let response = response
        .map_err(|_| intertextual::models::error::IntertextualError::PageDoesNotExist)
        .map_err(|e| e.into_app(&persistent))?;
    Ok(response)
}
