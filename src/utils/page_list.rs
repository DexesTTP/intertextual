//! # Utilities : Page Listing
//!
//! This module contains various utility methods related to pagination

use std::convert::TryFrom;

/// Maximum number of pages to show on each side of the currently selected page.
/// E.g. if there's 20 pages and page 10 is selected, a value of "3" will show the page listing :
/// 1 ... 7 8 9 [10] 11 12 13 ... 20
const MAX_PAGES_EACH_SIDE: i64 = 5;

/// Represents a list of pages to use for pagination
pub struct PageListing {
    /// The currently selected index
    pub current_index: i64,
    /// The currently selected page
    pub current_page: i64,
    /// The total number of items
    pub total_count: i64,
    /// The list of pages
    pub page_list: Vec<PageListEntry>,
}

impl PageListing {
    pub fn empty() -> Self {
        Self {
            current_index: 0,
            current_page: 0,
            total_count: 0,
            page_list: vec![],
        }
    }

    /// Gets the page listing from a series of data to paginate
    ///
    /// * `total_count` : The number of items to paginate
    /// * `current_index` : The starting index in the item list
    /// * `items_per_page` : The number of items per page to handle
    pub fn get_from_count(
        total_count: i64,
        current_index: i64,
        items_per_page: i64,
    ) -> PageListing {
        let page_count = ((total_count - 1) / items_per_page) + 1;
        let mut page_list: Vec<PageListEntry> = Vec::with_capacity(std::cmp::min(
            usize::try_from(page_count).unwrap_or(0),
            usize::try_from((2 * MAX_PAGES_EACH_SIDE) + 4).unwrap_or(0),
        ));
        let current_page = (current_index / items_per_page) + 1;
        for page in 0..page_count {
            if page == 0 {
                // Always display the first page
                page_list.push(PageListEntry {
                    start: page * items_per_page,
                    page: page + 1,
                    is_ellipses: false,
                });
                continue;
            }

            if page == page_count - 1 {
                // Always display the last page
                page_list.push(PageListEntry {
                    start: page * items_per_page,
                    page: page + 1,
                    is_ellipses: false,
                });
                continue;
            }

            if page < current_page - MAX_PAGES_EACH_SIDE {
                // Display ellipses once, otherwise skip
                if page == 1 {
                    page_list.push(PageListEntry {
                        start: page * items_per_page,
                        page: page + 1,
                        is_ellipses: true,
                    });
                }
                continue;
            }

            if page > current_page + MAX_PAGES_EACH_SIDE {
                // Display ellipses once, otherwise skip
                if page == page_count - 2 {
                    page_list.push(PageListEntry {
                        start: page * items_per_page,
                        page: page + 1,
                        is_ellipses: true,
                    });
                }
                continue;
            }

            // Display the pages properly
            page_list.push(PageListEntry {
                start: page * items_per_page,
                page: page + 1,
                is_ellipses: false,
            });
        }
        PageListing {
            current_index,
            current_page,
            total_count,
            page_list,
        }
    }

    pub fn should_display(&self) -> bool {
        self.page_list.len() > 1
    }
}

/// Represent a single page used for pagination
pub struct PageListEntry {
    /// Defines whether this is an actual page or this should be represented as ellipses
    /// Note : We are not using an enum here because it's very unwieldy with askama later
    pub is_ellipses: bool,
    /// The page number, used for display purposes
    pub page: i64,
    /// The start number, used for the URL components
    pub start: i64,
}
