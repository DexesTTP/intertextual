//! # Utilities : Text Trimming
//!
//! This module contains methods to limit the size of text

use unicode_segmentation::UnicodeSegmentation;

/// Limit a given string to a number of bytes, show ellipses at the cutting point if needed.
///
/// The resulting string is guaranteed to not be longer than the given limit.
/// Note that the resulting string can be shorter than the limit in order to remove whitespace or preserve unicode graphemes.
///
/// # Parameters
/// - `data` The string to limit
/// - `limit` The size of the resulting string, in bytes. Note that this size is in raw bytes and not in character points.
///
/// # Examples
///
/// Ellipses might be inserted in the middle of a word if needed  
/// ```
/// let text = "This is a test with many words";
/// let trimmed = intertextual::utils::text_trim::limit_bytes_with_ellipses_if_needed(text, 15);
/// assert_eq!(&trimmed, "This is a te...");
/// assert!(trimmed.len() <= 15);
/// ```
///
/// All whitespace before the ellipses will be removed
/// ```
/// let text = "This is       a test";
/// let trimmed = intertextual::utils::text_trim::limit_bytes_with_ellipses_if_needed(text, 15);
/// assert_eq!(&trimmed, "This is...");
/// assert!(trimmed.len() <= 15);
/// ```
///
/// The ellipses will only be added if the string is longer than the limit
/// ```
/// let text = "This is a test";
/// let trimmed = intertextual::utils::text_trim::limit_bytes_with_ellipses_if_needed(text, 20);
/// assert_eq!(&trimmed, "This is a test");
/// assert!(trimmed.len() <= 20);
/// ```
///
/// And ellipses will only be inserted between graphemes
/// ```
/// let text = "क्क्‍क्ष";
/// let trimmed = intertextual::utils::text_trim::limit_bytes_with_ellipses_if_needed(text, 10);
/// assert_eq!(&trimmed, "क्...");
/// assert!(trimmed.len() <= 10);
/// ```
pub fn limit_bytes_with_ellipses_if_needed(data: &str, limit: usize) -> String {
    if data.len() <= limit {
        return data.to_string();
    }

    let splitter = UnicodeSegmentation::grapheme_indices(data, true);

    let mut last_index = 0;
    let mut last_was_whitespace = false;
    for (index, content) in splitter {
        if index > limit - 3 {
            break;
        }

        if !last_was_whitespace {
            last_index = index;
        }

        last_was_whitespace = content.trim_start().is_empty();
    }

    format!("{}...", &data[0..last_index])
}

/// Limit a given string to a number of graphemes, show ellipses at the cutting point if needed.
///
/// The resulting string is guaranteed to not be longer than the given limit.
/// Note that the resulting string can be shorter than the limit in order to remove whitespace.
///
/// # Parameters
/// - `data` The string to limit
/// - `limit` The number of graphenes in the resulting string. Note that this size is in graphenes, and not in Unicode characters nor raw bytes.
///
/// # Examples
///
/// Ellipses might be inserted in the middle of a word if needed  
/// ```
/// let text = "This is a test with many words";
/// let trimmed = intertextual::utils::text_trim::limit_graphemes_with_ellipses_if_needed(text, 15);
/// assert_eq!(&trimmed, "This is a te...");
/// assert!(unicode_segmentation::UnicodeSegmentation::graphemes(trimmed.as_str(), true).count() <= 15);
/// ```
///
/// All whitespace before the ellipses will be removed
/// ```
/// let text = "This is           a test";
/// let trimmed = intertextual::utils::text_trim::limit_graphemes_with_ellipses_if_needed(text, 15);
/// assert_eq!(&trimmed, "This is...");
/// assert!(unicode_segmentation::UnicodeSegmentation::graphemes(trimmed.as_str(), true).count() <= 15);
/// ```
///
/// The ellipses will only be added if the string is longer than the limit
/// ```
/// let text = "This is a test";
/// let trimmed = intertextual::utils::text_trim::limit_graphemes_with_ellipses_if_needed(text, 20);
/// assert_eq!(&trimmed, "This is a test");
/// assert!(unicode_segmentation::UnicodeSegmentation::graphemes(trimmed.as_str(), true).count() <= 20);
/// ```
///
/// And ellipses will only be inserted between graphemes
/// ```
/// let text = "क्क्‍क्षषषष";
/// let trimmed = intertextual::utils::text_trim::limit_graphemes_with_ellipses_if_needed(text, 5);
/// assert_eq!(&trimmed, "क्क्‍...");
/// assert!(unicode_segmentation::UnicodeSegmentation::graphemes(trimmed.as_str(), true).count() <= 5);
/// ```
pub fn limit_graphemes_with_ellipses_if_needed(data: &str, limit: usize) -> String {
    let splitter: Vec<(usize, &str)> = UnicodeSegmentation::grapheme_indices(data, true).collect();
    if splitter.len() <= limit {
        return data.to_string();
    }

    let mut last_index = 0;
    let mut last_was_whitespace = false;
    for (index, content) in splitter.into_iter().take(limit - 2) {
        if !last_was_whitespace {
            last_index = index;
        }

        last_was_whitespace = content.trim_start().is_empty();
    }

    format!("{}...", &data[0..last_index])
}

#[cfg(test)]
mod tests {
    use super::limit_bytes_with_ellipses_if_needed;
    use super::limit_graphemes_with_ellipses_if_needed;

    #[test]
    fn it_should_handle_untrimmed_below_limit_ascii_text_properly() {
        let data = "1234567890";
        assert_eq!(limit_bytes_with_ellipses_if_needed(data, 15), "1234567890");
    }

    #[test]
    fn it_should_handle_untrimmed_limit_ascii_text_properly() {
        let data = "1234567890";
        assert_eq!(limit_bytes_with_ellipses_if_needed(data, 10), "1234567890");
    }

    #[test]
    fn it_should_handle_trimmed_ascii_text_properly() {
        let data = "1234567890";
        assert_eq!(limit_bytes_with_ellipses_if_needed(data, 5), "12...");
    }

    #[test]
    fn it_should_handle_trimmed_limit_ascii_text_properly() {
        let data = "1234567890";
        assert_eq!(limit_bytes_with_ellipses_if_needed(data, 9), "123456...");
    }

    #[test]
    fn it_should_handle_untrimmed_below_limit_utf_text_properly() {
        let data = "123456789ä";
        assert_eq!(limit_bytes_with_ellipses_if_needed(data, 15), "123456789ä");
    }

    #[test]
    fn it_should_handle_untrimmed_limit_utf_text_properly() {
        let data = "123456789ä";
        assert_eq!(limit_bytes_with_ellipses_if_needed(data, 11), "123456789ä");
    }

    #[test]
    fn it_should_handle_trimmed_utf_text_properly() {
        let data = "123456789ä";
        assert_eq!(limit_bytes_with_ellipses_if_needed(data, 5), "12...");
    }

    #[test]
    fn it_should_handle_trimmed_limit_utf_text_properly() {
        let data = "123456789ä";
        assert_eq!(limit_bytes_with_ellipses_if_needed(data, 10), "1234567...");
    }

    #[test]
    fn it_should_handle_trimmed_utf_text_without_breaking_previous_boundaries() {
        let data = "12345ää89ä";
        assert_eq!(limit_bytes_with_ellipses_if_needed(data, 11), "12345ä...");
    }

    #[test]
    fn it_should_handle_trimmed_utf_text_without_breaking_next_boundaries() {
        let data = "12345ää89ä";
        assert_eq!(limit_bytes_with_ellipses_if_needed(data, 12), "12345ää...");
    }

    #[test]
    fn it_should_handle_trimmed_multiple_spaces_properly() {
        let data = "123     4567890";
        assert_eq!(limit_bytes_with_ellipses_if_needed(data, 8), "123...");
    }

    #[test]
    fn it_should_handle_trimmed_single_space_before_properly() {
        let data = "123 4567890";
        assert_eq!(limit_bytes_with_ellipses_if_needed(data, 7), "123...");
    }

    #[test]
    fn it_should_handle_trimmed_single_space_after_properly() {
        let data = "123 4567890";
        assert_eq!(limit_bytes_with_ellipses_if_needed(data, 8), "123 4...");
    }

    #[test]
    fn it_should_handle_untrimmed_below_limit_ascii_text_by_grapheme() {
        let data = "1234567890";
        assert_eq!(
            limit_graphemes_with_ellipses_if_needed(data, 15),
            "1234567890"
        );
    }

    #[test]
    fn it_should_handle_untrimmed_limit_ascii_text_by_grapheme() {
        let data = "1234567890";
        assert_eq!(
            limit_graphemes_with_ellipses_if_needed(data, 10),
            "1234567890"
        );
    }

    #[test]
    fn it_should_handle_trimmed_ascii_text_by_grapheme() {
        let data = "1234567890";
        assert_eq!(limit_graphemes_with_ellipses_if_needed(data, 5), "12...");
    }

    #[test]
    fn it_should_handle_trimmed_limit_ascii_text_by_grapheme() {
        let data = "1234567890";
        assert_eq!(
            limit_graphemes_with_ellipses_if_needed(data, 9),
            "123456..."
        );
    }

    #[test]
    fn it_should_handle_untrimmed_below_limit_utf_text_by_grapheme() {
        let data = "123456789ä";
        assert_eq!(
            limit_graphemes_with_ellipses_if_needed(data, 15),
            "123456789ä"
        );
    }

    #[test]
    fn it_should_handle_untrimmed_limit_utf_text_by_grapheme() {
        let data = "123456789ä";
        assert_eq!(
            limit_graphemes_with_ellipses_if_needed(data, 10),
            "123456789ä"
        );
    }

    #[test]
    fn it_should_handle_trimmed_utf_text_by_grapheme() {
        let data = "123456789ä";
        assert_eq!(limit_graphemes_with_ellipses_if_needed(data, 5), "12...");
    }

    #[test]
    fn it_should_handle_trimmed_limit_utf_text_by_grapheme() {
        let data = "123456789ä";
        assert_eq!(
            limit_graphemes_with_ellipses_if_needed(data, 9),
            "123456..."
        );
    }

    #[test]
    fn it_should_handle_trimmed_utf_text_without_breaking_previous_boundaries_by_grapheme() {
        let data = "12345ää89äb";
        assert_eq!(
            limit_graphemes_with_ellipses_if_needed(data, 9),
            "12345ä..."
        );
    }

    #[test]
    fn it_should_handle_trimmed_utf_text_without_breaking_next_boundaries_by_grapheme() {
        let data = "12345ää89äb";
        assert_eq!(
            limit_graphemes_with_ellipses_if_needed(data, 10),
            "12345ää..."
        );
    }

    #[test]
    fn it_should_handle_trimmed_multiple_spaces_by_grapheme() {
        let data = "123     4567890";
        assert_eq!(limit_graphemes_with_ellipses_if_needed(data, 8), "123...");
    }

    #[test]
    fn it_should_handle_trimmed_single_space_before_by_grapheme() {
        let data = "123 4567890";
        assert_eq!(limit_graphemes_with_ellipses_if_needed(data, 7), "123...");
    }

    #[test]
    fn it_should_handle_trimmed_single_space_after_by_grapheme() {
        let data = "123 4567890";
        assert_eq!(limit_graphemes_with_ellipses_if_needed(data, 8), "123 4...");
    }
}
