use crate::models::formats::SanitizedHtml;

pub fn count_words_from_html(html: &SanitizedHtml) -> i32 {
    count_words_from_raw_text(&html.text())
}

fn count_words_from_raw_text(text: &str) -> i32 {
    use std::convert::TryFrom;
    lazy_static! {
        static ref RE: regex::Regex = regex::Regex::new(r"([^\p{P}\s]['’-]?)+").unwrap();
    }
    i32::try_from(RE.find_iter(text).count()).unwrap_or(0)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_should_handle_an_empty_string() {
        let data = SanitizedHtml::from("");
        assert_eq!(0, count_words_from_html(&data));
    }

    #[test]
    fn it_should_count_raw_text_properly() {
        let data = SanitizedHtml::from("This is a simple text.");
        assert_eq!(5, count_words_from_html(&data));
    }

    #[test]
    fn it_should_count_simple_html_properly() {
        let data = SanitizedHtml::from("<p>This is a <strong>simple</strong> text.</p>");
        assert_eq!(5, count_words_from_html(&data));
    }

    #[test]
    fn it_should_count_advanced_html_properly() {
        let data = SanitizedHtml::from(
            r#"
            <p>This is an <strong>advanced</strong> HTML document.</p>
            <p>It contains <a href="http://www.twitter.com/">links</a>.</p>
            <p>It also contains $p3c1al characters, and non-latin characters such as 日本人</p>
        "#,
        );
        assert_eq!(20, count_words_from_html(&data));
    }
}
