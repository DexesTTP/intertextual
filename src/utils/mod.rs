//! # Utilities
//!
//! This module contains various utility methods, or methods that can't really fit in other places

use crate::models::error::IntertextualError;

pub mod hits;
pub mod htmlize;
pub mod page_list;
pub mod publication;
pub mod text_trim;
pub mod word_count;

/// Check whether the given error is an unique violation error
///
/// Unique violations are returned by the database when an insertion breaks the UNIQUE constraint of a SQL table
pub fn is_unique_violation<T>(res: &Result<T, IntertextualError>) -> bool {
    matches!(
        res,
        Err(IntertextualError::DBError(
            diesel::result::Error::DatabaseError(
                diesel::result::DatabaseErrorKind::UniqueViolation,
                _,
            ),
        ),)
    )
}
