//! # Utilities : Publication
//!
//! This module contains information about story publication data

use crate::models::error::IntertextualError;

/// Stores the publication settings
#[derive(Clone)]
pub struct PublicationDate {
    /// Which day of the week should the publication happen
    pub weekday: chrono::Weekday,
    /// Which hour of the day should the publication happpen
    pub hour: u32,
}

impl PublicationDate {
    /// Based on the settings and the current time, find the next suitable publication date
    pub fn get_next_publication_date(
        &self,
        current_time: chrono::NaiveDateTime,
    ) -> chrono::NaiveDateTime {
        use chrono::{Datelike, Timelike};
        let current_date = {
            if current_time.hour() > self.hour {
                current_time.date().succ()
            } else {
                current_time.date()
            }
        };
        let mut next_weekday = current_date;
        while next_weekday.weekday() != self.weekday {
            next_weekday = next_weekday.succ();
        }

        next_weekday.and_hms(self.hour, 0, 0)
    }

    /// Based on the settings and the current time, find the previous suitable publication date
    pub fn get_last_publication_date(
        &self,
        current_time: chrono::NaiveDateTime,
    ) -> chrono::NaiveDateTime {
        use chrono::{Datelike, Timelike};

        let current_date = {
            if current_time.hour() <= self.hour {
                current_time.date().pred()
            } else {
                current_time.date()
            }
        };
        let mut previous_weekday = current_date;
        while previous_weekday.weekday() != self.weekday {
            previous_weekday = previous_weekday.pred();
        }

        previous_weekday.and_hms(self.hour, 0, 0)
    }
}

/// The publication mode of a story
pub enum PublicationMode {
    /// If this mode is used, the story is published immediately
    PublishNow,
    /// If this mode is used, the story is publised at the given date (UTC+00)
    PublishLater(chrono::NaiveDateTime),
    /// If this mode is used, the story is not published and does not have a planned publication date
    DoNotPublish,
}

/// Gets the requested publication mode based on the input data
///
/// * `publication_mode` : Should be "publish_now", "publish_later" or "do_not_publish". Any other value will throw an error
/// * `publication_time` : Ignored if the mode isn't "publish_later", should be a valid string in the format "YY-mm-dd HH:MM". Any other format will throw an error
pub fn get_publication_mode(
    publication_mode: &str,
    publication_time: &str,
) -> Result<PublicationMode, IntertextualError> {
    match publication_mode {
        "publish_now" => Ok(PublicationMode::PublishNow),
        "publish_later" => Ok(PublicationMode::PublishLater(
            chrono::NaiveDateTime::parse_from_str(publication_time, "%Y-%m-%d %H:%M").map_err(
                |_| IntertextualError::FormFieldFormatError {
                    form_field_name: "Publication Date",
                    message:
                        "The publication date is invalid. The expected format is : 2020-08-24 11:30"
                            .to_string(),
                },
            )?,
        )),
        "do_not_publish" => Ok(PublicationMode::DoNotPublish),
        _ => Err(IntertextualError::FormFieldFormatError {
            form_field_name: "Publication",
            message: "Invalid publication mode found. Stop hacking the site".to_string(),
        }),
    }
}
