//! # Utilities : Hit Deduplication
//!
//! This module contains methods used to temporarily store hashes used to deduplicate
//! story hits based on a request's IP while maintaining the users privacy

use std::{collections::HashSet, sync::Mutex};

use log::{error, info};

use crate::models;
use crate::models::shared::DbPool;

#[derive(Debug, PartialEq, Eq, Hash)]
struct HitToStore {
    pub ip: String,
    pub chapter_id: uuid::Uuid,
    pub salt: u32,
}

struct SaltedTable {
    salt: u32,
    table: HashSet<u64>,
}

lazy_static! {
    static ref SALT_TABLE: Mutex<SaltedTable> = Mutex::new(SaltedTable {
        salt: rand::prelude::random(),
        table: HashSet::<u64>::new(),
    });
}

/// Checks whether the given HTTP request should cause a hit for this chapter.
pub fn should_add_hit(
    request: actix_web::HttpRequest,
    login: &Option<&models::users::User>,
    authors: &[models::users::User],
    chapter: &models::stories::Chapter,
    pool: &DbPool,
) -> bool {
    use std::collections::hash_map::DefaultHasher;
    use std::hash::{Hash, Hasher};
    if !chapter.is_published() {
        return false;
    }

    match login {
        Some(login) => {
            for author in authors {
                if author.id == login.id {
                    // Authors shouldn't self-hit their story
                    return false;
                }
            }
        }
        None => {}
    };

    if let Some(ip) = request.connection_info().realip_remote_addr() {
        let salt_table = SALT_TABLE.lock();
        match salt_table {
            Ok(mut salt_table) => {
                // Hash the current interaction
                let hashable_entry = HitToStore {
                    ip: ip.to_string(),
                    chapter_id: chapter.id,
                    salt: salt_table.salt,
                };
                let mut hasher = DefaultHasher::new();
                hashable_entry.hash(&mut hasher);

                let value = hasher.finish();
                if salt_table.table.contains(&value) {
                    // This user already hit this page somewhere in the transient DB.
                    // Ignore the new hit.
                    return false;
                }

                // Save a hash of this interaction in the transient DB.
                salt_table.table.insert(value);
            }
            Err(e) => error!(
                "Could not acquire hit deduplication table lock for storing hit : {:?}",
                e
            ),
        }
    }

    // TODO : validate further about whether we should register a hit (e.g.tracking of hits for logged-in users, etc...)
    let _ = pool;
    true
}

/// Resets the hit deduplication cache, allowing all duplicated hits to be valid again
pub fn clear_hit_dedup_cache() {
    info!("Wiping hit deduplication table.");
    let salt_table = SALT_TABLE.lock();
    match salt_table {
        Ok(mut salt_table) => {
            salt_table.salt = rand::prelude::random();
            salt_table.table.clear();
        }
        Err(e) => error!(
            "Could not acquire hit deduplication table lock for clearing : {:?}",
            e
        ),
    }
}
