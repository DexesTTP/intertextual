# intertextual

A Rust-based web server for user-submitted stories, like Ao3 but not Ao3

## Setup - Dev Environment

See [DEVELOPMENT.md](DEVELOPMENT.md)

## Setup - Deployment

See [PRODUCTION.md](PRODUCTION.md)

## Using the cli binary

- Setup your environment as described above
- Run the `cargo run --bin intertextual-cli [subcommands]` command to get the list of usages.
- Here are some example commands :
  - `cargo run --bin intertextual-cli user create -u username -d "Display Name"` to create a new user with the given parameters. This ignores the registration restrictions. A random password will be generated, and a reset of the password will be requested for this user on first login.
  - `cargo run --bin intertextual-cli user delete username` to delete the given user. This bypasses the deletion request process, and shouldn't be used unless necessary.
  - `cargo run --bin intertextual-cli user promote username` to promote the `username` user to administrator
  - `cargo run --bin intertextual-cli user demote username` to demote the `username` user back to normal status
  - `cargo run --bin intertextual-cli registrations disable` to disable registrations on the website. Note that the `registrations` subcommand contains other options.
  - `cargo run --bin intertextual-cli import -- [flags] <filename>` to import a series of stories from the file system.
    - Create a .yaml file to describe your stories (you can check the `import.yaml.example` file for reference)
    - `[flags]` can be :
      - `--pub_now` to publish chapters without a publication date now
      - `--force` to overwrite existing chapters
      - `--ignore-chapter-conflicts` to ignore stories and chapters with conflicts

## Implemented features

Intertextual already supports the vast majority of features required in a web fiction website.

### Story Reading and Editing Experience

As a publishing platform, stories are at the core of the website. As such, we made sure that the story reading experience, creation, edition and management covers all the use-cases readers and authors need. We also made sure that stories are easy to link and share with others.

- Simplfied story reading experience
- Simple story URLs for easy linking (`/@AuthorName/StoryURL`)
- Optional custom story URL for stories with multiple authors (`/collaboration/StoryURL`)
- Proper story metadata for social media sharing and search-engine referencement.
- Story descriptions
- Simplified story submission using either an intuitive user interface powered by [TinyMCE](https://www.tiny.cloud/), or direct file upload powered by [Pandoc](https://pandoc.org/)
- Story chapters (Muti-page stories)
- Instant story edition with optional notification for followers
- Story publishing and draft stories (by default, created stories are draft stories)
- Delayed and planified story publishing (e.g. "publish on 2020-12-23 at 12:00")
- Protected story deletion (requires draftification first to prevent mistakes)
- Complete access management for stories
  - Several authors can collaborate on a single story with full creadit for all authors
  - Possibility to add editors that can edit a story, including before publication
  - Possibility to add beta-readers that can read a story before publication
- Configurable default settings for newly published stories :
  - Default foreword and afterword
  - Publication date (can choose between : publish now, publish at a set weekday / hour, never publish)
  - Comment behaviour (can choose between : enabled / disabled)
  - Recommendation behaviour (can choose between : enabled / only on user pages / disabled)

### Discoverable Stories

In order to help users find the stories they want to read while browsing the website, we worked on an improved browsing experience. This is made possible by providing different views of stories and displaying relevant information each time stories are listed.

- Latest stories on the website index
- Weekly summary page of all published stories
- List of authors
- List of story for an author
- List of tags
- RSS feed
- "Follow a story / author", where the user is notified when a story is posted or updated
- Notifications when users receive a review or comment
- Word and chapter count for stories visible directly on story cards
- When logged-in, per-user tag denylist and author denylist
- When logged-in, highlights on the story cards directly based on per-user, customizable tag highlighting rules

### Advanced Tagging system and Search engine

In addition to the core discoverable features, we also propose a search engine allowing to easily find stories, based on an easy-to-use tagging system with relevant defaults.

- Stories can be tagged by the author either with default or custom tags
- Easy listing and selection of default tags
- Admin-defined mandatory tags (aka content warnings)
- Admins can regroup synonym tags together under a single common name
- Authors can add one or several tags to their stories based on categories
- Tags are autocompleted on the tag search page
- Search by author, title, description and tags (+ tag combination and excluded tags)
- Fuzzy matches while searching by tags (with a percent match for each story)
- Tags can have associated with categories and descriptions
- Per-story major tags and spoiler tags to indicate relevance and allow filtering without degrading the reader experience

### Interactions and feedback

Having direct feedback when your stories does well is important. We provide several features to help readers interact with authors

- Ability for registered users to give one "approval" to a story
- "Comment" sections where users can comment short messages after each chapter
- "Recommendations" feature where users can write longer reviews about a story. The recommendation will appear on the user page, below the user's stories.
- Notify the author when they receive a new approval, comment, or review
- Ability to follow an user's recommendations and be notified when an user posts a new recommendation

### Personalizable Browsing Experience

Because not every author or reader wants the same experience, we provide a few easily accessible themes for both logged-in and logged-out users.

- Per-user style changes, including when logged-out
- Light / Dark themes
- Font selection (serif / sans-serif)
- Font size selection
- Margin size selection
- Paragraph style selection

### Account Management

Account management is often tricky to manage properly. We implemented solutions to common problems, hardening accounts via state-of-the-art technology and always staying privacy-focused.

- Account registration & login
- Password hashing via [`argonautica`](https://github.com/bcmyers/argonautica)
- Token-based long-term logging with a per-user management and revocation screen
- Optional extra informations (user description & contact email)
- Dedicated settings screen to change your username / password / extra informations
- GDPR-compilant data export
- GDPR-compilant account deletion

### Informations to Authors

In order to help story authors find what their audience is, we provide statistics about each story's performance to authors. This data is collected while respecting the complete privacy of each reader.

- Story hits (i.e. number of views)
- Summary of approvals, comments and recommendations
- Global statistics per author
- Per-story statistics

### Website-specific configuration

Because each platform is different, we try to provide as much control as possible to the hosts without requiring changes to the code.

- Configurable website time via environment variables or configuration file
- Metadata (global tags and description) configurable via environment variables or configuration file
- Theme folder to select the website favicon, icon, and optionally custom light/dark themes
- Optional "18+" check popup on first arrival configurable via environment variables or configuration file

### Administration Tools

To help manage the community, we provide a set of moderation tools that provide control over publsished content, easy contact with each user, and a complete tracability of moderation actions taken.

- Administration accounts
- Story / comment / recommendation reporting
- Possibility to make reports as "handled", moderation list of all pending and handled reports
- Story locking (locked stories cannot be published until unlocked again)
- Basics of mandatory & suggested tags management
- Possibility to provide "default" tag highlighting rules for logged-in users to pick from
- Listing of unpublished / locked stories
- Listing of users + administrator promotion screen
- Mod mail : possibility for users to privately contact the moderation team "as a whole" and get replies
- Account deactivation
- Registration limitation tools
- Log of all moderation actions

## Planned features

The following features are not yet implemented, but are being worked on :

- [ ] Advanced statistics screen with, for example, per-tag information
- [ ] Internationalization of the website / support for changing the UI depending of the selected language
- [ ] Series of stories / Ordered collection of stories
